#!/system/bin/sh

packagename="${1}"
service="${2}"

if [ "${#service}" -gt 0 ]
then
    pm enable "${packagename}/${service}"
    echo "  enable service ${packagename}/${service} : rc=$?"
else
    pm enable "${packagename}"
    echo "  enable package ${packagename} : rc=$?"
fi

