#!/system/bin/sh

fd=$PWD/temp

if [ ! -d ${fd} ]
then
  mkdir -p ${fd}
  mount -t tmpfs -o size=12m tmpfs $PWD/temp
  echo "created tmpfs"
  ls -al ${fd}
fi

if [ $1 -eq 2 ]
then
  pgrep -f "temp.sh"
  exit 0
fi

if [ $1 -eq 0 ]
then
  if [ `pgrep -f "temp.sh" | wc -l` -gt 1 ]
  then
    touch ${fd}/tempstop
    pgrep -f "temp.sh"
    echo "asked process to exit"
    ls -al ${fd}
  else
    echo "no more running process"
    ls -al ${fd}
  fi
  exit 0
fi

if [ `pgrep -f "temp.sh" | wc -l` -gt 1 ]
then
  echo "process already runninng"
  ls -al ${fd}
  exit 1
fi

dt=${2}

rm -f ${fd}/tempstop
chmod 777 ${fd}/*

tlimit=10000

tcpu=""
#tgpu=""
#tbat=""
for f in /sys/class/thermal/thermal_zone*
do
  type=`cat ${f}/type`
  if [ "${type}" = "cpuss-0" ]
  then
    tcpu="${f}"
  elif [ "${type}" = "soc_thermal" ]
  then
    tcpu="${f}"
  #elif [ "${type}" = "gpuss-0" ]
  #then
  #  tgpu="${f}"
  #elif [ "${type}" = "battery" ]
  #then
  #  tbat="${f}"
  elif [ ${#tcpu} -eq 0 ]
  then
    tcpu="${f}"
  fi
done
echo "cpu $tcpu"
#echo "gpu $tgpu"
#echo "bat $tbat"
chargenow=/sys/class/power_supply/battery/charge_now
brightness=/sys/class/backlight/panel0-backlight/brightness

mmonitor(){
  i=0
  a=(0 0 0 0 0 0 0 0)
  initial=1
  onetd=0
  onetu=0
  oidle=0
  otot=0
  while [ ! -f ${fd}/tempstop ]
  do
    sleep ${dt}

    if [ $i -ge $tlimit ]
    then
      for i in 8 7 6 5 4 3 2 1 0
      do
        if [ -f ${fd}/temp${i}.log ]
        then
          mv ${fd}/temp${i}.log ${fd}/temp$(( ${i}+1 )).log
        fi
      done
      touch ${fd}/temp0.log
      chmod 777 ${fd}/*
      i=0
    fi

    tps=`date +%s`

    a[0]=$(( (`cat ${tcpu}/temp || echo 0`)/100 ))
    a[1]=0 #$(( (`cat ${tgpu}/temp || echo 0`)/100 ))
    a[2]=0 #$(( (`cat ${tbat}/temp || echo 0`)/100 ))
    a[3]=`cat /sys/class/power_supply/battery/capacity || echo 0`
    a[4]=0 #`cat /sys/class/power_supply/battery/charge_now || echo 0`

    net=(`awk '/wlan0/ {a1=$2; b1=$10;} /eth0/ {a2=$2; b2=$10;} END{if (a1 >= a2 && b1 >= b2) {print a1, b1;} else {print a2, b2}}' /proc/net/dev`)
    a[5]=$(( ${net[0]}-$onetd ))
    onetd=${net[0]}
    a[6]=$(( ${net[1]}-$onetu ))
    onetu=${net[1]}

    cpuc=(`grep "cpu " /proc/stat | awk '/cpu/ {tot=$2+$3+$4+$5+$6+$7+$8+$9+$10+$11; idle=tot-$5-$6; print idle, tot;}'`)
    idle=$(( ${cpuc[0]}-$oidle ))
    oidle=${cpuc[0]}
    tot=$(( ${cpuc[1]}-$otot ))
    otot=${cpuc[1]}
    a[7]=$(( ($idle*1000)/$tot ))

    if [ $initial -eq 1 ]
    then
      initial=0
      a[5]=0
      a[6]=0
      a[7]=0
    fi

    echo "$tps ${a[0]} ${a[1]} ${a[2]} ${a[3]} ${a[4]} ${a[5]} ${a[6]} ${a[7]}" >> ${fd}/temp0.log

    i=$(( $i+1 ))
  done
  rm -f ${fd}/temp*
  exit 0
}

mmonitor &

echo "temp.sh launched"
exit 0



























