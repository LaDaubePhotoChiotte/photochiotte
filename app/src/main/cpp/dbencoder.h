
int compress(unsigned char *msg, int msgl, unsigned char *out, int outl) {
    int write_size = 0;
    uint32_t preset = 6;
    lzma_stream strm = LZMA_STREAM_INIT;
    lzma_ret ret = lzma_easy_encoder(&strm, preset, LZMA_CHECK_CRC64);
    if (ret == LZMA_OK) {
        lzma_action action = LZMA_RUN;
        strm.next_in = NULL;
        strm.avail_in = 0;
        strm.next_out = out;
        strm.avail_out = outl;
        while (true) {
            if (strm.avail_in == 0) {
                strm.next_in = msg;
                strm.avail_in = msgl;
                action = LZMA_FINISH;
            }
            ret = lzma_code(&strm, action);
            if (strm.avail_out == 0 || ret == LZMA_STREAM_END) {
                write_size = outl - strm.avail_out;
                strm.next_out = NULL;
                strm.avail_out = 0;
            }
            if (ret != LZMA_OK) {
                if (ret == LZMA_STREAM_END) {
                    break;
                } else {
                    printf("compression error %d", ret);
                    break;
                }
            }
        }
    }
	lzma_end(&strm);
    return write_size;
}

int decompress(unsigned char *msg, int msgl, unsigned char *out, int outl) {
    int i;
    int write_size = 0;
    lzma_stream strm = LZMA_STREAM_INIT;
    lzma_ret ret = lzma_stream_decoder(&strm, UINT64_MAX, LZMA_CONCATENATED);
    if (ret == LZMA_OK) {
        lzma_action action = LZMA_RUN;
        int bufl = 1024;
        unsigned char inbuf[bufl];
        strm.next_in = NULL;
        strm.avail_in = 0;
        strm.next_out = out;
        strm.avail_out = outl;
        int pos = 0;
        while (true) {
            //printf("----------------------------------------------------------------------------------------------------------");
            //printf("----------------------------------------------------------------------------------------------------------");
            if (strm.avail_in == 0 && pos < msgl) {
                if (pos + bufl > msgl) {
                    bufl = msgl - pos;
                    action = LZMA_FINISH;
                }
                for (i = 0 ; i < bufl ; i++)
                    inbuf[i] = msg[pos + i];
                strm.next_in = inbuf;
                strm.avail_in = bufl;
                //printf("send %d - %d / %d", pos, pos + bufl, msgl);
                pos += bufl;
            }
            ret = lzma_code(&strm, action);
            write_size = outl - strm.avail_out;
            //printf("got data %d ret %d", write_size, ret);
            int pl = write_size;
            char p[pl * 1];
            for (i = 0 ; i < pl ; i++)
                snprintf(p + i, 2, "%c", out[i]);
            //printf("%s", p);
            if (strm.avail_out == 0 || ret == LZMA_STREAM_END) {
                //printf("write_size %d", write_size);
                strm.next_out = out;
                strm.avail_out = outl;
            }
            if (ret != LZMA_OK) {
                if (ret == LZMA_STREAM_END) {
                    //printf("LZMA_STREAM_END");
                    break;
                } else {
                    //printf("decompression error %d", ret);
                    break;
                }
            }
        }
    }
    return write_size;
}

int envigenerize(unsigned char *pub, int publ,
                  int offset,
                  unsigned char *msg, int msgl,
                  unsigned char **out) {
    int i, j;
    int outl = msgl + 1024; // overhead for short messages 72 msg -> 100 compressed
    *out = (unsigned char *) malloc(sizeof(char) * outl);
    outl = compress(msg, msgl, *out, outl);
    if (outl <= 8) {
        printf("compression error");
        return 0;
    }
    // header 6 bytes FD 37 7A 58 5A 00 footer 2 bytes 59 5A
    if (*out[0] != 0xfd || *out[1] != 0x37 || *out[2] != 0x7a || *out[3] != 0x58 || *out[4] != 0x5a || *out[5] != 0x00
        || *out[outl - 2] != 0x59 || *out[outl - 1] != 0x5a) {
        printf("compression error headers %d", outl);
        return 0;
    }
    //dumpbufx(" c compucod ", *out, outl);
    outl += offset;
    *out = (unsigned char *) realloc(*out, sizeof(char) * outl);
    int puboffset = pub[0] % publ;
    // shift to the right to make room for the header
    j = outl - 2 - offset - 6 - 1;
    for (i = outl - 1 ; i >= offset ; i--)
        if (i >= outl - 2)
            *out[i] = (char) (*out[i - offset]);
        else if (i >= offset + 6) {
            *out[i] = (char) (*out[i - offset] + pub[(j + puboffset) % publ] % 0x7f);
            j--;
        } else
            *out[i] = (char) (*out[i - offset]);
    //dumpbufx(" c compcod  ", *out, outl);
    return outl;
}

int devigenerize(unsigned char *pub, int publ,
                  int offset,
                  unsigned char *msg, int msgl,
                  unsigned char **out) {
    int i, j;
    int outl = MAXMSGSIZE;
    *out = (unsigned char *) malloc(sizeof(char) * outl);
    //dumpbufx(" d compcod  ", msg, msgl);
    int puboffset = pub[0] % publ;
    // shift to the left to remove the header
    j = 0;
    for (i = 0 ; i < msgl - offset ; i++)
        if (i < 6)
            msg[i] = (char) (msg[i + offset]);
        else if (i < msgl - offset - 2) {
            msg[i] = (char) (msg[i + offset] - pub[(j + puboffset) % publ] % 0x7f);
            j++;
        } else
            msg[i] = (char) (msg[i + offset]);
    //dumpbufx(" d compucod ", msg, msgl - offset);
    msgl -= offset;
    outl = decompress(msg, msgl, *out, outl);
    *out = (unsigned char *) realloc(*out, sizeof(char) * outl);
    return outl;
}

int try_to_get_size(unsigned char *buf, int pos) {
    int size1 = ((buf[pos++] << 24) & 0xff000000);
    size1 += ((buf[pos++] << 16) & 0xff0000);
    size1 += ((buf[pos++] << 8) & 0xff00);
    size1 += ((buf[pos++] << 0) & 0xff);
    int size2 = ((buf[pos++] << 0) & 0xff);
    size2 += ((buf[pos++] << 8) & 0xff00);
    size2 += ((buf[pos++] << 16) & 0xff0000);
    size2 += ((buf[pos++] << 24) & 0xff000000);
    if (size1 == size2)
        return size1;
    else
        return 0;
}

int header_size(unsigned char type) {
    int headersize = KEY_SIZE_L + KEY_SIZE + KEY_COM_SIZE_BELE_L + KEY_SIZE + KEY_TYPE_OF_MESSAGE_L + KEY_SIZE;
    return headersize;
}

int write_header(unsigned char *buf, int bufl, unsigned char type) {
    int i = 0;
    buf[i++] = KEY_SIZE;
    buf[i++] = KEY_COM_SIZE_BELE;
    buf[i++] = (bufl >> 24) & 0xff;
    buf[i++] = (bufl >> 16) & 0xff;
    buf[i++] = (bufl >> 8) & 0xff;
    buf[i++] = (bufl >> 0) & 0xff;
    buf[i++] = (bufl >> 0) & 0xff;
    buf[i++] = (bufl >> 8) & 0xff;
    buf[i++] = (bufl >> 16) & 0xff;
    buf[i++] = (bufl >> 24) & 0xff;
    buf[i++] = KEY_TYPE_OF_MESSAGE;
    buf[i++] = type;
    buf[i++] = KEY_CHUNK;
    return i;
}

int read_header(unsigned char *buf, int bytesRead, int *offset, unsigned char *type) {
    int i = 0;
    int j = 0;
    int headersize = header_size(*type);
    while (i + headersize < bytesRead) {
        j = i + 0;
        if (buf[j] == 1) { // keys will have size of 1
            j += KEY_SIZE_L;
            if (buf[j] == KEY_COM_SIZE_BELE) {
                j += KEY_SIZE;
                int size = try_to_get_size(buf, j);
                if (size > 0) {
                    j += KEY_COM_SIZE_BELE_L;
                    if (buf[j] == KEY_TYPE_OF_MESSAGE) {
                        j += KEY_SIZE;
                        *type = buf[j];
                        if (*type == TYPE_BENCODED_XZPE || *type == TYPE_COLLECTION_XZPE) {
                            j += KEY_TYPE_OF_MESSAGE_L;
                            if (buf[j] == KEY_CHUNK) {
                                j += KEY_SIZE;
                                *offset = j;
                                return size;
                            }
                        }
                    }
                }
            }
        }
        i++;
    }
    return 0;
}

void integertostring(unsigned char *buf, int *posi, int val) {
    int np, i;
    int mult = 10;
    int imax = 1;
    while (val >= mult) {
        mult *= 10;
        imax++;
    }
    for (i = 0 ; i < imax ; i++) {
        mult /= 10;
        np = val - val % mult;
        buf[*posi++] = (unsigned char) (np / mult) + 0x30;
        val -= np;
    }
}

int querycollectione(unsigned char *buf, int *bufl, char *sourcepath) {
    int i = 0;

    printf("  [ ]  sending pselsp file %s %d", sourcepath, (int) strlen(sourcepath));
    FILE *fp;
    fp = fopen(sourcepath, "rb");
    if (fp == NULL) {
        printf("  [ ]  error file does not exist");
    } else {
        fseek(fp, 0L, SEEK_END);
        int fl = (int) ftell(fp);
        rewind(fp);
        //resp.metadata = (unsigned char *) malloc(sizeof(unsigned char) * bufl);
        int r = fread(buf + 0, 1, fl, fp);
        //resp.metadatal = r;
        fclose(fp);
        if (r != fl) {
            printf("  [ ]  error mismatching read size %d / %d", r, fl);
        }
        i += r;
        printf("  [ ]  read size %d / %d", r, fl);
    }

    *bufl = i;

    return i;
}

int parsecollectione(unsigned char *b, int bl, char *destinationpath) {
    int i, j, k, rc;
    rc = -1;

    printf("  [c]  we've received data %d", bl);
    FILE *op;
    char oname[256];
    snprintf(oname, sizeof(oname), "%s/tempcollection.pselsp", DOSSIERDESSIN);
    op = fopen(oname, "wb");
    if (op == NULL) {
        printf("  [c]  could not write %s", oname);
    } else {
        rc = fwrite(b + 0, sizeof(unsigned char), bl - 0, op);
        fclose(op);
        printf("  [c]  wrote %s fwrite %d / metadatal %d errno %d", oname, rc, bl - 0, errno);
    }

    return rc;
}

int querybencodede(unsigned char *buf, int *bufl, struct AskResponse qr) {
    int k;
    int i = 0;

    buf[i++] = 'd';

    if (qr.tl > 0) {
        buf[i++] = '1';
        buf[i++] = ':';
        buf[i++] = 't';
        integertostring(buf, &i, qr.tl);
        buf[i++] = ':';
        for (k = 0; k < qr.tl; k++)
            buf[i++] = qr.t[k];
    }

    if (qr.vl > 0) {
        buf[i++] = '1';
        buf[i++] = ':';
        buf[i++] = 'v';
        integertostring(buf, &i, qr.vl);
        buf[i++] = ':';
        for (k = 0; k < qr.vl; k++)
            buf[i++] = qr.v[k];
    }

    buf[i++] = '1'; buf[i++] = ':';  buf[i++] = 'y';
    if (qr.query) {
        buf[i++] = '1'; buf[i++] = ':'; buf[i++] = 'q';

        if (qr.q == dic_find_node) {
            buf[i++] = '1';
            buf[i++] = ':';
            buf[i++] = 'q';
            buf[i++] = '9';
            buf[i++] = ':';
            buf[i++] = 'f';
            buf[i++] = 'i';
            buf[i++] = 'n';
            buf[i++] = 'd';
            buf[i++] = '_';
            buf[i++] = 'n';
            buf[i++] = 'o';
            buf[i++] = 'd';
            buf[i++] = 'e';
        } else if (qr.q == dic_get_peers) {
            buf[i++] = '1';
            buf[i++] = ':';
            buf[i++] = 'q';
            buf[i++] = '9';
            buf[i++] = ':';
            buf[i++] = 'g';
            buf[i++] = 'e';
            buf[i++] = 't';
            buf[i++] = '_';
            buf[i++] = 'p';
            buf[i++] = 'e';
            buf[i++] = 'e';
            buf[i++] = 'r';
            buf[i++] = 's';
        } else if (qr.q == dic_announce_peer) {
            buf[i++] = '1';
            buf[i++] = ':';
            buf[i++] = 'q';
            buf[i++] = '1';
            buf[i++] = '3';
            buf[i++] = ':';
            buf[i++] = 'a';
            buf[i++] = 'n';
            buf[i++] = 'n';
            buf[i++] = 'o';
            buf[i++] = 'u';
            buf[i++] = 'n';
            buf[i++] = 'c';
            buf[i++] = 'e';
            buf[i++] = '_';
            buf[i++] = 'p';
            buf[i++] = 'e';
            buf[i++] = 'e';
            buf[i++] = 'r';
        } else if (qr.q == dic_download) {
            buf[i++] = '1';
            buf[i++] = ':';
            buf[i++] = 'q';
            buf[i++] = '8';
            buf[i++] = ':';
            buf[i++] = 'd';
            buf[i++] = 'o';
            buf[i++] = 'w';
            buf[i++] = 'n';
            buf[i++] = 'l';
            buf[i++] = 'o';
            buf[i++] = 'a';
            buf[i++] = 'd';
        } else if (qr.q == dic_ping) {
            buf[i++] = '1';
            buf[i++] = ':';
            buf[i++] = 'q';
            buf[i++] = '4';
            buf[i++] = ':';
            buf[i++] = 'p';
            buf[i++] = 'i';
            buf[i++] = 'n';
            buf[i++] = 'g';
        }

        buf[i++] = '1'; buf[i++] = ':'; buf[i++] = 'a';
    } else {
        buf[i++] = '1'; buf[i++] = ':'; buf[i++] = 'r';

        buf[i++] = '1'; buf[i++] = ':'; buf[i++] = 'r';
    }
    buf[i++] = 'd';

    if (qr.idl > 0) {
        buf[i++] = '2';
        buf[i++] = ':';
        buf[i++] = 'i';
        buf[i++] = 'd';
        integertostring(buf, &i, qr.idl);
        buf[i++] = ':';
        for (k = 0; k < qr.idl; k++)
            buf[i++] = qr.id[k];
    }

    if (qr.porti > 0) {
        buf[i++] = '5';
        buf[i++] = ':';
        buf[i++] = 'p';
        buf[i++] = 'o';
        buf[i++] = 'r';
        buf[i++] = 't';
        buf[i++] = 's';
        integertostring(buf, &i, 4);
        buf[i++] = ':';
        buf[i++] = (qr.porti >> 8) & 0xff;
        buf[i++] = (qr.porti >> 0) & 0xff;
        buf[i++] = (qr.portf >> 8) & 0xff;
        buf[i++] = (qr.portf >> 0) & 0xff;
    }

    if (qr.targetl > 0) {
        buf[i++] = '6'; buf[i++] = ':'; buf[i++] = 't'; buf[i++] = 'a'; buf[i++] = 'r'; buf[i++] = 'g'; buf[i++] = 'e'; buf[i++] = 't';
        integertostring(buf, &i, qr.targetl);
        buf[i++] = ':';
        for (k = 0 ; k < qr.targetl ; k++)
            buf[i++] = qr.target[k];
    }

    if (qr.nodesl > 0) {
        buf[i++] = '5'; buf[i++] = ':'; buf[i++] = 'n'; buf[i++] = 'o'; buf[i++] = 'd'; buf[i++] = 'e'; buf[i++] = 's';
        integertostring(buf, &i, qr.nodesl);
        buf[i++] = ':';
        for (k = 0 ; k < qr.nodesl ; k++)
            buf[i++] = qr.nodes[k];
    }

    if (qr.peersl > 0) {
        buf[i++] = '5'; buf[i++] = ':'; buf[i++] = 'p'; buf[i++] = 'e'; buf[i++] = 'e'; buf[i++] = 'r'; buf[i++] = 's';
        integertostring(buf, &i, qr.peersl);
        buf[i++] = ':';
        for (k = 0 ; k < qr.peersl ; k++)
            buf[i++] = qr.peers[k];
    }

    buf[i++] = 'e';

    buf[i++] = 'e';

    *bufl = i;

    return i;
}
int getcount(unsigned char *b, int bl, int *i) {
    int s = 0;
    int j = 0;
    bool gotcount = false;
    int count = 0;
    bool negative = false;
    while (*i + j < bl && j < 12) {
        if (j == 0 && b[*i + j] == '-') {
            negative = true;
        } else if (b[*i + j] == ':' || b[*i + j] == 'e') {
            gotcount = true;
            break;
        } else if ('0' <= b[*i + j] && b[*i + j] <= '9') {
            s++;
        } else {
            s = 0;
            break;
        }
        j++;
    }
    if (0 < s && s < 12 && gotcount) {
        int mult = 1;
        for (j = *i + s - 1; j >= *i; j--) {
            count += mult * (b[j] - 0x30);
            mult *= 10;
        }
        *i += s + 1;
        if (negative)
            count *= -1;
        //printf("(+header %d) +%d", s + 1, count);
    } else if (s == 0 && gotcount) {
        *i += 1;
    } else {
        printf("getcount error bailing out");
    }
    return count;
}
int parsebencodede(unsigned char *b, int bl, struct AskResponse *resp) {
    int i, j, k, rc;

    //dump_buf("parse", reinterpret_cast<char *>(b), bl);
    char *o = (char *) malloc(sizeof(unsigned char) * MAXMSGSIZE * 2);
    int l = 0;
    for (i = 0 ; i < bl ; i++) {
        if (b[i] == ':' || b[i] == '_' || ('a' <= b[i] && b[i] <= 'z') || ('0' <= b[i] && b[i] <= '9'))
            l += snprintf(o + l, 512, "%c", (unsigned char) b[i]);
        else
            l += snprintf(o + l, 512, "%02x", (unsigned char) b[i]);
    }
    o[l++] = ' ';
    int curr = -1;
    int d[20];
    int di = -1;
    bool paar = false;
    bool paarheader = true;
    bool found;
    i = 0;
    while (i < bl) {
        if (b[i] == 'd') {
            o[l++] = '{';
            paar = true;
            paarheader = true;
            di++;
            d[di] = 1;
            i++;
        } else if (b[i] == 'l') {
            o[l++] = '[';
            paar = false;
            paarheader = true;
            di++;
            d[di] = 2;
            i++;
        } else if (b[i] == 'e') {
            if (d[di] == 1) {
                o[l++] = '}';
            } else if (d[di] == 2) {
                o[l++] = ']';
            } else {
                printf("parse : end unknown %s", o);
                free((char *) o);
                return -1;
            }
            di--;
            if (di >= 0) {
                if (d[di] == 1) {
                    paar = true;
                    paarheader = true;
                } else if (d[di] == 2) {
                    paar = false;
                    paarheader = true;
                }
            }
            i++;
        } else if (b[i] == 'i' && i + 3 < bl) { //   i0e   i-23e
            i++; // avance i
            int oi = i;
            j = getcount(b, bl, &i);
            if (oi == i) {
                printf("parse : failed to read integer %s", o);
                free((char *) o);
                return -1;
            }
            if (paar && paarheader) {
                printf("parse : integer as header");

            } else if (paar && !paarheader) {
                //printf("integer in dic ");
                if (curr == dic_ports) {
                    if (resp->porti == -1) {
                        l += snprintf(o + l, 512, " porti");
                        resp->porti = j;
                    } else {
                        curr = -1;
                    }
                }
            } else {
                //printf("integer in list ");
                if (curr == dic_ports) {
                    if (resp->porti == -1) {
                        l += snprintf(o + l, 512, " porti");
                        resp->porti = j;
                    } else if (resp->portf == -1) {
                        l += snprintf(o + l, 512, " portf");
                        resp->portf = j;
                    }
                }
            }
            l += snprintf(o + l, 512, " %d", j);
            paarheader = !paarheader;
        } else {
            int oi = i;
            j = getcount(b, bl, &i);
            if (oi == i) {
                printf("error failed to read integer %s", o);
                free((char *) o);
                return -1;
            }
            if (j > 0) {
                if (paar && j > 0) {
                    if (paarheader) {
                        if (j == 1) {
                            if (b[i+0] == 't') {
                                curr = dic_t;
                                l += snprintf(o + l, 512, " t:");
                            } else if (b[i+0] == 'v') {
                                curr = dic_v;
                                l += snprintf(o + l, 512, " v:");
                            } else if (b[i+0] == 'y') {
                                curr = dic_y;
                                l += snprintf(o + l, 512, " y:");
                            } else if (b[i+0] == 'q') {
                                curr = dic_q;
                                l += snprintf(o + l, 512, " q:");
                            } else if (b[i+0] == 'a') {
                                curr = -1;
                                l += snprintf(o + l, 512, " a:");
                            } else if (b[i+0] == 'r') {
                                curr = -1;
                                l += snprintf(o + l, 512, " r:");
                            } else {
                                curr = -1;
                                l += snprintf(o + l, 512, " ?unknown header?:");
                            }
                        } else if (j == 2) {
                            if (b[i + 0] == 'i'
                                && b[i + 1] == 'd') {
                                curr = dic_id;
                                l += snprintf(o + l, 512, " id:");
                            } else {
                                curr = -1;
                                l += snprintf(o + l, 512, " ?unknown header?:");
                            }
                        } else if (j == 5) {
                            if (b[i + 0] == 'n'
                                && b[i + 1] == 'o'
                                && b[i + 2] == 'd'
                                && b[i + 3] == 'e'
                                && b[i + 4] == 's') {
                                curr = dic_nodes;
                                l += snprintf(o + l, 512, " nodes:");
                            } else if (b[i + 0] == 'p'
                                       && b[i + 1] == 'o'
                                       && b[i + 2] == 'r'
                                       && b[i + 3] == 't'
                                       && b[i + 4] == 's') {
                                curr = dic_ports;
                                l += snprintf(o + l, 512, " ports:");
                            } else if (b[i + 0] == 'p'
                                       && b[i + 1] == 'e'
                                       && b[i + 2] == 'e'
                                       && b[i + 3] == 'r'
                                       && b[i + 4] == 's') {
                                curr = dic_peers;
                                l += snprintf(o + l, 512, " peers:");
                            } else {
                                curr = -1;
                                l += snprintf(o + l, 512, " ?unknown header?:");
                            }
                        } else if (j == 6) {
                            if (b[i + 0] == 't'
                                && b[i + 1] == 'a'
                                && b[i + 2] == 'r'
                                && b[i + 3] == 'g'
                                && b[i + 4] == 'e'
                                && b[i + 5] == 't') {
                                curr = dic_target;
                                l += snprintf(o + l, 512, " target:");
                            } else {
                                curr = -1;
                                l += snprintf(o + l, 512, " ?unknown header?:");
                            }
                        } else {
                            curr = -1;
                            l += snprintf(o + l, 512, " ?unknown header?:");
                        }
                    } else {
                        if (curr == dic_t) {
                            for (k = 0; k < j; k++)
                                l += snprintf(o + l, 512, "%02x", (unsigned char) b[i + k]);
                            resp->tl = j;
                            resp->t = (unsigned char *) malloc(sizeof(unsigned char) * j);
                            for (k = 0; k < j; k++)
                                resp->t[k] = b[i + k];
                        } else if (curr == dic_v) {
                            for (k = 0; k < j; k++)
                                if (k < 2)
                                    l += snprintf(o + l, 512, "%c", (unsigned char) b[i + k]);
                                else
                                    l += snprintf(o + l, 512, "%02x", (unsigned char) b[i + k]);
                            resp->vl = j;
                            resp->v = (unsigned char *) malloc(sizeof(unsigned char) * j);
                            for (k = 0; k < j; k++)
                                resp->v[k] = b[i + k];
                        } else if (curr == dic_y && j == 1) {
                            for (k = 0; k < j; k++)
                                l += snprintf(o + l, 512, "%c", (unsigned char) b[i + k]);
                            if (b[i + 0] == 'q')
                                resp->query = true;
                            else if (b[i + 0] == 'r')
                                resp->query = false;
                            else {
                                printf("parse : y is neither q nor r %s", o);
                                free((char *) o);
                                return -1;
                            }
                        } else if (curr == dic_q) {
                            for (k = 0; k < j; k++)
                                l += snprintf(o + l, 512, "%c", (unsigned char) b[i + k]);
                            if (j == 9) {
                                if (b[i + 0] == 'f' && b[i + 1] == 'i' && b[i + 2] == 'n' && b[i + 3] == 'd' && b[i + 4] == '_' && b[i + 5] == 'n' && b[i + 6] == 'o' && b[i + 7] == 'd' && b[i + 8] == 'e') {
                                    resp->q = dic_find_node;
                                } else if (b[i + 0] == 'g' && b[i + 1] == 'e' && b[i + 2] == 't' && b[i + 3] == '_' && b[i + 4] == 'p' && b[i + 5] == 'e' && b[i + 6] == 'e' && b[i + 7] == 'r' && b[i + 8] == 's') {
                                    resp->q = dic_get_peers;
                                } else {
                                    l += snprintf(o + l, 512, "?unknown query?");
                                }
                            } else if (j == 8) {
                                if (b[i + 0] == 'd' && b[i + 1] == 'o' && b[i + 2] == 'w' && b[i + 3] == 'n' && b[i + 4] == 'l' && b[i + 5] == 'o' && b[i + 6] == 'a' && b[i + 7] == 'd') {
                                    resp->q = dic_download;
                                } else {
                                    l += snprintf(o + l, 512, "?unknown query?");
                                }
                            } else if (j == 13) {
                                if (b[i + 0] == 'a' && b[i + 1] == 'n' && b[i + 2] == 'n' && b[i + 3] == 'o' && b[i + 4] == 'u' && b[i + 5] == 'n' && b[i + 6] == 'c' && b[i + 7] == 'e' && b[i + 8] == '_' && b[i + 9] == 'p' && b[i + 10] == 'e' && b[i + 11] == 'e' && b[i + 12] == 'r') {
                                    resp->q = dic_announce_peer;
                                } else {
                                    l += snprintf(o + l, 512, "?unknown query?");
                                }
                            } else if (j == 4) {
                                if (b[i + 0] == 'p' && b[i + 1] == 'i' && b[i + 2] == 'n' && b[i + 3] == 'g') {
                                    resp->q = dic_ping;
                                } else {
                                    l += snprintf(o + l, 512, "?unknown query?");
                                }
                            } else {
                                l += snprintf(o + l, 512, "?unknown query?");
                            }
                        } else if (curr == dic_id) {
                            for (k = 0; k < j; k++)
                                l += snprintf(o + l, 512, "%02x", (unsigned char) b[i + k]);
                            resp->idl = j;
                            resp->id = (unsigned char *) malloc(sizeof(unsigned char) * j);
                            for (k = 0; k < j; k++)
                                resp->id[k] = b[i + k];
                        } else if (curr == dic_ports) {
                            if (j == 2) {
                                resp->porti = (b[i + 0] << 8) + (b[i + 1]);
                                resp->portf = -1;
                                l += snprintf(o + l, 512, "%d %d", resp->porti, resp->portf);
                            } else if (j == 4) {
                                resp->porti = (b[i + 0] << 8) + (b[i + 1]);
                                resp->portf = (b[i + 2] << 8) + (b[i + 3]);
                                l += snprintf(o + l, 512, "%d %d", resp->porti, resp->portf);
                            } else {
                                printf("too much ports %s", o);
                                free((char *) o);
                                return -1;
                            }
                        } else if (curr == dic_target) {
                            for (k = 0; k < j; k++)
                                l += snprintf(o + l, 512, "%02x", (unsigned char) b[i + k]);
                            resp->targetl = j;
                            resp->target = (unsigned char *) malloc(sizeof(unsigned char) * j);
                            for (k = 0; k < j; k++)
                                resp->target[k] = b[i + k];
                        } else if (curr == dic_nodes) {
                            for (k = 0; k < j; k++)
                                l += snprintf(o + l, 512, "%02x", (unsigned char) b[i + k]);
                            resp->nodesl = j;
                            resp->nodes = (unsigned char *) malloc(sizeof(unsigned char) * j);
                            for (k = 0; k < j; k++)
                                resp->nodes[k] = b[i + k];
                        } else if (curr == dic_peers) {
                            for (k = 0; k < j; k++)
                                l += snprintf(o + l, 512, "%02x", (unsigned char) b[i + k]);
                            resp->peersl = j;
                            resp->peers = (unsigned char *) malloc(sizeof(unsigned char) * j);
                            for (k = 0; k < j; k++)
                                resp->peers[k] = b[i + k];
                        } else {
                            l += snprintf(o + l, 512, "?unknown value?");
                        }
                        curr = -1;
                    }
                } else {
                    curr = -1;
                    l += snprintf(o + l, 512, "?not a paar?");
                }
                i += j;
            } else {
                printf("error getcount < 0 %s", o);
                free((char *) o);
                return -1;
            }
            paarheader = !paarheader;
        }
    }

    printf("%s", o);
    free((char *) o);
    return 0;
}


/*
{
      't': tid,         2bytes (salt)
      'v': self.v       2bytes appname + 2 bytes version
      'y': 'q',
      'q': 'find_node',
          'a'['ping']: {
                'id': self.nodeid,
              },
          'a'['find_node']: {
                'id': self.nodeid,
                'target': self.target,
                'want':['n4', 'n6', 'no'],
              },
          'a'['get_peers']: {
                'id': self.nodeid,
                'info_hash': self.infohash,
                'want':['n4', 'n6', 'no'],                  # 4:wantl2:n42:n6e
              },
          'a'['announce_peer']: {
                'id': self.nodeid,
                'implied_port': self.implied_port,
                'info_hash': self.infohash,
                'port': self.port,
                'token': self.token,
              },
   'pref': [['nodes':''], ['nodes6':''], ['nodeso':'']]
   'salt': salt
   'pubkey': public_key
   'signature': signed_previous_and_next_bytes_zeros_64
}

 ping Query = {"t":"aa", "y":"q", "q":"ping",
              "a":{"id":"abcdefghij0123456789"}}
 bencoded = d1:ad2:id20:abcdefghij0123456789e1:q4:ping1:t2:aa1:y1:qe
 Response = {"t":"aa", "y":"r",
              "r": {"id":"mnopqrstuvwxyz123456"}}
 bencoded = d1:rd2:id20:mnopqrstuvwxyz123456e1:t2:aa1:y1:re

 find_node Query = {"t":"aa", "y":"q", "q":"find_node",
                   "a": {"id":"abcdefghij0123456789", "target":"mnopqrstuvwxyz123456"}}
 bencoded = d1:ad2:id20:abcdefghij01234567896:target20:mnopqrstuvwxyz123456e1:q9:find_node1:t2:aa1:y1:qe
 Response = {"t":"aa", "y":"r", "r":
                   {"id":"0123456789abcdefghij", "nodes": "def456..."}}
 bencoded = d1:rd2:id20:0123456789abcdefghij5:nodes9:def456...e1:t2:aa1:y1:re

 get_peers Query = {"t":"aa", "y":"q", "q":"get_peers",
                   "a": {"id":"abcdefghij0123456789", "info_hash":"mnopqrstuvwxyz123456"}}
 bencoded = d1:ad2:id20:abcdefghij01234567899:info_hash20:mnopqrstuvwxyz123456e1:q9:get_peers1:t2:aa1:y1:qe
 Response with peers = {"t":"aa", "y":"r",
                   "r": {"id":"abcdefghij0123456789", "token":"aoeusnth", "values": ["axje.u", "idhtnm"]}}
 bencoded = d1:rd2:id20:abcdefghij01234567895:token8:aoeusnth6:valuesl6:axje.u6:idhtnmee1:t2:aa1:y1:re
 Response with closest nodes = {"t":"aa", "y":"r",
                   "r": {"id":"abcdefghij0123456789", "token":"aoeusnth", "nodes": "def456..."}}
 bencoded = d1:rd2:id20:abcdefghij01234567895:nodes9:def456...5:token8:aoeusnthe1:t2:aa1:y1:re

 announce_peers Query = {"t":"aa", "y":"q", "q":"announce_peer",
                        "a": {"id":"abcdefghij0123456789", "implied_port": 1, "info_hash":"mnopqrstuvwxyz123456", "port": 6881, "token": "aoeusnth"}}
 bencoded = d1:ad2:id20:abcdefghij012345678912:implied_porti1e9:info_hash20:mnopqrstuvwxyz1234564:porti6881e5:token8:aoeusnthe1:q13:announce_peer1:t2:aa1:y1:qe
 Response = {"t":"aa", "y":"r",
                        "r": {"id":"mnopqrstuvwxyz123456"}}
 bencoded = d1:rd2:id20:mnopqrstuvwxyz123456e1:t2:aa1:y1:re


*/