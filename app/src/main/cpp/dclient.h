
int TCPclient(unsigned char *pub, struct AskResponse ask) {
    int i, j, rc;
    int client_fd;
    struct sockaddr_in address;

    // create socket
    client_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (client_fd < 0) {
        printf("  [c]  ERROR TCPclient create socket");
        return -1;
    }


    struct timeval rcvtimeout;
    rcvtimeout.tv_sec = CLIRCVTIMEOUT;
    rcvtimeout.tv_usec = 0;
    rc = setsockopt(client_fd, SOL_SOCKET, SO_RCVTIMEO, (char *) &rcvtimeout, sizeof(rcvtimeout));
    if (rc < 0) {
        printf("  [c]  ERROR TCPclient SO_RCVTIMEO");
        close(client_fd);
        return -1;
    }
    struct timeval sndtimeout;
    sndtimeout.tv_sec = CLISNDTIMEOUT;
    sndtimeout.tv_usec = 0;
    rc = setsockopt(client_fd, SOL_SOCKET, SO_SNDTIMEO, (char *) &sndtimeout, sizeof(sndtimeout));
    if (rc < 0) {
        printf("  [c]  ERROR TCPclient SO_SNDTIMEO");
        close(client_fd);
        return -1;
    }


    int dport = 0;
    char domain[1024];
    pthread_mutex_lock(&mutexclientinfo);
    int foundat = find_client(pub, HASHL);
    if (foundat >= 0) {
        dport = clientinfo[foundat].porti +
                rand() % (clientinfo[foundat].portf - clientinfo[foundat].porti + 1);
        for (i = 0; i <= clientinfo[foundat].onionl; i++) // \x00
            domain[i] = clientinfo[foundat].onion[i];
    }
    pthread_mutex_unlock(&mutexclientinfo);
    if (foundat < 0) {
        printf("  [c]  ERROR client disappeared from our table");
        close(client_fd);
        return -1;
    }


    address.sin_family = AF_INET;
    address.sin_port = htons(TORSOCKSPORT);
    address.sin_addr.s_addr = inet_addr("127.0.0.1");
    rc = connect(client_fd, (struct sockaddr *) &address, sizeof(address));
    if (rc < 0) {
        printf("  [c]  ERROR TCPclient connect SOCKS5 tor is down connection dropped");
        close(client_fd);
        return -2;
    }

    char socksrequest[256];
    char socksresponse[256];
    j = 0;
    socksrequest[j++] = 0x05; // SOCKS5
    socksrequest[j++] = 0x01; // One authentication method
    socksrequest[j++] = 0x00; // No authentication
    rc = send(client_fd, socksrequest, 3, MSG_NOSIGNAL);
    if (rc != 3) {
        printf("  [c]  ERROR TCPclient send auth SOCKS5 %d", rc);
        close(client_fd);
        return -3;
    }
    rc = recv(client_fd, socksresponse, 256, 0);
    if (socksresponse[1] != 0) {
        printf("  [c]  ERROR TCPclient recv auth SOCKS5 rc %d ", rc);
        char o[rc * 2];
        int l = 0;
        for (i = 0; i < rc; i++)
            l += snprintf(o + l, 256, "%02x", socksresponse[i]);
        printf("  [c]  %s", o);
        close(client_fd);
        return -3;
    }

    char domainlen = (char) strlen(domain);
    short destport = htons(dport);
    j = 0;
    socksrequest[j++] = 0x05; // SOCKS5
    socksrequest[j++] = 0x01; // CONNECT
    socksrequest[j++] = 0x00; // RESERVED
    socksrequest[j++] = 0x03; // DOMAIN
    memcpy(socksrequest + 4, &domainlen, 1);            // Domain Length
    memcpy(socksrequest + 5, domain, domainlen);        // Domain
    memcpy(socksrequest + 5 + domainlen, &destport, 2); // Port
    int sendl = 4 + 1 + domainlen + 2;
    rc = send(client_fd, (char *) socksrequest, sendl, MSG_NOSIGNAL);
    if (rc != sendl) {
        printf("  [c]  ERROR TCPclient send domain SOCKS5 %d", rc);
        close(client_fd);
        return -4;
    }
    rc = recv(client_fd, socksresponse, 256, 0);
    if (socksresponse[1] != 0) {
        printf("  [c]  ERROR TCPclient recv domain SOCKS5 rc %d errno %d", rc, errno);
        char o[rc * 2];
        int l = 0;
        for (i = 0; i < rc; i++)
            l += snprintf(o + l, 256, "%02x", socksresponse[i]);
        printf("  [c]  %s", o);
        close(client_fd);
        return -4; // si le serveur n'est pas trouvé par tor ou n'existe pas on supprime de la database
    }


    //int bufal = MAXMSGSIZE;
    //unsigned char *bufa = (unsigned char *) malloc(sizeof(unsigned char) * bufal);
    //querybencodede(bufa, &bufal, ask);
    int bufatmpl = MAXMSGSIZE;
    unsigned char *bufatmp = (unsigned char *) malloc(sizeof(unsigned char) * bufatmpl);
    querybencodede(bufatmp, &bufatmpl, ask);
    unsigned char *bufa;
    int offset = header_size(TYPE_BENCODED_XZPE);
    int bufal = envigenerize(pub, HASHL, offset, bufatmp, bufatmpl, &bufa);
    offset = write_header(bufa, bufal, TYPE_BENCODED_XZPE);
    free((unsigned char *) bufatmp);

    /*if (5 == 5) {
        unsigned char *bufrtmp;
        int bufrtmpl = devigenerize(pub, HASHL, offset, bufa, bufal, &bufrtmp);
        free((unsigned char*) bufrtmp);
    }*/
    //dumpbufx("  [c]  ", bufa, bufal);


    //bufal = send(client_fd, bufa, bufal, MSG_NOSIGNAL);
    int chunksize = CHUNKSIZE;
    int bytesWritten = 0;
    int bytesToWrite = bufal;
    while (bytesWritten != bytesToWrite) {
        int writtenThisTime;
        if (bytesToWrite - bytesWritten < chunksize)
            chunksize = bytesToWrite - bytesWritten;
        writtenThisTime = send(client_fd, bufa + bytesWritten, chunksize, MSG_NOSIGNAL);
        bytesWritten += writtenThisTime;
        if (writtenThisTime == -1) {
            printf("  [c]  send failed writtenThisTime %d", errno);
            bufal = -1;
            break;
        } else {
            printf("  [c]  sent %d total %d", writtenThisTime, bytesWritten);
        }
    }

    free((unsigned char *) bufa);
    if (bufal < 0) {
        printf("  [c]  send failed %d", errno);
        close(client_fd);
        return -5;
    }


    int bufrl = MAXMSGSIZE;
    unsigned char *bufr = (unsigned char *) malloc(sizeof(unsigned char) * bufrl);

    //bufrl = recv(client_fd, bufr, MAXMSGSIZE, 0);
    chunksize = CHUNKSIZE;
    int bytesRead = 0;
    int readThisTime = 1;
    int messagesize = -1;
    offset = 0;
    unsigned char type = 0;
    while (readThisTime > 0) {
        readThisTime = recv(client_fd, bufr + bytesRead, chunksize, 0);
        if (readThisTime != -1)
            bytesRead += readThisTime;
        if (readThisTime == -1 && errno == EAGAIN) {
            printf("  [c]  recv socket timeout EAGAIN %d total %d", readThisTime, bytesRead);
            bufrl = bytesRead;
        } else if (readThisTime == -1) {
            printf("  [c]  recv failed readThisTime %d", errno);
            bufrl = 0;
            break;
        } else {
            //printf("  [c]  recv %d total %d", readThisTime, bytesRead);
            bufrl = bytesRead;
            if (messagesize == -1) {
                if (bytesRead < 10) {
                    printf("  [c]  recv too few bytes");
                } else {
                    messagesize = read_header(bufr, bytesRead, &offset, &type);
                    if (messagesize == 0) {
                        //0500000100000000000 00000008700000087d3a1
                        char o[bytesRead * 2];
                        int l = 0;
                        for (i = 0; i < bytesRead; i++)
                            l += snprintf(o + l, 256, "%02x", bufr[i]);
                        printf("  [c]  rcv error bailing out %s", o);
                        bufrl = 0;
                        break;
                    } else {
                        printf("  [c]  recv message will have %d bytes", messagesize);
                    }
                }
            }
            if (bytesRead >= messagesize && messagesize >= 0) {
                printf("  [c]  recv we have the full message");
                break;
            }
        }
    }

    struct AskResponse resp;
    if (bufrl > 0) {
        if (type == TYPE_BENCODED_XZPE) {
            unsigned char *bufrtmp;
            int bufrtmpl = devigenerize(OURPUBKEY, HASHL, offset, bufr, bytesRead, &bufrtmp);
            rc = parsebencodede(bufrtmp, bufrtmpl, &resp);
            free((unsigned char *) bufrtmp);
        } else if (type == TYPE_COLLECTION_XZPE) {
            unsigned char *bufrtmp;
            int bufrtmpl = devigenerize(OURPUBKEY, HASHL, offset, bufr, bytesRead, &bufrtmp);
            rc =  parsecollectione(bufrtmp, bufrtmpl, DOSSIERDESSIN);
            free((unsigned char *) bufrtmp);
            if (rc > 0) {
                printf("  [c]  successfully parsed collection, changing TASK_SEARCH_KEYWORDS = 3");
                TASK_SEARCH_KEYWORDS = 3;
            } else {
                printf("  [c]  failed to parse collection");
            }
        } else {
            printf("  [c]  unknown type");
        }
    } else {
        bufrl = 0;
    }
    free((unsigned char *) bufr);
    if (bufrl <= 0 || rc < 0) {
        printf("  [c]  recv failed");
        close(client_fd);
        return -6;
    }



    rc = 0;
    if (!resp.query) {
        if (resp.nodesl > 0) {
            int bl = HASHL + 2 + 2;
            if (resp.nodesl % bl == 0) {
                printf("  [c]  we've received %d nodes", (int) (resp.nodesl / bl));
                for (i = 0 ; i < resp.nodesl ; i+=bl) {
                    struct NodeInfo client;
                    for (j = 0; j < HASHL; j++)
                        client.pub[j] = resp.nodes[i + j];
                    client.publ = HASHL;
                    // network byte order big indian
                    client.porti = (resp.nodes[i + HASHL + 0] << 8) + resp.nodes[i + HASHL + 1];
                    client.portf = (resp.nodes[i + HASHL + 2] << 8) + resp.nodes[i + HASHL + 3];
                    pthread_mutex_lock(&mutexclientinfo);
                    int foundati = find_client(client.pub, client.publ);
                    if (foundati == -1) {
                        int addedatposition = add_client(client);
                        if (addedatposition >= 0) {
                            rc++;
                        }
                    }
                    pthread_mutex_unlock(&mutexclientinfo);
                }
            } else {
                printf("  [c]  we've received a wrong number %d of %d nodes", resp.nodesl, bl);
            }
        }
        if (resp.peersl > 0 && resp.targetl > 0) {
            int bl = HASHL + 2 + 2;
            if (resp.peersl % bl == 0) {
                printf("  [c]  we've received %d peers", (int) (resp.peersl / bl));
                pthread_mutex_lock(&infohashmutex);
                int found = find_hash(resp.target, resp.targetl);
                if (found < 0) {
                    struct HashInfo hi;
                    hi.keywordshashl = resp.targetl;
                    for (i = 0 ; i < HASHL ; i++)
                        hi.keywordshash[i] = resp.target[i];
                    found = add_hashinfo(hi);
                }
                if (found < 0) {
                    printf("  [c]  we were unable to add the target hash");
                } else {
                    if (hashinfo[found].peerlistl > 0) {
                        printf("  [c]  we already have %d peers for this hash", hashinfo[found].peerlistl);
                        free((struct PeerInfo *) hashinfo[found].peerlist);
                        hashinfo[found].peerlistl = 0;
                    }
                    hashinfo[found].peerlistl = (int) (resp.peersl / bl);
                    hashinfo[found].peerlist = (struct PeerInfo *) malloc(sizeof(struct PeerInfo) * hashinfo[found].peerlistl);
                    for (i = 0 ; i < hashinfo[found].peerlistl ; i++) {
                        for (j = 0 ; j < bl ; j++)
                            hashinfo[found].peerlist[i].pub[j] = resp.peers[i * bl + j];
                    }
                }
                pthread_mutex_unlock(&infohashmutex);
            } else {
                printf("  [c]  we've received a wrong number %d of %d peers", resp.peersl, bl);
            }
        }
    }
    if (resp.tl > 0)
        free((unsigned char *) resp.t);
    if (resp.vl > 0)
        free((unsigned char *) resp.v);
    if (resp.idl > 0)
        free((unsigned char *) resp.id);
    if (resp.targetl > 0)
        free((unsigned char *) resp.target);
    if (resp.nodesl > 0)
        free((unsigned char *) resp.nodes);
    if (resp.peersl > 0)
        free((unsigned char *) resp.peers);

    if (ask.tl > 0)
        free((unsigned char *) ask.t);


    close(client_fd);
    return rc;
}