int compareSize(const void *const A, const void *const B) {
    return (*(unsigned long*) A - *(unsigned long*) B);
}
int scan_dir_create_links(char *path, char *prefix, char *suffix, char *linkprefix, bool epoch, FILE *op){
    int i, j;
    int r;
    int count;
    DIR *dir;
    unsigned long *list;
    struct dirent *dp;
    int pathl = strlen(path);
    int prefixl = strlen(prefix);
    int suffixl = strlen(suffix);
    char dirpath[pathl];
    snprintf(dirpath, 256, "%s", path);
    printf("folder : %s", dirpath);
    dir = opendir(dirpath);
    if (dir == NULL) {
        printf("cannot open dir");
        return -1;
    }
    count = 0;
    while ((dp = readdir(dir)) != NULL) {
        int namlen = strlen(dp->d_name);
        if (namlen > suffixl && namlen > prefixl) {
            bool match = true;
            for (i = 0; i < prefixl; i++) {
                if (dp->d_name[i] != prefix[i]) {
                    match = false;
                    break;
                }
            }
            if (match) {
                for (i = 0; i < suffixl; i++) {
                    if (dp->d_name[namlen - (suffixl - i)] != suffix[i]) {
                        match = false;
                        break;
                    }
                }
                if (match) {
                    //printf("found : %s", dp->d_name);
                    ++count;
                }
            }
        }
    }
    printf("found %d corresponding files", count);
    list = (unsigned long *) (malloc(count * sizeof(unsigned long)));
    if (list == NULL) {
        return -2;
    }
    rewinddir(dir);
    count = 0;
    while ((dp = readdir(dir)) != NULL) {
        int namlen = strlen(dp->d_name);
        if (namlen > suffixl && namlen > prefixl) {
            bool match = true;
            for (i = 0; i < prefixl; i++) {
                if (dp->d_name[i] != prefix[i]) {
                    match = false;
                    break;
                }
            }
            if (match) {
                for (i = 0; i < suffixl; i++) {
                    if (dp->d_name[namlen - (suffixl - i)] != suffix[i]) {
                        match = false;
                        break;
                    }
                }
                if (match) {
                    //printf("adding : %s", dp->d_name);
                    char timestamp[namlen - prefixl - suffixl];
                    char *endptr;
                    r = 0;
                    for (j = prefixl ; j < namlen - suffixl ; j++) {
                        timestamp[r++] = dp->d_name[j];
                    }
                    unsigned long tstp = strtoul(timestamp, &endptr, 10);
                    list[count++] = tstp;
                }
            }
        }
    }
    closedir(dir);
    qsort(list, count, sizeof(unsigned long), compareSize);
    write_int_to_file(count, op);
    char *ddlink = (char *) malloc(sizeof(char) * (256));
    for (i = 0 ; i < count ; i++) {
        //fwrite((const void*) tstp, sizeof(unsigned long), 1, op);
        snprintf(ddlink, 256, "%s%s%0ld%s", linkprefix, prefix, list[i], suffix);
        int ddlinkl = strlen(ddlink);
        write_int_string_to_filec(ddlink, ddlinkl, op);
        //printf("%ld : %s %d", list[i], ddlink, ddlinkl);
    }
    free(ddlink);
    free(list);
    return 0;
}
enum psels_types {
    psels_none,
    psels_printname,
    psels_keywords,
    psels_element,
    psels_add,
    psels_remove,
    psels_type_updatablefile,
    psels_type_folder,
    psels_scan_path,
    psels_scan_prefix,
    psels_scan_suffix,
    psels_epoch,
    psels_link_prefix,
};
struct Element {
    bool add;
    int element_type;

    // folder
    unsigned char printname[256]; int printnamel;

    // updatablefile
    unsigned char scanpath[256]; int scanpathl;
    unsigned char scanprefix[256]; int scanprefixl;
    unsigned char scansuffix[256]; int scansuffixl;
    unsigned char linkprefix[256]; int linkprefixl;
    bool epoch;

};
void addcollection(struct HashInfo hi, unsigned char *buf, int bufl, char *oname, FILE *op){
    int j;
    bool filechanged = false;
    printf("update this collection in our database");
    stringtohash(hi.keywords, hi.keywordsl, hi.keywordshash, &hi.keywordshashl);
    printf("keywords %s %d", hi.keywords, hi.keywordsl);
    stringtohash(buf, bufl, hi.filehash, &hi.filehashl);
    pthread_mutex_lock(&infohashmutex);
    int foundatposition = find_hash(hi.keywordshash, hi.keywordshashl);
    if (foundatposition >= 0) {
        for (j = 0 ; j < hi.filehashl ; j++) {
            if (hi.filehash[j] != hashinfo[foundatposition].filehash[j]) {
                filechanged = true;
                break;
            }
        }
        if (filechanged) {
            printf("file changed");
        } else {
            printf("file did not change");
        }
    }
    for (j = 0 ; j < HASHL ; j++)
        hi.pub[j] = OURPUBKEY[j];
    hi.publ = HASHL;
    if (hi.keywordsl > 0) {// && (foundatposition == -1 || filechanged)) {
        hi.filenamel = strlen(oname);
        for (j = 0 ; j < hi.filenamel ; j++)
            hi.filename[j] = oname[j];
        hi.filename[hi.filenamel] = '\0';
        if (filechanged) {
            remove_hashinfo(foundatposition);
        }
        add_hashinfo(hi);
    }
    pthread_mutex_unlock(&infohashmutex);

    printf("write collection header");
    write_int_string_to_fileuc(hi.printname, hi.printnamel, op);
    write_int_string_to_fileuc(hi.keywords, hi.keywordsl, op);
}
void addelement(struct Element el, FILE *op){
    if (el.add)
        fputc('\x01', op);
    else
        fputc('\x00', op);

    if (el.element_type == psels_type_folder
        && el.printnamel > 0
            ) {
        printf("add directory");

        fputc('d', op);

        write_int_string_to_fileuc(el.printname, el.printnamel, op);

        el.printnamel = 0;

    } else if (el.element_type == psels_type_updatablefile
               && el.scanpathl > 0
               && el.scanprefixl >= 0
               && el.scansuffixl > 0
               && el.linkprefixl > 0
            ) {
        printf("add updatablefile");

        fputc('u', op);

        //if (foundatposition == -1 || filechanged) {
        scan_dir_create_links((char*)(el.scanpath),
                              (char*)(el.scanprefix),
                              (char*)(el.scansuffix),
                              (char*)(el.linkprefix),
                              el.epoch, op);
    } else {
        printf("unknown element type");
    }
}
int scan_line(int *i, int *a , int *b, int *psels_type, unsigned char *buf, int bufl){
    *a = *i;
    *psels_type = psels_none;
    bool header = true;
    while (*i < bufl) {
        if (header && buf[*i] == (char) '=') {
            if (*i - *a == 10) {
                if (foundstring(buf, *a, "scanprefix"))
                    *psels_type = psels_scan_prefix;
                else if (foundstring(buf, *a, "linkprefix"))
                    *psels_type = psels_link_prefix;
                else if (foundstring(buf, *a, "scansuffix"))
                    *psels_type = psels_scan_suffix;
            } else if (*i - *a == 9) {
                if (foundstring(buf, *a, "printname"))
                    *psels_type = psels_printname;
            } else if (*i - *a == 8) {
                if (foundstring(buf, *a, "keywords"))
                    *psels_type = psels_keywords;
                else if (foundstring(buf, *a, "scanpath"))
                    *psels_type = psels_scan_path;
            } else if (*i - *a == 7) {
                if (foundstring(buf, *a, "element"))
                    *psels_type = psels_element;
            } else if (*i - *a == 5) {
                if (foundstring(buf, *a, "epoch"))
                    *psels_type = psels_epoch;
            }
            *a = *i + 1;
            header = false;
        } else if (buf[*i] == (char) '\n') {
          if (!header) {
            *b = *i;
            //printf("type %d from %d to %d size %d <%c%c...%c%c>", *psels_type, *a, *b, *b-*a, buf[*a + 0], buf[*a + 1], buf[*b - 2], buf[*b - 1]);
            return *i;
          } else {
            break;
          }
        }
        *i += 1;
    }
    return -1;
}
int create_pslp_from_psl(const char* dirpath) {
    FILE *fp, *op;
    char fname[256];
    char oname[256];
    int i, j;
    int r;
    DIR *dirp = opendir(dirpath);
    if (dirp == NULL) {
        printf("cannot open dir");
        return -1;
    }
    struct dirent *dp;
    while ((dp = readdir(dirp)) != NULL) {
        int namlen = strlen(dp->d_name);
        if (namlen > 4) {
            if (dp->d_name[namlen-4] == (char) '.'
                && dp->d_name[namlen-3] == (char) 'p'
                && dp->d_name[namlen-2] == (char) 's'
                && dp->d_name[namlen-1] == (char) 'l'
                    ) {
                printf("found file : %s (len %d)", dp->d_name, namlen);
                snprintf(oname, sizeof(oname), "%s/%sp", dirpath, dp->d_name);
                op = fopen(oname, "wb");
                snprintf(fname, sizeof(fname), "%s/%s", dirpath, dp->d_name);
                fp = fopen(fname, "rb");
                if (fp == NULL || op == NULL) {
                    printf("could not read %s or write %s", fname, oname);
                    continue;
                } else {
                    fseek(fp, 0L, SEEK_END);
                    int bufl = (int) ftell(fp);
                    rewind(fp);
                    int collectionpos = 0;
                    unsigned char buf[bufl];
                    r = fread(buf, 1, bufl, fp);
                    struct Element el;
                    int a = 0;
                    int b = -1;
                    bool hasprintname = false;
                    bool haskeywords = false;
                    bool headercreated = false;
                    int psels_type = psels_none;
                    struct HashInfo hi;
                    i = 0;
                    bool filechanged = false;
                    int foundatposition = -1;
                    while (i < r) {
                        if (scan_line(&i, &a, &b, &psels_type, buf, r) >= 0) {
                            //printf("%d/%d size %d line type %d pos %d value from %d to %d", i, bufl, b-a, psels_type, i, a, b);
                            if (psels_type == psels_none) {
                                //printf("psels_none");
                            } else if (!hasprintname && psels_type == psels_printname && b > a) {
                                //printf("psels_printname");
                                hi.printnamel = b - a;
                                for (j = a; j < b; j++)
                                    hi.printname[j - a] = buf[j];
                                hasprintname = true;
                            } else if (!haskeywords && psels_type == psels_keywords && b > a) {
                                //printf("psels_keywords");
                                hi.keywordsl = b - a;
                                for (j = a; j < b; j++)
                                    hi.keywords[j - a] = buf[j];
                                haskeywords = true;
                            } else if (psels_type == psels_element && b > a + 1) {
                                //printf("psels_element");
                                if (hasprintname && haskeywords) {
                                    if (el.element_type == psels_none) {
                                        if (!headercreated) {
                                            headercreated = true;
                                            addcollection(hi, buf, bufl, oname, op);
                                        }
                                    } else {
                                        addelement(el, op);
                                        el.element_type = psels_none;
                                        el.scanpathl = 0;
                                        el.scanprefixl = 0;
                                        el.scansuffixl = 0;
                                        el.linkprefixl = 0;
                                        el.epoch = false;
                                    }
                                }
                                if (buf[a] == (char) '+')
                                    el.add = true;
                                else
                                    el.add = false;
                                a++;
                                if (b - a == 13) {
                                    if (foundstring(buf, a, "updatablefile"))
                                        el.element_type = psels_type_updatablefile;
                                        //printf("add psels_type_updatablefile");
                                } else if (b - a == 6) {
                                    if (foundstring(buf, a, "folder"))
                                        el.element_type = psels_type_folder;
                                        //printf("add psels_type_folder");
                                } else {
                                    printf("unknown add element");
                                }
                                if (el.element_type != psels_none) {
                                    // we create a new element
                                }
                            } else if (el.element_type != psels_none && psels_type == psels_printname && b > a) {
                                //printf("psels_printname2");
                                el.printnamel = b - a;
                                for (j = a; j < b; j++)
                                    el.printname[j - a] = buf[j];
                            } else if (el.element_type != psels_none && psels_type == psels_scan_path && b > a) {
                                //printf("psels_scan_path");
                                el.scanpathl = b - a + 1;
                                for (j = a; j < b; j++)
                                    el.scanpath[j - a] = buf[j];
                                el.scanpath[el.scanpathl - 1] = '\0';
                            } else if (el.element_type != psels_none && psels_type == psels_scan_prefix && b >= a) {
                                //printf("psels_scan_prefix");
                                el.scanprefixl = b - a + 1;
                                for (j = a; j < b; j++)
                                    el.scanprefix[j - a] = buf[j];
                                el.scanprefix[el.scanprefixl - 1] = '\0';
                            } else if (el.element_type != psels_none && psels_type == psels_scan_suffix && b > a) {
                                //printf("psels_scan_suffix");
                                el.scansuffixl = b - a + 1;
                                for (j = a; j < b; j++)
                                    el.scansuffix[j - a] = buf[j];
                                el.scansuffix[el.scansuffixl - 1] = '\0';
                            } else if (el.element_type != psels_none && psels_type == psels_epoch && b > a) {
                                //printf("psels_epoch");
                                el.epoch = true;
                            } else if (el.element_type != psels_none && psels_type == psels_link_prefix && b > a) {
                                //printf("psels_link_prefix");
                                el.linkprefixl = b - a + 1;
                                for (j = a; j < b; j++)
                                    el.linkprefix[j - a] = buf[j];
                                el.linkprefix[el.linkprefixl - 1] = '\0';
                            } else {
                                printf("unknown el.element_type %d psels_type %d", el.element_type, psels_type);
                            }
                        }
                        i++;
                    }
                    if (hasprintname && haskeywords) {
                        if (el.element_type != psels_none) {
                            addelement(el, op);
                        }
                    }
                    fclose(fp);
                    fclose(op);
                }
            }
        }
    }
    (void) closedir(dirp);
    return 0;
}