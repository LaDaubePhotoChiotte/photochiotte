
int find_range_for_hash(const unsigned char *hash) {
    //printf("range for hash %02x%02x%02x", hash[0], hash[1], hash[2]);
    int i, j;
    for (i = rangesl - 1 ; i >= 0 ; i--) {
        //printf("looking for range %3d/%3d", i, rangesl - 1);
        for (j = 0 ; j < HASHL ; j++) {
            if (hash[j] > ranges[i * HASHL + j]) {
                //printf("%3d %3d %02x > %02x hash goes in this range", i, j, hash[j], ranges[i * HASHL + j]);
                return i;
            } else if (hash[j] < ranges[i * HASHL + j]) {
                //printf("%3d %3d %02x < %02x hash less than this range try next range", i, j, hash[j], ranges[i * HASHL + j]);
                break;
            } else {
                //printf("%3d %3d %02x = %02x", i, j, hash[j], ranges[i * HASHL + j]);
                if (j == HASHL) {
                    //printf("this is our range %d %d", i, j);
                    return i;
                }
            }
        }
    }
    return -1;
}

void expand_ranges_at(int position) {
    int i, j;

    // push right make room for new range
    for (i = rangesl; i > position + 1; i--) {
        for (j = 0; j < HASHL; j++)
            ranges[i * HASHL + j] = ranges[(i - 1) * HASHL + j];
        rangesclientscount[i] = rangesclientscount[i - 1];
    }
    // reset count for position
    rangesclientscount[position] = 0;
    // reset count for the newly created position
    rangesclientscount[position + 1] = 0;
    rangesl++;

    int min = position;
    int curr = position + 1;
    int max = position + 2;

    //dumpbufxx("ranges", ranges, HASHL * rangesl);
    //printf("min %8d curr %8d max %8d", min, curr, max);

    // sum
    unsigned short sum;
    unsigned char rest = 0;
    for (j = HASHL - 1 ; j >= 0; j--) {
        sum = ranges[min * HASHL + j] + ranges[max * HASHL + j];
        ranges[curr * HASHL + j] = (sum & 0x00ff) + rest;
        //printf("%02x + %02x = %02x %02x ; + rest %02x",
        //       ranges[min * HASHL + j], ranges[max * HASHL + j],
        //       (sum & 0xff00) >> 8, sum & 0x00ff, rest);
        if (sum > 0xff) {
            rest = 1;
        } else {
            rest = 0;
        }
    }
    //printf("il reste %02x", rest);
    //dumpbufxx("ranges", ranges, HASHL * rangesl);
    unsigned char octet, half1, half2;
    unsigned char high, low;
    for (j = 0 ; j < HASHL ; j++) {
        octet = ranges[curr * HASHL + j];
        high = octet >> 4;
        low = octet & 0x0f;
        //printf(" %3d %02x high %02x low %02x", j, octet, high, low);

        half1 = (rest << 4) + high;
        rest = half1 % 2;
        half1 = half1 / 2;
        //printf(" half1 %02x reste %02x", half1, rest);

        half2 = (rest << 4) + low;
        rest = ((rest << 4) + low) % 2;
        half2 = half2 / 2;
        //printf(" half2 %02x reste %02x", half2, rest);

        ranges[curr * HASHL + j] = (half1 << 4) + (half2 & 0x0f);
        //printf(" resultat %02x", octet);

        //printf("");
    }

    for (i = 0 ; i < clientsl ; i++) {
        if (clientinfo[i].range == position || clientinfo[i].range == position + 1) {
            printf("%d we recalculate this client's range %d", i, clientinfo[i].range);
            clientinfo[i].range = find_range_for_hash(clientinfo[i].pub);
            printf("   -> new range %d", clientinfo[i].range);
            rangesclientscount[clientinfo[i].range]++;
        } else if (clientinfo[i].range > position + 1) {
            printf("%d this client's range++ double check useless %d", i, clientinfo[i].range);
            clientinfo[i].range = find_range_for_hash(clientinfo[i].pub);
            printf("   should be == new range %d", clientinfo[i].range);
        }
    }

}
void remove_client(int position) {
    int i, j;
    printf("remove client %d", position);
    // push left copy clients
    rangesclientscount[clientinfo[position].range]--; // do this before swap
    for (i = position; i < clientsl - 1 ; i++) {
        for (j = 0; j < HASHL; j++)
            clients[i * HASHL + j] = clients[(i + 1) * HASHL + j];
        clientinfo[i] = clientinfo[i + 1];
    }
    clientsl--;
    dumpclients();
}
int find_client(unsigned char *pub, int publ) {
    int i, j;
    for (j = 0; j < clientsl; j++) {
        for (i = 0; i < HASHL; i++) {
            if (pub[i] != clients[j * HASHL + i])
                break;
        }
        if (i == HASHL) {
            //printf("found client at position %d", j);
            return j;
        }
    }
    return -1;
}
int add_client(struct NodeInfo client) {
    int i, j;
    int newpos = -1;

    if (clientsl > MAXCLIENTS) {
        printf("add_client we don't accept any new client we are full %d", clientsl);
        return -2;
    }
    /*if (find_client(client.pub, HASHL) != -1) {
        printf("we already have this client in our database");
        return -1;
    }*/

    client.range = find_range_for_hash(client.pub);
    printf("add_client new client wants to occupy range %d", client.range);

    if (OURNODE >= 0) {
        if (rangesclientscount[client.range] >= MAXCLIENTPERRANGE) {
            if (client.range == clientinfo[OURNODE].range) {
                printf("add_client range is full we need to split it");
                expand_ranges_at(client.range);
                //dumpbufxx("clients", clients, HASHL * clientsl);
                //dumpbufxx("ranges", ranges, HASHL * rangesl);
                //dumpclients();
                return add_client(client);
            } else {
                printf("add_client range is full we discard this new client");
                return -1;
            }
        }
    }
    rangesclientscount[client.range]++;

    dht_hostname_from_public(client.pub, client.publ, client.onion, &client.onionl);
    //printf("new client onion %s", client.onion);

    newpos = clientsl;
    for (i = 0; i < HASHL; i++) {
        clients[newpos * HASHL + i] = client.pub[i];
    }
    clientinfo[newpos] = client;
    clientsl++;

    printf("add_client new client accepted at clientinfo position %d range %d rangesize %d", newpos, client.range, rangesclientscount[client.range]);
    dumpclients();
    return newpos;
}
int find_hash(unsigned char *hash, int hashl) {
    int i, j;
    for (j = 0; j < hashesl; j++) {
        for (i = 0; i < HASHL; i++) {
            if (hash[i] != hashes[j * HASHL + i])
                break;
        }
        if (i == HASHL) {
            //printf("found hash at position %d", j);
            return j;
        }
    }
    return -1;
}
void remove_hashinfo(int position) {
    int i, j;
    printf("remove hashinfo %d / %d", position, hashesl);
    //if (hashinfo[i].metadatal > 0) {
    //    free((unsigned char*) hashinfo[i].metadata);
    //}
    // push left copy clients
    for (i = position; i < hashesl - 1 ; i++) {
        for (j = 0; j < HASHL; j++)
            hashes[i * HASHL + j] = hashes[(i + 1) * HASHL + j];
        hashinfo[i] = hashinfo[i + 1];
    }
    hashesl--;
    printf("removed hashinfo %d", hashesl);
}
int add_hashinfo(struct HashInfo hi) {
    int i, j;
    int newpos = -1;

    if (hashesl > MAXHASHES) {
        printf("we don't accept any new hashinfo");
        return -2;
    }
    int found = find_hash(hi.keywordshash, hi.keywordshashl);
    if (found >= 0) {
        printf("we already have this hashinfo in our database");
        return found;
    }
    newpos = hashesl;
    for (i = 0; i < HASHL; i++) {
        hashes[newpos * HASHL + i] = hi.keywordshash[i];
    }
    hashinfo[newpos] = hi;
    hashesl++;

    printf("new hashinfo accepted at hashinfo position %d / %d", newpos, hashesl);
    //printf("<%s> %d : ", (unsigned char *) hash.name, hash.namel);
    dumpbufx("hashinfo", hi.keywordshash, hi.keywordshashl);
    return newpos;
}
int briankernighansalgorithm(int number){
    int count = 0;
    while (number) {
        number &= (number - 1);
        count++;
    }
    return count;
}
void find_closest_nodes(unsigned char *ref, int refl,
                        int kmax,
                        unsigned int **found, int *foundl) {
    int i, b, j, k;
    int clientsll = clientsl;
    unsigned char *d = (unsigned char *) malloc(sizeof(unsigned char) * (clientsll * refl));
    /*int theresafullmatch = -1;
    for (b = 0 ; b < clientsll ; b++) {
        bool fullmatch = true;
        for (i = 0; i < refl; i++) {
            j = b * refl + i;
            d[j] = clients[j] ^ ref[i];
            if (d[j] != 0)
                fullmatch = false;
        }
        if (fullmatch)
            theresafullmatch = b;
    }
    if (theresafullmatch != -1)
        kmax++;*/
    if (kmax > clientsll)
        kmax = clientsll;
    bool *valid = (bool *) malloc(sizeof(bool) * clientsll);
    int *minifound = (int *) malloc(sizeof(int) * clientsll);
    int mininumber = 0;
    for (b = 0; b < clientsll; b++)
        minifound[b] = mininumber;
    unsigned char *min = (unsigned char *) malloc(sizeof(unsigned char) * (refl));

    for (k = 0 ; k < kmax ; k++) {
        //printf("\n----- searching for minimum number %3d", k+1);
        for (b = 0; b < clientsll; b++)
            valid[b] = true;
        for (b = 0; b < mininumber; b++) {
            //printf("already eliminated found as mini %3d", minifound[b]),
            valid[minifound[b]] = false;
        }
        for (i = 0; i < refl; i++)
            min[i] = 0xff;
        for (j = 0; j < refl; j++) {
            //printf("");
            // find minimal value at row j
            for (b = 0; b < clientsll; b++) {
                if (valid[b]) {
                    if (d[b * refl + j] < min[j]) {
                        min[j] = d[b * refl + j];
                    }
                }
            }
            //printf("row %3d minimum should be %3d ", j, min[j]);
            // eliminate all bigger values at row j
            int minifoundcount = 0;
            int minifounditem = -1;
            for (b = 0; b < clientsll; b++) {
                if (valid[b]) {
                    if (d[b * refl + j] != min[j]) {
                        valid[b] = false;
                        //printf("row %3d item %3d = %3d ", j, b, d[b * refl + j]);
                    } else {
                        //printf("row %3d item %3d = %3d mini found", j, b, min[j]);
                        minifoundcount++;
                        minifounditem = b;
                    }
                }
            }
            if (minifoundcount == 1) {
                //printf("\nfound minimum at item %3d", minifounditem);
                minifound[mininumber] = minifounditem;
                mininumber++;
                break;
            }
        }
    }
    *foundl = mininumber;
    if (mininumber > 0) {
        i = 0;
        b = 0;
        /*if (theresafullmatch != -1) {
            foundl = mininumber - 1;
            b = 1;
        }*/
        if (*foundl > 0) {
            *found = (unsigned int *) malloc(sizeof(unsigned int) * *foundl);
            while (b < mininumber) {
                *found[i] = minifound[b];
                //printf("%3d client %3d d ", b, *found[b]);
                //for (j = 0; j < refl; j++) {
                //    printf("%02x", d[*found[b] * refl + j]);
                //}
                //printf("");
                i++;
                b++;
            }
        }
    }

    free((unsigned char *) minifound);
    free((unsigned char *) min);
    free((bool *) valid);
    free((unsigned char *) d);
}


int check_repopulate_with_bootstrap() {
    int i, j, k, r, rc;
    FILE *fp;

    int bsl = 8192;
    unsigned char bs[bsl];
    fp = fopen("torbootstrap.txt", "rb");
    if (fp != NULL) {
        bsl = fread(bs, 1, bsl, fp);
        fclose(fp);
        extractpubkeys(bs, &bsl, bsl, (char *)(bs));
        printf("torbootstrap.txt bs read %d", bsl);
    } else {
        rc = getBootstrapNodes(bs, &bsl);
        printf("bs read %d", bsl);
    }
    if (bsl <= 0) {
        printf("check_repopulate_with_bootstrap error no bootstrap node found");
        return -1;
    }

    int rowl = HASHL + 2 + 2;
    int bsli = ((int) bsl) / (rowl);
    struct NodeInfo bootstrap;
    for (k = 0 ; k < bsli ; k++) {
        i = rand() % bsli;
        printf("bootstrap %2d / %2d ", i, bsli);
        bootstrap.thisisus = true;
        char pr[HASHL * 2];
        for (j = 0; j < HASHL; j++) {
            bootstrap.pub[j] = bs[i * rowl + j];
            snprintf(pr + j * 2, 512, "%02x", bs[i * rowl + j]);
            if (bootstrap.pub[j] != OURPUBKEY[j])
                bootstrap.thisisus = false;
        }
        bootstrap.porti = bs[i * rowl + HASHL + 1] + (bs[i * rowl + HASHL + 0] << 8);
        bootstrap.portf = bs[i * rowl + HASHL + 3] + (bs[i * rowl + HASHL + 2] << 8);
        if (bootstrap.thisisus) {
            printf("this is us don't pick it bootstrap %s ports %d %d", pr, bootstrap.porti, bootstrap.portf);
            continue;
        }
        printf("picked bootstrap %s ports %d %d", pr, bootstrap.porti, bootstrap.portf);
        break;
        //const char *p = "\x00\x00";
        //for (j = 0 ; j < HASHL; j++)
        //     bootstrap.pub[j] = (char) p[j];
        //bootstrap.porti = 8081;
        //bootstrap.portf = 8081;
    }

    i = find_client(bootstrap.pub, HASHL);
    if (i == -1) {
        i = add_client(bootstrap);
    }

    return i;
}

int find_node() {
    int i,j,k;
    int rc = 0;
    unsigned int *found;
    int foundl = 0;
    pthread_mutex_lock(&mutexclientinfo);
    find_closest_nodes(clientinfo[OURNODE].pub, clientinfo[OURNODE].publ, MAXCLIENTPERRANGE, &found, &foundl);
    if (foundl <= 0) {
        rc = -1;
    } else {
        time_t currenttime = time(NULL);
        for (i = 0 ; i < foundl ; i++) {
            if (currenttime - clientinfo[found[i]].timelastfindnode > DT_FIND_NODE
                && !clientinfo[found[i]].thisisus) {
                rc++;
                for (j = 0 ; j < THREADCLIENTL ; j++) {
                    if (COMMAND[j].status == status_isfree) {
                        printf("find_node() ask client at position %d last asked %lds ago",
                               found[i], currenttime - clientinfo[found[i]].timelastfindnode);
                        clientinfo[found[i]].timelastfindnode = currenttime;
                        struct AskResponse ar;
                        ar.query = true;
                        ar.q = dic_find_node;
                        ar.t = (unsigned char *) malloc(2);
                        ar.t[0] = rand() % 0xff;
                        ar.t[1] = rand() % 0xff;
                        ar.tl = 2;
                        ar.v = APPVERSION;
                        ar.vl = APPVERSIONL;
                        ar.id = clientinfo[OURNODE].pub;
                        ar.idl = clientinfo[OURNODE].publ;
                        ar.porti = clientinfo[OURNODE].porti;
                        ar.portf = clientinfo[OURNODE].portf;
                        ar.target = clientinfo[OURNODE].pub;
                        ar.targetl = clientinfo[OURNODE].publ;
                        for (k = 0 ; k < HASHL ; k++)
                            COMMAND[j].pub[k] = clientinfo[found[i]].pub[k];
                        COMMAND[j].ar = ar;
                        COMMAND[j].tasktype = task_type_find_node;
                        COMMAND[j].status = status_needswork;     // seulement en dernier
                        break;
                    }
                }
            }
        }
        free((unsigned int *) found);
    }
    pthread_mutex_unlock(&mutexclientinfo);
    return rc;
}

int get_peers(unsigned char *keywords_hash) {
    int i,j,k;
    int rc = 0;
    unsigned int *found;
    int foundl = 0;
    pthread_mutex_lock(&mutexclientinfo);
    find_closest_nodes(keywords_hash, HASHL, MAXCLIENTPERRANGE, &found, &foundl);
    if (foundl <= 0) {
        rc = -1;
    } else {
        time_t currenttime = time(NULL);
        for (i = 0; i < foundl; i++) {
            if (currenttime - clientinfo[found[i]].timelastgetpeers > DT_GET_PEERS
                || clientinfo[found[i]].timelastgetpeers < TIME_LAST_RESET_SEARCH) {// && !clientinfo[found[i]].thisisus) {
                rc++;
                for (j = 0; j < THREADCLIENTL; j++) {
                    if (COMMAND[j].status == status_isfree) {
                        printf("get_peers() ask client at position %d last asked %lds ago",
                               found[i], currenttime - clientinfo[found[i]].timelastgetpeers);
                        clientinfo[found[i]].timelastgetpeers = currenttime;
                        struct AskResponse ar;
                        ar.query = true;
                        ar.q = dic_get_peers;
                        ar.t = (unsigned char *) malloc(2);
                        ar.t[0] = rand() % 0xff;
                        ar.t[1] = rand() % 0xff;
                        ar.tl = 2;
                        ar.v = APPVERSION;
                        ar.vl = APPVERSIONL;
                        ar.id = clientinfo[OURNODE].pub;
                        ar.idl = clientinfo[OURNODE].publ;
                        ar.porti = clientinfo[OURNODE].porti;
                        ar.portf = clientinfo[OURNODE].portf;
                        ar.target = (unsigned char *) malloc(sizeof(unsigned char) * HASHL);
                        for (k = 0; k < HASHL; k++)
                            ar.target[k] = keywords_hash[k];
                        ar.targetl = HASHL;
                        for (k = 0 ; k < HASHL ; k++)
                            COMMAND[j].pub[k] = clientinfo[found[i]].pub[k];
                        COMMAND[j].ar = ar;
                        COMMAND[j].tasktype = task_type_get_peers;
                        COMMAND[j].status = status_needswork;     // seulement en dernier
                        break;
                    }
                }
            }
        }
    }
    pthread_mutex_unlock(&mutexclientinfo);
    return rc;
}

int announce_peer(unsigned char *keywords_hash) {
    int i,j,k;
    int rc = 0;
    unsigned int *found;
    int foundl = 0;
    pthread_mutex_lock(&mutexclientinfo);
    find_closest_nodes(keywords_hash, HASHL, MAXCLIENTPERRANGE, &found, &foundl);
    if (foundl <= 0) {
        rc = -1;
    } else {
        time_t currenttime = time(NULL);
        for (i = 0; i < foundl; i++) {
            if (currenttime - clientinfo[found[i]].timelastannouncepeer > DT_ANNOUNCE_PEER
                || clientinfo[found[i]].timelastannouncepeer < TIME_LAST_RESET_SEARCH) {// && !clientinfo[found[i]].thisisus) {
                rc++;
                for (j = 0; j < THREADCLIENTL; j++) {
                    if (COMMAND[j].status == status_isfree) {
                        printf("announce_peer() ask client at position %d last asked %lds ago",
                               found[i], currenttime - clientinfo[found[i]].timelastannouncepeer);
                        clientinfo[found[i]].timelastannouncepeer = currenttime;
                        struct AskResponse ar;
                        ar.query = true;
                        ar.q = dic_announce_peer;
                        ar.t = (unsigned char *) malloc(2);
                        ar.t[0] = rand() % 0xff;
                        ar.t[1] = rand() % 0xff;
                        ar.tl = 2;
                        ar.v = APPVERSION;
                        ar.vl = APPVERSIONL;
                        ar.id = clientinfo[OURNODE].pub;
                        ar.idl = clientinfo[OURNODE].publ;
                        ar.porti = clientinfo[OURNODE].porti;
                        ar.portf = clientinfo[OURNODE].portf;
                        ar.target = (unsigned char *) malloc(sizeof(unsigned char) * HASHL);
                        for (k = 0; k < HASHL; k++)
                            ar.target[k] = keywords_hash[k];
                        ar.targetl = HASHL;
                        for (k = 0 ; k < HASHL ; k++)
                            COMMAND[j].pub[k] = clientinfo[found[i]].pub[k];
                        COMMAND[j].ar = ar;
                        COMMAND[j].tasktype = task_type_announce_peer;
                        COMMAND[j].status = status_needswork;     // seulement en dernier
                        break;
                    }
                }
            }
        }
    }
    pthread_mutex_unlock(&mutexclientinfo);
    return rc;
}

int download(unsigned char *keywords_hash) {
    int i,j,k;
    int rc = 0;
    pthread_mutex_lock(&mutexclientinfo);
    pthread_mutex_lock(&infohashmutex);
    int found = find_hash(keywords_hash, HASHL);
    if (found < 0) {
        rc = -1;
    } else {
        if (hashinfo[found].peerlistl > 0) {
            rc++;
            i = 0;
            for (j = 0; j < THREADCLIENTL; j++) {
                if (COMMAND[j].status == status_isfree) {
                    printf("download() ask peer");
                    struct AskResponse ar;
                    ar.query = true;
                    ar.q = dic_download;
                    ar.t = (unsigned char *) malloc(2);
                    ar.t[0] = rand() % 0xff;
                    ar.t[1] = rand() % 0xff;
                    ar.tl = 2;
                    ar.v = APPVERSION;
                    ar.vl = APPVERSIONL;
                    ar.id = clientinfo[OURNODE].pub;
                    ar.idl = clientinfo[OURNODE].publ;
                    ar.porti = clientinfo[OURNODE].porti;
                    ar.portf = clientinfo[OURNODE].portf;
                    ar.target = (unsigned char *) malloc(sizeof(unsigned char) * HASHL);
                    for (k = 0; k < HASHL; k++)
                        ar.target[k] = keywords_hash[k];
                    ar.targetl = HASHL;
                    for (k = 0 ; k < HASHL ; k++)
                        COMMAND[j].pub[k] = hashinfo[found].peerlist[i].pub[k];
                    COMMAND[j].ar = ar;
                    COMMAND[j].tasktype = task_type_download;
                    COMMAND[j].status = status_needswork;     // seulement en dernier
                    break;
                }
            }
        }
    }
    pthread_mutex_unlock(&infohashmutex);
    pthread_mutex_unlock(&mutexclientinfo);
    return rc;
}