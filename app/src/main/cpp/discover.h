#ifndef PHOTOCHIOTTE_DISCOVER_H
#define PHOTOCHIOTTE_DISCOVER_H

#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/types.h>
#include <netdb.h>
#include <errno.h>
#include <err.h>
#include <stdarg.h>
#include <sys/time.h>
#include <fcntl.h>
#include <pthread.h>
#include <dirent.h>
#include <sys/stat.h>

#include "lzma.h"
#include "curl/curl.h"

#include "openssl/evp.h"
#include "openssl/pem.h"

#include <stdbool.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <poll.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/socket.h>
#include <sys/un.h>

#ifdef __clang__

#include <jni.h>
#include <android/log.h>

#include "feature/api/tor_api.h"
#include "feature/api/tor_api_internal.h"
#include "app/main/shutdown.h"
#include "orconfig.h"
#include "lib/malloc/malloc.h"

#define LOG_TAG "YYYdis"
#define printf(...) __android_log_print(ANDROID_LOG_VERBOSE, LOG_TAG, __VA_ARGS__)

#else

#define printf(...) (printf(__VA_ARGS__),printf("\n"))

#endif

/*


curl cmake tor make openssl gcc


cd /home/user/Android/StudioProjects/PhotoChiotte/app/src/main/cpp


cmake .. && make && ./discover $PWD/tor1/ 9053 8003 8003 8083 8083


 */

/**
 *    static const variables
 */

#define THREADSERVERL 1
#define THREADCLIENTL 1
#define CLISNDTIMEOUT 6
#define SRVRCVTIMEOUT 6
#define SRVSNDTIMEOUT 6
#define CLIRCVTIMEOUT 12

#define MAXCLIENTPERRANGE 2
#define HASHL 32
#define MAXCLIENTS 1024
#define MAXHASHES 1024
#define MAXRANGES 1024
#define CHUNKSIZE 2048
#define MAXMSGSIZE 65536

#define DEBUGONLY true

pthread_mutex_t debugwritetodiskmutex = PTHREAD_MUTEX_INITIALIZER;

static int TORSOCKSPORT        = 9053;
static int TORCLIENTPORT       = 8083;
static int TORSERVERPORT       = 8003;
static char PATHCONTROLSOCKET[256];
static const char *BOOTSTRAPS          = NULL;
static const char *PATHPREFIX          = NULL;
static const char *PATHPREFIXCACHE     = "cache/";
static const char *PATHPREFIXTOR       = "files/";
static const char *PATHPREFIXTORDATA   = "files/data/";
static const char *PATHPREFIXTORHIDDEN = "files/hidden_service3/";

static const int DT_FIND_NODE = 60 * 3;
static const int DT_GET_PEERS = 60 * 3;
static const int DT_ANNOUNCE_PEER = 60 * 5;
static const int DT_RE_COLLECTION_CREATE_FROM_SOURCE = 60 * 15;
static const int DT_RE_COLLECTION_ANNOUNCE = 60 * 5;
static const int DT_REMOVE_PEER_FAILED = 60 * 20;

static unsigned char APPVERSION[] = "PC\x02\x01";
static const int APPVERSIONL = 4;

/**
 *    communication header
 */
// keys of size 1 byte
static const unsigned char KEY_SIZE = 1; static const unsigned int KEY_SIZE_L = 1;

// key   : size of the whole socket send, integer big endian message size + integer little endian message size
static const unsigned char KEY_COM_SIZE_BELE = '\x00'; static const int KEY_COM_SIZE_BELE_L = 4 + 4;

// key   : type of message that will follow
static const unsigned char KEY_TYPE_OF_MESSAGE = '\x01'; static const unsigned int KEY_TYPE_OF_MESSAGE_L = 1;
//       - bencoded message     xz encoded with pubkey
static const unsigned char TYPE_BENCODED_XZPE = '\x00';
//       - collection           xz encoded with pubkey
static const unsigned char TYPE_COLLECTION_XZPE = '\x01';
//       - data                 xz encoded with pubkey
static const unsigned char TYPE_DATA_XZPE = '\x02';

// keys   : chunk info
static const unsigned char KEY_ALLCHUNKS_SIZE = '\x02';
static const unsigned char KEY_CHUNK_START = '\x03';
static const unsigned char KEY_CHUNK_END = '\x04';
static const unsigned char KEY_BITFIELD_SIZE = '\x05';
static const unsigned char KEY_BITFIELD_HAVE = '\x06';
static const unsigned char KEY_BITFIELD_WANT = '\x07';

// key   : chunk will follow
static const unsigned char KEY_CHUNK = '\x08';

/**
 *    common basic structures
 */
enum tor_signal_name {
    signal_getinfo_version,
    signal_getinfo_circuitestablished,
    signal_getinfo_circuitstatus,
    signal_shutdown,
};
enum Dic_Misc_Enum {
    dic_none,
    dic_t,             // transaction id
    dic_v,             // app id & version
    dic_y,             // query or response
    dic_q,             // query name : find_node, ping, get_peers, announce_peer
    dic_find_node,     //
    dic_ping,          //
    dic_get_peers,     //
    dic_announce_peer, //
    dic_id,            // id
    dic_ports,         // id reachable ports
    dic_target,        // find_node query    find this target
    dic_nodes,         // find_node response list of nodes found
    dic_peers,         // get_peers response list of peers found for the target hashinfo
    dic_download,      // not in the DHT, but in the collection sharing
};
struct AskResponse {
    bool query;                                  // query or response
    int q;                                       // query name find_node,
    unsigned char *t; int tl;                    // transaction id
    unsigned char *v;  int vl;                   // app id & version
    unsigned char *id;  int idl;                 // id
    int porti;  int portf;                       // id reachable ports
    unsigned char *target;  int targetl;         // find_node query or get_peers keyword search
    unsigned char *targethashof; int targethashofl; bool targethashofpubprefix;
    unsigned char *nodes;  int nodesl;           // find_node response
    unsigned char *peers;  int peersl;           // get_peers response
};
struct NodeInfo {
    time_t timeadded;
    time_t timelastfindnode;
    time_t timelastgetpeers;
    time_t timelastannouncepeer;
    time_t timelastsuccessfulcomm;
    int commfailed;
    int commsuccess;
    bool thisisus;
    int range;
    int publ;
    unsigned char pub[HASHL];
    int porti;
    int portf;
    int onionl;
    char onion[256];
};
struct PeerInfo {
    unsigned char pub[HASHL + 2 + 2]; unsigned int publ;
    time_t timeadded;
};
struct HashInfo {

    // we have the pselsp, announce us to other nodes
    char filename[256]; int filenamel;
    time_t lastannouncedkeywords;
    time_t lastannouncedpubkeywords;
    // hash of psels file only to detect changes
    unsigned char filehash[HASHL]; unsigned int filehashl;

    // keywords hash for searches
    unsigned char keywordshash[HASHL]; unsigned int keywordshashl;
    // publisher+keywords hash for metadata retrieval
    unsigned char publisherkeywordshash[HASHL]; unsigned int publisherkeywordshashl;

    // header :
    // - signature
    // - publisher
    // - keywords
    // - version
    unsigned char pub[HASHL]; unsigned int publ;
    unsigned char printname[256]; unsigned int printnamel;
    unsigned char keywords[256]; unsigned int keywordsl;
    unsigned int version;

    // collection :
    // - signature
    // - publisher
    // - keywords
    // - version
    // - hash
    // - hashes hashblocks

    struct PeerInfo *peerlist;
    int peerlistl;
};
struct ThreadServerData {
    int server_fd_id;
    struct sockaddr_in address;
    int addrlen;
};
struct ThreadServerPortData {
    int id;
    int port;
};
int *server_fd;
enum threadclientstatus {
    status_isfree,
    status_needswork,
    status_isworking,
    status_finished,
};
enum threadclienttasktype {
    task_type_idle,
    task_type_find_node,
    task_type_get_peers,
    task_type_announce_peer,
    task_type_download,
};
struct ThreadClientData {
    int status;
    int tasktype;
    unsigned char pub[HASHL + 2 + 2];
    struct AskResponse ar;
    int rc;
};
/**
 *    must have
 */
unsigned char PRIV[128];
int PRIVL = 128;
/**
 *    global dynamic variables
 */
pthread_mutex_t mutexclientinfo = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t servermutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t clientmutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t infohashmutex = PTHREAD_MUTEX_INITIALIZER;
/**
 *    all ranges
 */
static int rangesl = 0;
static unsigned char ranges[HASHL * sizeof(unsigned char) * MAXRANGES];
static unsigned int rangesclientscount[MAXRANGES];
/**
 *    all nodes
 */
static int clientsl = 0;
static unsigned char clients[HASHL * sizeof(unsigned short) * MAXCLIENTS];
struct NodeInfo clientinfo[MAXCLIENTS];
static int OURNODE = -1;
/**
 *    all peers
 */
static int hashesl = 0;
static unsigned char hashes[HASHL * sizeof(unsigned short) * MAXHASHES];
struct HashInfo hashinfo[MAXHASHES];
/**
 *    global dynamic thread shared variables
 */
struct ThreadClientData COMMAND[THREADCLIENTL];
static bool FINISHED;

static time_t TIME_LAST_RESET_SEARCH = 0;
static int TASK_SEARCH_KEYWORDS = 0;
static char DOSSIERDESSIN[256];
static char SEARCH_KEYWORDS[256];
static unsigned char SEARCH_KEYWORDS_HASH[HASHL];

static time_t LASTTIME_RE_COLLECTION_CREATE_FROM_SOURCE = 0;
/**
 *    android retrieve discover stats
 */
static unsigned char OURPUBKEY[HASHL];
static int STATUS_GET_PEERS_SUCCESS = 0;
static int STATUS_GET_PEERS_FAILED = 0;
static int STATUS_FIND_OUR_POSITION_FAILED = 0;
static int STATUS_FIND_OUR_POSITION_SUCCESS = 0;
static int STATUS_ANNOUNCE_PEER_FAILED = 0;
static int STATUS_ANNOUNCE_PEER_SUCCESS = 0;
static int STATUS_DOWNLOAD_FAILED = 0;
static int STATUS_DOWNLOAD_SUCCESS = 0;



void remove_client(int);
int find_client(unsigned char *, int);
int server(int, struct sockaddr_in, int);
int TCPclient(unsigned char *, struct AskResponse);



static void dumpbufx(const char *title, unsigned char *buf, int len) {
  if (DEBUGONLY) {
    int i;
    char o[len * 2 + 128];
    int l = 0;
    l += snprintf(o + l, 256, "%s (%d) ", title, len);
    for (i = 0; i < len; i++)
        l += snprintf(o + l, 256, "%02x", buf[i]);
    //l += snprintf(o + l, 256, "\"");
    printf("%s", o);
  }
}
static void dumpbufc(const char *title, char *buf, int len) {
  if (DEBUGONLY) {
    int i;
    char o[len * 2 + 128];
    int l = 0;
    l += snprintf(o + l, 256, "%s(%d)<", title, len);
    for (i = 0; i < len; i++) {
        if (buf[i] == ':' || buf[i] == '_' || buf[i] == '-'
        ||  buf[i] == '.' ||  buf[i] == ' ' ||  buf[i] == '/'
        ||  buf[i] == '+' ||  buf[i] == '='
        || ('a' <= buf[i] && buf[i] <= 'z')
        || ('A' <= buf[i] && buf[i] <= 'Z')
        || ('0' <= buf[i] && buf[i] <= '9')) {
            l += snprintf(o + l, 256, "%c", (unsigned char) buf[i]);
        } else {
            l += snprintf(o + l, 256, "\\x%02x", (unsigned char) buf[i]);
        }
    }
    l += snprintf(o + l, 256, ">");
    printf("%s", o);
  }
}
static void dumpbufuc(const char *title, unsigned char *buf, int len) {
  if (DEBUGONLY) {
    dumpbufc(title, (char *) buf, len);
  }
}
void dumpclients(){
  if (DEBUGONLY) {
    pthread_mutex_lock(&debugwritetodiskmutex);
    int i, j, r;
    FILE *op;
    char oname[256];
    snprintf(oname, sizeof(oname), "%s%s/clients.status", PATHPREFIX, PATHPREFIXTOR);
    op = fopen(oname, "w");
    if (op == NULL) {
      printf("  [c]  could not write %s", oname);
    } else {
      for (r = 0; r < rangesl; r++) {
        if (rangesclientscount[r] > 0) {

          fprintf(op, "____");
          for (j = 0; j < HASHL; j++) {
            fprintf(op, "%02x", ranges[HASHL * r + j]);
          }
          fprintf(op, "____\n");

          for (i = 0; i < clientsl; i++) {
            if (clientinfo[i].range == r) {
              fprintf(op, "%3d ", i);
              for (j = 0; j < HASHL; j++) {
                fprintf(op, "%02x", clientinfo[i].pub[j]);
              }
              fprintf(op, " %d/%d", rangesclientscount[clientinfo[i].range], MAXCLIENTPERRANGE);
              if (clientinfo[i].thisisus)
                fprintf(op, " <-- us\n");
              else
                fprintf(op, "\n");
            }
          }

          fprintf(op, "____");
          for (j = 0; j < HASHL; j++) {
            fprintf(op, "%02x", ranges[HASHL * (r + 1) + j]);
          }
          fprintf(op, "____\n");

        }
      }
      fprintf(op, "\n");
      fclose(op);
    }
    pthread_mutex_unlock(&debugwritetodiskmutex);
  }
}

void write_int_to_file(int l, FILE *op) {
    fputc((unsigned char) ((l >> 24) & 0xff), op);
    fputc((unsigned char) ((l >> 16) & 0xff), op);
    fputc((unsigned char) ((l >> 8) & 0xff), op);
    fputc((unsigned char) ((l) & 0xff), op);
}
void write_int_string_to_fileuc(unsigned char *s, int l, FILE *op) {
    write_int_to_file(l, op);
    fwrite(s, 1, l,  op);
}
void write_int_string_to_filec(char *s, int l, FILE *op) {
    write_int_to_file(l, op);
    fwrite(s, 1, l,  op);
}
bool foundstring(unsigned char *buf, int a, const char *to){
    int i;
    int tol = strlen(to);
    for (i = 0 ; i < tol ; i++) {
        if (buf[a + i] != to[i])
            return false;
    }
    return true;
}

#endif //PHOTOCHIOTTE_DISCOVER_H






















