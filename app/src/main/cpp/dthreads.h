
void *thread_server(void *threadarg) {
    struct ThreadServerData *my_data;
    my_data = (struct ThreadServerData *) threadarg;

    printf("starting thread_server listening");
    int rc = server(my_data->server_fd_id, my_data->address, my_data->addrlen);
    printf("thread_server return code %d", rc);

    pthread_exit(NULL);
}

int server_port(int port, int id) {

    // create socket
    server_fd[id] = socket(AF_INET, SOCK_STREAM, 0);
    if (server_fd[id] < 0)
        return -1;
    int opt = 1;
    if (setsockopt(server_fd[id], SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)) < 0)
        return -2;
    struct sockaddr_in address;
    int addrlen = sizeof(address);
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(port);
    // bind
    if (bind(server_fd[id], (struct sockaddr *) &address, sizeof(address)) < 0)
        return -3;
    // listen
    if (listen(server_fd[id], 40) < 0)
        return -4;

    /*struct timeval timeout;
    timeout.tv_sec = 7;
    timeout.tv_usec = 0;*/
    //if (setsockopt (server_fd, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout)) < 0) return -7;
    //if (setsockopt (server_fd, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout, sizeof(timeout)) < 0) return -8;

    int i, rc;
    struct ThreadServerData td[THREADSERVERL];
    pthread_t thread[THREADSERVERL];
    pthread_attr_t attr;
    void *status;

    // Initialize and set thread joinable
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

    for (i = 0; i < THREADSERVERL; i++) {
        td[i].server_fd_id = id;
        td[i].address = address;
        td[i].addrlen = addrlen;
        rc = pthread_create(&thread[i], &attr, thread_server, (void *) &td[i]);
        if (rc) {
            printf("unable to create thread %d", i);
        }
    }

    // free attribute and wait for the other threads
    pthread_attr_destroy(&attr);
    for (i = 0; i < THREADSERVERL; i++) {
        rc = pthread_join(thread[i], &status);
        if (rc) {
            printf("unable to join thread %d", i);
        }
        printf("exiting thread %d with status %ld", i, (long) ((int *) status));
    }

    close(server_fd[id]);
    return 0;
}

void *thread_server_port(void *threadarg) {
    struct ThreadServerPortData *my_data;
    my_data = (struct ThreadServerPortData *) threadarg;

    printf("starting thread_server_port listening on port %d", my_data->port);
    int rc = server_port(my_data->port, my_data->id);
    printf("thread_server_port %d return code %d", my_data->port, rc);

    printf("all thread_server_port exited");
    pthread_exit(NULL);
}

void *thread_client(void *threadarg) {
    int i, j;
    int cmd = -1;
    struct ThreadClientData *my_data;
    my_data = (struct ThreadClientData *) threadarg;

    while (!FINISHED) {

        pthread_mutex_lock(&clientmutex);
        cmd = -1;
        while (cmd == -1 && !FINISHED) {
            usleep(1000000);
            for (i = 0; i < THREADCLIENTL && cmd == -1; i++)
                if (COMMAND[i].status == status_needswork)
                    cmd = i;
        }
        COMMAND[cmd].status = status_isworking;
        pthread_mutex_unlock(&clientmutex);

        if (FINISHED)
            break;

        printf("  [c]   + TCPclient start status_isworking tasktype %d", COMMAND[cmd].tasktype);
        COMMAND[cmd].rc = TCPclient(COMMAND[cmd].pub, COMMAND[cmd].ar);

        pthread_mutex_lock(&mutexclientinfo);
        int foundat = find_client(COMMAND[cmd].pub, HASHL);
        if (foundat >= 0) {
            time_t currenttime = time(NULL);

            if (COMMAND[cmd].tasktype == task_type_find_node) {

                if (COMMAND[cmd].rc < -3) {
                    clientinfo[foundat].commfailed++;
                    clientinfo[foundat].timelastfindnode = 0;
                    STATUS_FIND_OUR_POSITION_FAILED++;
                    printf("task_type_find_node failed");
                } else if (COMMAND[cmd].rc < 0) {
                    clientinfo[foundat].timelastfindnode = 0;
                    STATUS_FIND_OUR_POSITION_FAILED++;
                    printf("task_type_find_node failed problem on our part no internet connection");
                } else if (COMMAND[cmd].rc == 0) {
                    clientinfo[foundat].commsuccess++;
                    clientinfo[foundat].timelastsuccessfulcomm = currenttime;
                    clientinfo[foundat].timelastfindnode = currenttime;
                    clientinfo[foundat].commfailed = 0;
                    STATUS_FIND_OUR_POSITION_SUCCESS++;
                    printf("task_type_find_node success but we didn't receive new nodes");
                } else {
                    clientinfo[foundat].commsuccess++;
                    clientinfo[foundat].timelastsuccessfulcomm = currenttime;
                    clientinfo[foundat].timelastfindnode = currenttime;
                    clientinfo[foundat].commfailed = 0;
                    STATUS_FIND_OUR_POSITION_SUCCESS++;
                    printf("task_type_find_node found %d new nodes retry and ask them", COMMAND[cmd].rc);
                }

            } else if (COMMAND[cmd].tasktype == task_type_get_peers) {

                if (COMMAND[cmd].rc < -3) {
                    clientinfo[foundat].commfailed++;
                    clientinfo[foundat].timelastgetpeers = 0;
                    STATUS_GET_PEERS_FAILED++;
                    printf("task_type_get_peers failed");
                } else if (COMMAND[cmd].rc < 0) {
                    clientinfo[foundat].timelastgetpeers = 0;
                    STATUS_GET_PEERS_FAILED++;
                    printf("task_type_get_peers failed problem on our part no internet connection");
                } else {
                    clientinfo[foundat].commsuccess++;
                    clientinfo[foundat].timelastsuccessfulcomm = currenttime;
                    clientinfo[foundat].timelastgetpeers = currenttime;
                    clientinfo[foundat].commfailed = 0;
                    STATUS_GET_PEERS_SUCCESS++;
                    printf("task_type_get_peers successful");
                }

            } else if (COMMAND[cmd].tasktype == task_type_announce_peer) {

                if (COMMAND[cmd].rc < -3) {
                    clientinfo[foundat].commfailed++;
                    clientinfo[foundat].timelastannouncepeer = 0;
                    STATUS_ANNOUNCE_PEER_FAILED++;
                    printf("task_type_announce_peer failed");
                } else if (COMMAND[cmd].rc < 0) {
                    clientinfo[foundat].timelastannouncepeer = 0;
                    STATUS_ANNOUNCE_PEER_FAILED++;
                    printf("task_type_announce_peer problem on our part no internet connection");
                } else {
                    clientinfo[foundat].commsuccess++;
                    clientinfo[foundat].timelastsuccessfulcomm = currenttime;
                    clientinfo[foundat].timelastannouncepeer = currenttime;
                    clientinfo[foundat].commfailed = 0;
                    STATUS_ANNOUNCE_PEER_SUCCESS++;
                    printf("task_type_announce_peer successful");
                }

            } else if (COMMAND[cmd].tasktype == task_type_download) {

                if (COMMAND[cmd].rc < -3) {
                    clientinfo[foundat].commfailed++;
                    clientinfo[foundat].timelastannouncepeer = 0;
                    STATUS_DOWNLOAD_FAILED++;
                    printf("task_type_download failed");
                } else if (COMMAND[cmd].rc < 0) {
                    clientinfo[foundat].timelastannouncepeer = 0;
                    STATUS_DOWNLOAD_FAILED++;
                    printf("task_type_download problem on our part no internet connection");
                } else {
                    clientinfo[foundat].commsuccess++;
                    clientinfo[foundat].timelastsuccessfulcomm = currenttime;
                    clientinfo[foundat].timelastannouncepeer = currenttime;
                    clientinfo[foundat].commfailed = 0;
                    STATUS_DOWNLOAD_SUCCESS++;
                    printf("task_type_download successful");
                }

            }

            if (clientinfo[foundat].commfailed > 30 && currenttime - clientinfo[foundat].timelastsuccessfulcomm > DT_REMOVE_PEER_FAILED) {
                printf("check connectivity, do not remove ourself from the table");
                remove_client(foundat);
            }

        }
        pthread_mutex_unlock(&mutexclientinfo);
        printf("  [c]   - TCPclient finished rc = %d with client %d", COMMAND[cmd].rc, foundat);

        //COMMAND[cmd].status = status_finished;
        COMMAND[cmd].status = status_isfree;

    }

    printf("all thread_clients exited");
    pthread_exit(NULL);

}






















