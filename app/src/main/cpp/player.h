#include <jni.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <locale.h>
#include <sstream>
#include <stddef.h>
#include <time.h>
#include <chrono>

#include "mpv/client.h"
#include "mpv/stream_cb.h"

#include <android/log.h>
#define LOG_TAG "YYYplay"
#define ALOGV(...) __android_log_print(ANDROID_LOG_VERBOSE, LOG_TAG, __VA_ARGS__)


extern "C" {

    #include "libavcodec/jni.h"

}


#define INTANCEARRAYSIZE 20

#ifndef PHOTOCHIOTTE_CONTAINER_H
#define PHOTOCHIOTTE_CONTAINER_H

struct singleinstance {
    bool wasactive = false;
    mpv_handle *mpv = NULL;
    jobject surface = NULL;
    char *vidbuffer = NULL;
    jint vidbuffersize = 0;
    jint vidbufferend = 0;
    jint vidbufferreaduntil = 0;
};

#endif //PHOTOCHIOTTE_CONTAINER_H

























