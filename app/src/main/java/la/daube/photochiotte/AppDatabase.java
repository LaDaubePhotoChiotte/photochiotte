package la.daube.photochiotte;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {Fichier.class, Dossier.class}, version = 9, exportSchema = false) // 8
public abstract class AppDatabase extends RoomDatabase {
  public abstract DBFichier dbFichier();
  public abstract DBDossier dbDossier();
}

