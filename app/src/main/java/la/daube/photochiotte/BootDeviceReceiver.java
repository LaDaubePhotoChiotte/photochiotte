package la.daube.photochiotte;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;

import androidx.preference.PreferenceManager;

public class BootDeviceReceiver extends BroadcastReceiver {
    private static final String TAG = "YYYboo";

    @Override
    public void onReceive(final Context context, Intent intentRecieve) {
        llog.d(TAG, "onReceive BootCompleted");
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        boolean optionaudioplayeractive = preferences.getBoolean("playmusic", false);
        if (optionaudioplayeractive) {
            llog.d(TAG, "optionaudioplayeractive " + optionaudioplayeractive);
        }

        boolean ThreadDiscoverActive = preferences.getBoolean("ThreadDiscoverActive", false);
        if (ThreadDiscoverActive) {
            llog.d(TAG, "ThreadDiscoverActive " + optionaudioplayeractive);
        }

        if (optionaudioplayeractive || ThreadDiscoverActive) {
            Intent intent = new Intent(context, backgroundService.class);
            intent.putExtra("BootDeviceReceiver", true);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                context.startForegroundService(intent);
            else
                context.startService(intent);
        }


    }

}







