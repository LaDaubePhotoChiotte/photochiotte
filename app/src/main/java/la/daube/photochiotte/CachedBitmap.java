package la.daube.photochiotte;

import android.graphics.Bitmap;

import java.util.ArrayList;

public class CachedBitmap {
  Bitmap bitmap;
  float width;
  float height;
  int bitmapcurrent;
  int bitmapcount;
  boolean bitmapaskednext;
  public CachedBitmap(Bitmap mbitmap,
                      float mwidth, float mheight,
                      int mbitmapcurrent, int mbitmapcount
  ){
    bitmap = mbitmap;
    width = mwidth;
    height = mheight;
    bitmapcurrent = mbitmapcurrent;
    bitmapcount = mbitmapcount;
    bitmapaskednext = false;
  }
}














