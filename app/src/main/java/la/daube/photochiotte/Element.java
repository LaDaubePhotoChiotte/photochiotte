package la.daube.photochiotte;

public class Element { // 4+

    public static final byte KEY_LENGTH = 1; // size of each key ELEMENT_LENGTH, ELEMENT_ADD, ELEMENT_TYPE written : 1 = 1 byte for the key, no more than 256 different keys are possible
    public static final int ELEMENT_LENGTH = 1; // element entry size written on disc 4 BE

    public boolean add = true; public static final int ELEMENT_ADD = 2;

    public int type = -1; public static final int ELEMENT_TYPE = 3; // must be initialized to -1
    public static final int element_media = 0;
    public static final int element_ordner = 1;
    public static final int element_collection = 2;

    /** is a media                                                 **/
    public Media media = null;

    /** is an ordner                                                  **/
    public Ordner ordner = null;

    /** is a collection that will insert/remove elements whenever loaded     **/
    public Collection collection = null;

    public Element() {
    }
    public Element(boolean _add, int _type) {
        add = _add;
        type = _type;
        if (type == element_media) {
            media = new Media();
        } else if (type == element_collection) {
            collection = new Collection();
        } else if (type == element_ordner) {
            ordner = new Ordner();
        }
    }

}
