package la.daube.photochiotte;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentDrawing.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentDrawing#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentDrawing extends Fragment {
  private static final String ARG_PARAM1 = "param1";
  private static final String ARG_PARAM2 = "param2";
  private static final String TAG = "YYYfd";

  private Gallery model;

  public int myid = -1;
  private Surf mysurf;
  private String cefichier = "";

  public FragmentDrawing() {
    // Required empty public constructor
  }

  public static FragmentDrawing newInstance(String param1, String param2) {
    FragmentDrawing fragment = new FragmentDrawing();
    Bundle args = new Bundle();
    args.putString(ARG_PARAM1, param1);
    args.putString(ARG_PARAM2, param2);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (getArguments() != null) {
      myid = getArguments().getInt("myid", -1);
      cefichier = getArguments().getString("cefichier", "");
    }
    llog.d(TAG, myid+" onCreate()");
    //model = ViewModelProviders.of(getActivity()).get(myViewModel.class);
    model = new ViewModelProvider(requireActivity()).get(Gallery.class);
    model.currentselectedfragment = myid;
  }

  @Override
  public void onResume() {
    super.onResume();
    llog.d(TAG, myid+" onResume()");
  }

  @Override
  public void onStart() {
    super.onStart();
    llog.d(TAG, myid+" onStart()");
  }

  @Override
  public void onPause() {
    super.onPause();
    llog.d(TAG, myid+" onPause()");
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    llog.d(TAG, myid+" onDestroy()");
    if (mysurf != null) {
      if (mysurf.drawingSurfaceHolder != null) {
        Surface surface = mysurf.drawingSurfaceHolder.getSurface();
        if (surface != null) {
          if (surface.isValid()) {
            surface.release();
          }
        }
        if (mycallback != null) {
          mysurf.drawingSurfaceHolder.removeCallback(mycallback);
        }
        mysurf.drawingSurfaceHolder = null;
      }
    }
  }
  
  private void setallthepaints(){

    mysurf.GraphWidth = model.GraphWidth;
    if (mysurf.GraphWidth > Gallery.graphwidthmax * mysurf.mywidth && !model.iswatch) {
      mysurf.GraphWidth = (int)(Gallery.graphwidthmax * mysurf.mywidth);
    }
    mysurf.SettingsWidth = model.SettingsWidth;
    if (mysurf.SettingsWidth > Gallery.settingswidthmax * mysurf.mywidth && !model.iswatch) {
      mysurf.SettingsWidth = (int)(Gallery.settingswidthmax * mysurf.mywidth);
    }

    mysurf.SettingsXmin = mysurf.mywidth - mysurf.SettingsWidth;
    mysurf.SettingsYmax = model.GenericCaseH;

    mysurf.CaseInvisibleW = (int) (model.GenericCaseH * 1.50f);
    mysurf.CaseInvisiblePrecXmax = mysurf.CaseInvisibleW;
    mysurf.CaseInvisibleSuivXmin = mysurf.mywidth - mysurf.CaseInvisibleW;
    mysurf.CaseInvisiblePrecSuivYmin = mysurf.myheight - mysurf.CaseInvisibleW;
    mysurf.CaseInvisibleOptionXmax = mysurf.CaseInvisibleW;
    mysurf.CaseInvisibleOptionYmax = mysurf.CaseInvisibleW;
  }

  private final SurfaceHolder.Callback mycallback = new SurfaceHolder.Callback() {
    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
      llog.d(TAG, myid+" surfaceCreated()");
      mysurf.drawingSurfaceHolder = surfaceHolder;
      if (mysurf.drawingSurfaceHolder != null)
        mysurf.drawingSurfaceHolder.setSizeFromLayout();
      mysurf.foldoptions();
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
      llog.d(TAG, myid+" surface surfacechanged() : "+i1 +"x"+i2 + " mywidth " + mysurf.mywidth + "x" + mysurf.myheight + " ScreenWidth " + mysurf.ScreenWidth + "x" + mysurf.ScreenHeight + " bigScreenWidth " + model.bigScreenWidth + "x" + model.bigScreenHeight);
      if (i1 != mysurf.mywidth || i2 != mysurf.myheight) {
        llog.d(TAG, myid+" surfacechanged() : different sizes need to modify surface !");
      }
      if (mysurf.mywidth != mysurf.ScreenWidth || mysurf.myheight != mysurf.ScreenHeight) {
        llog.d(TAG, myid+" surfacechanged() different size from beforee " + i1 + " " + i2);
        mysurf.ScreenWidth = mysurf.mywidth;
        mysurf.ScreenHeight = mysurf.myheight;
      }
      setallthepaints();
      mysurf.mysurfacestopdrawing = false;
      mysurf.mysurfaceisdestroyed = false;
      try {
        model.commandethreaddrawing.put(new String[]{String.valueOf(myid), "surfacechanged"});
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
      llog.d(TAG, myid+" surfaceDestroyed()");
      mysurf.putbigpictureinmemory = true;
      mysurf.centeronscreen = true;
      mysurf.mysurfacestopdrawing = true;
      while (mysurf.surfaceIsCurrentlyDrawing) {
        try {
          Thread.sleep(10);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
      mysurf.mysurfaceisdestroyed = true;
    }
  };

  private final View.OnTouchListener ontouchlistener = new View.OnTouchListener() {
    float pos0ix = 0.0f;
    float pos0iy = 0.0f;
    float pos0fx = 0.0f;
    float pos0fy = 0.0f;
    float pos1ix = 0.0f;
    float pos1iy = 0.0f;
    float pos1fx = 0.0f;
    float pos1fy = 0.0f;
    float delta10ix = 0.0f;
    float delta10iy = 0.0f;
    float delta10isqr = 0.0f;
    float bpxi = 0.0f;
    float bpyi = 0.0f;
    float bscalei = 0.0f;
    float sizemin = 0.0f;
    float sizemax = 0.0f;
    float selectedtraceresizeratio = 1.0f;
    float thumbsizei = 0.0f;
    float bscalecx = 0.0f;
    float bscalecy = 0.0f;
    float posmix = 0.0f;
    float posmiy = 0.0f;
    float posmfx = 0.0f;
    float posmfy = 0.0f;
    long timetouchdown = System.currentTimeMillis();
    long lasttime = System.currentTimeMillis();
    int maxpointerdown=0;
    boolean wemoved = false;
    boolean wetouchedbigimage = false;
    boolean wetouchednavigationbars = false;
    boolean wetouchedmenuascenseur = false;
    boolean wepinched = false;
    long minmovingtime = 250;
    float optionsbase = 0.0f;
    final int nbremaxptr = 20;
    int[] pointerids = new int[nbremaxptr];

    boolean onautilisestylus = false;
    boolean onacliquemenudessiner = false;

    ArrayList<float[]> drawrefposzoom = new ArrayList<>();
    int drawrefposzooml = 0;

    int cechoixdepossibilite = -1;

    @Override
    public boolean onTouch(View v, MotionEvent ev) {
      model.currentselectedfragment = myid;
      if (mysurf.ScreenWidth < 4 || mysurf.ScreenHeight < 4) {
        return true;
      }
      int action = ev.getAction() & MotionEvent.ACTION_MASK;
      int pointerindex = ev.getActionIndex();
      int pointerid = ev.getPointerId(pointerindex);
      int pointercount = ev.getPointerCount();
      int pointerindex0, pointerindex1;
      if (pointercount > maxpointerdown) {
        maxpointerdown = pointercount;
      }

      if (mysurf.drawmovezoom || mysurf.drawmovemenusize || mysurf.drawmenusize || mysurf.selectedtrace == Gallery.selectedtraceresize) {

        if (action == MotionEvent.ACTION_DOWN) {
          cechoixdepossibilite =  0;
        } else if (cechoixdepossibilite != 0){
          return false;
        }

        switch (action) {
          case MotionEvent.ACTION_DOWN:
            for (int i = 0 ; i < nbremaxptr; i++) {
              pointerids[i] = -99;
            }
            pointerids[0] = pointerid;
            pointerindex0 = ev.findPointerIndex(pointerids[0]);
            pos0ix = ev.getX(pointerindex0);
            pos0iy = ev.getY(pointerindex0);
            if (pos0ix < mysurf.drawmenutotalw && pos0iy < mysurf.drawmenutotalh) {
              onacliquemenudessiner = true;
              try {
                model.commandethreaddrawing.put(new String[]{String.valueOf(myid), "menudessiner", "init", String.valueOf(pos0ix), String.valueOf(pos0iy)});
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
              return false;
            }
            // on reprend les autres valeurs de déplacement
            drawrefposzoom.clear();
            drawrefposzooml = mysurf.listecouche.size();
            for (int i = 0; i < drawrefposzooml; i++) {
              drawrefposzoom.add(new float[]{mysurf.listecouche.get(i).x, mysurf.listecouche.get(i).y, mysurf.listecouche.get(i).scale});
            }
            bscalei = model.DessinCaseH;
            bpyi = mysurf.drawmovemenusizeyi;
            sizemin = mysurf.drawtracecurrsizemin;
            sizemax = mysurf.drawtracecurrsizemax;
            selectedtraceresizeratio = mysurf.selectedtraceresizeratio;
            posmix = pos0ix;
            posmiy = pos0iy;
            posmfx = posmix;
            posmfy = posmiy;
            maxpointerdown = 0;
            wemoved = false;
            wepinched = false;
            lasttime = System.currentTimeMillis();
            timetouchdown = System.currentTimeMillis();
            break;
          case MotionEvent.ACTION_POINTER_DOWN:
            if (maxpointerdown == 2) {
              if (pointerids[1] == -99) {
                pointerids[1] = pointerid;
                pointerindex1 = ev.findPointerIndex(pointerids[1]);
                pos1ix = ev.getX(pointerindex1);
                pos1iy = ev.getY(pointerindex1);
                delta10ix = pos1ix - pos0ix;
                delta10iy = pos1iy - pos0iy;
                delta10isqr = (float) Math.sqrt(delta10ix * delta10ix + delta10iy * delta10iy);
                drawrefposzoom.clear();
                for(int i = 0 ; i < drawrefposzooml ; i++) {
                  drawrefposzoom.add(new float[]{mysurf.listecouche.get(i).x, mysurf.listecouche.get(i).y, mysurf.listecouche.get(i).scale});
                }
                posmix = (pos0ix + pos1ix) / 2.0f;
                posmiy = (pos0iy + pos1iy) / 2.0f;
                posmfx = posmix;
                posmfy = posmiy;
              }
            }
            wemoved = false;
            wepinched = false;
            break;
          case MotionEvent.ACTION_MOVE:
            long temps = System.currentTimeMillis();
            if (temps - lasttime > 30) {
              if (pointerid == pointerids[0]) {
                pointerindex0 = ev.findPointerIndex(pointerids[0]);
                pos0fx = ev.getX(pointerindex0);
                pos0fy = ev.getY(pointerindex0);
              }
              float delta0fix = pos0fx - pos0ix;
              float delta0fiy = pos0fy - pos0iy;
              float delta0fisqr = delta0fix * delta0fix + delta0fiy * delta0fiy;
              if (!wemoved) {
                if (delta0fisqr > model.deltaminmove2 || temps - timetouchdown > minmovingtime) {
                  wemoved = true;
                }
              }
              if (pointercount == 1 && maxpointerdown == 1) {
                float ratio = (float) Math.sqrt(delta0fisqr / model.deltapowermoveunit2);
                if (ratio < 1.0f) ratio = 1.0f;
                if (ratio > 2.0f) ratio = 2.0f;
                if (mysurf.drawmovezoom) {
                  for (int i = 0; i < drawrefposzooml; i++) {
                    if (mysurf.listecouche.get(i).active) {
                      mysurf.listecouche.get(i).x = drawrefposzoom.get(i)[0] + ratio * delta0fix;
                      mysurf.listecouche.get(i).y = drawrefposzoom.get(i)[1] + ratio * delta0fiy;
                    }
                  }
                } else if (mysurf.drawmovemenusize) {
                  mysurf.drawmovemenusizeyi = bpyi + delta0fiy;
                }
                try {
                  model.commandethreaddrawing.put(new String[]{String.valueOf(myid), "update"});
                } catch (InterruptedException e) {
                  e.printStackTrace();
                }
              } else if (pointercount == 2 && maxpointerdown == 2) {
                pointerindex1 = ev.findPointerIndex(pointerids[1]);
                pos1fx = ev.getX(pointerindex1);
                pos1fy = ev.getY(pointerindex1);
                posmfx = (pos0fx + pos1fx) / 2.0f;
                posmfy = (pos0fy + pos1fy) / 2.0f;
                float delta10fx = pos1fx - pos0fx;
                float delta10fy = pos1fy - pos0fy;
                float delta10fsqr = (float) Math.sqrt(delta10fx * delta10fx + delta10fy * delta10fy);
                float ratio = delta10fsqr / delta10isqr;
                if (mysurf.drawmovezoom) {
                  for(int i = 0 ; i < drawrefposzooml ; i++) {
                    if (mysurf.listecouche.get(i).active) {
                      mysurf.listecouche.get(i).x = ratio * drawrefposzoom.get(i)[0] + posmix - ratio * posmix;
                      mysurf.listecouche.get(i).y = ratio * drawrefposzoom.get(i)[1] + posmiy - ratio * posmiy;
                      float sc = drawrefposzoom.get(i)[2] * ratio;
                      if (0.98f < sc && sc < 1.02f)
                        sc = 1;
                      mysurf.listecouche.get(i).scale = sc;
                    }
                  }
                } else if (mysurf.drawmovemenusize) {
                  model.DessinCaseH = bscalei * ratio;
                  if (model.DessinCaseH < 10.0f) {
                    model.DessinCaseH = model.GenericCaseH;
                  }
                  model.DessinCaseTextWantedH = model.DessinCaseH * 0.50f;
                  model.DessinCaseTextH = model.DessinCaseTextWantedH;
                  model.DessinInterSpace = model.DessinCaseH * 0.15f;
                  model.DessinTextPaint.setTextSize(model.DessinCaseTextH);
                  model.preferences.edit().putFloat("DessinCaseH", model.DessinCaseH).commit();
                } else if (mysurf.drawmenusize) {
                  mysurf.drawtracecurrsizemin = sizemin * ratio;
                  mysurf.drawtracecurrsizemax = sizemax * ratio;
                } else if (mysurf.selectedtrace == Gallery.selectedtraceresize) {
                  mysurf.selectedtraceresizeratio = selectedtraceresizeratio * ratio;
                }
                wepinched = true;
                try {
                  model.commandethreaddrawing.put(new String[]{String.valueOf(myid), "updateredrawicons"});
                } catch (InterruptedException e) {
                  e.printStackTrace();
                }
              }
              lasttime = temps;
            }
            break;
          case MotionEvent.ACTION_POINTER_UP:
            break;
          case MotionEvent.ACTION_UP:
            if (pointerid == pointerids[0]) {
              pointerindex0 = ev.findPointerIndex(pointerids[0]);
              pos0fx = ev.getX(pointerindex0);
              pos0fy = ev.getY(pointerindex0);
            }
            if (!wepinched && mysurf.selectedtrace == Gallery.selectedtraceresize) {
              try {
                model.commandethreaddrawing.put(new String[]{String.valueOf(myid), "menudessiner", "init", String.valueOf(pos0fx), String.valueOf(pos0fy)});
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
            }
            break;
          default:
            llog.d(TAG, "on ordonne autre");
            break;
        }
      } else {
        if(action == MotionEvent.ACTION_DOWN){
          cechoixdepossibilite =  1;
        } else if (cechoixdepossibilite != 1){
          return false;
        }
        switch (action) {
          case MotionEvent.ACTION_DOWN:
            // on initialise toutes les valeurs
            onautilisestylus = false;
            onacliquemenudessiner = false;
            maxpointerdown = 1;
            for (int i = 0; i < nbremaxptr; i++) {
              pointerids[i] = -999;
            }
          case MotionEvent.ACTION_POINTER_DOWN:
            for (int i = 0; i < pointercount; i++) {
              if (mysurf.styluspalmrejection) {
                // on prend le dernier stylus valide peut importe lequel
                if (ev.getToolType(i) == MotionEvent.TOOL_TYPE_STYLUS) {
                  // stylus
                  pointerids[0] = ev.getPointerId(i);
                  onautilisestylus = true;
                } else if (ev.getToolType(i) == MotionEvent.TOOL_TYPE_ERASER) {
                  // bouton erase du stylus
                  pointerids[1] = ev.getPointerId(i);
                }
              } else {
                // on prend les premiers pointers si on en a pas encore
                if (pointerids[0] == -999) {
                  pointerids[0] = ev.getPointerId(i);
                } else if (pointerids[1] == -999) {
                  pointerids[1] = ev.getPointerId(i);
                }
              }
            }
            if (pointerids[0] != -999) {
              pointerindex0 = ev.findPointerIndex(pointerids[0]);
              if (pointerindex0 == -1) {
                return false;
              }
              pos0ix = ev.getX(pointerindex0);
              pos0iy = ev.getY(pointerindex0);
              if (pos0ix < mysurf.drawmenutotalw && pos0iy < mysurf.drawmenutotalh) {
                onacliquemenudessiner = true;
                float pos0fp = ev.getPressure(pointerindex0);
                try {
                  model.commandethreaddrawing.put(new String[]{String.valueOf(myid), "menudessiner", "init", String.valueOf(pos0ix), String.valueOf(pos0iy), String.valueOf(pos0fp)});
                } catch (InterruptedException e) {
                  e.printStackTrace();
                }
              } else if (mysurf.pickacolor > 0 || mysurf.selectedtrace > 0) {
                onacliquemenudessiner = true;
                try {
                  model.commandethreaddrawing.put(new String[]{String.valueOf(myid), "menudessiner", "init", String.valueOf(pos0ix), String.valueOf(pos0iy)});
                } catch (InterruptedException e) {
                  e.printStackTrace();
                }
              } else if (!mysurf.onlymouseup) {
                float pos0fp = ev.getPressure(pointerindex0);
                float pos0ft = ev.getAxisValue(MotionEvent.AXIS_TILT, pointerindex0);
                float pos0fd = ev.getAxisValue(MotionEvent.AXIS_DISTANCE, pointerindex0);
                try {
                  model.commandethreaddrawing.put(new String[]{String.valueOf(myid), "trace", "i", String.valueOf(pos0ix), String.valueOf(pos0iy), String.valueOf(pos0fp), String.valueOf(pos0ft), String.valueOf(pos0fd)});
                } catch (InterruptedException e) {
                  e.printStackTrace();
                }
              }
              lasttime = System.currentTimeMillis();
            }
            break;
          case MotionEvent.ACTION_MOVE:
            if (pointerids[0] != -999) {
              pointerindex0 = ev.findPointerIndex(pointerids[0]);
              if (pointerindex0 == -1) {
                return false;
              }
              pos0fx = ev.getX(pointerindex0);
              pos0fy = ev.getY(pointerindex0);
              if (onacliquemenudessiner) {
                if (pos0fx < mysurf.drawmenutotalw && pos0fy < mysurf.drawmenutotalh) {
                  long temps = System.currentTimeMillis();
                  if (temps - lasttime > 100) {
                    try {
                      model.commandethreaddrawing.put(new String[]{String.valueOf(myid), "menudessiner", "move", String.valueOf(pos0fx), String.valueOf(pos0fy)});
                    } catch (InterruptedException e) {
                      e.printStackTrace();
                    }
                    lasttime = temps;
                  }
                } else if (mysurf.pickacolor > 0 || mysurf.selectedtrace > 0) {
                  onacliquemenudessiner = true;
                  long temps = System.currentTimeMillis();
                  if (temps - lasttime > 100) {
                    try {
                      model.commandethreaddrawing.put(new String[]{String.valueOf(myid), "menudessiner", "move", String.valueOf(pos0fx), String.valueOf(pos0fy)});
                    } catch (InterruptedException e) {
                      e.printStackTrace();
                    }
                    lasttime = temps;
                  }
                }
              } else if (!mysurf.onlymouseup) {
                float pos0fp = ev.getPressure(pointerindex0);
                float pos0ft = ev.getAxisValue(MotionEvent.AXIS_TILT, pointerindex0);
                float pos0fd = ev.getAxisValue(MotionEvent.AXIS_DISTANCE, pointerindex0);
                try {
                  model.commandethreaddrawing.put(new String[]{String.valueOf(myid), "trace", "n", String.valueOf(pos0fx), String.valueOf(pos0fy), String.valueOf(pos0fp), String.valueOf(pos0ft), String.valueOf(pos0fd)});
                } catch (InterruptedException e) {
                  e.printStackTrace();
                }
              }
            }
            break;
          case MotionEvent.ACTION_POINTER_UP:
            // on ne s'en occupe pas
            // si notre pointeur n'existe plus par la suite
            // il sera de toute façon supprimé dans move ou final
            // if (pointerids[0] != -999){
            break;
          case MotionEvent.ACTION_CANCEL:
          case MotionEvent.ACTION_UP:
            if (!onacliquemenudessiner) {
              if (pointerids[0] == -999) {
                if (pointercount >= 1) {
                  // si on n'a enregistré aucun pointer
                  // par ex. on est en mode stylus mais le stylus n'est pas disponible/chargé
                  // on regarde quand même s'il ne faut pas prendre le menu
                  // mais tout à la fin uniquement
                  pos0fx = ev.getX();
                  pos0fy = ev.getY();
                  if (pos0fx < mysurf.drawmenutotalw && pos0fy < mysurf.drawmenutotalh) {
                    onacliquemenudessiner = true;
                    try {
                      model.commandethreaddrawing.put(new String[]{String.valueOf(myid), "menudessiner", "init", String.valueOf(pos0fx), String.valueOf(pos0fy)});
                    } catch (InterruptedException e) {
                      e.printStackTrace();
                    }
                  }
                }
              } else {
                pointerindex0 = ev.findPointerIndex(pointerids[0]);
                if (pointerindex0 == -1) {
                  return false;
                }
                pos0fx = ev.getX(pointerindex0);
                pos0fy = ev.getY(pointerindex0);
                float pos0fp = ev.getPressure(pointerindex0);
                float pos0ft = ev.getAxisValue(MotionEvent.AXIS_TILT, pointerindex0);
                float pos0fd = ev.getAxisValue(MotionEvent.AXIS_DISTANCE, pointerindex0);
                String etape = "f";
                if (mysurf.onlymouseup) {
                  if (mysurf.onlymouseupstatus == 0) {
                    etape = "i";
                    mysurf.onlymouseupstatus = 1;
                  } else if (mysurf.onlymouseupstatus == 1) {
                    etape = "n";
                  } else {
                    etape = "f";
                    mysurf.onlymouseupstatus = 1;
                  }
                }
                try {
                  model.commandethreaddrawing.put(new String[]{String.valueOf(myid), "trace", etape, String.valueOf(pos0fx), String.valueOf(pos0fy), String.valueOf(pos0fp), String.valueOf(pos0ft), String.valueOf(pos0fd)});
                } catch (InterruptedException e) {
                  e.printStackTrace();
                }
              }
            }
            break;
          default:
            llog.d(TAG, "motionevent non traité");
            return false;
        }
      }
      return true;


    }
  };


  @SuppressLint("ClickableViewAccessibility")
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    llog.d(TAG, myid+" onCreateView()");
    if (myid >= model.surf.size() || myid < 0) {
      llog.d(TAG, "error myid " + myid + " surfsize " + model.surf.size());
      return null;
    }
    mysurf = model.surf.get(myid);
    mysurf.fragmenttype = Surf.FRAGMENT_TYPE_DRAWING;
    mysurf.fragmentView = inflater.inflate(R.layout.fragment_drawing, container, false);

    mysurf.fragmentView.setX(model.surf.get(myid).myx);
    mysurf.fragmentView.setY(model.surf.get(myid).myy);
    ViewGroup.LayoutParams parms = mysurf.fragmentView.getLayoutParams();
    parms.width = model.surf.get(myid).mywidth;
    parms.height = model.surf.get(myid).myheight;
    mysurf.fragmentView.setLayoutParams(parms);

    mysurf.drawingSurfaceView = mysurf.fragmentView.findViewById(R.id.imageViewImageDrawing);
    mysurf.drawingSurfaceHolder = mysurf.drawingSurfaceView.getHolder();
    mysurf.drawingSurfaceHolder.addCallback(mycallback);

    mysurf.fragmentView.setOnTouchListener(ontouchlistener);
    
    // Inflate the layout for this fragment
    return mysurf.fragmentView;
  }
  
  

  public OnFragmentInteractionListener fragmentlistener;
  
  public interface OnFragmentInteractionListener {
    // TODO: Update argument type and name
    Integer onFragmentInteraction(Uri uri);
  }
  
  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    if (context instanceof OnFragmentInteractionListener) {
      fragmentlistener = (OnFragmentInteractionListener) context;
    } else {
      llog.d(TAG, context.toString() + " must implement OnFragmentInteractionListener");
    }
  }
  
  
}






















