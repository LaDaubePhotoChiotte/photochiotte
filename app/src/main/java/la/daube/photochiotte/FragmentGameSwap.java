package la.daube.photochiotte;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.util.Random;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentGameSwap.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentGameSwap#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentGameSwap extends Fragment {
  private static final String ARG_PARAM1 = "param1";
  private static final String ARG_PARAM2 = "param2";
  private static final String TAG = "YYYfg4";

  private Gallery model;

  public int myid = -1;
  private Surf mysurf = null;

  public FragmentGameSwap() {
    // Required empty public constructor
  }

  public static FragmentGameSwap newInstance(String param1, String param2) {
    FragmentGameSwap fragment = new FragmentGameSwap();
    Bundle args = new Bundle();
    args.putString(ARG_PARAM1, param1);
    args.putString(ARG_PARAM2, param2);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (getArguments() != null) {
      myid = getArguments().getInt("myid", -1);
      cefichier = getArguments().getString("cefichier", "");
    }
    //model = ViewModelProviders.of(getActivity()).get(myViewModel.class);
    model = new ViewModelProvider(requireActivity()).get(Gallery.class);
    model.currentselectedfragment = myid;
    llog.d(TAG, myid+" onCreate() " + myid + " -> " + cefichier);
  }

  @Override
  public void onResume() {
    super.onResume();
    llog.d(TAG, myid+" onResume()");
  }

  @Override
  public void onStart() {
    super.onStart();
    llog.d(TAG, myid+" onStart()");
  }

  @Override
  public void onPause() {
    super.onPause();
    llog.d(TAG, myid+" onPause()");
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    llog.d(TAG, myid+" onDestroy()");
    mWood.recycle();
    //mamaow.recycle();
    for(int uu=0;uu<parterre.length;uu++)
      parterre[uu].recycle();
    for(int uu=0;uu<pasterre.length;uu++)
      pasterre[uu].recycle();
    //if(affiche==true)
    //  zzz.start();
  }

  private Handler mChildHandler;

  private int supernw,supernh,largeur,hauteur;
  private String cefichier;
  private float[][] pose;
  private int parta;
  private int decalw,decalh,wt,ht;
  private float wwth=20.0f;

  private Bitmap mWood = null;
  private Bitmap mamaow = null;
  private Bitmap[] parterre = null;
  private Bitmap[] pasterre = null;
  private Paint paint = new Paint();
  private int prefcouleur;
  private int videsauve=0;

  private volatile boolean rotage=false;
  private volatile float[] touche={0.0f,0.0f};
  private volatile int compteur=0;
  private volatile int vidz=0;
  private volatile int[] mixpose;
  private volatile boolean tourne=false;
  private volatile boolean isrunning=false;
  private volatile float[] triche0={-1.0f,-1.0f};
  private volatile float[] triche1={-1.0f,-1.0f};
  private volatile boolean pleasemove=false;
  private volatile boolean triche=false;
  private int maxicola;

  @SuppressLint("ClickableViewAccessibility")
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    mysurf = model.surf.get(myid);
    mysurf.fragmenttype = Surf.FRAGMENT_TYPE_PUZZLE;
    mysurf.fragmentView = inflater.inflate(R.layout.fragment_game_puzzle, container, false);

    mysurf.fragmentView.setX(model.surf.get(myid).myx);
    mysurf.fragmentView.setY(model.surf.get(myid).myy);
    ViewGroup.LayoutParams parms = mysurf.fragmentView.getLayoutParams();
    parms.width = model.surf.get(myid).mywidth;
    parms.height = model.surf.get(myid).myheight;
    mysurf.fragmentView.setLayoutParams(parms);

    final SurfaceView imageviewpicture = mysurf.fragmentView.findViewById(R.id.imageViewImagePuzzle);
    mysurf.browserSurfaceHolder = imageviewpicture.getHolder();
    final SurfaceHolder.Callback mycallback = new SurfaceHolder.Callback() {
      @Override
      public void surfaceCreated(SurfaceHolder surfaceHolder) {
        llog.d(TAG, myid+" surfaceCreated()");
        mysurf.browserSurfaceHolder.setSizeFromLayout();
        _thread.setRunning(true);
      }
      @Override
      public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
        llog.d(TAG, myid+" surfacechanged() : "+i1 +"x"+i2);

        /*Intent intent = new Intent();
        intent.setAction(myViewModel.broadcastname);
        intent.putExtra("goal", "correctallfragmentlayouts");
        intent.putExtra("myid", myid);
        intent.putExtra("x", mysurf.myx);
        intent.putExtra("y", mysurf.myy);
        intent.putExtra("w", mysurf.mywidth);
        intent.putExtra("h", mysurf.myheight);
        LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);*/

        //mysurf.SettingsYmin = model.settingsYmin;
        //mysurf = model.surf.get(myid);
        if (mysurf.mywidth != mysurf.ScreenWidth || mysurf.myheight != mysurf.ScreenHeight) {
          llog.d(TAG, myid+" surfacechanged() different size from before");
          mysurf.ScreenWidth = mysurf.mywidth;
          mysurf.ScreenHeight = mysurf.myheight;
          largeur =  mysurf.mywidth;
          hauteur =  mysurf.myheight;
          //setallthepaints();
          // on ne veut pas que ça bouge quand on modifie le split ou la rotation
          //mysurf.putbigpictureinmemory = true;
          //mysurf.centeronscreen = true;
        }
        mysurf.mysurfacestopdrawing = false;
        mysurf.mysurfaceisdestroyed = false;
        /*try {
          model.commandethreadbrowser.put(new String[]{String.valueOf(myid), "dontmissupdate"});
        } catch (InterruptedException e) {
          e.printStackTrace();
        }*/
      }
      @Override
      public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        llog.d(TAG, myid+" surfaceDestroyed()");
        _thread.setRunning(false);
        mysurf.mysurfacestopdrawing = true;
        while (mysurf.surfaceIsCurrentlyDrawing) {
          try {
            Thread.sleep(10);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        }
        mysurf.mysurfaceisdestroyed = true;
      }
    };
    mysurf.browserSurfaceHolder.addCallback(mycallback);

    llog.d(TAG, myid+" onCreateView()");
    supernw=model.preferences.getInt("cut_col2z",4);
    supernh=model.preferences.getInt("cut_lig2z",6);
    supernw = model.gamecolumns;
    supernh = model.gamelines;
    prefcouleur=model.preferences.getInt("grid_color",Color.YELLOW);

    String merdouille="maximz"+supernw+supernh;
    maxicola=model.preferences.getInt(merdouille, 50000);

    largeur= model.surf.get(myid).mywidth;
    hauteur= model.surf.get(myid).myheight;

    if (mysurf.isincache(cefichier)) {
      mamaow = mysurf.getcachedbitmap(cefichier);
    }
    if (mamaow.getWidth() >  mamaow.getHeight()) {
      int supernhb = supernh;
      supernh = supernw;
      supernw = supernhb;
    }

    BitmapFactory.Options opts = new BitmapFactory.Options();
    opts.inPreferredConfig = Bitmap.Config.RGB_565;
    Bitmap fondfond = BitmapFactory.decodeResource(getResources(), R.drawable.pat2, opts);
    int patw=fondfond.getWidth();
    int path=fondfond.getHeight();
    int ww=largeur;
    int wh=hauteur;
    Bitmap fond=Bitmap.createBitmap(ww, wh, Bitmap.Config.RGB_565);
    Canvas fdcanv=new Canvas(fond);
    for(int fnd=0;fnd<ww;fnd+=patw)
      for(int fndh=0;fndh<wh;fndh+=path)
        fdcanv.drawBitmap(fondfond, fnd, fndh, new Paint());
    fondfond.recycle();
    ColorMatrix couleurs;
    ColorMatrixColorFilter coul;
    Bitmap fond2 = Bitmap.createBitmap(ww, wh, Bitmap.Config.RGB_565);
    Canvas c = new Canvas(fond2);
    couleurs=modifyback();
    coul=new ColorMatrixColorFilter(couleurs);
    paint.setColorFilter(coul);
    c.drawBitmap(fond, 0.0f, 0.0f, paint);
    fond.recycle();
    mWood = Bitmap.createScaledBitmap(fond2, ww, wh, true);
    fond2.recycle();
    paint=new Paint();
    traitementimage(ww,wh);

    mysurf.fragmentView.setOnTouchListener(new View.OnTouchListener() {
      public boolean onTouch(View arg0, MotionEvent arg1) {
        model.currentselectedfragment = myid;
        if(arg1.getAction()==MotionEvent.ACTION_DOWN){
          triche1[0]=arg1.getX();
          triche1[1]=arg1.getY();
          return true;
        }
        if(arg1.getAction()==MotionEvent.ACTION_UP){

          touche[0]=arg1.getX();
          touche[1]=arg1.getY();
          float d1=Math.abs(triche1[0]-touche[0]);
          float d2=Math.abs(triche1[1]-touche[1]);
          if(d1<wwth&&d2<wwth){
            Message toMain = new Message();
            Bundle bundble=new Bundle();
            bundble.putFloatArray("posarray", triche1);
            bundble.putInt("status", 22);
            toMain.obj = (Bundle) bundble;
            mChildHandler.sendMessage(toMain);
            return false;
          }

          touche[0]=arg1.getX();
          touche[1]=arg1.getY();
          if(!triche){
            Message toMain = new Message();
            Bundle bundble=new Bundle();
            bundble.putFloatArray("posarray", touche);
            bundble.putFloatArray("posarray0", triche1);
            bundble.putInt("status", 2);
            toMain.obj = (Bundle) bundble;
            mChildHandler.sendMessage(toMain);
          }
          else{
            if(triche0[0]==-1.0f){
              triche0[0]=touche[0];
              triche0[1]=touche[1];
            }
            else {
              triche1[0]=touche[0];
              triche1[1]=touche[1];
              Message toMain = new Message();
              Bundle bundble=new Bundle();
              bundble.putFloatArray("posarray", triche0);
              bundble.putFloatArray("posarray2", triche1);
              bundble.putInt("status", 3);
              toMain.obj = (Bundle) bundble;
              mChildHandler.sendMessage(toMain);
            }
          }
          return false;
        }
        return false;
      }
    });
    isrunning=true;

    new Panel();
    _thread.start();

    // Inflate the layout for this fragment
    return mysurf.fragmentView;
  }



  public void traitementimage(int ww, int wh){
    Bitmap mamaoww;
    int w2=mamaow.getWidth();
    int h2=mamaow.getHeight();
    double ratiof=((double)(ww))/((double)(wh));
    double ratio2=((double)(w2))/((double)(h2));
    if(ratio2>ratiof){
      h2=(int)(Math.floor((double)(ww)/ratio2));
      w2=ww;
    }
    else {
      h2=wh;
      w2=(int)(Math.floor((double)(wh)*ratio2));
    }
    if(h2>wh)
      h2=wh;
    if(w2>ww)
      w2=ww;
    mamaoww = Bitmap.createScaledBitmap(mamaow,w2, h2, true);
    //mamaow.recycle(); bien mais si on recycle on recycle aussi la cachedbitmap du threadbrowser (image noire)
    decalh=(int)(Math.floor(Math.abs((float)(h2-wh)/2.0f)));
    decalw=(int)(Math.floor(Math.abs((float)(w2-ww)/2.0f)));

    Bitmap fondimage= Bitmap.createBitmap(mWood);
    Canvas cvwood=new Canvas(mWood);
    Canvas cvfondimage= new Canvas(fondimage);
    cvfondimage.drawBitmap(mamaoww, decalw, decalh, paint);


    int nw=supernw;
    int nh=supernh;
    parta=nw*nh;
    wt=(int)(Math.floor(((float)w2)/((float)(nw))));
    ht=(int)(Math.floor(((float)h2)/((float)(nh))));
    //on créé les différentes parties découpées en paires
    parterre = new Bitmap[parta];
    pasterre = new Bitmap[parta];
    pose=new float[parta][2];

    //gaffe aux arrondis on dépasse pas les bornes
    while(nw*wt>w2 || nw*wt+decalw>ww)
      wt--;
    while(nh*ht>h2 || nh*ht+decalh>wh)
      ht--;

    //quelle est la marge carrée ?
    int ecarth=0;
    int ecartw=0;
    if(wt<ht){
      ecarth=(ht-wt)*nh;
      ht=wt;
      wwth=((float)ht)/4.0f;
    }
    else{
      ecartw=(wt-ht)*nw;
      wt=ht;
      wwth=((float)wt)/4.0f;
    }
    decalh+=ecarth/2;
    decalw+=ecartw/2;
    if(decalh>0){
      int second=decalh+ht*nh;
      Bitmap one=Bitmap.createBitmap(fondimage, 0, 0, ww, decalh);
      Bitmap two=Bitmap.createBitmap(fondimage, 0, second, ww, decalh);
      cvwood.drawBitmap(one, 0.0f,0.0f, paint);
      cvwood.drawBitmap(two, 0.0f,second, paint);
      one.recycle();
      two.recycle();
    }
    if(decalw>0){
      int second=decalw+wt*nw;
      Bitmap one=Bitmap.createBitmap(fondimage, 0, 0, decalw, wh);
      Bitmap two=Bitmap.createBitmap(fondimage, second, 0, decalw, wh);
      cvwood.drawBitmap(one, 0.0f,0.0f, paint);
      cvwood.drawBitmap(two, second,0.0f, paint);
      one.recycle();
      two.recycle();
    }
    mamaoww=Bitmap.createBitmap(fondimage, decalw, decalh, w2-ecartw, h2-ecarth);
    fondimage.recycle();

    int z1,z2,z3;
    for (int xn=0;xn<nw;xn++){
      for(int yn=0;yn<nh;yn++){
        z1=yn+xn*nh;
        z2=xn*wt;
        z3=yn*ht;
        parterre[z1]=Bitmap.createBitmap(mamaoww, z2, z3, wt, ht);
        z2+=decalw;
        z3+=decalh;
        pasterre[z1]=Bitmap.createBitmap(mWood, z2, z3, wt, ht);
        pose[z1][0]=(float)(z2);
        pose[z1][1]=(float)(z3);
      }
    }
    mamaoww.recycle();

    int nbc=parta;
    mixpose=new int[nbc];
    for(int oo=0;oo<nbc;oo++)
      mixpose[oo]=oo;

    int fafa2,choix,vz,z;
    Random yiu= Gallery.rand;
    int vide=yiu.nextInt(supernw*supernh);/*yiu.nextInt(4);
      	switch(vide){
      	case 0:
      		vide=0;
      		break;
      	case 1:
      		vide=supernh-1;
      		break;
      	case 2:
      		vide=(supernw-1)*supernh;
      		break;
      	case 3:
      		vide=(supernw*supernh)-1;
      		break;
      	}*/
    int supermaximum=50000;
    int superminimum=25;
    videsauve=vide;
    boolean prems=false;
    boolean continu=false;
    while((continu || compteur<superminimum) && compteur<supermaximum){
      compteur++;
      z=-1;
      while((z<0 || z>=parta)){
        choix=yiu.nextInt(4);
        switch(choix){
          case 0:
            vz=vide+1;
            while(vz>0)
              vz-=supernh;
            if(vz!=0)
              z=vide+1;
            break;
          case 1:
            vz=vide;
            while(vz>0)
              vz-=supernh;
            if(vz!=0)
              z=vide-1;
            break;
          case 2:
            z=vide+supernh;
            break;
          case 3:
            z=vide-supernh;
            break;
        }
      }
      fafa2=mixpose[z];
      mixpose[z]=mixpose[vide];
      mixpose[vide]=fafa2;
      vide=z;
      continu=continuer();
      if(continu==false && prems==false){
        prems=true;
        superminimum=compteur*2;
      }
    }
  }

  public boolean continuer(){
    int temp;
    for(int jo=0;jo<parta;jo++){
      if(mixpose[jo]==jo)
        return true;
    }
    for(int jo=0;jo<parta;jo++){
      if(jo>0){
        if(mixpose[jo]==mixpose[jo-1]+1){
          temp=jo;
          while(temp>0)
            temp-=supernh;
          if(temp!=0)
            return true;
        }
      }
      if(jo>=supernh){
        if(mixpose[jo]==mixpose[jo-supernh]+supernh)
          return true;
      }
    }
    return false;
  }

  public ColorMatrix modifyback(){
    ColorMatrix couleurs=new ColorMatrix();
    float[] colors= new float[]{
            1,0,0,0,0,//g0
            0,1,0,0,0,//g1
            0,0,1,0,0,//g2
            0,0,0,1,0 //g3
    };
    for (int u=0;u<3;u++){
      for (int g=0;g<3;g++){
        if(u==g)
          colors[u+g*5]=(float)(Math.random());
        else if (u==4)
          colors[u+g*5]=(float)(Math.random()*512.0-256.0);
      }
    }
    couleurs.set(colors);
    return couleurs;
  }

  public Bitmap ShrinkBitmap(String file){
    int height=1000;
    int width=height;
    BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
    bmpFactoryOptions.inJustDecodeBounds = true;
    Bitmap bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
    double oh=(double)(bmpFactoryOptions.outHeight);
    double ow=(double)(bmpFactoryOptions.outWidth);
    int heightRatio = (int)Math.ceil(oh/(double)height);//ceil
    int widthRatio = (int)Math.ceil(ow/(double)width);

    if (heightRatio > 1 || widthRatio > 1)
    {
      if (heightRatio > widthRatio)
      {
        bmpFactoryOptions.inSampleSize = heightRatio;
      } else {
        bmpFactoryOptions.inSampleSize = widthRatio;
      }
    }

    bmpFactoryOptions.inJustDecodeBounds = false;
    bmpFactoryOptions.inPreferQualityOverSpeed=true;
    bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
    return bitmap;
  }

  public Bitmap rotimage(Bitmap droitt){
    int www2=droitt.getWidth();
    int wwh2=droitt.getHeight();
    Bitmap mamie=Bitmap.createBitmap(wwh2, www2, Bitmap.Config.ARGB_8888);
    for (int v=0;v<wwh2;v++)
      for (int u=0;u<www2;u++)
        mamie.setPixel(v, (www2-1-u), droitt.getPixel(u, v));
    return mamie;
  }

  public Bitmap rotimage1(Bitmap droitt){
    int fullwidth=droitt.getWidth();
    int fullheight=droitt.getHeight();
    Bitmap mamie=Bitmap.createBitmap(fullheight, fullwidth, Bitmap.Config.ARGB_8888);
    for (int heightindex=0;heightindex<fullheight;heightindex++)
      for (int widthindex=0;widthindex<fullwidth;widthindex++)
        mamie.setPixel(heightindex, (fullwidth-1-widthindex),
                droitt.getPixel(widthindex, heightindex));
    return mamie;
  }

  public Bitmap rotimage2(Bitmap droitt){
    int fullwidth=droitt.getWidth();
    int fullheight=droitt.getHeight();
    Bitmap mamie=Bitmap.createBitmap(fullwidth, fullheight, Bitmap.Config.ARGB_8888);
    for (int heightindex=0;heightindex<fullheight;heightindex++)
      for (int widthindex=0;widthindex<fullwidth;widthindex++)
        mamie.setPixel((fullwidth-1-widthindex), (fullheight-1-heightindex),
                droitt.getPixel(widthindex, heightindex));
    return mamie;
  }

  public Bitmap rotimage3(Bitmap droitt){
    int fullwidth=droitt.getWidth();
    int fullheight=droitt.getHeight();
    Bitmap mamie=Bitmap.createBitmap(fullheight, fullwidth, Bitmap.Config.ARGB_8888);
    for (int heightindex=0;heightindex<fullheight;heightindex++)
      for (int widthindex=0;widthindex<fullwidth;widthindex++)
        mamie.setPixel((fullheight-1-heightindex), widthindex,
                droitt.getPixel(widthindex, heightindex));
    return mamie;
  }

  public void terminetout(boolean affiche,int score){
    if(mChildHandler != null){
      Message toMain = new Message();
      Bundle bundble=new Bundle();
      bundble.putInt("status", 0);
      toMain.obj = (Bundle) bundble;
      mChildHandler.sendMessage(toMain);
      mChildHandler.getLooper().quit();
    } else {
      llog.d(TAG, "error childhandler null+++++++++++++++++++++++++++");
    }

    SharedPreferences.Editor prefEdit = model.preferences.edit();
    String merdouille="maximz"+supernw+supernh;
    prefEdit.putInt(merdouille, score);
    prefEdit.commit();

    Intent intent = new Intent();
    intent.setAction(Gallery.broadcastname);
    intent.putExtra("goal", "startbrowser");
    intent.putExtra("id", myid);
    LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
  }

  /*@Override
  public boolean onKeyUp(int keyCode, KeyEvent event) {
      Message toMain = new Message();
      Bundle bundble=new Bundle();
      switch (keyCode) {
      case KeyEvent.KEYCODE_BACK:
          terminetout(false,0);
          return true;
      case KeyEvent.KEYCODE_MENU:
          triche=!triche;
          rotage=false;
          toMain = new Message();
          bundble=new Bundle();
          bundble.putInt("status", 70);
          toMain.obj = (Bundle) bundble;
          mChildHandler.sendMessage(toMain);
          return true;
      case KeyEvent.KEYCODE_VOLUME_DOWN:
          rotage=!rotage;
          triche=false;
          toMain = new Message();
          bundble=new Bundle();
          bundble.putInt("status", 69);
          toMain.obj = (Bundle) bundble;
          mChildHandler.sendMessage(toMain);
          return true;
      case KeyEvent.KEYCODE_VOLUME_UP:
          rotage=!rotage;
          triche=false;
          toMain = new Message();
          bundble=new Bundle();
          bundble.putInt("status", 69);
          toMain.obj = (Bundle) bundble;
          mChildHandler.sendMessage(toMain);
          return true;
      case KeyEvent.KEYCODE_SEARCH:
          rotage=!rotage;
          triche=false;
          toMain = new Message();
          bundble=new Bundle();
          bundble.putInt("status", 69);
          toMain.obj = (Bundle) bundble;
          mChildHandler.sendMessage(toMain);
          return true;
      default: return super.onKeyUp(keyCode, event);
      }
	}
  }*/

  private TutorialThread _thread;
  private Paint nopaint,nopainttext;
  private final int taille=25;

  class Panel {

    public Panel() {
      _thread = new TutorialThread(mysurf.browserSurfaceHolder, this);
      nopaint=new Paint();
      nopainttext=new Paint();
      nopainttext.setColor(prefcouleur);
      nopainttext.setTextSize(taille);
      nopainttext.setShadowLayer(4.0f, 1.0f, 1.0f, Color.BLACK);
      nopainttext.setTypeface(Typeface.MONOSPACE);
      nopainttext.setAntiAlias(true);
      nopainttext.setTextAlign(Paint.Align.CENTER);
    }

    public void nDraw(Canvas canvas, float[] glisse, int c0, int c1,int[] glisseici) {
      int fafa2,fafa,z;
      int gici=glisseici.length;
      canvas.drawBitmap(mWood,0,0,nopaint);
      boolean dontdrow=false;
      for(fafa=0;fafa<parta;fafa++){
        dontdrow=false;
        for(int slmt=0;slmt<gici;slmt++){
          z=glisseici[slmt];
          if(fafa==z){
            dontdrow=true;
          }
        }
        fafa2=mixpose[fafa];
        if(dontdrow==false){
          canvas.drawBitmap(parterre[fafa2],pose[fafa][0],pose[fafa][1],nopaint);
        }
      }

      fafa=glisseici[0];
      fafa2=mixpose[glisseici[gici-1]];
      canvas.drawBitmap(parterre[fafa2],pose[fafa][0]+glisse[0],pose[fafa][1]+glisse[1],paint);
      canvas.drawText(c1+"/"+maxicola,pose[fafa][0]+glisse[0],pose[fafa][1]+glisse[1]+taille,nopainttext);
      if(gici-2>0){
        fafa=glisseici[gici-2];
        canvas.drawBitmap(parterre[fafa2],pose[fafa][0]+glisse[0],pose[fafa][1]+glisse[1],paint);
        canvas.drawText(c1+"/"+maxicola,pose[fafa][0]+glisse[0],pose[fafa][1]+glisse[1]+taille,nopainttext);
      }
      for(int slmt=1;slmt<gici;slmt++){
        z=glisseici[slmt];
        fafa2=mixpose[glisseici[slmt-1]];
        canvas.drawBitmap(parterre[fafa2],pose[z][0]-glisse[0],pose[z][1]-glisse[1],nopaint);
      }
		    /*
		    for(int fafa=0;fafa<parta;fafa++){
		    	for(int slmt=0;slmt<gici;slmt++){
		    		z=glisseici[slmt];
		        	if(fafa!=slmt){
			        	fafa2=mixpose[fafa];
			        	canvas.drawBitmap(parterre[fafa2],pose[fafa][0],pose[fafa][1],nopaint);
		        	}
			        if (fafa==vide || fafa==fin){
				        canvas.drawBitmap(parterre[fafa2],pose[fafa][0]+glisse[0],pose[fafa][1]+glisse[1],paint);
						canvas.drawText(c1+"/"+c0,pose[fafa][0]+glisse[0],pose[fafa][1]+glisse[1]+taille,nopainttext);
					}
			        else if(fafa==z){
				        canvas.drawBitmap(parterre[fafa2],pose[fafa][0]-glisse[0],pose[fafa][1]-glisse[1],nopaint);
			        }
		    	}
		    }*/
    }

    public void onDrawFirst(Canvas canvas,int vide) {
      int fafa2;
      canvas.drawBitmap(mWood,0,0,null);
      for(int fafa=0;fafa<parta;fafa++){
        fafa2=mixpose[fafa];
        if(fafa!=vide){
          canvas.drawBitmap(parterre[fafa2], pose[fafa][0], pose[fafa][1], nopaint);
          if(triche)
            canvas.drawText(""+fafa2,pose[fafa][0],pose[fafa][1]+taille,nopainttext);
        }
      }
      fafa2=mixpose[vide];
      canvas.drawBitmap(parterre[fafa2], pose[vide][0], pose[vide][1], paint);
      canvas.drawText("Start here",pose[vide][0],pose[vide][1]+taille,nopainttext);
      triche0[0]=-1.0f;
    }

    public void onDrawFinal(Canvas canvas) {
      canvas.drawColor(Color.BLACK);
    }

    public void onDrawProgress(Canvas canvas, String texte) {
      canvas.drawColor(Color.DKGRAY);
      if(tourne==true)
        canvas.drawText("Please turn screen ---->",largeur/2.0f,taille+hauteur/2.0f,nopainttext);
      canvas.drawText("Search : Rotate pieces",largeur/2.0f,2.0f*taille,nopainttext);
      canvas.drawText("Menu : Cheat",largeur/2.0f,3.0f*taille,nopainttext);
      canvas.drawText(texte,largeur/2.0f,hauteur/2.0f,nopainttext);
    }

    public void onDrawInfo(Canvas canvas, int vide,String texte1,String texte2) {
      nopainttext.setTextAlign(Paint.Align.LEFT);
      onDrawFirst(canvas,vide);
      nopainttext.setTextAlign(Paint.Align.CENTER);
      if(tourne==true)
        canvas.drawText("Please turn screen ---->",largeur/2.0f,taille+hauteur/2.0f,nopainttext);
      canvas.drawText(texte1,largeur/2.0f,2.0f*taille,nopainttext);
      canvas.drawText(texte2,largeur/2.0f,3.0f*taille,nopainttext);
      nopainttext.setTextAlign(Paint.Align.LEFT);
    }
  }

  class TutorialThread extends Thread {
    private SurfaceHolder _surfaceHolder;
    private Panel _panel;
    private boolean _run = false;
    private Canvas c;
    private int vide=0;
    private int votrecompteur=0;
    private int sacc=12;//8
    private long pooose=0;//30

    public TutorialThread(SurfaceHolder surfaceHolder, Panel panel) {
      _surfaceHolder = surfaceHolder;
      _panel = panel;
    }

    public void setRunning(boolean run) {
      _run = run;
      if (_run && mChildHandler != null) {
        Message toMain = new Message();
        Bundle bundble=new Bundle();
        bundble.putInt("status", 8);
        toMain.obj = (Bundle) bundble;
        mChildHandler.sendMessage(toMain);
      }
    }

    public void infos(final String texte1, final String texte2){
      if (_run) {
        c = null;
        try {
          if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            c = _surfaceHolder.lockHardwareCanvas();
          } else {
            c = _surfaceHolder.lockCanvas();
          }
          synchronized (_surfaceHolder) {
            _panel.onDrawInfo(c,vide,texte1,texte2);
          }
        } finally {
          if (c != null) {
            _surfaceHolder.unlockCanvasAndPost(c);
          }
        }
      }
    }

    public void progres(final String texte){
      if (_run) {
        c = null;
        try {
          if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            c = _surfaceHolder.lockHardwareCanvas();
          } else {
            c = _surfaceHolder.lockCanvas();
          }
          synchronized (_surfaceHolder) {
            _panel.onDrawProgress(c,texte);
          }
        } finally {
          if (c != null) {
            _surfaceHolder.unlockCanvasAndPost(c);
          }
        }
      }
    }

    public void debut(){
      if (_run) {
        c = null;
        try {
          if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            c = _surfaceHolder.lockHardwareCanvas();
          } else {
            c = _surfaceHolder.lockCanvas();
          }
          synchronized (_surfaceHolder) {
            _panel.onDrawFirst(c,vide);
          }
        } finally {
          if (c != null) {
            _surfaceHolder.unlockCanvasAndPost(c);
          }
        }
      }
    }

    public void fin(){
      if (_run) {
        c = null;
        try {
          if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            c = _surfaceHolder.lockHardwareCanvas();
          } else {
            c = _surfaceHolder.lockCanvas();
          }
          synchronized (_surfaceHolder) {
            _panel.onDrawFinal(c);
          }
        } finally {
          if (c != null) {
            _surfaceHolder.unlockCanvasAndPost(c);
          }
        }
      }
    }

    @Override
    public void run() {
      while(!isrunning){
        try {
          Thread.sleep(500);
        } catch (InterruptedException e) {}
      }
      Random sss= Gallery.rand;
      for(int joz=0;joz<parta;joz++){
        if(mixpose[joz]==videsauve)
          vide=joz;
        int ss=sss.nextInt(4);
        switch(ss){
          case 1:
            parterre[joz]=Bitmap.createBitmap(rotimage1(parterre[joz]));
            break;
          case 2:
            parterre[joz]=Bitmap.createBitmap(rotimage2(parterre[joz]));
            break;
          case 3:
            parterre[joz]=Bitmap.createBitmap(rotimage3(parterre[joz]));
            break;
          default:
            break;
        }
        progres("Loading "+joz+"/"+parta);
      }
      infos("Menu : Rotate pieces","Search : Cheat");
      System.gc();
      Looper.prepare();
      mChildHandler = new Handler() {
        public void handleMessage(Message msg) {
          Bundle bunbdleb= (Bundle) msg.obj;
          int status=bunbdleb.getInt("status");
          if(status==69){
            String texte="Rotation of pieces ";
            if(rotage)
              texte+="enabled";
            else
              texte+="disabled";
            infos(texte,"");
          }
          else if(status==70){
            String texte="Cheating ";
            if(triche)
              texte+="enabled";
            else
              texte+="disabled";
            infos(texte,"");
          }
          else{
            if(!rotage){
              if(status==2){
                float[] position=bunbdleb.getFloatArray("posarray");
                float[] position0=bunbdleb.getFloatArray("posarray0");
                float posx=position[0]-(float)decalw;
                float posy=position[1]-(float)decalh;
                if(posx>=0.0f && posx<=(float)(supernw*wt)
                        && posy>=0.0f && posy<=(float)(supernh*ht)){
                  int x=(int)(Math.floor(posx/((float)wt)));
                  int y=(int)(Math.floor(posy/((float)ht)));
                  int videdebut=vide;
                  int videy,videx;
                  videy=vide;
                  videx=-1;
                  while(videy>=0){
                    videy-=supernh;
                    videx++;
                  }
                  videy+=supernh;
                  int diffix=x-videx;
                  int diffx=(int)Math.signum(diffix);
                  diffix=(int)(Math.abs(diffix));
                  int diffiy=y-videy;
                  int diffy=(int)Math.signum(diffiy);
                  diffiy=(int)(Math.abs(diffiy));
                  float dx=Math.abs(position[0]-position0[0]);
                  float dy=Math.abs(position[1]-position0[1]);
                  float[] glisse={0.0f,0.0f};
                  int[] glisseici;
                  if(dx>dy){
                    if(diffiy>0){
                      glisseici=new int[diffiy+1];
                      glisseici[0]=videdebut;
                      for(int pm=0;pm<diffiy;pm++){
                        videy+=diffy;
                        glisseici[pm+1]=videy+videx*supernh;
                        glisse=update(videx,videy);
                      }
                      videdebut=videy+videx*supernh;
                      updatetrace(glisse,glisseici);
                    }
                    if(diffix>0){
                      glisseici=new int[diffix+1];
                      glisseici[0]=videdebut;
                      for(int pm=0;pm<diffix;pm++){
                        videx+=diffx;
                        glisseici[pm+1]=videy+videx*supernh;
                        glisse=update(videx,videy);
                      }
                      updatetrace(glisse,glisseici);
                    }
                  }
                  else{
                    if(diffix>0){
                      glisseici=new int[diffix+1];
                      glisseici[0]=videdebut;
                      for(int pm=0;pm<diffix;pm++){
                        videx+=diffx;
                        glisseici[pm+1]=videy+videx*supernh;
                        glisse=update(videx,videy);
                      }
                      videdebut=videy+videx*supernh;
                      updatetrace(glisse,glisseici);
                    }
                    if(diffiy>0){
                      glisseici=new int[diffiy+1];
                      glisseici[0]=videdebut;
                      for(int pm=0;pm<diffiy;pm++){
                        videy+=diffy;
                        glisseici[pm+1]=videy+videx*supernh;
                        glisse=update(videx,videy);
                      }
                      updatetrace(glisse,glisseici);
                    }
                  }
                }
              }
              else if(status==0){
                fin();
              }
              else if(status==3){
                float[] position=bunbdleb.getFloatArray("posarray");
                float[] position2=bunbdleb.getFloatArray("posarray2");
                trich(position,position2);
              }else if(status==22){
                float[] position=bunbdleb.getFloatArray("posarray");
                float posx=position[0]-(float)decalw;
                float posy=position[1]-(float)decalh;
                if(posx>=0.0f && posx<=(float)(supernw*wt)
                        && posy>=0.0f && posy<=(float)(supernh*ht)){
                  int x=(int)(Math.floor(posx/((float)wt)));
                  int y=(int)(Math.floor(posy/((float)ht)));
                  int z=y+x*supernh;
                  parterre[mixpose[z]]=Bitmap.createBitmap(rotimage(parterre[mixpose[z]]));
                  debut();
                }
              }
              else if(status==8){
                debut();
              }
            }else{
              if(status==2){
                float[] position=bunbdleb.getFloatArray("posarray");
                float posx=position[0]-(float)decalw;
                float posy=position[1]-(float)decalh;
                if(posx>=0.0f && posx<=(float)(supernw*wt)
                        && posy>=0.0f && posy<=(float)(supernh*ht)){
                  int x=(int)(Math.floor(posx/((float)wt)));
                  int y=(int)(Math.floor(posy/((float)ht)));
                  int z=y+x*supernh;
                  parterre[mixpose[z]]=Bitmap.createBitmap(rotimage(parterre[mixpose[z]]));
                  debut();
                }
              }
            }
          }
        }
      };
      Looper.loop();
    }

    public float[] update(int x, int y){
      boolean okgo=false;
      int z=y+x*supernh;
      int fafa2;
      float[] glisse={0.0f,0.0f};
      float sach=(float)sacc;
      float gg0=((float)ht)/sach;
      float gg1=((float)wt)/sach;
      if(vide+1==z){
        int vz=z;
        while(vz>0)
          vz-=supernh;
        if(vz!=0)
          glisse[1]=gg0;
      }
      else if (vide-1==z){
        int vv=vide;
        while(vv>0)
          vv-=supernh;
        if(vv!=0)
          glisse[1]=-gg0;
      }
      else if (vide+supernh==z)
        glisse[0]=gg1;
      else if (vide-supernh==z)
        glisse[0]=-gg1;
      if(glisse[0]!=0.0f || glisse[1]!=0.0f){
        fafa2=mixpose[z];
        mixpose[z]=mixpose[vide];
        mixpose[vide]=fafa2;
        vide=z;
        okgo=true;
      }

      return glisse;
    }

    public void updatetrace(float[] glisse, int[] glisseici){
      votrecompteur++;
      float g0=glisse[0];
      float g1=glisse[1];
      glisse[0]=0;
      glisse[1]=0;
      for(int p=0;p<=sacc;p++){
        try {
          Thread.sleep(pooose);
        } catch (InterruptedException e) {}
        c = null;
        try {
          if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            c = _surfaceHolder.lockHardwareCanvas();
          } else {
            c = _surfaceHolder.lockCanvas();
          }
          synchronized (_surfaceHolder) {
            _panel.nDraw(c,glisse,compteur,votrecompteur,glisseici);
          }
        } finally {
          if (c != null) {
            _surfaceHolder.unlockCanvasAndPost(c);
          }
        }
        glisse[0]+=g0;
        glisse[1]+=g1;
      }
      boolean termine=true;
      for(int jo=0;jo<parta;jo++){
        if(mixpose[jo]!=jo)
          termine=false;
      }
      if(termine==true)
        terminetout(true,votrecompteur);
    }

    public void trich(float[] position,float[] position2){
      double posx=(double)position[0]-(double)decalw;
      double posy=(double)position[1]-(double)decalh;
      double posx2=(double)position2[0]-(double)decalw;
      double posy2=(double)position2[1]-(double)decalh;
      if(posx>=0.0 && posx<=(double)(supernw*wt)
              && posy>=0.0 && posy<=(double)(supernh*ht)
              && posx2>=0.0 && posx2<=(double)(supernw*wt)
              && posy2>=0.0 && posy2<=(double)(supernh*ht)){
        boolean okgo=false;
        int x=(int)(Math.floor(posx/((double)wt)));
        int y=(int)(Math.floor(posy/((double)ht)));
        int z=y+x*supernh;
        int x2=(int)(Math.floor(posx2/((double)wt)));
        int y2=(int)(Math.floor(posy2/((double)ht)));
        int z2=y2+x2*supernh;

        int fafa2;
        fafa2=mixpose[z];
        mixpose[z]=mixpose[z2];
        mixpose[z2]=fafa2;

        okgo=true;
        if (_run && okgo) {
          debut();
          boolean termine=true;
          for(int jo=0;jo<parta;jo++){
            if(mixpose[jo]!=jo)
              termine=false;
          }
          if(termine==true)
            terminetout(true,0);
        }
      }
    }
  }
  
  
  /**
   * This interface must be implemented by activities that contain this
   * fragment to allow an interaction in this fragment to be communicated
   * to the activity and potentially other fragments contained in that
   * activity.
   * <p>
   * See the Android Training lesson <a href=
   * "http://developer.android.com/training/basics/fragments/communicating.html"
   * >Communicating with Other Fragments</a> for more information.
   */
  
  public OnFragmentInteractionListener fragmentlistener;
  
  public interface OnFragmentInteractionListener {
    // TODO: Update argument type and name
    Integer onFragmentInteraction(Uri uri);
  }
  
  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    if (context instanceof OnFragmentInteractionListener) {
      fragmentlistener = (OnFragmentInteractionListener) context;
    } else {
      llog.d(TAG, context.toString() + " must implement OnFragmentInteractionListener");
    }
  }
  
  
}






















