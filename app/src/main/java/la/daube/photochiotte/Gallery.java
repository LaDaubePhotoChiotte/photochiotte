package la.daube.photochiotte;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.icu.util.ULocale;
import android.webkit.CookieManager;

import androidx.documentfile.provider.DocumentFile;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.ViewModel;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Pattern;


public class Gallery extends ViewModel {
  private static final String TAG = "YYYgal";
  public static final Pattern archivepattern = Pattern.compile("\\.(zip|cbz|cbr|7z)$", Pattern.CASE_INSENSITIVE);
  public static final Pattern imagepattern = Pattern.compile("\\.(jpe?g|png|bmp|tif?f|webp|ppm|pgm|raw|arw|cr2|nrw|k25|heif|heic)$", Pattern.CASE_INSENSITIVE);
  public static final String externalviewerpatterns = "pdf|eps|apk";//|txt|conf";
  public static final Pattern externalviewerpattern = Pattern.compile("\\.(" + externalviewerpatterns + ")$", Pattern.CASE_INSENSITIVE);
  public static final String collectionpatterns = "sel";
  public static final Pattern collectionpattern = Pattern.compile("\\.(" + collectionpatterns + ")$", Pattern.CASE_INSENSITIVE);
  public static final String audiopatterns = "mp3|flac|wa?v|ape|ogg|wma|aac|ac4|m3u|m4a";
  public static final Pattern audiopattern = Pattern.compile("\\.(" + audiopatterns + ")$", Pattern.CASE_INSENSITIVE);
  public static final String videoppatterns = "mpe?g|mkv|mp4|avi|wmv|x264|divx|mov|3gp|webm|m4v|m3u8|flv|ts|gif|vob";
  public static final Pattern videoppattern = Pattern.compile("\\.(" + videoppatterns + ")$", Pattern.CASE_INSENSITIVE);


  public final static int mediastorelookupmaxcount = 100000;
  public final static long maxpicturecount = 30;
  public final static float nombredecranenram = 4.0f;
  public final static int messagetimestay = 6000;
  public final static long polltimeoutdefault = 750L;
  public final static long polltimeoutfast = 40L; // 80 good
  public final static int maxstorelastvideoposition = 25;
  public final static float percentofpicturecenter = 0.30f;
  public final static float percentofvideoheightforprogressbarclick = 0.93f; // 83% c'est trop
  public final static int monitordeltatime = 1;

  public final static float graphwidthmax = 0.20f;
  public final static float settingswidthmax = 0.20f;

  public final static SecureRandom rand = new SecureRandom();

  public final static String howtomessage =
                    "menu     3 fingers / top-left corner\n"
                  + "zoom     pinch 2 fingers            \n"
                  + "next     bottom right / pic right   \n"
                  + "prev     bottom left  / pic left    \n"
                  + "zoom mode        pic center         \n"
                  + "info             4 fingers          \n"
                  + "big pic only     5 fingers          \n"
          ;
  public final static String howtomessagewatch =
            "menu     2 fingers                  \n"
          + "zoom     pinch 2 fingers            \n"
          + "next     bottom right / pic right   \n"
          + "prev     bottom left  / pic left    \n"
          + "zoom mode        pic center         \n"
          + "back     2 fingers top+left or top+right  \n"
          + "exit     back twice                 \n"
      ;
  public final static String howtomessagetv =
                            "0    next  media     \n" +
                            " 1   zoom  in         \n" +
                            " 2         out        \n" +
                            " 3         mode       \n" +
                            "4    music previous  \n" +
                            "5          next      \n" +
                            "6          play/pause\n" +
                            " 7         stop      \n" +
                            " 8         random    \n" +
                            " 9         linear    \n"
          ;
  public final static String virtualCollectionFolder = "/collection/";
  public final static String virtualBookmarkFolder = virtualCollectionFolder + "bookmarks/";
  public final static String virtualAppFolder = virtualCollectionFolder + "apps/";
  public final static String virtualWidgetFolder = virtualCollectionFolder + "widgets/";

  public final static int filterrotate = 0;
  public final static int filterhflipvflip = 1;
  public final static int filtervflip = 2;
  public final static int filterb = 5;
  public final static int filterc = 6;
  public final static int filters = 7;
  public final static int filterg = 8;
  public final static int filterw = 9;
  public final static int filterbcsgwi = filterb;
  public final static int filterbcsgwf = filterw;
  public final static int filterreset = 10;
  public final static int filteradvanced = 14;
  public final static String[] filterlist = new String[]{
          "rotate", "transpose=1", "rotate by 90°",
          "hflip", "hflip", "flip horizontally",
          "vflip", "vflip", "flip vertically",
          "brighten", "curves=interp=natural:all=0/0 0.06/0.2 0.12/0.3 1/1", "brighten dark levels\n(x/y 0.1/0.2 0.2/0.3)\noriginal curve 0/0 1/1\nnatural,pchip\nmaster,r,g,b,all",
          "sharpen", "unsharp=5:5:1.0:5:5:0.0", "sharpen or blur\nluma lx:ly [3:23] :la blur[-1.5:1.5]sharpen\nchroma cx:cy:ca\nalpha ax:ax:aa",
          "brightness", "eq=brightness=0.0", "[-1.0:1.0]",
          "contrast", "eq=contrast=1.0", "[-1000.0:1000.0]",
          "saturation", "eq=saturation=1.0", "[0.0:3.0]",
          "gamma", "eq=gamma=1.0", "[0.1:10.0]",
          "gamma_weight", "eq=gamma_weight=1.0", "[0.0:1.0]\non bright image areas\n1.0 leaves it at its full strength",
          "", "", "click again to create", // 10 this line is overriden +/- reset
          "scale", "scale=w=iw*0.4:h=-1:flags=lanczos", "0 output=input\n-1 keep aspect ratio\niw,ih ow,oh\nbilinear, bicubic, experimental, neighbor,\narea, bicublin, gauss, sinc, lanczos, spline",
          "crop width", "crop=w=0:x=0", "iw,ih\now,oh cropped output\nPlease focus wanted area\nto crop its width.",
          "crop height", "crop=h=0:y=0", "iw,ih\now,oh cropped output\nPlease focus wanted area\nto crop its height.",
          "", "", "click again to create", // 14 this line is overriden advanced
          "lutyuv", "lutyuv=y=(maxval+minval-val)*0.4", "y,u,v=negval\nlutrgb a,r,g,b=negval\nval,minval,maxval,gammaval(0.5)",
          "lighter", "curves=preset=lighter", "",
          "darker", "curves=preset=darker", "",
          "increase_contrast", "curves=preset=increase_contrast", "",
          "linear_contrast", "curves=preset=linear_contrast", "",
          "medium_contrast", "curves=preset=medium_contrast", "",
          "strong_contrast", "curves=preset=strong_contrast", "",
          "color_negative", "curves=preset=color_negative", "",
          "negative", "curves=preset=negative", "",
          "cross_process", "curves=preset=cross_process", "",
          "vintage", "curves=preset=vintage", "",
          "", "", "click again to create",
          "edgedetect", "edgedetect=mode=wires:low=0.1:high=0.2", "low [0.0:1.0] < high [0.0:1.0]\nmode wires,colormix,canny",
          "negate", "negate", "components=y,u,v,a,r,g,b\nnegate_alpha=1,0",
          "sharpen", "convolution=0 -1 0 -1 5 -1 0 -1 0:0 -1 0 -1 5 -1 0 -1 0:0 -1 0 -1 5 -1 0 -1 0:0 -1 0 -1 5 -1 0 -1 0", "",
          "blur", "convolution=1 1 1 1 1 1 1 1 1:1 1 1 1 1 1 1 1 1:1 1 1 1 1 1 1 1 1:1 1 1 1 1 1 1 1 1:1/9:1/9:1/9:1/9", "",
          "edgeenhance", "convolution=0 0 0 -1 1 0 0 0 0:0 0 0 -1 1 0 0 0 0:0 0 0 -1 1 0 0 0 0:0 0 0 -1 1 0 0 0 0:5:1:1:1:0:128:128:128", "edge enhance",
          "edgedetect", "convolution=0 1 0 1 -4 1 0 1 0:0 1 0 1 -4 1 0 1 0:0 1 0 1 -4 1 0 1 0:0 1 0 1 -4 1 0 1 0:5:5:5:1:0:128:128:128", "edge detect",
          "lap edgedetect", "convolution=1 1 1 1 -8 1 1 1 1:1 1 1 1 -8 1 1 1 1:1 1 1 1 -8 1 1 1 1:1 1 1 1 -8 1 1 1 1:5:5:5:1:0:128:128:0", "laplacian edge detector with diagonals",
          "emboss", "convolution=-2 -1 0 -1 1 1 0 1 2:-2 -1 0 -1 1 1 0 1 2:-2 -1 0 -1 1 1 0 1 2:-2 -1 0 -1 1 1 0 1 2", "",
          "rotate", "rotate=45*(PI/180)", "",
          "boxblur", "boxblur=luma_radius=2:luma_power=2", "w,h\nradius < min(w,h)/2\npower : how many times boxblur is applied",
          "exposure", "exposure=-0.3:black=-0.1", "exposure [-3.0:3.0] EV\nblack [-1.0:1.0]",
          "histogram", "histogram", "",
          "", "", "click again to create",
  };
  public static backgroundService backgroundService = null;
  public static boolean isMyServiceRunning(Class<?> serviceClass, Context context) {
    if (context != null) {
      ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
      for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
        if (serviceClass.getName().equals(service.service.getClassName())) {
          backgroundService = backgroundService.getInstance();
          return true;
        }
      }
    }
    backgroundService = null;
    return false;
  }

  public final static String packagename = BuildConfig.APPLICATION_ID;
  public final static String broadcastname = BuildConfig.APPLICATION_ID + ".broadcast";

  public final static String collectionextension = ".sel";

  public final static int lockscreencalendarwidget = 0;
  public final static int lockscreencountdownwidget = 1;
  public final static int lockscreenmonitorwidget = 2;
  public final static int screenmonitorwidget = 3;
  public final static String lockscreencalendarwidgetset = "x=5% y=5% w=12% h=60%    3 months";
  public final static String lockscreencalendarwidgetsets = "or    5 5 12 60   3";
  public final static Pattern lockscreencalendarwidgetpattern = Pattern.compile("(\\d+).*?(\\d+).*?(\\d+).*?(\\d+).*?(\\d+)");
  public final static String lockscreencountdownwidgetset = "x=85% y=5% w=10% h=10%    18/04/2025   spiral/grid/bar   name ends with slash/";
  public final static String lockscreencountdownwidgetsets = "or    85 5 10 10    18042025  s   release";
  public final static Pattern lockscreencountdownwidgetsetpattern = Pattern.compile("(\\d+).*?(\\d+).*?(\\d+).*?(\\d+).*?(\\d\\d).*?(\\d\\d).*?(\\d+).*?(spiral|grid|linear|s|g|l).*? +([^/]+)");
  public final static String lockscreenmonitorwidgetset = "x=15% y=5% w=15% h=90%   0 maxtime";
  public final static String lockscreenmonitorwidgetsets = "or    15 5 15 90   0";
  public final static Pattern lockscreenmonitorwidgetsetpattern = Pattern.compile("(\\d+).*?(\\d+).*?(\\d+).*?(\\d+).*?(\\d+)");

  public volatile static boolean isConnectedToInternet = true;
  public volatile static int isConnectedToInternetRetry = 0;
  public final static long isConnectedToInternetRetryDeltaTime = 10000;
  public final static int isConnectedToInternetRetryMax = 1;
  public volatile static int debugi = BuildConfig.DEBUG ? 1 : 0;

  public static final Pattern maybeonline = Pattern.compile("^(https?|s?ftp)://", Pattern.CASE_INSENSITIVE);
  public static boolean couldBeOnline(String str){
    if (maybeonline.matcher(str).find())
      return true;
    else
      return false;
  }
  public static String ifUrlDecode(String path) {
    if (couldBeOnline(path)) {
      try {
        return URLDecoder.decode(path, "utf-8");
      } catch (UnsupportedEncodingException e) {
        e.printStackTrace();
      }
    }
    return path;
  }
  public static String urlEncode(String path) {
    try {
      path = URLEncoder.encode(path, "utf-8")
          .replaceAll("%2F", "/")
          .replaceAll("%3A", ":")
          .replaceAll("\\+", "%20")
      ;
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    }
    return path;
  }

  public static int actionopendocumentreeasked = -1;

  public final LinkedBlockingQueue<String[]> commandethreaddatabase = new LinkedBlockingQueue<>();
  public final LinkedBlockingQueue<String[]> commandethreadminiature = new LinkedBlockingQueue<>();
  public final LinkedBlockingQueue<String[]> commandeminiaturethreadqueue = new LinkedBlockingQueue<>();
  public final LinkedBlockingQueue<String[]> commandebigimagethreadqueue = new LinkedBlockingQueue<>();
  public final LinkedBlockingQueue<String[]> commandethreadbrowser = new LinkedBlockingQueue<>();
  public final LinkedBlockingQueue<String[]> commandethreaddrawing = new LinkedBlockingQueue<>();
  public final LinkedBlockingQueue<String[]> commandethreadvideo = new LinkedBlockingQueue<>();

  public volatile long filmstripanimatetime = 4000L;
  public volatile boolean searchMusicAskedFromWidget = false;
  public volatile String searchMusicAskedFromWidgetR = null;
  public volatile boolean showmenuprevnextbutton = true;
  public volatile boolean messagelocknext = false;
  public volatile boolean optionautostartboot = false;
  public volatile boolean optionhardwareaccelerationb = true;
  public volatile boolean optionaudioplayeractive = false;
  public volatile boolean optiondiscoverserviceactive = false;
  public volatile boolean optiondecodelibextractor = false;
  public volatile boolean optionhidesurface = false;
  public volatile boolean killonexit = false;
  public volatile boolean thereIsAnUpdatableFolder = false;
  public volatile boolean startVideoClearBrowserSurfaceTransparent = false;
  public final static int onlinemediaremove = 0;
  public final static int onlinemediaautoloadatstartup = 1;
  public final static int onlinemediaupdateapachefolderlist = 2;
  public volatile int onlinemediabutton = onlinemediaremove;
  public volatile boolean autoloadlastonlinemediaviewed = true;
  public volatile String musiclastsearch = null;
  public volatile String lastbrowsedonline = null;
  public void forgetlastbrowsedonline(){
    if (lastbrowsedonline != null) {
      SharedPreferences.Editor edit = preferences.edit();
      if (preferences.contains("lastbrowsedonline"))
        edit.remove("lastbrowsedonline");
      if (preferences.contains("lastbrowsedonlined"))
        edit.remove("lastbrowsedonlined");
      if (preferences.contains("lastbrowsedonlinef"))
        edit.remove("lastbrowsedonlinef");
      if (preferences.contains("lastbrowsedonlinez"))
        edit.remove("lastbrowsedonlinez");
      edit.apply();
      lastbrowsedonline = null;
    }
  }

  public AppDatabase db = null;
  public SharedPreferences preferences;
  public Context activitycontext = null;

  public final ReentrantLock cookielock = new ReentrantLock();
  public final ArrayList<String> cookies = null;
  public CookieManager cookieManager = null;
  private void initcookies() {
    if (cookies == null) {
    }
  }
  private void initcookiemanager() {
    if (cookieManager == null) {
      cookieManager = CookieManager.getInstance();
      cookieManager.setAcceptCookie(true);
    }
  }
  public void addcookie(String name, String value) {
    cookielock.lock();
    if (iswatch) {
      initcookies();
    } else {
      initcookiemanager();
      if (cookieManager != null)
        cookieManager.setCookie(name, value);
    }
    cookielock.unlock();
  }
  public String getcookie(String url) {
    cookielock.lock();
    String cookies = null;
    if (iswatch) {
      initcookies();
    } else {
      initcookiemanager();
      if (cookieManager != null)
        cookies = cookieManager.getCookie(url);
    }
    cookielock.unlock();
    return cookies;
  }

  public volatile ArrayList<String> basefolder = new ArrayList<>();
  public volatile ArrayList<String> surveyfolder = new ArrayList<>();
  public volatile ArrayList<String> autorescan = new ArrayList<>();

  public volatile boolean iswatch = false;
  public volatile boolean isauto = false;

  public volatile String searchtor = null;
  public volatile String bootstraptor = "https://codeberg.org/LaDaube/PhotoChiotte/raw/branch/master/torbootstrap.txt";
  public volatile String runcmddownloadurl = "http://media:8083/public/apps/PhotoChiotteBdbg.apk";
  public volatile String runcmd = "bc -l <<< 0";

  public volatile String odestinationfolder = "fghfghf";

  public volatile boolean alreadymessagedexpandsplitscreen = false;
  public volatile boolean alreadymessagedthumbnailselection = false;
  public volatile long filmstripreftime = 1000L;

  public volatile int optiondestinationmemory = -1;
  public volatile boolean optionshowappsdisplay = false;
  public volatile boolean optionshowappsdisplays = false;
  public volatile boolean optionshowappsdisplaya = false;
  public volatile boolean optionshowappsdisplayp = false;
  public volatile boolean optionshowwidgetsdisplay = false;
  public volatile boolean optionshowbookmarkeddisplay = false;
  public volatile boolean optionshowbookmarkedlist = true;
  public volatile String bookmarkshowthisfilewhenfolderready = null;
  public volatile boolean showrandomfilewhenfolderready = false;

  public volatile boolean keypadnavactive = false;
  public volatile boolean databasecurrentlyremovingfiles = false;
  public volatile boolean redrawsplitviewbitmapm = false;

  public volatile boolean usesaf = false;
  public volatile boolean confirmswitch = false;

  public volatile float refreshrate = -1.0f;
  public volatile float buttontextsize = 1.0f;

  public volatile int weLastClickedDPadUp = -1;

  public volatile String searchthis = null;
  public volatile String createnewfolder = null;
  public volatile String renameprefix = null;

  public volatile int uploadthreadsuccess = 0;
  public volatile boolean uploadthreadbailalreadypresent = false;
  public volatile int uploadthreadbailalreadypresentcount = 0;
  public volatile int uploadthreadfail = 0;
  public volatile int uploadthreadtotal = 0;
  public volatile double uploadthreadbytes = 0;
  public volatile double uploadthreadtime = System.currentTimeMillis();
  public volatile double uploadthreadbytesin = 0;
  public volatile double uploadthreadtimein = System.currentTimeMillis();
  public volatile String uploadthreadstatus = null;

  public volatile boolean copyoverwritenosync = false;

  public volatile DocumentFile pickedDir = null;

  public volatile boolean deactivateactivitykeydown = false;
  public volatile int currentselectedfragment = 0; // >= 0

  public volatile float settingsYmin = 1.0f;

  public volatile int nombreonline = 0;
  public volatile int nombredossierbookmarked = 0;
  public volatile int nombredestinationsaved = 0;
  public volatile int miniaturevideo = 1;

  public final static int videohwaamodesw = 0;
  public final static int videohwaamodehw = 1;
  public final static int videohwaamodehwp = 2;

  public final static int videohwaamodehwpp = 3;

  public final static int videohwaamodensw = 4;
  public final static int videohwaamodenhw = 5;
  public final static int videohwaamodenhwp = 6;

  public final static int videohwaamodevksw = 7;
  public final static int videohwaamodevkhw = 8;
  public final static int videohwaamodevkhwp = 9;

  public final static int videohwaamodevknsw = 10;
  public final static int videohwaamodevknhw = 11;
  public final static int videohwaamodevknhwp = 12;

  public final static int videohwaamodecount = 7;
  public volatile int videohardwaremode = videohwaamodenhw;
  public volatile boolean videoaaspectresize = false;
  public volatile boolean videomute = false;
  public volatile boolean videoaudiotrack = false;
  public volatile boolean videoqueue = false;
  public volatile boolean videoscale = false;
  public volatile boolean videoperformance = false;
  public volatile int videoprofile = 0;
  public volatile int videofpsasked = 0;
  public volatile boolean videofpsor = false;
  public volatile boolean videohidebrowser = false;
  public volatile boolean videofastdecode = false;
  public volatile int videovolume = 100;
  public volatile boolean videoaudioweightroot1 = true;
  public volatile boolean videoaudioweightlevel1 = false;
  public volatile int videoaudiovolume = 100;
  public volatile int musicrandminreplay = 0;
  public volatile int videosubfontsize = 55;
  public volatile int videoseekmode = 2;
  public final static int videoloopNoLoop = 0;
  public final static int videoloopLoop = 1;
  public final static int videoloopSequence = 2;
  public final static int videoloopl = 3;
  public volatile int videoloop = videoloopNoLoop;
  public volatile int videosyncaudio = 0;
  public volatile int videovthreadcount = 0;
  public volatile int videoathreadcount = 0;
  public volatile int videocachesize = 200;
  public volatile float videoaudiodelay = 0.0f;
  public volatile float videolastaudiodelay = 0.0f;
  public volatile float videolastsubtitledelay = 0.0f;

  public volatile boolean forceatvmode = false;

  public volatile String dossiercache = null;
  public static String staticdossierbitmaptemp = "/files/bitmaptemp/";
  public static String staticdossierconfig = "/files/config/";
  public static String staticdossierminiature = "/cache/miniatures/";
  public static String staticdossierdessin = "/files/documents/";
  public static String staticdossiercollections = "/files/collections/";
  public static String staticdossierscreenshot = "/files/screenshots/";
  public volatile String dossierbitmaptemp = null;
  public volatile String dossierconfig = null;
  public volatile String dossierminiature = null;
  public volatile String dossierdessin = null;
  public volatile String dossiercollections = null;
  public volatile String dossierscreenshot = null;

  public volatile String lastmessage = "";
  public volatile long lastmessagetime = 0;

  public volatile int gamecolumns = 6;
  public volatile int gamelines = 10;

  public volatile float deltacoin = 0.8f;
  public volatile float zoomconfortratio = 0.8f;
  public volatile int thumbsize = 1;
  public volatile int maxnumberthumsincache = 50;
  public volatile int numberthumsincache = 0;

  public volatile String showonlinesite = null;
  public volatile String showthisfolder = null;
  public volatile String showthisfile = null;
  public volatile boolean showthisfilelockjump = false;

  public volatile String googlerecaptchacookie = null;

  public volatile int screenshotdelay = 10;
  public volatile int screenrecordlength = 0;
  public volatile int screenrecordlengthu = 1;




  public volatile List<AppInfo> allappinfo = new ArrayList<>();
  public PackageManager packageManager = null;




  private Thread threadBrowser = null;
  private Thread threadVideo = null;
  private Thread threadDrawing = null;
  private Thread threadMiniature = null;
  private Thread threadDatabase = null;

  public volatile long threadbrowserrunninglastpoll = System.currentTimeMillis();
  public volatile long threadvideorunninglastpoll = System.currentTimeMillis();
  public final static int miniaturethreadl = 8;
  public volatile boolean[] miniaturethreadrunning = new boolean[miniaturethreadl];
  public final static int bigimagethreadl = 3;
  public volatile boolean[] bigimagethreadrunning = new boolean[bigimagethreadl];
  public volatile boolean uploadthreadrunning = false;
  public volatile boolean threadvideoplaylistrunning = false;

  public volatile boolean threaddrawingrunning = false;
  public volatile boolean threadbrowserrunning = false;
  public volatile boolean threadvideorunning = false;
  public volatile boolean threadminiaturerunning = false;
  public volatile boolean threaddatabaserunning = false;

  public volatile boolean threaddrawingon = false;
  public volatile boolean threadbrowseron = false;
  public volatile boolean threadvideoon = false;
  public volatile boolean threadminiatureon = false;
  public volatile boolean threaddatabaseon = false;

  public boolean threadbrowserstart() {
    threadbrowseron = true;
    try {
      if (threadBrowser == null) {
        threadBrowser = new Thread(new ThreadBrowser(this));
        threadBrowser.start();
      } else if (!threadBrowser.isAlive()) {
        threadBrowser = new Thread(new ThreadBrowser(this));
        threadBrowser.start();
      }
    } catch (Exception e) {
      llog.d(TAG, "error thread start");
      e.printStackTrace();
    }
    return true;
  }
  public boolean threadvideostart(){
    threadvideoon = true;
    try {
      if (threadVideo == null) {
        threadVideo = new Thread(new ThreadVideo(this));
        threadVideo.start();
      } else if (!threadVideo.isAlive()) {
        threadVideo = new Thread(new ThreadVideo(this));
        threadVideo.start();
      }
    } catch (Exception e) {
      llog.d(TAG, "error thread start");
      e.printStackTrace();
    }
    return true;
  }
  public boolean threadminiaturestart(){
    threadminiatureon = true;
    try {
      if (threadMiniature == null) {
        threadMiniature = new Thread(new ThreadMiniature(this));
        threadMiniature.start();
      } else if (!threadMiniature.isAlive()) {
        threadMiniature = new Thread(new ThreadMiniature(this));
        threadMiniature.start();
      }
    } catch (Exception e) {
      llog.d(TAG, "error thread start");
      e.printStackTrace();
    }
    return true;
  }
  public boolean threaddatabasestart(){
    threaddatabaseon = true;
    try {
      if (threadDatabase == null) {
        threadDatabase = new Thread(new ThreadDatabase(this));
        threadDatabase.start();
      } else if (!threadDatabase.isAlive()) {
        threadDatabase = new Thread(new ThreadDatabase(this));
        threadDatabase.start();
      }
    } catch (Exception e) {
      llog.d(TAG, "error thread start");
      e.printStackTrace();
    }
    return true;
  }
  public boolean threaddrawingstart(){
    threaddrawingon = true;
    try {
      if (threadDrawing == null) {
        threadDrawing = new Thread(new ThreadDrawing(this));
        threadDrawing.start();
      } else if (!threadDrawing.isAlive()) {
        threadDrawing = new Thread(new ThreadDrawing(this));
        threadDrawing.start();
      }
    } catch (Exception e) {
      llog.d(TAG, "error thread start");
      e.printStackTrace();
    }
    return true;
  }

  public void closethreads(boolean allthreads){
    threaddrawingon = false;
    threadbrowseron = false;
    threadvideoon = false;
    threadminiatureon = false;
    threaddatabaseon = false;
    try {
      commandethreadbrowser.put(new String[]{"-1", "quit"});
      commandethreadvideo.put(new String[]{"-1", "quit"});
      commandethreaddrawing.put(new String[]{"-1", "quit"});
      commandethreaddatabase.put(new String[]{"quit"});
      commandethreadminiature.put(new String[]{"quit"});
      for (int i = 0; i < miniaturethreadl; i++)
        commandeminiaturethreadqueue.put(new String[0]);
      for (int i = 0; i < bigimagethreadl; i++)
        commandebigimagethreadqueue.put(new String[0]);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    new Thread(new Runnable() {
      @Override
      public void run() {
        boolean fullclose = true;
        int k;
        final int kmax = 200000;
        for (k = 0 ; k < kmax ; k++) {
          if (threadbrowseron || threadvideoon || threadminiatureon || threaddatabaseon || threaddrawingon) {
            llog.d(TAG, "        closethreads threadbrowseron bail out termination");
            fullclose = false;
            for (int i = 0; i < miniaturethreadl; i++)
              if (miniaturethreadrunning[i])
                commandeminiaturethreadqueue.clear(); // on annule le shutdown après la tâche finie
            for (int i = 0; i < bigimagethreadl; i++)
              if (bigimagethreadrunning[i])
                commandebigimagethreadqueue.clear();
            break;
          }
          boolean attend = false;
          if (uploadthreadrunning)
            attend = true;
          for (int i = 0; i < miniaturethreadl; i++)
            if (miniaturethreadrunning[i])
              attend = true;
          for (int i = 0; i < bigimagethreadl; i++)
            if (bigimagethreadrunning[i])
              attend = true;
          if (threadbrowserrunning
              || threadvideorunning
              || threaddrawingrunning
              || threadminiaturerunning
              || threaddatabaserunning
              || attend
          ) {
            if ((k % 100) == 0)
              llog.d(TAG, "still sleeping " + k + " : " + attend + " b" + threadbrowserrunning + " v" + threadvideorunning + " w" + threaddrawingrunning + " d" + threaddatabaserunning + " m" + threadminiaturerunning);
            try {
              Thread.sleep(30);
            } catch (InterruptedException e) {
            }
          } else {
            break;
          }
        }
        if (k >= kmax)
          fullclose = false;
        if (fullclose) { // otherwise don't we still need the collections and the gallery
          llog.d(TAG, "        closethreads fullclose");
          if (threadDrawing != null) {
            if (threadDrawing.isAlive())
              threadDrawing.interrupt();
            threadDrawing = null;
          }
          if (threadBrowser != null) {
            if (threadBrowser.isAlive())
              threadBrowser.interrupt();
            threadBrowser = null;
          }
          if (threadVideo != null) {
            if (threadVideo.isAlive())
              threadVideo.interrupt();
            threadVideo = null;
          }
          if (threadMiniature != null) {
            if (threadMiniature.isAlive())
              threadMiniature.interrupt();
            threadMiniature = null;
          }
          if (threadDatabase != null) {
            if (threadDatabase.isAlive())
              threadDatabase.interrupt();
            threadDatabase = null;
          }
          int collectionsl = collections.size() - 1;
          for (int i = collectionsl; i >= 0; i--) {
            if (collections.get(i).address == null) {
              llog.d(TAG, i + " remove cleared collection");
              collections.remove(i);
            }
          }
          /*if (availCollectionsOnDisk != null) {
            collectionsl = availCollectionsOnDisk.size() - 1;
            for (int i = collectionsl; i >= 0; i--) {
              if (availCollectionsOnDisk.get(i).length() == 0) {
                llog.d(TAG, i + " remove cleared availCollectionsOnDisk");
                availCollectionsOnDisk.remove(i);
              }
            }
          }
          collectionsl = currCollectionOrdnerList.size() - 1;
          for (int i = collectionsl; i >= 0; i--) {
            if (currCollectionOrdnerList.get(i).length() == 0) {
              llog.d(TAG, i + " remove cleared currCollectionOrdnerList");
              currCollectionOrdnerList.remove(i);
            }
          }*/
          int surfl = surf.size() - 1;
          currentselectedfragment = 0;
          for (int i = surfl; i >= 0; i--) {
            if (surf.get(i).fragmenttype == Surf.FRAGMENT_TYPE_NONE) {
              llog.d(TAG, i + " remove cleared surf");
              surf.remove(i);
            } else {
              break;
            }
          }
          //deleteRecursive(new File(dossiercache + unzipped));
          /*if (tempBitmap != null)
            if (!tempBitmap.isRecycled())
              tempBitmap.recycle();
          tempBitmap = null;*/
          if (allthreads) {
            for (int i = 0; i < surf.size(); i++) {
              removesurf(i, true);
            }
          }
          commandethreaddrawing.clear();
          commandethreadbrowser.clear();
          commandethreadvideo.clear();
          commandethreaddatabase.clear();
          commandethreadminiature.clear();
          commandeminiaturethreadqueue.clear();
          commandebigimagethreadqueue.clear();
          threadDrawing = null;
          threadDatabase = null;
          threadMiniature = null;
          threadVideo = null;
          threadBrowser = null;
          //getFileFreeAllBitmapsFromFolders(false);
          //clearbitmaps();
          if (allthreads && killonexit)
            android.os.Process.killProcess(android.os.Process.myPid());
        }
      }
    }).start();
  }

  public void removesurf(int myid, boolean unknown){
    Surf mysurf = surf.get(myid);
    mysurf.idle = true;
    mysurf.myscreenwidth = -1;
    mysurf.myscreenheight = -1;
    mysurf.mywidth = -1;
    mysurf.myheight = -1;
    mysurf.fragmenttype = Surf.FRAGMENT_TYPE_NONE;
    if (mysurf.drawtoptempbitmap != null) {
      if(!mysurf.drawtoptempbitmap.isRecycled()){
        mysurf.drawtoptempbitmap.recycle();
        mysurf.drawtoptempbitmap = null;
      }
    }
    if (mysurf.drawpilebitmap != null) {
      if(!mysurf.drawpilebitmap.isRecycled()){
        mysurf.drawpilebitmap.recycle();
        mysurf.drawpilebitmap = null;
      }
    }
    if (mysurf.touslesdossierssplitviewbitmap != null) {
      if(!mysurf.touslesdossierssplitviewbitmap.isRecycled()) {
        mysurf.touslesdossierssplitviewbitmap.recycle();
        mysurf.touslesdossierssplitviewbitmap = null;
      }
    }
    mysurf.removekeysfromcache();
    mysurf.removefromcache();
    //mysurf.foldoptions(); non sinon ideoaskclose = true est false
    /*
    List<String> popthese = new ArrayList<>();
    for (String cached : mysurf.cachedbitmap.keySet()) {
        if (!mysurf.cachedbitmap.get(cached).isRecycled()) {
          mysurf.cachedbitmap.get(cached).recycle();
          popthese.add(cached);
        }
    }
    for (String cached : popthese) {
      //llog.d(TAG, "remove from cache " + cached);
      mysurf.cachedbitmap.remove(cached);
      mysurf.cachedbitmapnfo.remove(cached);
    }
    */
  }



  public static void saveStringsToFile(ArrayList<String> data, File file, boolean append){
    int datal = data.size();
    FileWriter fos;
    try{
      fos = new FileWriter(file, append);
      for (int i = 0 ; i < datal ; i++)
        fos.write(data.get(i) + "\n");
      fos.flush();
      fos.close();
    } catch (IOException e) {
      llog.d(TAG, "error writing to file");
      e.printStackTrace();
    }
  }
  public final static String filelisttxt = "filelist.txt";
  public boolean updateApacheFolderList(String onaffichecesiteinternet) {
    folderlist.clear();
    folderliststartt = System.currentTimeMillis();
    String baselink = onaffichecesiteinternet.replaceAll("/[^/]+\\.txt$", "/");
    String filename = onaffichecesiteinternet.replaceAll("^.*/", "");
    if (baselink.length() <= 0 || filename.length() <= 0) {
      llog.d(TAG, "error name " + onaffichecesiteinternet);
      return false;
    }
    scanApacheForFolders(baselink, baselink.length(), baselink);
    int folderlistl = folderlist.size();
    boolean success = false;
    if (folderlistl > 0) {
      folderlist.sort(String.CASE_INSENSITIVE_ORDER);
      /*for (int i = 0 ; i < folderlist.size() ; i++) {
        llog.d(TAG, " " + folderlist.get(i));
      }*/
      File file = new File(dossierminiature + filelisttxt);
      saveStringsToFile(folderlist, file, false);
      folderlist.clear();
      success = InternetSession.putApache(this, file, onaffichecesiteinternet, true, null);
      file.delete();
    }
    if (success)
      message(folderlistl + " folders found.\nSuccessfully updated\n" + onaffichecesiteinternet);
    else
      message(folderlistl + " folders found.\nFailed to update\n" + onaffichecesiteinternet);
    return success;
  }
  public long folderliststartt = System.currentTimeMillis();
  public ArrayList<String> folderlist = new ArrayList<>();
  public void scanApacheForFolders(String baselink, int baselinkl, String onaffichecesiteinternet) {
    long newt = System.currentTimeMillis();
    if (newt - folderliststartt > 1000) {
      folderliststartt = newt;
      message(folderlist.size() + " folders found so far...");
    }
    List<Media> listmediaaddd = InternetSession.getApache(onaffichecesiteinternet, false);
    if (listmediaaddd == null) {
      llog.d(TAG, "no connection or error apache " + onaffichecesiteinternet);
    } else if (listmediaaddd.size() > 0) {
      int mediacount = 0;
      for (int i = 0; i < listmediaaddd.size(); i++) {
        Media media = listmediaaddd.get(i);
        if (media.isALinkThatCreatesAnOrdner) {
          //llog.d(TAG, " d " + media.address);
          scanApacheForFolders(baselink, baselinkl, media.address);
        } else {
          //llog.d(TAG, "   " + media.address);
          mediacount += 1;
        }
      }
      if (mediacount > 0) {
        String save = "./" + onaffichecesiteinternet.substring(baselinkl);
        try {
          save = URLDecoder.decode(save, "utf-8");
        } catch (UnsupportedEncodingException e) {
          e.printStackTrace();
        }
        folderlist.add(save);
        //llog.d(TAG, " + " + onaffichecesiteinternet + " " + mediacount);
      }
      listmediaaddd.clear();
    }
  }



  public volatile ArrayList<Collection> collections = new ArrayList<>();

  private final ReentrantLock availlock = new ReentrantLock();
  private ArrayList<String> availCollectionsOnDisk = null;
  public volatile int availCollectionsOnDiskl = -1;
  public void setAvailCollectionsOnDisk(ArrayList<String> newarraylist) {
    availlock.lock();
    if (availCollectionsOnDisk == null)
      availCollectionsOnDisk = new ArrayList<>();
    availCollectionsOnDisk.clear();
    if (newarraylist != null) {
      int newarraylistl = newarraylist.size();
      for (int i = 0; i < newarraylistl ; i++) {
        availCollectionsOnDisk.add(newarraylist.get(i));
      }
    }
    availCollectionsOnDiskl = availCollectionsOnDisk.size();
    availlock.unlock();
    if (newarraylist != null)
      newarraylist.clear();
  }
  public void addAvailCollectionsOnDisk(String newarrayelement) {
    availlock.lock();
    if (availCollectionsOnDisk == null)
      availCollectionsOnDisk = new ArrayList<>();
    if (newarrayelement != null) {
      availCollectionsOnDisk.add(newarrayelement);
    }
    availCollectionsOnDiskl = availCollectionsOnDisk.size();
    availlock.unlock();
  }
  public String getAvailCollectionsOnDiskI(int i) {
    String returned = "";
    availlock.lock();
    if (i >= 0 && i < availCollectionsOnDisk.size()) {
      returned = availCollectionsOnDisk.get(i);
    }
    availlock.unlock();
    return returned;
  }

  private final ReentrantLock currlock = new ReentrantLock();
  public volatile ArrayList<Ordner> currCollectionGallery = null;
  public volatile String currCollectionAddress = null;
  public volatile String currCollectionPrintName = null;
  public volatile int currCollectionSelectedCount = 0;
  public volatile int currCollectionAddToFolderi = 0;
  private final ArrayList<String> currCollectionOrdnerList = new ArrayList<>();
  public volatile int currCollectionOrdnerListl = 0;
  public void setCurrCollectionOrdnerList(ArrayList<String> newarraylist) {
    currlock.lock();
    currCollectionOrdnerList.clear();
    if (newarraylist != null) {
      int newarraylistl = newarraylist.size();
      for (int i = 0; i < newarraylistl ; i++) {
        currCollectionOrdnerList.add(newarraylist.get(i));
        //llog.d(TAG, "setCurrCollectionOrdnerList " + i + " : " + newarraylist.get(i));
      }
    }
    currCollectionOrdnerListl = currCollectionOrdnerList.size();
    if (currCollectionAddToFolderi >= currCollectionOrdnerListl)
      currCollectionAddToFolderi = 0;
    currlock.unlock();
  }
  public String getCurrCollectionOrdnerListI(int i, String folderNameIfExistsAlready) {
    String returned = "";
    currlock.lock();
    if (folderNameIfExistsAlready != null) {
      for (int j = 0 ; j < currCollectionOrdnerList.size() ; j++) {
        if (currCollectionOrdnerList.get(j).equals(folderNameIfExistsAlready)) {
          //llog.d(TAG, "already have this folder " + folderNameIfExistsAlready + " " + j);
          returned = folderNameIfExistsAlready;
        }
      }
    }
    if (returned.length() == 0) {
      if (i >= 0 && i < currCollectionOrdnerList.size()) {
        returned = currCollectionOrdnerList.get(i);
      } else if (currCollectionOrdnerList.size() > 0) {
        returned = currCollectionOrdnerList.get(0);
      } else {
        llog.d(TAG, "error currCollectionOrdnerList is empty shoudn't happen");
      }
    }
    currlock.unlock();
    return returned;
  }

  public static void arraylistCopy(ArrayList<String> source, ArrayList<String> dest){
    int sourcel;
    if (source != null)
      sourcel = source.size();
    else
      sourcel = 0;
    if (dest == null)
      dest = new ArrayList<>();
    int destl = dest.size();
    String sources;
    for (int s = 0 ; s < sourcel ; s++) {
      sources = source.get(s);
      if (dest.size() > s)
        dest.set(s, sources);
      else
        dest.add(sources);
    }
    for (int d = sourcel ; d < destl ; d++) {
      dest.set(d, "");
    }
  }

  public static String colladdresstoname(String address){
    String monoption = null;
    if (address != null) {
      int lastslash = address.lastIndexOf('/');
      int lastext = address.lastIndexOf('.');
      if (lastslash >= 0 && lastext > lastslash + 1) {
        monoption = address.substring(lastslash + 1, lastext);
      }
    }
    return monoption;
  }
  public Collection getCollection(String address) {
    Collection collection;
    if (address != null) {
      int collectionsl = collections.size();
      for (int i = 0; i < collectionsl; i++) {
        collection = collections.get(i);
        if (collection.address != null) {
          if (collection.address.equals(address)) {
            return collection;
          }
        }
      }
    }
    return null;
  }

  public class TempGallery {
    public String collectionAddress = null;
    public ArrayList<Ordner> tempgallery = new ArrayList<>();
    public boolean success = false;
    public TempGallery(String _collectionAddress) {
      collectionAddress = _collectionAddress;
      success = mergeCollection(collectionAddress, null);
    }

    private int findOrdnerInTempGallery(String address) {
      int tempgallerylm = tempgallery.size();
      Ordner ordner;
      for (int i = 0; i < tempgallerylm; i++) {
        ordner = tempgallery.get(i);
        if (ordner.address.equals(address)) {
          return i;
        }
      }
      return -1;
    }

    private int findMediaInTempGallery(int ordnerindex, String address) {
      List<Media> medialist = tempgallery.get(ordnerindex).mediaList;
      int medialistlm = medialist.size();
      Media media;
      for (int i = 0; i < medialistlm; i++) {
        media = medialist.get(i);
        if (media.address.equals(address)) {
          return i;
        }
      }
      return -1;
    }

    private boolean mergeCollection(String colladdress, String askedbycolladdress) {
      if (askedbycolladdress != null)
        if (askedbycolladdress.equals(colladdress))
          return false;
      Collection collection = getCollection(colladdress);
      if (collection == null) { // collection no in memory yet
        return false;
      } else if (collection.elementList.size() == 0) { // collection has not been loaded yet
        return false;
      }
      ArrayList<Element> elementList = collection.elementList;
      int colll = elementList.size();
      for (int i = 0; i < colll; i++) {
        Element ele = elementList.get(i);
        if (!ele.add) {
          if (ele.type == Element.element_collection) {
            //llog.d(TAG, colladdress + " -c   ");
          } else if (ele.type == Element.element_ordner) {
            //llog.d(TAG, colladdress + " - o  " + ele.ordner.address);
            if (ele.ordner.address != null) {
              collection.lastOrdnerAddress = ele.ordner.address;
              int found = findOrdnerInTempGallery(ele.ordner.address);
              if (found != -1) {
                llog.d(TAG, colladdress + " error need to cleanup that ordner first " + ele.ordner.address);
                tempgallery.remove(found);
              }
            }
          } else if (ele.type == Element.element_media) {
            //llog.d(TAG, colladdress + " -  m " + ele.media.address);
            if (ele.media.ordnerAddress == null)
              ele.media.ordnerAddress = collection.lastOrdnerAddress;
            int found = findOrdnerInTempGallery(ele.media.ordnerAddress);
            if (found == -1) {
              llog.d(TAG, colladdress + " error no ordner with address " + ele.media.ordnerAddress);
            } else {
              Ordner ordner = tempgallery.get(found);
              if (ordner.mediaList == null) {
                llog.d(TAG, colladdress + " error no media in that ordner yet " + ele.media.ordnerAddress);
              } else {
                int foundm = findMediaInTempGallery(found, ele.media.address);
                if (foundm == -1) {
                  llog.d(TAG, colladdress + " error no media with address " + ele.media.address);
                } else {
                  //llog.d(TAG, "remove " + ordner.mediaList.get(foundm).address);
                  ordner.mediaList.remove(foundm);
                }
              }
            }
          }
        } else {
          if (ele.type == Element.element_collection) {
            //llog.d(TAG, colladdress + " +c   ");
          } else if (ele.type == Element.element_media) {
            if (ele.media.ordnerAddress == null)
              ele.media.ordnerAddress = collection.lastOrdnerAddress;
            Media media = ele.media.copyMedia();
            int found = findOrdnerInTempGallery(media.ordnerAddress);
            //llog.d(TAG, colladdress + " +  m " + ele.media.address + " " + ele.media.printName);
            Ordner ordner;
            if (found != -1) {
              ordner = tempgallery.get(found);
            } else {
              ordner = new Ordner();
              ordner.address = media.ordnerAddress;
              tempgallery.add(ordner);
            }
            ordner.hasNotBeenLoadedYet = false;
            /*if (media.isOnline == Media.online_apache) {
              if (media.addressToGetThumbnail == null)
                media.addressToGetThumbnail = media.address + ".thmb";
              if (media.type != Media.media_picture && media.addressToGetPreviewFullSize == null)
                media.addressToGetPreviewFullSize = media.address + ".thmb";
            }*/
            if (ordner.mediaList == null)
              ordner.mediaList = new ArrayList<>();
            ordner.mediaList.add(media);
            if (media.isALinkThatCreatesAnOrdner) {
              if (media.address.endsWith(".sel")) {
                boolean success = mergeCollection(media.address, colladdress);
                if (success)
                  ordner.hasNotBeenLoadedYet = false; // do not keep this ordner if it is empty it's only a link to a collection
              }
            }
          } else if (ele.type == Element.element_ordner) {
            if (ele.ordner.address != null) {
              //llog.d(TAG, colladdress + " + o  " + ele.ordner.address);
              collection.lastOrdnerAddress = ele.ordner.address;
              int found = findOrdnerInTempGallery(ele.ordner.address);
              if (found != -1) {
                Ordner ordner = tempgallery.get(found);
                ele.ordner.copyOrdnerInto(ordner);
              } else {
                Ordner ordner = ele.ordner.copyOrdner();
                tempgallery.add(ordner);
                if (ordner.address.endsWith(".sel")) {
                  boolean success = mergeCollection(ordner.address, colladdress);
                  if (success)
                    ordner.hasNotBeenLoadedYet = false; // do not keep this ordner if it is empty it's only a link to a collection
                }/* else if (ordner.isOnline == Media.online_no) {
                  gallerylock.lock();
                  for (int o = 0 ; o < folderCount ; o++) {
                    if (gallery.get(o).address.equals(ele.ordner.address)) {
                      Ordner gord = gallery.get(o);
                      ordner = gord.copyOrdner();
                      ordner.mediaListCount = gord.mediaListCount;
                      if (gord.mediaListCount > 0) {
                        ordner.mediaList = new ArrayList<>();
                        for (int m = 0 ; m < gord.mediaListCount ; m++) {
                          ordner.mediaList.add(gord.mediaList.get(m).copyMedia());
                          llog.d(TAG, "add m " + gord.mediaList.get(m).address);
                        }
                      }
                      ordner.hasNotBeenLoadedYet = gallery.get(o).hasNotBeenLoadedYet;
                      break;
                    }
                  }
                  gallerylock.unlock();
                  if (ordner.mediaListCount > 0) {
                    ordner.hasNotBeenLoadedYet = false;
                    llog.d(TAG, "added " + ordner.mediaListCount + " medias to " + ordner.address);
                  }
                }*/
              }
            }
          }
        }
      }
      //llog.d(TAG, "collection " + colladdress + " updateDeltaTime " + collection.updateDeltaTime);
      return true;
    }

  }

  public ArrayList<Ordner> mergeCollections(String maincollection) {
    if (maincollection == null)
      return null;
    TempGallery tempgall = new TempGallery(maincollection);
    if (!tempgall.success) {
      llog.d(TAG, "mergeCollections error collection not loaded " + maincollection);
      return null;
    }
    return tempgall.tempgallery;
  }

  public ArrayList<Ordner> mergeCollectionsRemoveEmpty(String maincollection) {
    if (maincollection == null)
      return null;
    TempGallery tempgall = new TempGallery(maincollection);
    if (!tempgall.success) {
      llog.d(TAG, "mergeCollectionsRemoveEmpty error collection not loaded " + maincollection);
      return null;
    }
    ArrayList<Ordner> tempgallery = tempgall.tempgallery;
    int tempgalleryl = tempgallery.size();
    ArrayList<Integer> removeThisOrdner = new ArrayList<>();
    for (int i = 0 ; i < tempgalleryl ; i++) {
      Ordner ordner = tempgallery.get(i);
      if (ordner.mediaList == null) {
        //llog.d(TAG, " +ordner -> mediaList == null " + ordner.address);
        if (ordner.hasNotBeenLoadedYet) {
          ordner.fileListCanBeRemovedFromCache = false;
          ordner.mediaListCount = 0;
          if (ordner.isOnline != Media.online_no && ordner.addressEscaped == null) {
            try {
              ordner.addressEscaped = URLDecoder.decode(ordner.address, "utf-8");
            } catch (UnsupportedEncodingException e) {
              e.printStackTrace();
            }
          }
        } else {
          //llog.d(TAG, " +ordner -> do not add : "+ tempgallery.get(i).address + " mediaList == null ordner hasalreadyBeenLoaded");
          removeThisOrdner.add(i);
        }
      } else if (ordner.mediaList.size() > 0) { // always set the filescount if already populated
        //llog.d(TAG, " +ordner -> " + ordner.mediaListCount);
        ordner.hasNotBeenLoadedYet = false;
        ordner.fileListCanBeRemovedFromCache = false;
        /**
         *   ordner /collection//http:// are not marked online, yet might want to be escaped
         */
        if (ordner.isOnline != Media.online_no && ordner.addressEscaped == null) {
          try {
            ordner.addressEscaped = URLDecoder.decode(ordner.address, "utf-8");
          } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
          }
        }
        ordner.mediaListCount = ordner.mediaList.size();
        for (int j = 0; j < ordner.mediaListCount; j++) {
          Media media = ordner.mediaList.get(j);
          // generated shouldn't be saved in collections, reset them
          media.isSelected = false; // deselect them before showing them
          media.selectedchecked = true;
          // media.filmstripCount = Media.filmstripNotCheckedYet; // shouldn't be saved in collections // but necessary for widgets menu_buttons
          //llog.d(TAG, media.address + " mini " + media.addressToGetThumbnail + " maxi " + media.addressToGetPreviewFullSize);
          if (media.isOnline != Media.online_no && media.addressescaped == null) {
            try {
              media.addressescaped = URLDecoder.decode(media.address, "utf-8");
            } catch (UnsupportedEncodingException e) {
              e.printStackTrace();
            }
          }
        }
      } else {
        //llog.d(TAG, " +ordner -> do not add : "+ tempgallery.get(i).address + " mediaList size == 0");
        removeThisOrdner.add(i);
      }
    }
    int removel = removeThisOrdner.size() - 1;
    for (int i = removel ; i >= 0 ; i--) {
      int pos = removeThisOrdner.get(i);
      //llog.d(TAG, "remove " + pos + " : " + tempgallery.get(pos).address);
      tempgallery.remove(pos);
    }
    removeThisOrdner.clear();
    return tempgallery;
  }

  public int collectionsToDisplay(String maincollection, int p) {
    //llog.d(TAG, "collectionToDisplay maincollection " + maincollection);
    ArrayList<Ordner> tempgallery = mergeCollectionsRemoveEmpty(maincollection);
    if (tempgallery == null) {
      llog.d(TAG, "collectionToDisplay maincollection " + maincollection + " tempgallery == null");
      return -1;
    }

    int tempgalleryl = tempgallery.size();

    /** 0 - portion that will be replaced **/
    Collection maincoll = getCollection(maincollection);
    gallerylock.lock();
    int surfl = surf.size();
    int gi = -1;
    int gf = -1;
    int mdisplayedOrdnerListl = maincoll.displayedOrdnerList.size();
    if (mdisplayedOrdnerListl > 0) {
      String address = maincoll.displayedOrdnerList.get(0);
      for (int j = 0; j < folderCount; j++) {
        Ordner gordner = gallery.get(j);
        if (gordner.address.equals(address)) {
          gi = j;
        }
      }
      if (gi >= 0) {
        p = gi;
        if (mdisplayedOrdnerListl > 1) {
          address = maincoll.displayedOrdnerList.get(mdisplayedOrdnerListl - 1);
          for (int j = gi + 1; j < folderCount; j++) {
            Ordner gordner = gallery.get(j);
            if (gordner.address.equals(address)) {
              gf = j + 1;
            }
          }
        }
        if (gf < 0) {
          gf = gi + 1;
        }
      }
    }
    //llog.d(TAG, maincollection + " replace from " + gi + " to " + gf + " / " + folderCount + " p " + p);

    /** 0.5 - set new positions for each surf **/
    for (int s = 0 ; s < surfl ; s++) {
      Surf thissurf = surf.get(s);
      if (thissurf.ordnerIndex < 0)
        continue;
      if (thissurf.ordnerIndexAddress.equals(""))
        continue;
      if (gi < 0) { // on ajoutera à la fin donc aucun supérieur à corriger
      } else if (gi <= thissurf.ordnerIndex && thissurf.ordnerIndex < gf) {
        if (tempgalleryl == 0) {
          thissurf.ordnerIndex = gi - 1;
          if (gi - 1 >= 0) {
            thissurf.ordnerIndexAddress = gallery.get(gi - 1).address;
          } else {
            thissurf.ordnerIndexAddress = "";
          }
          thissurf.mediaIndex = -1;
          thissurf.mediaIndexAddress = "";
        } else {
          //llog.d(TAG, "surf " + s + " " + thissurf.ordnerIndex + " : " + thissurf.ordnerIndexAddress);
          boolean found = false;
          int pos = thissurf.ordnerIndex;
          while (!found && pos >= gi) {
            String gaddress = gallery.get(pos).address;
            found = false;
            for (int t = 0; t < tempgalleryl; t++) {
              Ordner tordner = tempgallery.get(t);
              String taddress = tordner.address;
              if (gaddress.equals(taddress)) {
                //llog.d(TAG, " compare match ! " + pos + " -> " + (gi+t) + " " + gaddress + " -> " + taddress);
                thissurf.ordnerIndex = gi + t;
                if (taddress.equals(thissurf.ordnerIndexAddress)) {
                  // vérifier les changements dans ce répertoire
                  if (thissurf.mediaIndex >= 0) {
                    for (int j = 0; j < tordner.mediaListCount; j++) {
                      if (tordner.mediaList.get(j).address.equals(thissurf.mediaIndexAddress)) {
                        thissurf.mediaIndex = j;
                        break;
                      }
                    }
                  }
                } else {
                  //llog.d(TAG, " compare different folder than origin");
                  thissurf.ordnerIndexAddress = gaddress;
                  thissurf.mediaIndex = -1;
                  thissurf.mediaIndexAddress = "";
                }
                found = true;
                break;
              }
            }
            pos -= 1;
          }
          if (!found) {
            //llog.d(TAG, " not found ! " + gi + " " + thissurf.ordnerIndexAddress + " ->  " +  tempgallery.get(0).address);
            thissurf.ordnerIndex = gi;
            thissurf.ordnerIndexAddress = tempgallery.get(0).address;
            thissurf.mediaIndex = -1;
            thissurf.mediaIndexAddress = "";
          }
        }
      } else if (thissurf.ordnerIndex >= gf) {
        thissurf.ordnerIndex += tempgalleryl - (gf - gi);
      }
    }

/*
    // TODO remove
    llog.d(TAG, "tempgalleryl " + tempgallery.size());
    for (int j = 0 ; j < tempgalleryl ; j++) {
      Ordner tordner = tempgallery.get(j);
      llog.d(TAG, " + " + (p+j) + " " + tordner.address + " (" + tordner.mediaListCount + ")");
      for (int i = 0 ; i < tordner.mediaListCount ; i++) {
        llog.d(TAG, " + " + (p+j) + " " + tordner.mediaList.get(i).address);
        Media tmedia = tordner.mediaList.get(i);
      }
    }
*/


    /** 1 - copy gallery existing thumbs into temp **/
    for (int t = 0 ; t < tempgalleryl ; t++) {
      Ordner tordner = tempgallery.get(t);
      String taddress = tordner.address;
      boolean found = false;
      for (int g = 0; g < folderCount; g++) {
        Ordner gordner = gallery.get(g);
        if (gordner.address.equals(taddress)) {
          //llog.d(TAG, "we already have it, copy bitmap data " + taddress + " from gallery " + gordner.mediaListCount);

          List<Media> tmediaList = tordner.mediaList;
          int tordnermedialistcount = tordner.mediaListCount;
          if (0 < gordner.byDefaultShowMediaNumber && gordner.byDefaultShowMediaNumber < gordner.mediaListCount) {
            String gmediaddress = gordner.mediaList.get(gordner.byDefaultShowMediaNumber).address;
            for (int l = 0 ; l < tordnermedialistcount ; l++) {
              if (tmediaList.get(l).address.equals(gmediaddress)) {
                //llog.d(TAG, "byDefaultShowMediaNumber =l " + l + " " + tordner.address);
                tordner.byDefaultShowMediaNumber = l;
                break;
              }
            }
          } else if (tordner.byDefaultShowMediaNumber < 0 && tordner.mediaListCount > 0 && gordner.mediaList == null) {
            tordner.byDefaultShowMediaNumber = tordner.mediaListCount + tordner.byDefaultShowMediaNumber;
            if (tordner.byDefaultShowMediaNumber < 0) {
              //llog.d(TAG, "byDefaultShowMediaNumber < 0 a " + tordner.address);
              tordner.byDefaultShowMediaNumber = 0;
            }// else {
              //llog.d(TAG, "byDefaultShowMediaNumber = a " +  tordner.mediaListCount + "+" + tordner.byDefaultShowMediaNumber + " " + tordner.address);
            //}
          }


          List<Media> gmediaList = gordner.mediaList;
          boolean decalageinitial = true;
          int decalage = 0;
          for (int i = 0 ; i < gordner.mediaListCount ; i++) {
            Media gmedia = gmediaList.get(i);

            String gaddress = gmedia.address;
            int j = (i + decalage) % tordnermedialistcount;
            int count = 0;
            while (count < tordnermedialistcount) {
              //llog.d(TAG, i + " " + j);
              // save identical media, the others will be recycled afterwards
              Media tmedia = tmediaList.get(j);
              if (tmedia.address.equals(gaddress)) { // only copy back what's related to the loaded thumbnail

                if (decalageinitial) {
                  decalage = j - i;
                  decalageinitial = false;
                }

                tmedia.index = gmedia.index;
                tmedia.ordnerIndex = gmedia.ordnerIndex;

                tmedia.width = gmedia.width;
                tmedia.height = gmedia.height;
                tmedia.offsetX = gmedia.offsetX;
                tmedia.offsetY = gmedia.offsetY;

                // new collection overrides the old one, but only set variables
                if (tmedia.printName == null) tmedia.printName = gmedia.printName;
                if (tmedia.printDetails == null) tmedia.printDetails = gmedia.printDetails;
                if (tmedia.printFooter == null) tmedia.printFooter = gmedia.printFooter;
                if (tmedia.ordnerPrintName == null) tmedia.ordnerPrintName = gmedia.ordnerPrintName;
                if (tmedia.addressToGetPreviewFullSize == null) tmedia.addressToGetPreviewFullSize = gmedia.addressToGetPreviewFullSize;
                if (tmedia.addressToGetThumbnail == null) tmedia.addressToGetThumbnail = gmedia.addressToGetThumbnail;
                if (tmedia.bookmarkToOrdner == null) tmedia.bookmarkToOrdner = gmedia.bookmarkToOrdner;

                if (tmedia.addressToGetLibextractorsThumbnail == -1) tmedia.addressToGetLibextractorsThumbnail = gmedia.addressToGetLibextractorsThumbnail;

                tmedia.selectedchecked = gmedia.selectedchecked;
                tmedia.isSelected = gmedia.isSelected;
                tmedia.filmstriplastchange = gmedia.filmstriplastchange;
                tmedia.bitmapaskednext = gmedia.bitmapaskednext;

                tmedia.filmstripcurrent = gmedia.filmstripcurrent;
                tmedia.filmstripCount = gmedia.filmstripCount;

                if (tmedia.addressescaped == null) tmedia.addressescaped = gmedia.addressescaped;
                if (tmedia.metadatawidth == 0) tmedia.metadatawidth = gmedia.metadatawidth;
                if (tmedia.metadataheight == 0) tmedia.metadataheight = gmedia.metadataheight;
                if (tmedia.metadatamaxwidth == 0) tmedia.metadatamaxwidth = gmedia.metadatamaxwidth;
                if (tmedia.metadatamaxheight == 0) tmedia.metadatamaxheight = gmedia.metadatamaxheight;
                if (tmedia.metadatarotation == 0) tmedia.metadatarotation = gmedia.metadatarotation;
                if (tmedia.metadataaudio == 0) tmedia.metadataaudio = gmedia.metadataaudio;
                if (tmedia.metadatasubtitle == 0) tmedia.metadatasubtitle = gmedia.metadatasubtitle;
                if (tmedia.metadatacreationtime == null) tmedia.metadatacreationtime = gmedia.metadatacreationtime;
                if (tmedia.metadataduration == 0) tmedia.metadataduration = gmedia.metadataduration;

                tmedia.bitmap = gmedia.bitmap;
                gmedia.bitmap = null;

                if (!tmedia.playInSequence) tmedia.playInSequence = gmedia.playInSequence;
                if (tmedia.playStartAtPosition == 0) tmedia.playStartAtPosition = gmedia.playStartAtPosition;
                if (tmedia.subtitleAddress == null) tmedia.subtitleAddress = gmedia.subtitleAddress;
                gmedia.subtitleAddress = null;

                break;
              }
              count += 1;
              j = (j + 1) % (tordnermedialistcount);
            }
          }

          found = true;
          break;
        }
      }
      if (!found) {
        if (tordner.byDefaultShowMediaNumber < 0 && tordner.mediaListCount > 0) {
          tordner.byDefaultShowMediaNumber = tordner.mediaListCount + tordner.byDefaultShowMediaNumber;
          if (tordner.byDefaultShowMediaNumber < 0) {
            //llog.d(TAG, "byDefaultShowMediaNumber < 0 b " + tordner.address);
            tordner.byDefaultShowMediaNumber = 0;
          }// else {
          //  llog.d(TAG, "byDefaultShowMediaNumber = b " +  tordner.mediaListCount + "+" + tordner.byDefaultShowMediaNumber + " " + tordner.address);
          //}
        }
      }
    }

    /** 2 - remove from gi to gf **/
    if (gi >= 0) {
      for (int i = gf - 1; i >= gi; i--) {
        Ordner gordner = gallery.get(i);
        if (gordner.mediaList != null) {
          for (Media media : gordner.mediaList) {
            if (media.bitmap != null) {
              for (int b = 0; b < media.bitmap.length; b++) {
                if (media.bitmap[b] != null) {
                  if (!media.bitmap[b].isRecycled()) {
                    media.bitmap[b].recycle();
                    media.bitmap[b] = null;
                  }
                }
              }
              media.bitmap = null;
            }
          }
          gordner.mediaListCount = 0;
          gordner.mediaList.clear();
          gordner.mediaList = null;
        }
        //llog.d(TAG, " - " + i + " / " + folderCount + " " + gordner.address);
        gallery.remove(i);
      }
      folderCount = gallery.size();
    }
    if (p < 0 || p > folderCount) {
      p = folderCount;
    }

    /** 3 - set temp at position gi **/
    maincoll.displayedOrdnerList.clear();
    maincoll.selectedCount = 0;
    for (int j = 0 ; j < tempgalleryl ; j++) {
      Ordner tordner = tempgallery.get(j);
      //llog.d(TAG, tordner.address + " !fileListCanBeRemovedFromCache");
      tordner.fileListCanBeRemovedFromCache = false;
      if (!thereIsAnUpdatableFolder)
        if (tordner.updateDeltaTime > 0)
          thereIsAnUpdatableFolder = true;
      gallery.add(p + j, tordner);
      maincoll.displayedOrdnerList.add(tordner.address);
      maincoll.selectedCount += tordner.mediaListCount;
    }
    folderCount = gallery.size();

    gallerylock.unlock();

    redrawsplitviewbitmapm = true;
    tempgallery.clear();

    return p;

  }





  public List<String> deletefiles = new ArrayList<>();

  private final ReentrantLock gallerylock = new ReentrantLock();
  public List<Ordner> gallery = new ArrayList<>();
  public volatile int folderCount = 0;

  public void changeBigPicture(int surfid, int ForceFolderToIndex, int folderdelta, int ForceFileToIndex, int filedelta, boolean putinmemory, boolean switchtonextfolder){
    //llog.d(TAG, surfid + " changeBigPicture " + ForceFolderToIndex + " +/-  " + folderdelta + "   " + ForceFileToIndex + " +/- " + filedelta + "     " + putinmemory + "   " + switchtonextfolder);
    gallerylock.lock();
    Surf thissurf = surf.get(surfid);
    //llog.d(TAG, thissurf.ordnerIndex + " i: " + thissurf.mediaIndex);
    if (folderCount > 0) {
      if (ForceFolderToIndex >= 0 || thissurf.ordnerIndex < 0) {
        //llog.d(TAG, "ForceFolderToIndex >= 0 || thissurf.ordnerIndex < 0");
        if (ForceFolderToIndex >= folderCount)
          ForceFolderToIndex = folderCount - 1;
        if (ForceFolderToIndex < 0)
          ForceFolderToIndex = 0;
        thissurf.ordnerIndex = ForceFolderToIndex;
        thissurf.ordnerIndexAddress = gallery.get(ForceFolderToIndex).address;
      } else { // if (folderdelta != 0) {
        //llog.d(TAG, "else");
        int d = correctFolderIndex(thissurf.ordnerIndexAddress, thissurf.ordnerIndex);
        int newfolder = d + folderdelta;
        if (newfolder >= folderCount)
          newfolder = folderCount - 1;
        if (newfolder < 0)
          newfolder = 0;
        thissurf.ordnerIndex = newfolder;
        thissurf.ordnerIndexAddress = gallery.get(newfolder).address;
      }
      //llog.d(TAG, thissurf.ordnerIndex + " d: " + thissurf.mediaIndex);
      int foldersize = gallery.get(thissurf.ordnerIndex).mediaListCount;
      if (ForceFileToIndex >= 0) {
        //llog.d(TAG, "ForceFileToIndex >= 0");
        if (ForceFileToIndex >= foldersize)
          ForceFileToIndex = foldersize - 1;
        thissurf.mediaIndex = ForceFileToIndex; // -1 si foldersize = 0
      } else if (filedelta != 0) {
        //llog.d(TAG, "filedelta != 0");
        int f = correctFileIndex(thissurf.ordnerIndex, thissurf.mediaIndexAddress, thissurf.mediaIndex);
        int newfile = f + filedelta;
        if (newfile < 0 || newfile >= foldersize) {
          if (!switchtonextfolder) {
            //llog.d(TAG, "!switchtonextfolder");
            if (newfile >= foldersize) // cyclique
              newfile = 0;
            if (newfile < 0) // cyclique
              newfile = foldersize - 1; // -1 si foldersize = 0 good sera traité après loadnextfolderinmemory
          } else {
            //llog.d(TAG, "switchtonextfolder");
            // il faut charger le dossier suivant avant de pouvoir faire ça
            int d = correctFolderIndex(thissurf.ordnerIndexAddress, thissurf.ordnerIndex);
            int newfolder;
            if (filedelta < 0)
              newfolder = d - 1;
            else
              newfolder = d + 1;
            if (newfolder >= folderCount)
              newfolder = folderCount - 1;
            if (newfolder < 0)
              newfolder = 0;
            thissurf.ordnerIndex = newfolder;
            thissurf.ordnerIndexAddress = gallery.get(newfolder).address;
            foldersize = gallery.get(thissurf.ordnerIndex).mediaListCount;
            if (filedelta < 0)
              newfile = foldersize - 1;
            else
              newfile = 0;
            if (newfile < 0)
              newfile = 0;
            if (newfile >= foldersize) // -1 si foldersize = 0
              newfile = foldersize - 1; // -1 si foldersize = 0
          }
        }
        thissurf.mediaIndex = newfile; // -1 si foldersize = 0
      } else {
        //llog.d(TAG, "else");
        int newfile = gallery.get(thissurf.ordnerIndex).byDefaultShowMediaNumber;
        if (newfile < 0)
          newfile = 0;
        if (newfile >= foldersize) // -1 si foldersize = 0
          newfile = foldersize - 1; // -1 si foldersize = 0
        thissurf.mediaIndex = newfile; // -1 si foldersize = 0
      }
      //llog.d(TAG, thissurf.ordnerIndex + " f: " + thissurf.mediaIndex);
      if (thissurf.mediaIndex >= 0 && thissurf.mediaIndex < gallery.get(thissurf.ordnerIndex).mediaListCount) {
        thissurf.mediaIndexAddress = gallery.get(thissurf.ordnerIndex).mediaList.get(thissurf.mediaIndex).address;
        gallery.get(thissurf.ordnerIndex).byDefaultShowMediaNumber = thissurf.mediaIndex;

        if (lastbrowsedonline != null) {
          if (gallery.get(thissurf.ordnerIndex).mediaList.get(thissurf.mediaIndex).isOnline == Media.online_no) {
            forgetlastbrowsedonline();
          } else {
            SharedPreferences.Editor edit = preferences.edit();
            edit.putString("lastbrowsedonline", lastbrowsedonline).commit();
            edit.putString("lastbrowsedonlined", thissurf.ordnerIndexAddress);
            edit.putString("lastbrowsedonlinef", thissurf.mediaIndexAddress);
            edit.putInt("lastbrowsedonlinez", thissurf.zoommode);
            edit.apply();
            llog.d(TAG, "save lastbrowsedonline " + thissurf.ordnerIndexAddress + " " + thissurf.mediaIndexAddress + " " + thissurf.zoommode);
          }
        }

        //llog.d(TAG, surfid + " changeBigPicture thissurf.fichierprincipal >= 0 && thissurf.fichierprincipal < gallery.get(thissurf.dossierprincipal).filesCount");

        // sinon la deuxième cachebitmap n'est mise en cache que quand !isoincahce threadbrowser
        if (thissurf.bigimagecurrentlydisplayed > 999999998)
          thissurf.bigimagecurrentlydisplayed = 1;
        else
          thissurf.bigimagecurrentlydisplayed += 1;
        try {
          commandebigimagethreadqueue.put(new String[]{"cachebitmap", String.valueOf(surfid), String.valueOf(thissurf.ordnerIndex), String.valueOf(thissurf.mediaIndex),
                  String.valueOf(thissurf.bigimagecurrentlydisplayed), String.valueOf(false), "-1"});
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        thissurf.putbigpictureinmemory = putinmemory;

      } else { // sinon le dossier suivant n'est pas encore mis en mémoire il faut le faire
        llog.d(TAG, surfid + " changeBigPicture rechargetoutledossier " + thissurf.mediaIndex + " " + gallery.get(thissurf.ordnerIndex).mediaListCount);
        try {
          commandethreaddatabase.put(new String[]{"rechargetoutledossier", "-1", String.valueOf(thissurf.ordnerIndex), String.valueOf(false)});
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        thissurf.mediaIndex = -1; // nécessaire sinon pas de Le fichier principal dépasse la taille du dossier on corrige son index
      }
      //llog.d(TAG, surfid + " changeBigPicture " + thissurf.ordnerIndex + " " + thissurf.mediaIndex);
    }
    long temps = System.currentTimeMillis();
    if (thissurf.optiondiaporamaactive && thissurf.diaporamalastchange > 0)
      thissurf.diaporamalastchange = temps;
    if (filmstripanimatetime > 1000)
      thissurf.filmstriplastchange = (long) (temps - filmstripanimatetime * 0.5);
    else
      thissurf.filmstriplastchange = temps;
    gallerylock.unlock();
    //llog.d(TAG, "changeBigPicture surf" + surfid + " : " + folder + "d" + folderdelta + ", " + file + "d" +filedelta + " -> " + thissurf.ordnerIndex + "," + thissurf.mediaIndex + " " + putinmemory);
    try {
      commandethreadbrowser.put(new String[]{String.valueOf(surfid), "update"});
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  public int correctFolderIndex(String cedossier, int presde){
    /**
     *   seulement nécessaire pour écrire depuis tous les autres threads que threaddatabase
     */
    if (folderCount > 0) {
      final int last = folderCount - 1;
      if (presde < 0 || presde > last) // c'est sans doute le dernier dossier
        presde = last - 1;
      int i = presde;
      int j = i + 1;
      while (i >= 0 || j < folderCount) {
        if (i >= 0) {
          if (gallery.get(i).address.equals(cedossier))
            return i;
          i -= 1;
        }
        if (j < folderCount) {
          if (gallery.get(j).address.equals(cedossier))
            return j;
          j += 1;
        }
      }
    }
    return -1;
  }
  public int correctFileIndex(int cedossier, String cefichier, int presde){
    /**
     *   seulement nécessaire pour écrire depuis tous les autres threads que threaddatabase
     */
    if (cedossier >= 0 && cedossier < folderCount) {
      Ordner odner = gallery.get(cedossier);
      final List<Media> files = odner.mediaList;
      final int count = odner.mediaListCount;
      if (count > 0) {
        final int last = count - 1;
        if (presde < 0 || presde > last) // c'est sans doute le dernier dossier
          presde = last - 1;
        int i = presde;
        int j = i + 1;
        while (i >= 0 || j < count) {
          if (i >= 0) {
            if (files.get(i).address.equals(cefichier))
              return i;
            i -= 1;
          }
          if (j < count) {
            if (files.get(j).address.equals(cefichier))
              return j;
            j += 1;
          }
        }
      }
    }
    return -1;
  }
  public int findFolder(String cedossier, int presde){
    int idossier = -1;
    if (cedossier != null) {
      gallerylock.lock();
      if (folderCount > 0) {
        final int last = folderCount - 1;
        int i = presde;
        if (i < 0 || i > last)
          i = (int) (folderCount * 0.5f);
        int j = i + 1;
        boolean mini = false;
        boolean maxi = false;
        if (j > last)
          maxi = true;
        while (!mini || !maxi) {
          if (!mini) {
            if (gallery.get(i).address.equals(cedossier)) {
              idossier = i;
              break;
            }
            if (i > 0)
              i -= 1;
            else
              mini = true;
          }
          if (!maxi) {
            if (gallery.get(j).address.equals(cedossier)) {
              idossier = j;
              break;
            }
            if (j < last)
              j += 1;
            else
              maxi = true;
          }
        }
      }
      gallerylock.unlock();
    }
    if (idossier == -1)
      llog.d(TAG, "findfolder presde not found " + cedossier);
    return idossier;
  }
  public String[] getOrdnerMediaList(String address, int d){
    String[] medialist = null;
    gallerylock.lock();
    if (address != null)
      d = correctFolderIndex(address, d);
    if (d >= 0 && d < folderCount) {
      Ordner ordner = gallery.get(d);
      int filecount = ordner.mediaListCount;
      medialist = new String[filecount];
      for (int i = 0 ; i < filecount ; i++) {
        medialist[i] = ordner.mediaList.get(i).address;
      }
    }
    gallerylock.unlock();
    return medialist;
  }
  public ArrayList<Integer> getOrdnersFirstLevel(){
    ArrayList<Integer> o = new ArrayList<>();
    gallerylock.lock();
    if (folderCount > 1) {
      String first = gallery.get(0).address;
      int firstl = first.length();
      String last = gallery.get(folderCount - 1).address;
      int lastl = last.length();
      int shortest;
      if (firstl < lastl)
        shortest = firstl;
      else
        shortest = lastl;
      int basel = shortest;
      for (int i = 0 ; i < shortest ; i++) {
        if (first.charAt(i) != last.charAt(i)) {
          basel = i;
          break;
        }
      }
      //llog.d(TAG, "from " + first);
      //llog.d(TAG, "to " + last);
      //llog.d(TAG, "basel " + basel);
      String lastprefix = "dummy/dummy/dummy";
      for (int i = 0 ; i < folderCount ; i++) {
        String prefix = gallery.get(i).address;
        if (prefix.length() <= basel) {
          o.add(i);
          //llog.d(TAG, i + " <=l " + prefix);
        } else {
          int slashpos = prefix.indexOf('/', basel);
          if (slashpos < 0) {
            o.add(i);
            //llog.d(TAG, i + " /<0 " + prefix);
          } else {
            String firstlevel = prefix.substring(basel, slashpos);
            if (!firstlevel.equals(lastprefix)) {
              o.add(i);
              lastprefix = firstlevel;
              //llog.d(TAG, i + " != " + firstlevel + " " + prefix);
            }/* else {
              llog.d(TAG, i + " == " + firstlevel + " " + prefix);
            }*/
          }
        }
      }
    }
    gallerylock.unlock();
    return o;
  }
  public int getFolderFileCount(int d){
    int filecount = -1;
    if (d >= 0) {
      gallerylock.lock();
      if (d < folderCount) {
        filecount = gallery.get(d).mediaListCount;
      }
      gallerylock.unlock();
    }
    return filecount;
  }
  public int findFolder(String cedossier){
    int idossier = -1;
    if (cedossier != null) {
      gallerylock.lock();
      for (int i = 0; i < folderCount; i++) {
        if (gallery.get(i).address.equals(cedossier)) {
          idossier = i;
          break;
        }
      }
      gallerylock.unlock();
    }
    return idossier;
  }
  public int[] findFolderStartsWith(String basisfolderclicked, int dossiercourant){
    int[] basisfoldercorresp = new int[]{dossiercourant, dossiercourant + 1};
    if (basisfolderclicked != null) {
      int basisfolderclickedl = basisfolderclicked.length();
      gallerylock.lock();
      for (int k = dossiercourant; k >= 0; k--) {
        String foldername = gallery.get(k).address;
        if (foldername == null)
          break;
        else if (!foldername.startsWith(basisfolderclicked))
          break;
        else {
          if (foldername.length() == basisfolderclickedl)
            basisfoldercorresp[0] = k;
          else if (foldername.length() > basisfolderclickedl) {
            if (foldername.charAt(basisfolderclickedl - 1) == '/')
              basisfoldercorresp[0] = k;
            else
              break;
          } else
            break;
        }
      }
      for (int k = dossiercourant; k < folderCount; k++) {
        String foldername = gallery.get(k).address;
        if (foldername == null)
          break;
        else if (!foldername.startsWith(basisfolderclicked))
          break;
        else {
          if (foldername.length() == basisfolderclickedl)
            basisfoldercorresp[1] = k + 1;
          else if (foldername.length() > basisfolderclickedl) {
            if (foldername.charAt(basisfolderclickedl - 1) == '/')
              basisfoldercorresp[1] = k + 1;
            else
              break;
          } else
            break;
        }
      }
      gallerylock.unlock();
    }
    return basisfoldercorresp;
  }
  public int findFolderEndsSame(String foldername){
    int idossier = -1;
    if (foldername != null) {
      int lastslash = foldername.lastIndexOf("/");
      if (lastslash != -1) {
        int beforelastslash = foldername.lastIndexOf("/", lastslash - 1);
        if (beforelastslash != -1 && lastslash > beforelastslash + 2) {
          String folderend = foldername.substring(beforelastslash, lastslash);
          gallerylock.lock();
          for (int i = 0; i < folderCount; i++) {
            //llog.d(TAG, "findFolderEndsSame " + folderend + "/");
            if (gallery.get(i).address.endsWith("/")) {
              if (gallery.get(i).address.endsWith(folderend + "/")) {
                idossier = i;
                break;
              }
            } else {
              if (gallery.get(i).address.endsWith(folderend)) {
                idossier = i;
                break;
              }
            }
          }
          gallerylock.unlock();
        }
      }
    }
    return idossier;
  }
  public int findFolderStartsWith(String startswith){
    int idossier = -1;
    if (startswith != null) {
      gallerylock.lock();
      for (int i = 0; i < folderCount; i++) {
        if (gallery.get(i).address.startsWith(startswith)) {
          idossier = i;
          break;
        }
      }
      gallerylock.unlock();
    }
    return idossier;
  }
  public int[] findFolder(Pattern pattern, int di, int fi){
    int trouvedans = -1;
    int fichier = -1;
    gallerylock.lock();
    List<Media> medialist;
    for (int d = di ; d < folderCount ; d++) {
      Ordner dodossier = gallery.get(d);
      if (pattern.matcher(dodossier.address).find()) {
        if (d > di) {
          trouvedans = d;
          fichier = 0;
        } else {
          if (dodossier.mediaListCount == 0) {
            trouvedans = d;
            fichier = 0;
          } else {
            if (fi == dodossier.mediaListCount - 1) {
              continue; // on cherche depuis le dossier suivant
            } else {
              trouvedans = d;
              fichier = fi + 1; // on ne renvoie jamais (di,fi)
            }
          }
        }
        break;
      }
      if (dodossier.printName != null) {
        if (pattern.matcher(dodossier.printName).find()) {
          if (d > di) {
            trouvedans = d;
            fichier = 0;
          } else {
            if (dodossier.mediaListCount == 0) {
              trouvedans = d;
              fichier = 0;
            } else {
              if (fi == dodossier.mediaListCount - 1) {
                continue; // on cherche depuis le dossier suivant
              } else {
                trouvedans = d;
                fichier = fi + 1; // on ne renvoie jamais (di,fi)
              }
            }
          }
          break;
        }
      }
      if (dodossier.addressEscaped != null) {
        if (pattern.matcher(dodossier.addressEscaped).find()) {
          if (d > di) {
            trouvedans = d;
            fichier = 0;
          } else {
            if (dodossier.mediaListCount == 0) {
              trouvedans = d;
              fichier = 0;
            } else {
              if (fi == dodossier.mediaListCount - 1) {
                continue; // on cherche depuis le dossier suivant
              } else {
                trouvedans = d;
                fichier = fi + 1; // on ne renvoie jamais (di,fi)
              }
            }
          }
          break;
        }
      }
      if (dodossier.mediaListCount > 0) {
        medialist = dodossier.mediaList;
        if (medialist.size() != dodossier.mediaListCount) {
          llog.d(TAG, "error mediaListCount shouldn't happen");
          continue;
        }
        Media media;
        int fichi = 0;
        if (d == di) { // on cherche depuis le fichier suivant pour ce dossier
          fichi = fi + 1;
        }
        for (int fich = fichi ; fich < dodossier.mediaListCount ; fich++) {
          media = medialist.get(fich);
          if (pattern.matcher(media.address).find()) {
            trouvedans = d;
            fichier = fich;
            break;
          }
          if (media.printName != null) {
            if (pattern.matcher(media.printName).find()) {
              trouvedans = d;
              fichier = fich;
              break;
            }
          }
          if (media.addressescaped != null) {
            if (pattern.matcher(media.addressescaped).find()) {
              trouvedans = d;
              fichier = fich;
              break;
            }
          }
        }
        if (trouvedans != -1) {
          break;
        }
      }
    }
    if (trouvedans == -1) {
      for (int d = 0; d < di; d++) {
        Ordner dodossier = gallery.get(d);
        if (pattern.matcher(dodossier.address).find()) {
          if (d > di) {
            trouvedans = d;
            fichier = 0;
          } else {
            if (dodossier.mediaListCount == 0) {
              trouvedans = d;
              fichier = 0;
            } else {
              if (fi == dodossier.mediaListCount - 1) {
                continue; // on cherche depuis le dossier suivant
              } else {
                trouvedans = d;
                fichier = fi + 1; // on ne renvoie jamais (di,fi)
              }
            }
          }
          break;
        }
        if (dodossier.printName != null) {
          if (pattern.matcher(dodossier.printName).find()) {
            if (d > di) {
              trouvedans = d;
              fichier = 0;
            } else {
              if (dodossier.mediaListCount == 0) {
                trouvedans = d;
                fichier = 0;
              } else {
                if (fi == dodossier.mediaListCount - 1) {
                  continue; // on cherche depuis le dossier suivant
                } else {
                  trouvedans = d;
                  fichier = fi + 1; // on ne renvoie jamais (di,fi)
                }
              }
            }
            break;
          }
        }
        if (dodossier.addressEscaped != null) {
          if (pattern.matcher(dodossier.addressEscaped).find()) {
            if (d > di) {
              trouvedans = d;
              fichier = 0;
            } else {
              if (dodossier.mediaListCount == 0) {
                trouvedans = d;
                fichier = 0;
              } else {
                if (fi == dodossier.mediaListCount - 1) {
                  continue; // on cherche depuis le dossier suivant
                } else {
                  trouvedans = d;
                  fichier = fi + 1; // on ne renvoie jamais (di,fi)
                }
              }
            }
            break;
          }
        }
        if (dodossier.mediaListCount > 0) {
          medialist = dodossier.mediaList;
          if (medialist.size() != dodossier.mediaListCount) {
            llog.d(TAG, "error mediaListCount shouldn't happen");
            continue;
          }
          Media media;
          int fichi = 0;
          if (d == di) { // on cherche depuis le fichier suivant pour ce dossier
            fichi = fi + 1;
          }
          for (int fich = fichi; fich < dodossier.mediaListCount; fich++) {
            media = medialist.get(fich);
            if (pattern.matcher(media.address).find()) {
              trouvedans = d;
              fichier = fich;
              break;
            }
            if (media.printName != null) {
              if (pattern.matcher(media.printName).find()) {
                trouvedans = d;
                fichier = fich;
                break;
              }
            }
            if (media.addressescaped != null) {
              if (pattern.matcher(media.addressescaped).find()) {
                trouvedans = d;
                fichier = fich;
                break;
              }
            }
          }
          if (trouvedans != -1) {
            break;
          }
        }
      }
    }
    gallerylock.unlock();
    return new int[]{trouvedans, fichier};
  }
  public List<String> findFoldersStartsWith(String startswith){
    List<String> dossiers = new ArrayList<>();
    gallerylock.lock();
    for (int i = 0 ; i < folderCount ; i++) {
      String dossier = gallery.get(i).address;
      if (dossier.startsWith(startswith)) {
        dossiers.add(dossier);
      }
    }
    gallerylock.unlock();
    return dossiers;
  }
  public List<String> findFoldersStartsWith(String startswith, int a, int b){
    List<String> dossiers = new ArrayList<>();
    gallerylock.lock();
    for (int i = a ; i < b && i < folderCount ; i++) {
      String dossier = gallery.get(i).address;
      if (dossier.startsWith(startswith)) {
        dossiers.add(dossier);
      }
    }
    gallerylock.unlock();
    return dossiers;
  }
  public void setFoldersFromDB(List<Dossier> folders){
    if (folders == null)
      folders = new ArrayList<>();
    gallerylock.lock();
    gallery.clear();
    int foldercount = folders.size();
    for (int i = 0; i < foldercount; i++) {
      gallery.add(new Ordner(folders.get(i).dossier, folders.get(i).fichierpardefaut, null));
    }
    folderCount = foldercount;
    redrawsplitviewbitmapm = true;
    gallerylock.unlock();
  }
  public void setFolders(List<String> folders, List<Integer> fichierpardefaut){
    if (folders == null)
      folders = new ArrayList<>();
    gallerylock.lock();
    gallery.clear();
    int foldercount = folders.size();
    for (int i = 0; i < foldercount; i++) {
      gallery.add(new Ordner(folders.get(i), fichierpardefaut.get(i), null));
    }
    folderCount = foldercount;
    redrawsplitviewbitmapm = true;
    gallerylock.unlock();
  }
  public int addFolder(int ajoutealaposition, String folder, boolean escape, int fichierpardefaut, List<Media> medias, int isonline){
    if (folder != null) {
      gallerylock.lock();
      if (ajoutealaposition < 0 || ajoutealaposition > folderCount)
        ajoutealaposition = folderCount;
      Ordner ordner = new Ordner(folder, fichierpardefaut, medias);
      if (escape) {
        try {
          ordner.addressEscaped = URLDecoder.decode(folder, "utf-8");
        } catch (UnsupportedEncodingException e) {
          e.printStackTrace();
        }
      }
      ordner.isOnline = isonline;
      gallery.add(ajoutealaposition, ordner);
      folderCount = gallery.size();
      int surfl = surf.size();
      for (int s = 0 ; s < surfl ; s++) {
        Surf thissurf = surf.get(s);
        if (ajoutealaposition < thissurf.ordnerIndex) {
          thissurf.ordnerIndex += 1;
        }
      }
      gallerylock.unlock();
      redrawsplitviewbitmapm = true;
    }
    return ajoutealaposition;
  }
  public void removeOnlineFolders() {
    gallerylock.lock();
    for (int d = folderCount - 1 ; d >= 0 ; d--) {
      Ordner ordnern = gallery.get(d);
      if (ordnern.isOnline != Media.online_no || couldBeOnline(ordnern.address)) {
        int filecount = ordnern.mediaListCount;
        for (int f = 0 ; f < filecount ; f++) {
          Media media = ordnern.mediaList.get(f);
          if (media.bitmap != null) {
            for (int b = 0; b < media.bitmap.length; b++) {
              if (media.bitmap[b] != null) {
                if (!media.bitmap[b].isRecycled()) {
                  media.bitmap[b].recycle();
                  media.bitmap[b] = null;
                }
              }
            }
            media.bitmap = null;
            numberthumsincache--;
          }
        }
        ordnern.mediaListCount = 0;
        if (ordnern.mediaList != null) {
          ordnern.mediaList.clear();
          ordnern.mediaList = null;
        }
        gallery.remove(d);
        folderCount = gallery.size();
        int surfl = surf.size();
        for (int s = 0; s < surfl; s++) {
          Surf thissurf = surf.get(s);
          if (d < thissurf.ordnerIndex) {
            //llog.d(TAG, s + " on garde le même dossier mais pos -1");
            thissurf.ordnerIndex -= 1; // -1 si c'était le dernier et seul dossier
          } else if (d == thissurf.ordnerIndex) {
            //llog.d(TAG, s + " on supprime le dossier principal on choisit le précédent");
            thissurf.ordnerIndex -= 1; // -1 si c'était le dernier et seul dossier
            if (thissurf.ordnerIndex < 0) {
              thissurf.ordnerIndexAddress = "";
              thissurf.mediaIndex = -1;
              thissurf.mediaIndexAddress = "";
            } else {
              Ordner ordner = gallery.get(thissurf.ordnerIndex);
              thissurf.ordnerIndexAddress = ordner.address;
              int ordnerfilessize = ordner.mediaListCount;
              if (ordnerfilessize > 0) {
                thissurf.mediaIndex = ordner.byDefaultShowMediaNumber;
                if (thissurf.mediaIndex >= ordnerfilessize)
                  thissurf.mediaIndex = ordnerfilessize - 1;
                if (thissurf.mediaIndex < 0)
                  thissurf.mediaIndex = 0;
                thissurf.mediaIndexAddress = ordner.mediaList.get(thissurf.mediaIndex).address;
              } else {
                thissurf.mediaIndex = -1;
                thissurf.mediaIndexAddress = "";
              }
            }
            thissurf.putbigpictureinmemory = true;
          }
        }
        redrawsplitviewbitmapm = true;
      }
    }
    gallerylock.unlock();
  }

  public void removeMediaFromGallery(String ordnerAddress, String mediaAddress, int d, int f){
    gallerylock.lock();
    if (ordnerAddress != null)
      d = correctFolderIndex(ordnerAddress, d);
    if (d >= 0 && d < folderCount) {
      if (mediaAddress != null)
        f = correctFileIndex(d, mediaAddress, f);
      if (f >= 0) {
        Ordner ordner = gallery.get(d);
        if (f < ordner.mediaListCount) {

          Media media = ordner.mediaList.get(f);
          media.filmstripCount = Media.filmstripNotCheckedYet;
          media.filmstripcurrent = -1;
          if (media.bitmap != null) {
            for (int b = 0; b < media.bitmap.length; b++) {
              if (media.bitmap[b] != null) {
                if (!media.bitmap[b].isRecycled()) {
                  media.bitmap[b].recycle();
                  media.bitmap[b] = null;
                  numberthumsincache -= 1;
                }
              }
            }
            media.bitmap = null;
          }

          ordner.mediaList.remove(f);
          ordner.mediaListCount = ordner.mediaList.size();

          int surfl = surf.size();
          for (int s = 0; s < surfl; s++) {
            Surf thissurf = surf.get(s);
            if (d == thissurf.ordnerIndex) {
              if (f < thissurf.mediaIndex) {
                //llog.d(TAG, s + " on garde le même fichier mais pos -1");
                thissurf.mediaIndex -= 1;
              } else if (f == thissurf.mediaIndex) {
                //llog.d(TAG, s + " on supprime le fichier principal on choisit le précédent");
                thissurf.mediaIndex -= 1; // -1 si c'était le dernier et seul fichier
                if (thissurf.mediaIndex < 0) {
                  thissurf.mediaIndexAddress = "";
                } else {
                  thissurf.mediaIndexAddress = ordner.mediaList.get(thissurf.mediaIndex).address;
                }
                thissurf.putbigpictureinmemory = true;
              }
            }
          }
        }
      }
    }
    gallerylock.unlock();
  }

  public void removeOrdnerFromGallery(String address, int position) {
    gallerylock.lock();
    if (address != null)
      position = correctFolderIndex(address, position);
    if (position >= 0 && position < folderCount) {

      gallery.remove(position);
      folderCount = gallery.size();

      int surfl = surf.size();
      for (int s = 0; s < surfl; s++) {
        Surf thissurf = surf.get(s);
        if (position < thissurf.ordnerIndex) {
          thissurf.ordnerIndex -= 1; // -1 si c'était le dernier et seul dossier
        } else if (position == thissurf.ordnerIndex) {
          thissurf.ordnerIndex -= 1; // -1 si c'était le dernier et seul dossier
          if (thissurf.ordnerIndex < 0) {
            thissurf.ordnerIndexAddress = "";
            thissurf.mediaIndex = -1;
            thissurf.mediaIndexAddress = "";
          } else {
            Ordner ordner = gallery.get(thissurf.ordnerIndex);
            thissurf.ordnerIndexAddress = ordner.address;
            int ordnerfilessize = ordner.mediaListCount;
            if (ordnerfilessize > 0) {
              thissurf.mediaIndex = ordner.byDefaultShowMediaNumber;
              if (thissurf.mediaIndex >= ordnerfilessize)
                thissurf.mediaIndex = ordnerfilessize - 1;
              if (thissurf.mediaIndex < 0)
                thissurf.mediaIndex = 0;
              thissurf.mediaIndexAddress = ordner.mediaList.get(thissurf.mediaIndex).address;
            } else {
              thissurf.mediaIndex = -1;
              thissurf.mediaIndexAddress = "";
            }
          }
          thissurf.putbigpictureinmemory = true;
        }
      }
      redrawsplitviewbitmapm = true;
    }
    gallerylock.unlock();
  }

  public void addFileToFolder(int d, Media file){
    if (d >= 0 && file != null) {
      gallerylock.lock();
      if (d < folderCount) {
        Ordner ordner = gallery.get(d);
        ordner.mediaList.add(file);
        ordner.mediaListCount = ordner.mediaList.size();
      }
      gallerylock.unlock();
    }
  }
  public void addFilesToFolder(int d, List<Media> filelist){
    if (d >= 0 && filelist != null) {
      gallerylock.lock();
      if (d < folderCount) {
        Ordner ordner = gallery.get(d);
        ordner.mediaList.addAll(filelist);
        ordner.mediaListCount = ordner.mediaList.size();
      }
      gallerylock.unlock();
    }
  }
  public int findFileInFolder(int d, String fichier){
    int filefound = -1;
    if (d >= 0 && fichier != null) {
      gallerylock.lock();
      if (d < folderCount) {
        Ordner ordner = gallery.get(d);
        List<Media> files = ordner.mediaList;
        int filecount = ordner.mediaListCount;
        for (int l = 0; l < filecount; l++) {
          if (fichier.equals(files.get(l).address)) {
            filefound = l;
            break;
          }
        }
      }
      gallerylock.unlock();
    }
    return filefound;
  }
  public final List<Integer> currentlyDisplayed = new ArrayList<>();
  public int getFileFreeAllBitmapsFromFolders(boolean forceclear){
    int freed = 0;
    gallerylock.lock();
    numberthumsincache = 0;
    if (forceclear)
      currentlyDisplayed.clear();
    int cdi = currentlyDisplayed.size() / 3;
    for (int d = 0 ; d < folderCount ; d++) {
      boolean isdisplayed = false;
      for (int h = 0 ; h < cdi ; h++) {
        if (d == currentlyDisplayed.get(h * 3)) {
          isdisplayed = true;
          //llog.d(TAG, "keep " + currentlyDisplayed.get(h * 3) + " : " + currentlyDisplayed.get(h * 3 + 1) + " to " + currentlyDisplayed.get(h * 3 + 2));
          break;
        }
      }
      Ordner ordner = gallery.get(d);
      int filecount = ordner.mediaListCount;
      for (int f = 0 ; f < filecount ; f++) {
        Media media = ordner.mediaList.get(f);
        if (isdisplayed) {
          boolean cont = false;
          for (int h = 0; h < cdi; h++) {
            if (d == currentlyDisplayed.get(h * 3)) {
              if (currentlyDisplayed.get(h * 3 + 1) <= f && f <= currentlyDisplayed.get(h * 3 + 2)) {
                if (media.bitmap != null) {
                  numberthumsincache++;
                }
                cont = true;
                break;
              }
            }
          }
          if (cont) {
            //llog.d(TAG, d + " " + f + " keep");
            continue;
          }
        }
        if (media.bitmap != null) {
          for (int b = 0; b < media.bitmap.length; b++) {
            if (media.bitmap[b] != null) {
              if (!media.bitmap[b].isRecycled()) { // only do necessary freeing
                media.bitmap[b].recycle();
                media.bitmap[b] = null;
                //llog.d(TAG, d + " " + f + " recycle");
              }
            }
          }
          media.bitmap = null;
          media.width = 0;
          media.height = 0;
          media.offsetX = 0;
          media.offsetY = 0;
          freed += 1;
        }
      }
      if (ordner.fileListCanBeRemovedFromCache) {
        if (!isdisplayed) {
          for (int i = 0; i < surf.size(); i++) {
            if (surf.get(i).ordnerIndex == d) {
              isdisplayed = true;
              break;
            }
          }
        }
        if (!isdisplayed) {
          ordner.hasNotBeenLoadedYet = true;
          ordner.mediaListCount = 0;
          if (ordner.mediaList != null) {
            ordner.mediaList.clear();
            ordner.mediaList = null;
          }
        }
      }
    }
    currentlyDisplayed.clear();
    gallerylock.unlock();
    return freed;
  }
  public int setFilesOrSetNotLoadedYet(int d, List<Media> filelist){
    int filecount = 0;
    if (d >= 0) {
      gallerylock.lock();
      if (d < folderCount) {
        Ordner ordner = gallery.get(d);
        filecount = ordner.mediaListCount;
        for (int f = 0 ; f < filecount ; f++) {
          Media media = ordner.mediaList.get(f);
          media.filmstripCount = Media.filmstripNotCheckedYet;
          media.filmstripcurrent = -1;
          if (media.bitmap != null) {
            for (int b = 0 ; b < media.bitmap.length ; b++) {
              if (media.bitmap[b] != null) {
                if (!media.bitmap[b].isRecycled()) {
                  media.bitmap[b].recycle();
                  media.bitmap[b] = null;
                  numberthumsincache -= 1;
                }
              }
            }
            media.bitmap = null;
          }
        }
        ordner.mediaListCount = 0;
        if (ordner.mediaList != null) {
          ordner.mediaList.clear();
          ordner.mediaList = null;
        }
        if (filelist == null) {
          filecount = 0;
          ordner.hasNotBeenLoadedYet = true;
        } else {
          ordner.mediaList = filelist;
          filecount = filelist.size();
          ordner.mediaListCount = filecount;
          ordner.hasNotBeenLoadedYet = false;
          // si on était sur ce dossier avant
          // on restaure notre position
          // (avec copy c'est toujours le cas)
          String dossiername = ordner.address;
          int surfl = surf.size();
          for (int s = 0 ; s < surfl ; s++) {
            Surf thissurf = surf.get(s);
            if (thissurf.ordnerIndexAddress.equals(dossiername)) {
              //llog.d(TAG, thissurf.myid + " est sur le dossier " + dossiername);
              for (int i = 0; i < filecount; i++) {
                if (thissurf.mediaIndexAddress.equals(ordner.mediaList.get(i).address)) {
                  //llog.d(TAG, thissurf.myid + " on corrige son index " + thissurf.fichierprincipal + " -> " + i);
                  thissurf.mediaIndex = i;
                  break;
                }
              }
            }
          }
          /*if (ordner.byDefaultShowMediaNumber == 0 && ordner.updateDeltaTime > 0) {
            llog.d(TAG, "dossier updatable on place en " + ordner.byDefaultShowMediaNumber + " -> " + (ordner.mediaListCount - 4));
            if (ordner.mediaListCount > 4)
              ordner.byDefaultShowMediaNumber = ordner.mediaListCount - 4;
          }*/
        }
      }
      gallerylock.unlock();
    }
    return filecount;
  }
  public void setFileIsOnlineLinkPage(int d, int f, int add){
    if (d >= 0 && f >= 0) {
      gallerylock.lock();
      if (d < folderCount) {
        Ordner ordner = gallery.get(d);
        int filecount = ordner.mediaListCount;
        if (f < filecount)
          ordner.mediaList.get(f).isonlinelinktonextpagei += add;
      }
      gallerylock.unlock();
    }
  }
  public int getFileIsOnlineLinkPage(int d, int f){
    int isonlinelinktonextpagei = 1;
    if (d >= 0 && f >= 0) {
      gallerylock.lock();
      if (d < folderCount) {
        Ordner ordner = gallery.get(d);
        int filecount = ordner.mediaListCount;
        if (f < filecount)
          isonlinelinktonextpagei = ordner.mediaList.get(f).isonlinelinktonextpagei;
      }
      gallerylock.unlock();
    }
    return isonlinelinktonextpagei;
  }
  public int getFileIsOnlineLinkPageL(int d, int f){
    int isonlinelinktonextpagetot = 1;
    if (d >= 0 && f >= 0) {
      gallerylock.lock();
      if (d < folderCount) {
        Ordner ordner = gallery.get(d);
        int filecount = ordner.mediaListCount;
        if (f < filecount)
          isonlinelinktonextpagetot = ordner.mediaList.get(f).isonlinelinktonextpagetot;
      }
      gallerylock.unlock();
    }
    return isonlinelinktonextpagetot;
  }
  public int getFileIsOnlineForumLevel(int d, int f){
    int isonlineforum = -1;
    if (d >= 0 && f >= 0) {
      gallerylock.lock();
      if (d < folderCount) {
        Ordner ordner = gallery.get(d);
        int filecount = ordner.mediaListCount;
        if (f < filecount)
          isonlineforum = ordner.mediaList.get(f).isonlineforumlevel;
      }
      gallerylock.unlock();
    }
    return isonlineforum;
  }
  public float[] getMediaDrawingPosition(int d, int f){
    float[] position = new float[]{0, 0, 1, 1};
    if (d >= 0 && f >= 0) {
      gallerylock.lock();
      if (d < folderCount) {
        Ordner ordner = gallery.get(d);
        int filecount = ordner.mediaListCount;
        if (f < filecount) {
          Media file = ordner.mediaList.get(f);
          position[0] = file.offsetX;
          position[1] = file.offsetY;
          position[2] = file.width;
          position[3] = file.height;
        }
      }
      gallerylock.unlock();
    }
    return position;
  }
  public String getMediaMetadata(int d, int f) {
    String metadata = null;
    if (d >= 0 && f >= 0) {
      gallerylock.lock();
      if (d < folderCount) {
        Ordner ordner = gallery.get(d);
        int filecount = ordner.mediaListCount;
        if (f < filecount) {
          Media file = ordner.mediaList.get(f);
          metadata = "";
          if (file.type == Media.media_picture) {
            if (file.printDetails != null)
              metadata += file.printDetails + "\n";
            if (file.printFooter != null)
              metadata += file.printFooter + "\n";
          }
          if (file.metadataaudio != 0) {
            metadata += file.metadataaudio + " audio";
          }
          if (file.metadatasubtitle != 0) {
            if (metadata.length() > 0)
              metadata += " ";
            metadata += file.metadatasubtitle + " sub";
          }
          if (file.metadatarotation != 0) {
            if (metadata.length() > 0)
              metadata += " ";
            metadata += file.metadatarotation + "°";
          }
          if (metadata.length() > 0)
            metadata += "\n";
          if (file.metadatacreationtime != null) {
            metadata += file.metadatacreationtime + "\n";
          }
         if (metadata.length() == 0)
           metadata = null;
        }
      }
      gallerylock.unlock();
    }
    return metadata;
  }
  public void setMediaMetadata(Media media){
    if (media.ordnerIndex >= 0 && media.index >= 0) {
      gallerylock.lock();
      media.ordnerIndex = correctFolderIndex(media.ordnerAddress, media.ordnerIndex);
      if (media.ordnerIndex >= 0) {
        media.index = correctFileIndex(media.ordnerIndex, media.address, media.index);
        if (media.index >= 0) {
          Ordner ordner = gallery.get(media.ordnerIndex);
          if (media.index < ordner.mediaListCount) {
            Media fichier = ordner.mediaList.get(media.index);
            fichier.metadatawidth = media.metadatawidth;
            fichier.metadataheight = media.metadataheight;
            fichier.metadatamaxwidth = media.metadatamaxwidth;
            fichier.metadatamaxheight = media.metadatamaxheight;
            fichier.metadataduration = media.metadataduration;
            fichier.metadatarotation = media.metadatarotation;
            fichier.metadataaudio = media.metadataaudio;
            fichier.metadatasubtitle = media.metadatasubtitle;
            if (media.playInSequence)
              fichier.playInSequence = media.playInSequence;
            if (media.playStartAtPosition != 0)
              fichier.playStartAtPosition = media.playStartAtPosition;
            if (media.subtitleAddress != null)
              fichier.subtitleAddress = media.subtitleAddress;
            if (media.printName != null) {
              if (media.printName.length() == 0)
                media.printName = null;
              fichier.printName = media.printName;
            }
            if (media.printDetails != null) {
              if (media.printDetails.length() == 0)
                media.printDetails = null;
              fichier.printDetails = media.printDetails;
            }
            if (media.printFooter != null) {
              if (media.printFooter.length() == 0)
                media.printFooter = null;
              fichier.printFooter = media.printFooter;
            }
            if (media.metadatacreationtime != null) {
              //media.metadatacreationtime = media.metadatacreationtime.replace('/', '-');
              fichier.metadatacreationtime = media.metadatacreationtime;
            }
          }
        }
      }
      gallerylock.unlock();
    }
  }
  public void setMediaPrintName(int d, int f, String prettyprint){
    if (d >= 0 && f >= 0 && d < folderCount && prettyprint != null) {
      gallerylock.lock();
      if (f < gallery.get(d).mediaListCount)
        gallery.get(d).mediaList.get(f).printName = prettyprint;
      gallerylock.unlock();
    }
  }
  public void setMediaPrintFooter(int d, int f, String prettyprint){
    if (d >= 0 && f >= 0 && d < folderCount && prettyprint != null) {
      gallerylock.lock();
      if (f < gallery.get(d).mediaListCount)
        gallery.get(d).mediaList.get(f).printFooter = prettyprint;
      gallerylock.unlock();
    }
  }
  public void setMediaPrintDetails(int d, int f, String prettyprint){
    if (d >= 0 && f >= 0 && d < folderCount && prettyprint != null) {
      gallerylock.lock();
      if (f < gallery.get(d).mediaListCount)
        gallery.get(d).mediaList.get(f).printDetails = prettyprint;
      gallerylock.unlock();
    }
  }
  public void setMediaPrintNameFooterDetails(Media media, String[] prettyprint){ // n'est plus utilisée
    if (media.ordnerIndex >= 0 && media.index >= 0) {
      gallerylock.lock();
      media.ordnerIndex = correctFolderIndex(media.ordnerAddress, media.ordnerIndex);
      if (media.ordnerIndex >= 0) {
        media.index = correctFileIndex(media.ordnerIndex, media.address, media.index);
        if (media.index >= 0) {
          Ordner ordner = gallery.get(media.ordnerIndex);
          if (media.index < ordner.mediaListCount) {
            Media fichier = ordner.mediaList.get(media.index);
            fichier.printName = prettyprint[0];
            fichier.printFooter = prettyprint[1];
            fichier.printDetails = prettyprint[2];
          }
        }
      }
      gallerylock.unlock();
    }
  }
  public void setMediaAddressToGetThumbnail(Media media, String minipic){
    if (media.ordnerIndex >= 0 && media.index >= 0) {
      gallerylock.lock();
      media.ordnerIndex = correctFolderIndex(media.ordnerAddress, media.ordnerIndex);
      if (media.ordnerIndex >= 0) {
        media.index = correctFileIndex(media.ordnerIndex, media.address, media.index);
        if (media.index >= 0) {
          Ordner ordner = gallery.get(media.ordnerIndex);
          if (media.index < ordner.mediaListCount) {
            ordner.mediaList.get(media.index).addressToGetThumbnail = minipic;
          }
        }
      }
      gallerylock.unlock();
    }
  }
  private final static float tolerance = 3.0f;
  public void setMediaFilmstripCurrentAndCount(Media media, int bitmapcount, int bitmapcurrent){
    if (media.ordnerIndex >= 0 && media.index >= 0) {
        gallerylock.lock();
        media.ordnerIndex = correctFolderIndex(media.ordnerAddress, media.ordnerIndex);
        if (media.ordnerIndex >= 0) {
          media.index = correctFileIndex(media.ordnerIndex, media.address, media.index);
          if (media.index >= 0) {
            Ordner ordner = gallery.get(media.ordnerIndex);
            if (media.index < ordner.mediaListCount) {
              Media media2 = ordner.mediaList.get(media.index);
              media2.filmstripCount = bitmapcount;
              media2.filmstripcurrent = bitmapcurrent;
            }
          }
        }
        gallerylock.unlock();
    }
  }
  public String[] getMediaButtonAction(Media media){
    String[] buttonaction = null;
    if (media.ordnerIndex >= 0 && media.index >= 0) {
      gallerylock.lock();
      media.ordnerIndex = correctFolderIndex(media.ordnerAddress, media.ordnerIndex);
      if (media.ordnerIndex >= 0) {
        media.index = correctFileIndex(media.ordnerIndex, media.address, media.index);
        if (media.index >= 0) {
          Ordner ordner = gallery.get(media.ordnerIndex);
          if (media.index < ordner.mediaListCount) {
            Media media2 = ordner.mediaList.get(media.index);
            if (media2.buttonaction != null) {
              int bl = media2.buttonaction.length;
              buttonaction = new String[bl];
              for (int i = 0; i < bl; i++)
                buttonaction[i] = media2.buttonaction[i];
            }
          }
        }
      }
      gallerylock.unlock();
    }
    return buttonaction;
  }
  public void setMediaButtonAction(int d, int f, String[] mbuttonaction){
    if (d >= 0 && f >= 0 && d < folderCount) {
      gallerylock.lock();
      if (f < gallery.get(d).mediaListCount)
        gallery.get(d).mediaList.get(f).buttonaction = mbuttonaction;
      gallerylock.unlock();
    }
  }
  public void setMediaButtonAction(Media media, String[] mbuttonaction){
    if (media.ordnerIndex >= 0 && media.index >= 0) {
      gallerylock.lock();
      media.ordnerIndex = correctFolderIndex(media.ordnerAddress, media.ordnerIndex);
      if (media.ordnerIndex >= 0) {
        media.index = correctFileIndex(media.ordnerIndex, media.address, media.index);
        if (media.index >= 0) {
          Ordner ordner = gallery.get(media.ordnerIndex);
          if (media.index < ordner.mediaListCount) {
            Media media2 = ordner.mediaList.get(media.index);
            media2.buttonaction = mbuttonaction;
          }
        }
      }
      gallerylock.unlock();
    }
  }
  public int setFileBitmapinmemory(Media media, Bitmap[] bitmap, int bitmapcount, int bitmapcurrent){
    int successmem = -1;
    if (media.ordnerIndex >= 0 && media.index >= 0 && bitmap != null) {
      if (bitmap[0] != null) {
        gallerylock.lock();
        int idossier = correctFolderIndex(media.ordnerAddress, media.ordnerIndex);
        if (idossier != media.ordnerIndex)
          llog.d(TAG, "idossier different " + idossier + " " + media.ordnerIndex);
        media.ordnerIndex = idossier;
        if (media.ordnerIndex >= 0) {
          int ifichier = correctFileIndex(media.ordnerIndex, media.address, media.index);
          if (ifichier != media.index)
            llog.d(TAG, "ifichier different " + ifichier + " " + media.index);
          media.index = ifichier;
          if (media.index >= 0) {
            Ordner ordner = gallery.get(media.ordnerIndex);
            if (media.index < ordner.mediaListCount) {
              int width = bitmap[0].getWidth();
              int height = bitmap[0].getHeight();
              float decalx = (thumbsize - width) / 2.0f;
              float decaly = (thumbsize - height) / 2.0f;
              boolean regenerate = false;
              /*if (media.metadatamaxwidth > 0 && media.metadatamaxwidth != thumbsize)
                regenerate = false;*/
              if (   width > thumbsize + tolerance
                  || height > thumbsize + tolerance
                  || !(Math.abs(decalx) < tolerance || Math.abs(decaly) < tolerance)) // souvent 2.0x*
                regenerate = true;
              Media media2 = ordner.mediaList.get(media.index);
              if (media2.bitmap != null) {
                for (int i = 0; i < media2.bitmap.length; i++) {
                  if (media2.bitmap[i] != null) {
                    if (!media2.bitmap[i].isRecycled()) {
                      media2.bitmap[i].recycle();
                      numberthumsincache -= 1;
                    }
                    media2.bitmap[i] = null;
                  }
                }
              }
              media2.bitmap = null;
              media2.width = 0;
              media2.height = 0;
              media2.offsetX = 0;
              media2.offsetY = 0;
              if (!regenerate) {
                media2.bitmap = bitmap;
                media2.bitmap[0].prepareToDraw();
                if (bitmapcount < 0) {
                  llog.w(TAG, "bitmapcount < 0 rectify " + media.address);
                  bitmapcount = 1;
                }
                if (bitmapcurrent < 0) {
                  llog.w(TAG, "bitmapcurrent < 0 rectify " + media.address);
                  bitmapcurrent = 0;
                }
                media2.filmstripCount = bitmapcount;
                media2.filmstripcurrent = bitmapcurrent;
                media2.bitmapaskednext = false;
                media2.width = width;
                media2.height = height;
                media2.offsetX = decalx;
                media2.offsetY = decaly;
                numberthumsincache += bitmap.length;
                // si on a pas de miniature a thumbsize on les recréé
                successmem = 0;
                //llog.d(TAG, "setFileBitmapinmemory " + media.address);
              } else { // sinon on redimensionne les thumbs
                //llog.d(TAG, "recreate miniature since decalxy = " + Math.abs(decalx) + "x" + Math.abs(decaly));
                successmem = 1;
                for (Bitmap bmp : bitmap) {
                  if (bmp != null) {
                    if (!bmp.isRecycled()) {
                      bmp.recycle();
                    }
                    bmp = null;
                  }
                }
                bitmap = null;
              }
            }
          } else {
            llog.d(TAG, "setFileBitmapinmemory ifichier " + media.ordnerIndex + " " + media.address);
          }
        } else {
          llog.d(TAG, "setFileBitmapinmemory idossier " + media.ordnerAddress + " " + media.address);
        }
        gallerylock.unlock();
      } else {
        llog.d(TAG, "setFileBitmapinmemory idossier " + media.ordnerIndex + " ifichier " + media.index + " bitmap[0] is null ? " + (bitmap[0]==null));
      }
    } else {
      llog.d(TAG, "setFileBitmapinmemory idossier " + media.ordnerIndex + " ifichier " + media.index + " bitmap is null ? " + (bitmap==null) + " : " + media.ordnerAddress + " : " + media.address);
    }
    return successmem;
  }
  public Bitmap tempBitmap = null;
  public Bitmap genTempBitmap() {
    float ih = 40.0f;
    float iw = ih * 1.5f;
    Bitmap bitmap = Bitmap.createBitmap((int) iw, (int) ih, Bitmap.Config.ARGB_8888);
    Canvas canvas = new Canvas(bitmap);
    Paint paint = new Paint();
    paint.setStrokeWidth(4.0f);
    paint.setColor(CouleurBgNoMiniature);
    paint.setStyle(Paint.Style.STROKE);
    paint.setAntiAlias(true);
    /*float bot = ih * 0.94f;
    Point point1_draw = new Point((int) (iw * 0.04f), (int) (bot));
    Point point2_draw = new Point((int) (iw * 0.25f), (int) (ih * 0.44f));
    Point point3_draw = new Point((int) (iw * 0.45f), (int) (bot));
    Point point4_draw = new Point((int) (iw * 0.37f), (int) (bot * 0.71f));
    Point point5_draw = new Point((int) (iw * 0.61f), (int) (ih * 0.09f));
    Point point6_draw = new Point((int) (iw * 0.95f), (int) (bot));
    Path path = new Path();
    path.moveTo(point1_draw.x, point1_draw.y);
    path.lineTo(point2_draw.x, point2_draw.y);
    path.lineTo(point3_draw.x, point3_draw.y);
    path.moveTo(point4_draw.x, point4_draw.y);
    path.lineTo(point5_draw.x, point5_draw.y);
    path.lineTo(point6_draw.x, point6_draw.y);
    canvas.drawPath(path, paint);*/
    float bot = ih * 0.94f;
    Point point00_draw =new Point((int) (iw * 0.18f), (int) (ih * 1.0f));
    Point point0_draw = new Point((int) (iw * 0.04f), (int) (ih * 1.0f));
    Point point1_draw = new Point((int) (iw * 0.04f), (int) (bot));
    Point point2_draw = new Point((int) (iw * 0.25f), (int) (ih * 0.44f));
    Point point3_draw = new Point((int) (iw * 0.45f), (int) (bot));
    Point point33_draw =new Point((int) (iw * 0.45f), (int) (ih * 1.0f));
    Point point34_draw =new Point((int) (iw * 0.66f), (int) (ih * 1.0f));
    Point point4_draw = new Point((int) (iw * 0.37f), (int) (bot * 0.71f));
    Point point5_draw = new Point((int) (iw * 0.61f), (int) (ih * 0.09f));
    Point point6_draw = new Point((int) (iw * 0.95f), (int) (bot));
    Point point7_draw = new Point((int) (iw * 0.95f), (int) (ih * 1.0f));
    Point point8_draw = new Point((int) (iw * 0.66f), (int) (ih * 1.0f));
    Path path = new Path();
    path.moveTo(point00_draw.x, point00_draw.y);
    path.lineTo(point0_draw.x, point0_draw.y);
    path.lineTo(point1_draw.x, point1_draw.y);
    path.lineTo(point2_draw.x, point2_draw.y);
    path.lineTo(point3_draw.x, point3_draw.y);
    path.moveTo(point4_draw.x, point4_draw.y);
    path.lineTo(point5_draw.x, point5_draw.y);
    path.lineTo(point6_draw.x, point6_draw.y);
    canvas.drawPath(path, paint);
    bitmap.prepareToDraw();
    return bitmap;
  }
  public void drawTempBitmap(Canvas canvas, float posx, float posy){
    if (tempBitmap == null) {
      tempBitmap = genTempBitmap();
    }
    if (tempBitmap.isRecycled()) {
      tempBitmap = genTempBitmap();
    }
    canvas.drawBitmap(tempBitmap, posx, posy, null);
  }
  public void drawFileBitmap(int d, int f, Canvas canvas, float posx, float posy, int currid, int thumbcurrentlydisplayed){
    if (d >= 0 && f >= 0) {
      gallerylock.lock();
      if (d < folderCount) {
        Ordner ordner = gallery.get(d);
        int filecount = ordner.mediaListCount;
        if (f < filecount) {
          Media media = ordner.mediaList.get(f);
          if (media.bitmap != null) {
            long currtime = System.currentTimeMillis();
            if (media.type != Media.media_picture
                    && media.filmstripCount > 1
                    && !media.bitmapaskednext
                    && currtime - surf.get(currid).filmstriplastchange > filmstripanimatetime
            ) {
              int bitmapcurrent = (int) ((filmstripreftime / filmstripanimatetime) % (media.filmstripCount));
              if (bitmapcurrent != media.filmstripcurrent && currtime - media.filmstriplastchange > filmstripanimatetime) {
                media.filmstriplastchange = currtime;
                media.bitmapaskednext = true;
                // ici thumbcurrentlydisplayed n'est plus valable car changé juste après donc on l'ignore
                //llog.d(TAG, "update miniature " + media.filmstripcurrent + " -> " + bitmapcurrent + " / " + media.filmstripCount);
                try {
                  commandeminiaturethreadqueue.put(new String[]{"updateminiature",
                          String.valueOf(currid), String.valueOf(d), String.valueOf(f),
                          String.valueOf(thumbcurrentlydisplayed)});
                } catch (InterruptedException e) {
                  e.printStackTrace();
                }
              }
            }
            if (media.bitmap.length > 0) {
              if (media.bitmap[0] != null) {
                if (!media.bitmap[0].isRecycled()) {
                  canvas.drawBitmap(media.bitmap[0], posx, posy, null);
                } else {
                  llog.d(TAG, "drawFileBitmap file.bitmap[0].isRecycled() shouldn't happen");
                  media.filmstripCount = Media.filmstripNotCheckedYet;
                  media.filmstripcurrent = -1;
                  media.bitmap = null;
                }
              } else {
                llog.d(TAG, "drawFileBitmap file.bitmap[0] == null shouldn't happen");
                media.filmstripCount = Media.filmstripNotCheckedYet;
                media.filmstripcurrent = -1;
                media.bitmap = null;
              }
            } else {
              llog.d(TAG, "drawFileBitmap cetindex >= file.bitmapcount shouldn't happen");
              media.filmstripCount = Media.filmstripNotCheckedYet;
              media.filmstripcurrent = -1;
              media.bitmap = null;
            }
          }
        }
      }
      gallerylock.unlock();
    }
  }
  public boolean getFileBitmapInMemory(int d, int f){
    boolean bitmapinmemory = false;
    if (d >= 0 && f >= 0) {
      gallerylock.lock();
      if (d < folderCount) {
        Ordner ordner = gallery.get(d);
        int filecount = ordner.mediaListCount;
        if (f < filecount) {
          Media media = ordner.mediaList.get(f);
          if (media.bitmap != null && media.filmstripCount != Media.filmstripNotCheckedYet) {
            bitmapinmemory = true;
          }
        }
      }
      gallerylock.unlock();
    }
    return bitmapinmemory;
  }
  public Media copymedia(int d, int f){
    Media media = null;
    if (d >= 0 && f >= 0) {
      gallerylock.lock();
      if (d < folderCount) {
        Ordner ordner = gallery.get(d);
        int filecount = ordner.mediaListCount;
        if (f < filecount) {
          Media mediasource = ordner.mediaList.get(f);
          media = mediasource.copyMedia();
        }
      }
      gallerylock.unlock();
    }
    return media;
  }
  public int getMediaType(int d, int f){
    int type = Media.media_picture;
    if (d >= 0 && f >= 0) {
      gallerylock.lock();
      if (d < folderCount) {
        Ordner ordner = gallery.get(d);
        int filecount = ordner.mediaListCount;
        if (f < filecount)
          type = ordner.mediaList.get(f).type;
      }
      gallerylock.unlock();
    }
    return type;
  }
  public int getMediaAddressToGetLibextractorsThumbnail(int d, int f){
    int addressToGetLibextractorsThumbnail = -1;
    if (d >= 0 && f >= 0) {
      gallerylock.lock();
      if (d < folderCount) {
        Ordner ordner = gallery.get(d);
        int filecount = ordner.mediaListCount;
        if (f < filecount)
          addressToGetLibextractorsThumbnail = ordner.mediaList.get(f).addressToGetLibextractorsThumbnail;
      }
      gallerylock.unlock();
    }
    return addressToGetLibextractorsThumbnail;
  }
  public int getMediaIsOnline(int d, int f){
    int isonline = Media.online_no;
    if (d >= 0 && f >= 0) {
      gallerylock.lock();
      if (d < folderCount) {
        Ordner ordner = gallery.get(d);
        int filecount = ordner.mediaListCount;
        if (f < filecount)
          isonline = ordner.mediaList.get(f).isOnline;
      }
      gallerylock.unlock();
    }
    return isonline;
  }
  public int getMediaIsOnlineLevel(int d, int f){
    int isonlinelevel = 0;
    if (d >= 0 && f >= 0) {
      gallerylock.lock();
      if (d < folderCount) {
        Ordner ordner = gallery.get(d);
        int filecount = ordner.mediaListCount;
        if (f < filecount)
          isonlinelevel = ordner.mediaList.get(f).isonlinelevel;
      }
      gallerylock.unlock();
    }
    return isonlinelevel;
  }
  public String[] getMediaIsAButtonAction(int d, int f){
    String[] buttonaction = null;
    if (d >= 0 && f >= 0) {
      gallerylock.lock();
      if (d < folderCount) {
        Ordner ordner = gallery.get(d);
        int filecount = ordner.mediaListCount;
        if (f < filecount) {
          Media media2 = ordner.mediaList.get(f);
          if (media2.buttonaction != null) {
            int bl = media2.buttonaction.length;
            buttonaction = new String[bl];
            for (int i = 0 ; i < bl ; i++)
              buttonaction[i] = media2.buttonaction[i];
          }
        }
      }
      gallerylock.unlock();
    }
    return buttonaction;
  }
  public boolean getMediaIsALinkThatCreatesAnOrdner(int d, int f){
    boolean isonlinelink = false;
    if (d >= 0 && f >= 0) {
      gallerylock.lock();
      if (d < folderCount) {
        Ordner ordner = gallery.get(d);
        int filecount = ordner.mediaListCount;
        if (f < filecount)
          isonlinelink = ordner.mediaList.get(f).isALinkThatCreatesAnOrdner;
      }
      gallerylock.unlock();
    }
    return isonlinelink;
  }
  public String getMediaPrintName(int d, int f){
    String prettyprint = null;
    if (d >= 0 && f >= 0) {
      gallerylock.lock();
      if (d < folderCount) {
        Ordner ordner = gallery.get(d);
        int filecount = ordner.mediaListCount;
        if (f < filecount)
          prettyprint = ordner.mediaList.get(f).printName;
      }
      gallerylock.unlock();
    }
    return prettyprint;
  }
  public String getMediaPrintDetails(int d, int f){
    String printFooter = null;
    if (d >= 0 && f >= 0) {
      gallerylock.lock();
      if (d < folderCount) {
        Ordner ordner = gallery.get(d);
        int filecount = ordner.mediaListCount;
        if (f < filecount)
          printFooter = ordner.mediaList.get(f).printDetails;
      }
      gallerylock.unlock();
    }
    return printFooter;
  }
  public String getMediaPrintFooter(int d, int f){
    String printFooter = null;
    if (d >= 0 && f >= 0) {
      gallerylock.lock();
      if (d < folderCount) {
        Ordner ordner = gallery.get(d);
        int filecount = ordner.mediaListCount;
        if (f < filecount)
          printFooter = ordner.mediaList.get(f).printFooter;
      }
      gallerylock.unlock();
    }
    return printFooter;
  }
  public String getMediaOrdnerAddress(int d, int f){
    String dossier = "";
    if (d >= 0 && f >= 0) {
      gallerylock.lock();
      if (d < folderCount) {
        Ordner ordner = gallery.get(d);
        int filecount = ordner.mediaListCount;
        if (f < filecount)
          dossier = ordner.mediaList.get(f).ordnerAddress;
      }
      gallerylock.unlock();
    }
    return dossier;
  }
  public String getMediaOrdnerPrintName(int d, int f){
    String dossier = "";
    if (d >= 0 && f >= 0) {
      gallerylock.lock();
      if (d < folderCount) {
        Ordner ordner = gallery.get(d);
        int filecount = ordner.mediaListCount;
        if (f < filecount)
          dossier = ordner.mediaList.get(f).ordnerPrintName;
      }
      gallerylock.unlock();
    }
    return dossier;
  }
  public boolean getMediaPlayInSequence(int d, int f){
    boolean playInSequence = false;
    if (d >= 0 && f >= 0) {
      gallerylock.lock();
      if (d < folderCount) {
        Ordner ordner = gallery.get(d);
        int filecount = ordner.mediaListCount;
        if (f < filecount)
          playInSequence = ordner.mediaList.get(f).playInSequence;
      }
      gallerylock.unlock();
    }
    return playInSequence;
  }
  public int getMediaPlayStartAtPosition(int d, int f){
    int playStartAtPosition = 0;
    if (d >= 0 && f >= 0) {
      gallerylock.lock();
      if (d < folderCount) {
        Ordner ordner = gallery.get(d);
        int filecount = ordner.mediaListCount;
        if (f < filecount)
          playStartAtPosition = ordner.mediaList.get(f).playStartAtPosition;
      }
      gallerylock.unlock();
    }
    return playStartAtPosition;
  }
  public String[] getMediaSubtitleAddress(int d, int f, String address){
    String[] subtitleAddress = null;
    if (d >= 0 && f >= 0) {
      gallerylock.lock();
      if (d < folderCount) {
        Ordner ordner = gallery.get(d);
        int filecount = ordner.mediaListCount;
        if (f < filecount) {
          if (ordner.mediaList.get(f).subtitleAddress != null) {
            int subtitleAddressl = ordner.mediaList.get(f).subtitleAddress.size();
            if (subtitleAddressl > 0) {
              subtitleAddress = new String[subtitleAddressl + 1];
              subtitleAddress[0] = address;
              for (int i = 0 ; i < subtitleAddressl ; i++) {
                subtitleAddress[i + 1] = ordner.mediaList.get(f).subtitleAddress.get(i);
              }
            }
          }
        }
      }
      gallerylock.unlock();
    }
    return subtitleAddress;
  }

  public String getMediaAddressEscaped(int d, int f){
    String filename = "";
    if (d >= 0 && f >= 0) {
      gallerylock.lock();
      if (d < folderCount) {
        Ordner ordner = gallery.get(d);
        int filecount = ordner.mediaListCount;
        if (f < filecount) {
          if (ordner.mediaList.get(f).addressescaped != null)
            filename = ordner.mediaList.get(f).addressescaped;
          else
            filename = ordner.mediaList.get(f).address;
        }
      }
      gallerylock.unlock();
    }
    return filename;
  }
  public String getMediaAddress(int d, int f){
    String filename = "";
    if (d >= 0 && f >= 0) {
      gallerylock.lock();
      if (d < folderCount) {
        Ordner ordner = gallery.get(d);
        int filecount = ordner.mediaListCount;
        if (f < filecount)
          filename = ordner.mediaList.get(f).address;
      }
      gallerylock.unlock();
    }
    return filename;
  }
  public String getBookmarkToOrdner(int d, int f){
    String filename = null;
    if (d >= 0 && f >= 0) {
      gallerylock.lock();
      if (d < folderCount) {
        Ordner ordner = gallery.get(d);
        int filecount = ordner.mediaListCount;
        if (f < filecount)
          filename = ordner.mediaList.get(f).bookmarkToOrdner;
      }
      gallerylock.unlock();
    }
    return filename;
  }
  public int findMediaAddress(int d, String fichier){
    int filefound = -1;
    if (d >= 0 && fichier != null) {
      gallerylock.lock();
      if (d < folderCount) {
        Ordner ordner = gallery.get(d);
        if (!ordner.hasNotBeenLoadedYet)
          filefound = -2;
        if (ordner.mediaListCount > 0) {
          List<Media> files = ordner.mediaList;
          for (int i = 0; i < ordner.mediaListCount; i++) {
            if (files.get(i).address.equals(fichier)) {
              filefound = i;
              break;
            }
          }
        }
      }
      gallerylock.unlock();
    }
    return filefound;
  }
  public int[] findFileNameInAllBut(int dn, int fn, String fichier){
    int[] found = null;
    if (fichier != null && dn >= 0 && dn < folderCount) {
      int fichendi = fichier.lastIndexOf('/');
      if (fichendi >= 0 && fichendi < fichier.length() - 1) {
        String fichend = fichier.substring(fichendi); // keep the slash for the search
        gallerylock.lock();
        for (int d = dn; d < folderCount; d++) {
          Ordner ordner = gallery.get(d);
          List<Media> files = ordner.mediaList;
          int filecount = ordner.mediaListCount;
          int fi = 0;
          if (d == dn)
            fi = fn + 1;
          for (int f = fi ; f < filecount ; f++) {
            if (files.get(f).address.endsWith(fichend)) {
              found = new int[]{d, f};
              break;
            }
          }
          if (found != null)
            break;
        }
        if (found == null) {
          for (int d = 0; d <= dn; d++) {
            Ordner ordner = gallery.get(d);
            List<Media> files = ordner.mediaList;
            int filecount = ordner.mediaListCount;
            int ff = filecount;
            if (d == dn)
              ff = fn;
            for (int f = 0 ; f < ff ; f++) {
              if (files.get(f).address.endsWith(fichend)) {
                found = new int[]{d, f};
                break;
              }
            }
            if (found != null)
              break;
          }
        }
        gallerylock.unlock();
      }
    }
    return found;
  }
  public void copySelectedToData(int d, int df, boolean selectit, String collectionAddress, boolean bailIfAlreadyPresent){
    if (d >= 0 && collectionAddress != null && d < folderCount) {
      Collection collection = getCollection(collectionAddress); // always currCollectionAddress
      if (collection != null) {
        boolean istempselection = false;
        if (collectionAddress.equals("temp"))
          istempselection = true;
        boolean iscurrentcollection = false;
        if (collectionAddress.equals(currCollectionAddress))
          iscurrentcollection = true;
        gallerylock.lock();
        Ordner ordner = gallery.get(d);
        String defaultOrdnerName = Gallery.virtualCollectionFolder + currCollectionPrintName + "/" + ordner.address;
        if (ordner.addressEscaped != null)
          defaultOrdnerName = Gallery.virtualCollectionFolder + currCollectionPrintName + "/" + ordner.addressEscaped;
        else
          defaultOrdnerName = Gallery.virtualCollectionFolder + currCollectionPrintName + "/" + ordner.address;
        String toOrdnerNamed = getCurrCollectionOrdnerListI(currCollectionAddToFolderi, defaultOrdnerName);
        if (toOrdnerNamed.length() == 0)
          toOrdnerNamed = Gallery.virtualCollectionFolder + currCollectionPrintName + "/";
        int filecount = ordner.mediaListCount;
        int fi = df;
        int ff = df + 1;
        if (df < 0) {
          fi = 0;
          ff = filecount;
          if (!istempselection) // create and put inside its own folder instead of everything in the default one
            toOrdnerNamed = defaultOrdnerName;
        }
        if (ff > filecount)
          ff = filecount;
        for (int f = fi ; f < ff ; f++) {
          Media media = ordner.mediaList.get(f);
          collection.copyMediaToData(media, toOrdnerNamed, selectit, bailIfAlreadyPresent);
          if (!istempselection && iscurrentcollection && f == fi)
            setCurrCollectionOrdnerList(collection.displayedOrdnerList);
        }
        gallerylock.unlock();
        if (iscurrentcollection) {
          currCollectionSelectedCount = collection.selectedCount;
        }
      }
    }
  }
  public boolean getFileIsBitmapSelected(int d, int f){
    boolean isselected = false;
    if (d >= 0 && f >= 0) {
      gallerylock.lock();
      if (d < folderCount) {
        Ordner ordner = gallery.get(d);
        int filecount = ordner.mediaListCount;
        if (f < filecount) {
          isselected = ordner.mediaList.get(f).isSelected;
          if (!ordner.mediaList.get(f).selectedchecked) {
            // we already loaded the selection data
            // but we still didn't populate this thumbnail info with it
            ordner.mediaList.get(f).selectedchecked = true;
            String dossier = ordner.address;
            String fichier = ordner.mediaList.get(f).address;
            try {
              commandethreaddatabase.put(new String[]{"isbitmapselectedcheck", String.valueOf(d), String.valueOf(f), dossier, fichier});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          }
        }
      }
      gallerylock.unlock();
    }
    return isselected;
  }
  public void setFileIsBitmapSelected(int d, int f, boolean isselected){
    if (d >= 0 && f >= 0) {
      gallerylock.lock();
      if (d < folderCount) {
        Ordner ordner = gallery.get(d);
        int filecount = ordner.mediaListCount;
        if (f < filecount) {
          ordner.mediaList.get(f).isSelected = isselected;
        }
      }
      gallerylock.unlock();
    }
  }
  public boolean clearAllSelected(){
    boolean switchedselection = false;
    gallerylock.lock();
    for (int d = 0 ; d < folderCount ; d++) {
      Ordner ordner = gallery.get(d);
      int filecount = ordner.mediaListCount;
      for (int f = 0 ; f < filecount ; f++) {
        Media file = ordner.mediaList.get(f);
        file.isSelected = false;
        file.selectedchecked = false;
      }
    }
    gallerylock.unlock();
    return switchedselection;
  }
  public boolean getFileClearSelected(int d, int f){
    boolean switchedselection = false;
    if (d >= 0 && f >= 0) {
      gallerylock.lock();
      if (d < folderCount) {
        Ordner ordner = gallery.get(d);
        int filecount = ordner.mediaListCount;
        if (f < filecount) {
          Media file = ordner.mediaList.get(f);
          file.isSelected = false;
          file.selectedchecked = false;
        }
      }
      gallerylock.unlock();
    }
    return switchedselection;
  }
  public void getFileSwitchSelected(int d, int f){
    if (d >= 0 && f >= 0) {
      gallerylock.lock();
      if (d < folderCount) {
        Ordner ordner = gallery.get(d);
        int filecount = ordner.mediaListCount;
        if (f < filecount) {
          Media media = ordner.mediaList.get(f);
          boolean selectit = !media.isSelected;
          //llog.d(TAG, "getFileSwitchSelected " + media.isSelected + " -> " + selectit);
          try {
            commandethreaddatabase.put(new String[]{"selectfile", String.valueOf(d), String.valueOf(f), String.valueOf(selectit), media.address, String.valueOf(false)});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        }
      }
      gallerylock.unlock();
    }
  }
  public ArrayList<Integer> oo = null;
  public int[] played = null;
  public void savePlayCountToFile(String dest, int o, int f, String address){
    gallerylock.lock();
    if (played == null || oo == null) {
      oo = getOrdnersFirstLevel();
      int tl = oo.size();
      played = new int[tl];
      for (int d = 0; d < tl; d++)
        played[d] = 0;
    }
    int tl = oo.size();
    for (int d = 0 ; d < tl ; d++) {
      int firstordner = oo.get(d);
      int lastordner;
      if (d == tl - 1)
        lastordner = folderCount;
      else
        lastordner = oo.get(d + 1);
      if (firstordner <= o && o < lastordner) {
        played[d] += 1;
        if (address != null)
          llog.d(TAG, "pmalc " + String.format("> %3d %4d %3dx ", d, oo.get(d), played[d]) + gallery.get(oo.get(d)).address + " o,f " + o + "," + f + " " + address + "\n");
      } else {
        if (address != null)
          llog.d(TAG, "pmalc " + String.format("  %3d %4d %3dx ", d, oo.get(d), played[d]) + gallery.get(oo.get(d)).address + "\n");
      }
    }
    /*
    llog.d(TAG, "wrote to " + dest + "/file.txt");
    FileWriter fos;
    try{
      fos = new FileWriter(dest + "/file.txt", false);
      for (int d = 0 ; d < tl ; d++) {
        fos.write(d + "\t" + oo.get(d) + "\t" + played[d] + "\t" + gallery.get(oo.get(d)).address + "\n");
      }
      fos.flush();
      fos.close();
    } catch (IOException e) {
      llog.d(TAG, "error writing to file");
      e.printStackTrace();
    }
    */
    gallerylock.unlock();
  }
  public boolean getFileSetSelected(int d, int f, boolean selectit){
    boolean switchedselection = false;
    if (d >= 0 && f >= 0) {
      gallerylock.lock();
      if (d < folderCount) {
        Ordner ordner = gallery.get(d);
        int filecount = ordner.mediaListCount;
        if (f < filecount) {
          Media media = ordner.mediaList.get(f);
          if (selectit != media.isSelected) {
            //llog.d(TAG, "getFileSetSelected " + media.isSelected + " -> " + selectit);
            try {
              commandethreaddatabase.put(new String[]{"selectfile", String.valueOf(d), String.valueOf(f), String.valueOf(selectit), media.address, String.valueOf(false)});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          } else {
            llog.d(TAG, "NOT getFileSetSelected " + media.isSelected + " -> " + selectit);
          }
        }
      }
      gallerylock.unlock();
    }
    return switchedselection;
  }
  public void getFoldersSetSelected(int di, boolean selectit){
    if (di >= 0 && di < folderCount) {
      gallerylock.lock();
      Ordner ordner = gallery.get(di);
      int filecount = ordner.mediaListCount;
      for (int f = 0; f < filecount; f++) {
        Media file = ordner.mediaList.get(f);
        file.isSelected = selectit;
        file.selectedchecked = true;
      }
      gallerylock.unlock();
    }
  }
  public String getOrnderAddressEscaped(int i){
    String folder = null;
    if (i >= 0) {
      gallerylock.lock();
      if (i < folderCount) {
        if (gallery.get(i).addressEscaped != null)
          folder = gallery.get(i).addressEscaped;
        else
          folder = gallery.get(i).address;
      }
      gallerylock.unlock();
    }
    return folder;
  }
  public String getOrnderAddress(int i){
    String folder = null;
    if (i >= 0) {
      gallerylock.lock();
      if (i < folderCount)
        folder = gallery.get(i).address;
      gallerylock.unlock();
    }
    return folder;
  }
  public String getMediaSplitName(int d, int f){
    String print = null;
    if (f >= 0) {
      gallerylock.lock();
      if (d < folderCount) {
        int fl =  gallery.get(d).mediaListCount;
        if (f < fl) {
          Media media = gallery.get(d).mediaList.get(f);
          if (media.printName != null) {
            print = media.printName;
          } else if (media.addressescaped != null) {
            print = media.addressescaped;
          } else if (media.address != null) {
            print = media.address;
          } else {
            print = "dummy";
          }
          if (media.printName == null) {
            int dernierslash = print.lastIndexOf("/");
            if (dernierslash != -1 && dernierslash + 1 < print.length() - 1)
              print = print.substring(dernierslash + 1);
          }
          if (media.metadatacreationtime != null)
            print += " " + media.metadatacreationtime;
        }
      }
      gallerylock.unlock();
    }
    return print;
  }
  public String[] getOrdnerSplitName(int i){
    String []print = null;
    if (i >= 0) {
      gallerylock.lock();
      if (i < folderCount) {
        Ordner ordner = gallery.get(i);
        if (ordner.printName != null) {
          print = new String[]{ordner.printName, ordner.address};
        } else if (ordner.addressEscaped != null) {
          print = new String[]{ordner.addressEscaped, ordner.address};
        } else if (ordner.address != null)  {
          print = new String[]{ordner.address, ordner.address};
        } else {
          print = new String[]{"/dummy/folder/", ordner.address};
        }
      }
      gallerylock.unlock();
    }
    return print;
  }
  public String getOrdnerAddress(int i){
    String folder = null;
    if (i >= 0) {
      gallerylock.lock();
      if (i < folderCount)
        folder = gallery.get(i).address;
      gallerylock.unlock();
    }
    return folder;
  }
  public String getOrdnerPrintName(int i){
    String folder = null;
    if (i >= 0) {
      gallerylock.lock();
      if (i < folderCount)
        folder = gallery.get(i).printName;
      gallerylock.unlock();
    }
    return folder;
  }
  public int getFolderIsOnline(int i){
    int isonline = Media.online_no;
    if (i >= 0) {
      gallerylock.lock();
      if (i < folderCount)
        isonline = gallery.get(i).isOnline;
      gallerylock.unlock();
    }
    return isonline;
  }
  public boolean getFolderIsUpdatable(int i){
    boolean updateable = false;
    if (i >= 0) {
      gallerylock.lock();
      if (i < folderCount)
        if (gallery.get(i).updateDeltaTime > 0)
          updateable = true;
      gallerylock.unlock();
    }
    return updateable;
  }
  /*public boolean getFolderNeedsUpdate(int i){
    boolean updateable = false;
    if (i >= 0) {
      gallerylock.lock();
      if (i < folderCount) {
        Ordner ordner = gallery.get(i);
        if (ordner.updateDeltaTime > 0) {
          if (System.currentTimeMillis() - ordner.timestamplastitem > 1000L * ((long) ordner.updateDeltaTime)) {
            //llog.d(TAG, ordner.timestamplastitem + " : diff " + (System.currentTimeMillis() - ordner.timestamplastitem) + " > 1000 * " + ordner.updateDeltaTime);
            updateable = true;
          }
        }
      }
      gallerylock.unlock();
    }
    return updateable;
  }
  public void setFolderUpdated(int d){
    if (d >= 0) {
      gallerylock.lock();
      if (d < folderCount) {
        Ordner ordner = gallery.get(d);
        ordner.timestamplastitem = System.currentTimeMillis();
      }
      gallerylock.unlock();
    };
  }*/
  public int getFolderDefaultFile(int i){
    int defaultfile = 0;
    if (i >= 0) {
      gallerylock.lock();
      if (i < folderCount)
        defaultfile = gallery.get(i).byDefaultShowMediaNumber;
      gallerylock.unlock();
    }
    return defaultfile;
  }
  public int setFolderDefaultFile(int d, int defaultfile){
    if (d >= 0 && defaultfile >= 0) {
      gallerylock.lock();
      if (d < folderCount) {
        Ordner ordner = gallery.get(d);
        int filecount = ordner.mediaListCount;
        if (defaultfile >= filecount)
          defaultfile = filecount - 1;
        if (defaultfile < 0)
          defaultfile = 0;
        ordner.byDefaultShowMediaNumber = defaultfile;
      }
      gallerylock.unlock();
    }
    return defaultfile;
  }
  public void setFolderNotLoadedYet(int d, boolean isNotLoadedYet) {
    if (d >= 0) {
      gallerylock.lock();
      if (d < folderCount) {
        Ordner ordner = gallery.get(d);
        ordner.hasNotBeenLoadedYet = isNotLoadedYet;
      }
      gallerylock.unlock();
    };
  }
  public boolean getFolderNotLoadedYet(int i){
    boolean folderNotLoadedYet = true;
    if (i >= 0) {
      gallerylock.lock();
      if (i < folderCount)
        folderNotLoadedYet = gallery.get(i).hasNotBeenLoadedYet;
      gallerylock.unlock();
    }
    return folderNotLoadedYet;
  }

  public volatile int surftagi = 0;
  public volatile ArrayList<Surf> surf = new ArrayList<>();
  
  public volatile float unmillimetre;
  public volatile float mXDpi;
  public volatile float mYDpi;
  public volatile float deltaminmove;
  public volatile float deltaminmove2;
  public volatile float deltapowermoveunit2;
  public volatile int bigScreenWidth;
  public volatile int bigScreenHeight;
  //public volatile int rectangleScreenx;
  //public volatile int rectangleScreeny;
  //public volatile int rectangleScreenWidth;
  //public volatile int rectangleScreenHeight;
  public volatile boolean isandroidtv = false;
  public volatile boolean allowfakebitmaps = true; // useless for wallpapers
  public volatile float GenericCaseH = 1.0f;
  public volatile float GenericTextH = 1.0f;
  public volatile float GenericTextWantedH = 1.0f;
  public volatile float GenericInterSpace = 1.0f;
  public volatile int GraphWidth = 1;
  public volatile int SettingsWidth = 1;
  public volatile float strokewidth = 1.0f;

  public Paint VideoTextPaint = new Paint();
  public Paint VideoTextPaintRight = new Paint();
  public Paint VideoTextBgPaint = new Paint();

  public Paint VideoSubPaint = new Paint();
  public Paint VideoSubBgPaint = new Paint();

  public Paint Menu1TextePaint = new Paint();
  public Paint SettingsUnderPaint = new Paint();
  public Paint SettingsHighlightPaint = new Paint();
  public Paint Menu1BgPainta = new Paint();
  public Paint Menu1BgPainto = new Paint();
  public Paint Menu2BgPainta = new Paint();
  public Paint Menu2BgPainto = new Paint();
  public Paint MenuStatusPaint = new Paint();
  public Paint AppinfoDisabled = new Paint();
  public Paint AppinfoUninstalled = new Paint();
  public Paint AppinfoActive = new Paint();
  public Paint SettingsSelPaint = new Paint();
  public Paint BitmapSelectedPaint = new Paint();
  public Paint BitmapSelectedBgPaint = new Paint();
  public Paint KeyboardFocus = new Paint();
  public Paint PaintGenericText = new Paint();
  public Paint GraphCurrentScreenPaint = new Paint();
  public Paint GraphCurrentScreenPaintWarn = new Paint();
  public Paint GraphCurrPosPaint = new Paint();
  public Paint FolderNamePaint = new Paint();
  public Paint BackgroundColorPaint = new Paint();
  public Paint FolderNameBgPaint = new Paint();
  public Paint GraphOnePaint = new Paint();
  public Paint GraphTwoPaint = new Paint();
  public Paint GraphBookmarkPaint = new Paint();
  public Paint PaintMenuButton = new Paint();
  public Paint MiniatureBgPaint = new Paint();
  public Paint VideoBottomBarPaint = new Paint();
  public Paint PictureLabel = new Paint();
  public Paint PictureLabelSmall = new Paint();
  public Paint PictureLabelThumb = new Paint();
  public Paint PictureLabelSmallThumb = new Paint();

  /*
  public final static int CouleurTresSombre = Color.parseColor("#341515");
  public final static int CouleurSombre = Color.parseColor("#a5694b");
  public final static int CouleurClaire = Color.parseColor("#b98667");
  public final static int CouleurTresClaire = Color.parseColor("#e4bc98");
  public final static int CouleurTresClaireAlt = Color.parseColor("#e4d1c1");
  public final static int CouleurTresClaireHighlight = Color.parseColor("#e4d1c1");
  */

  public final static int CouleurPastelBlack = Color.rgb(29, 31, 33);
  public final static int[] CouleurPastel = new int[]{
          Color.rgb(204, 102, 102), // rouge
          Color.rgb(222, 147, 95), // orange
          Color.rgb(240, 198, 116), // yellow
          Color.rgb(181, 189, 104), // green
          Color.rgb(138, 190, 183), // cyan
          Color.rgb(129, 162, 190), // blue
          Color.rgb(178, 148, 187), // purple
  };
  public final static int CouleurPastelFailed = CouleurPastel[0];
  public final static int CouleurPastelSuccess = CouleurPastel[3];
  public final static int CouleurPastelStandard = CouleurPastel[6];

  public final static int CouleurBackground = Color.parseColor("#121212");
  public final static int CouleurTresSombre = Color.parseColor("#000000");
  public final static int CouleurSombre = Color.parseColor("#242424");
  public final static int CouleurClaire = Color.parseColor("#dedede");
  public final static int CouleurTresClaire = Color.parseColor("#e9e9e9");
  public final static int CouleurTresTresClaire = Color.parseColor("#ffffff");

  public final static int CouleurBgQuickAccessButton = CouleurSombre;
  // MESSAGES
  public final static int CouleurTexteMessage = CouleurTresTresClaire;
  public final static int CouleurBgMessage = Color.parseColor("#bf2b34");
  public final static int CouleurContourMessage = CouleurBgMessage;
  // THUMBS LABELS
  public final static int CouleurTextLabel = CouleurTresClaire;
  public final static int CouleurTextLabelSmall = CouleurClaire;
  public final static int CouleurBgNoMiniature = CouleurSombre;
  // GRAPH
  public final static int CouleurGraph1 = Color.parseColor("#f3c802");
  public final static int CouleurGraph2 = Color.parseColor("#e45735");
  // FOLDER
  public final static int CouleurTexteFolder = CouleurTresTresClaire;
  public final static int CouleurBgFolder = Color.parseColor("#4d3f69");
  // MENU
  public final static int CouleurTexteMenu1 = CouleurTresTresClaire;
  //public final static int CouleurBgMenu1 = Color.parseColor("#47454c");
  //public final static int CouleurBgMenu2 = Color.parseColor("#4d3f69");
  public final static int CouleurBgMenu1 = Color.parseColor("#54525a");
  public final static int CouleurBgMenu2 = Color.parseColor("#5f4e82");
  public final static int CouleurSouligneMenu = Color.parseColor("#bfbd2b");



  public final static int CouleurImageSelection = CouleurBgMessage;
  public final static int CouleurVideoBottomBar = CouleurBgFolder;



  public final static int ZoomKeep = 0;
  public final static int ZoomFitToWindow = 1;
  public final static int ZoomComic = 2;
  public final static int ZoomComicTopLeft = 0;
  public final static int ZoomComicTopRight = 1;
  public final static int ZoomFitStrict = 0;
  public final static int ZoomFitFill = 1;
  public volatile int sortmode = 0;
  public final static int SortAZ = 0;
  public final static int SortDate = 1;
  /*public volatile int atvzoommode = 0;
  public final static int AtvZoomButtonVolFfwd = 0;
  public final static int AtvZoomButtonFfwd = 1;
  public final static int AtvZoomButtonVol = 2;*/
  public volatile boolean flagsecure = false;
  
  /**
   *   drawing stuff
   */
  public static final int selectedtracecancel = 0;
  public static final int selectedtraceselectmultiple = 1;
  public static final int selectedtraceselectmultipleremove = 2;
  public static final int selectedtraceselectmultipleadd = 3;
  public static final int selectedtraceerase = 4;
  public static final int selectedtracecolor = 5;
  public static final int selectedtracethickness = 6;
  public static final int selectedtraceduplicate = 7;
  public static final int selectedtracemovetrace = 8;
  public static final int selectedtracemovepoint = 9;
  public static final int selectedtracemoveto = 10;
  public static final int selectedtracechangetype = 11;
  public static final int selectedtraceunder = 12;
  public static final int selectedtraceover = 13;
  public static final int selectedtraceresize = 14;
  
  /**
   *  trace en cours de dessin
   */
  public volatile float drawtracepressuremin = -99.99f; // dummy mais important pour initialiser
  public volatile float drawtracepressuremax = -99.99f; // dummy mais important pour initialiser
  
  public volatile float DessinCaseTextH = 1.0f;
  public volatile float DessinCaseTextWantedH = 1.0f;
  public volatile float DessinInterSpace = 1.0f;
  public volatile float DessinCaseH = 1.0f;

  public Paint TracePaint = new Paint();
  public Paint DessinBgPaint = new Paint();
  public Paint DessinSelectedPaint = new Paint();
  public Paint DessinTextPaint = new Paint();
  public Paint MessageTextPaint = new Paint();
  public Paint AppinfoTextPaint = new Paint();
  public Paint MessageBgPaint = new Paint();
  public Paint MessageContourPaint = new Paint();

  public volatile int surfzappernumber = 0;
  public volatile int surfzapperoriginal = -1;
  public volatile String surfzapperoriginalvideo = "reset initial original";
  public volatile int surfzappercommander = -1;
  public volatile int surfzapperplayer = -1;
  public volatile String surfzapperplayervideo = "reset initial zapper";
  public class Videotemps {
    public String fichiername;
    public int dossierprincipal;
    public int fichierprincipal;
    public long starttime;
    public long totaltime = 0L;
    public Videotemps(String mfichiername, int mdossierprincipal, int mfichierprincipal) {
      fichiername = mfichiername;
      dossierprincipal = mdossierprincipal;
      fichierprincipal = mfichierprincipal;
      starttime = System.currentTimeMillis();
    }
  }
  public volatile ArrayList<Videotemps> videotemps = new ArrayList<>();
  public void addvideotemps(String mfichiername, int mdossierprincipal, int mfichierprincipal){
    if (mfichiername != null) {
      int vtl = videotemps.size();
      int deja = -1;
      for (int i = 0; i < vtl; i++) {
        if (videotemps.get(i).fichiername.equals(mfichiername)) {
          deja = i;
          break;
        }
      }
      if (deja == -1) {
        //llog.d(TAG, "+++++++++++++++++++addvideotemps " + mfichiername);
        videotemps.add(new Videotemps(mfichiername, mdossierprincipal, mfichierprincipal));
        //message(" +" + mfichiername);
      } else {
        //llog.d(TAG, "+++++++++++++++++++addvideotemps already have " + mfichiername);
        videotemps.get(deja).starttime = System.currentTimeMillis();
        //message(" =" + mfichiername);
      }
    }
  }
  public void endvideotemps(String mfichiername){
    if (mfichiername != null) {
      int vtl = videotemps.size();
      for (int i = 0; i < vtl; i++) {
        if (videotemps.get(i).fichiername.equals(mfichiername)) {
          videotemps.get(i).totaltime += System.currentTimeMillis() - videotemps.get(i).starttime;
          //message(videotemps.get(i).totaltime/1000L + " " + mfichiername);
          //llog.d(TAG, "---------------endvideotemps " + videotemps.get(i).totaltime);
          break;
        }
      }
    }
  }
  public volatile int videotempslongestlevel = 1;
  private final long minivideotemps = 30000L;
  public Videotemps findlongestvideotemps(int lvl){
    //llog.d(TAG, "findlongestvideotemps " + lvl);
    Videotemps lonlon = null;
    long longest;
    int longesti = -1;
    int vtl = videotemps.size();
    long prevlongest = 999999999L;
    for (int k = 0 ; k < lvl && k < vtl ; k++) {
      longest = minivideotemps;
      for (int i = 0 ; i < vtl ; i++) {
        long totaltime = videotemps.get(i).totaltime;
        if (totaltime > longest && totaltime < prevlongest) {
          //llog.d(TAG, "findlongestvideotemps totaltime > longest && totaltime < prevlongest " + i);
          longesti = i;
          longest = totaltime;
        }
      }
      prevlongest = longest;
    }
    if (longesti == -1) {
      //llog.d(TAG, "longest viewed video not found");
    } else {
      lonlon = videotemps.get(longesti);
      //llog.d(TAG, String.format("%d %ds", lvl, (int) (lonlon.totaltime / 1000L)));
    }
    //videotempslongestlevel += 1;
    return lonlon;
  }

  private SavedStateHandle mState;
  public Gallery(SavedStateHandle savedStateHandle){
    llog.d(TAG, "+++++++++++++++++++++ myViewModel constructor");
    mState = savedStateHandle;
    //internetsession = new InternetSession(this);
    /*
    try {
      float myfloat = savedStateHandle.get("query");
      llog.d(TAG, "+++++++++++++++++++++  got a float : " + myfloat);
    } catch (NullPointerException e) {
      llog.d(TAG, "+++++++++++++++++++++  no float found");
      e.printStackTrace();
    }
    savedStateHandle.set("query", 5.0f);
    */
  }

  public void setallthepaints(){
    float cotepetit;
    if (bigScreenWidth > bigScreenHeight)
      cotepetit = bigScreenHeight;
    else
      cotepetit = bigScreenWidth;

    llog.d(TAG, "button text size " + buttontextsize);
    /** GenericCaseH */
    if (isandroidtv)
      GenericCaseH = buttontextsize * 2.6f;
    else
      GenericCaseH = buttontextsize * 3.8f;
    GenericInterSpace = GenericCaseH * 0.075f;
    /** settings width */
    settingsYmin = GenericCaseH * 4.5f; // 2.5 bien mais ne montre pas le zoom
    if (settingsYmin > bigScreenHeight * 0.33f) {
      settingsYmin = bigScreenHeight * 0.33f;
      llog.d(TAG, "settingsYmin " + settingsYmin + " bigScreenHeight " + bigScreenHeight);
    }
    strokewidth = GenericInterSpace * 0.33f;

    GraphWidth = (int) (GenericCaseH * 1.33f);
    if (iswatch)
      GraphWidth = (int) (bigScreenWidth * 0.30f);

    SettingsWidth = (int) (GenericCaseH * 2.0f);
    if (iswatch)
      SettingsWidth = (int) (bigScreenWidth * 0.40f);

    /** zoomconfortratio */
    zoomconfortratio = 0.40f; // 0.45f was good
    deltacoin = cotepetit * 0.22f;
    /** thumbsize */
    if (isandroidtv)
      thumbsize = (int) ((cotepetit * zoomconfortratio) / 2.0f - 1.0f * GenericInterSpace);
    else if (thumbsize <= 1)
      thumbsize = (int) (cotepetit * 0.20f);
    /** GenericTextH */
    if (isandroidtv)
      GenericTextWantedH = GenericCaseH * 0.42f;
    else
      GenericTextWantedH = GenericCaseH * 0.30f;

    maxnumberthumsincache = (int) Math.floor((bigScreenWidth * bigScreenHeight * nombredecranenram) / (thumbsize * thumbsize));
    String textheighttomeasure = "Oprbg:!Ty|=gp$§";
    int textheighttomeasurel = textheighttomeasure.length();
    Rect bounds = new Rect();
    Paint testpaint = new Paint();
    testpaint.setTextSize(GenericTextWantedH);
    testpaint.getTextBounds(textheighttomeasure, 0, textheighttomeasurel, bounds);
    GenericTextH = bounds.height();
    llog.d(TAG, " GenericCaseH="+GenericCaseH+" thumbsize="+thumbsize+" DisplayMetrics() " + bigScreenWidth + " x " + bigScreenHeight+" ; dpi : "+unmillimetre+" = 1 mm ");


    // assez faible : on bouge un peu le doigt : ne pas quitter
    // 2 millimètres c'est trop
    deltaminmove = unmillimetre * 0.7f;
    deltaminmove2 = deltaminmove * deltaminmove;
    float deltapowermoveunit = unmillimetre * 14.0f;
    deltapowermoveunit2 = deltapowermoveunit * deltapowermoveunit;
    llog.d(TAG, "deltapowermoveunit2 = " + deltapowermoveunit2);

    textheighttomeasure = "Oprbg:!Ty|=";
    textheighttomeasurel = textheighttomeasure.length();
    bounds = new Rect();
    testpaint = new Paint();

    /**
     *    général : taille du texte, hauteur d'un menu item et espace entre les thumbnails
     */

    PaintGenericText.setColor(Gallery.CouleurTresClaire);
    PaintGenericText.setTextSize(GenericTextH);
    PaintGenericText.setStyle(Paint.Style.FILL);
    PaintGenericText.setTextAlign(Paint.Align.RIGHT);
    PaintGenericText.setAntiAlias(true);

    /**
     *    dessin : case et texte dans case
     */
    DessinCaseH = preferences.getFloat("DessinCaseH", GenericCaseH);
    if (DessinCaseH <= unmillimetre * 2.0f) {
      DessinCaseH = GenericCaseH;
    }
    DessinCaseTextWantedH = DessinCaseH * 0.50f;
    testpaint.setTextSize(DessinCaseTextWantedH);
    testpaint.getTextBounds(textheighttomeasure, 0, textheighttomeasurel, bounds);
    DessinCaseTextH = bounds.height();
    DessinInterSpace = DessinCaseH * 0.15f;

    DessinBgPaint.setColor(Gallery.CouleurTresSombre);
    DessinBgPaint.setStyle(Paint.Style.FILL);
    DessinBgPaint.setAntiAlias(true);
    DessinSelectedPaint.setColor(Color.GREEN);
    DessinSelectedPaint.setAntiAlias(true);
    DessinSelectedPaint.setStyle(Paint.Style.STROKE);
    DessinSelectedPaint.setStrokeWidth(strokewidth);
    DessinTextPaint.setColor(Color.WHITE);
    DessinTextPaint.setAntiAlias(true);
    DessinTextPaint.setTextSize(DessinCaseTextH);
    DessinTextPaint.setTextAlign(Paint.Align.LEFT);

    /**
     *    message
     */
    MessageTextPaint.setTextSize(GenericTextH);
    MessageTextPaint.setTextAlign(Paint.Align.LEFT);
    MessageTextPaint.setTypeface(Typeface.MONOSPACE);
    MessageTextPaint.setColor(Gallery.CouleurTexteMessage);
    MessageTextPaint.setAntiAlias(true);
    MessageContourPaint.setStrokeWidth(strokewidth);
    MessageContourPaint.setStyle(Paint.Style.STROKE);
    MessageContourPaint.setColor(Gallery.CouleurContourMessage);
    MessageContourPaint.setAntiAlias(true);
    MessageBgPaint.setColor(Gallery.CouleurBgMessage);
    MessageBgPaint.setStyle(Paint.Style.FILL);
    MessageBgPaint.setAntiAlias(true);

    /**
     *     graph
     */
    GraphOnePaint.setColor(Gallery.CouleurGraph1);
    GraphOnePaint.setStyle(Paint.Style.FILL);
    GraphOnePaint.setAlpha(225);
    //GraphOnePaint.setShadowLayer(GenericInterSpace, GenericInterSpace, 0.0f, Color.argb(48, 0, 0, 0));
    GraphTwoPaint.setColor(Gallery.CouleurGraph2);
    GraphTwoPaint.setStyle(Paint.Style.FILL);
    GraphTwoPaint.setAlpha(225);
    //GraphTwoPaint.setShadowLayer(GenericInterSpace, GenericInterSpace, 0.0f, Color.argb(48, 0, 0, 0));
    GraphBookmarkPaint.setColor(Gallery.CouleurTresClaire);
    GraphBookmarkPaint.setStyle(Paint.Style.FILL);
    GraphBookmarkPaint.setAntiAlias(true);
    GraphCurrentScreenPaint.setStyle(Paint.Style.STROKE);
    GraphCurrentScreenPaint.setStrokeWidth(strokewidth);
    GraphCurrentScreenPaint.setColor(Gallery.CouleurTresClaire);
    GraphCurrentScreenPaint.setAntiAlias(true);
    GraphCurrentScreenPaintWarn.setStyle(Paint.Style.FILL);
    GraphCurrentScreenPaintWarn.setStrokeWidth(strokewidth * 10.0f);
    GraphCurrentScreenPaintWarn.setColor(Color.WHITE);
    GraphCurrentScreenPaintWarn.setAntiAlias(true);
    GraphCurrPosPaint.setStyle(Paint.Style.FILL);
    GraphCurrPosPaint.setStrokeWidth(strokewidth);
    GraphCurrPosPaint.setColor(Gallery.CouleurTresClaire);
    GraphCurrPosPaint.setAntiAlias(true);

/*
    SettingsHighlightBackPaint.setAntiAlias(true);
    SettingsHighlightBackPaint.setStyle(Paint.Style.STROKE);
    SettingsHighlightBackPaint.setStrokeWidth(GenericInterSpace * 1.5f);
    SettingsHighlightBackPaint.setColor(CouleurTresClaireHighlight);
    SettingsHighlightBackPaint.setAlpha(185);
    SettingsHighlightBackPaint.setShadowLayer(GenericInterSpace, GenericInterSpace, GenericInterSpace, Color.argb(140, 0, 0, 0));
    SettingsHighlightPaint.setAntiAlias(true);
    SettingsHighlightPaint.setStyle(Paint.Style.STROKE);
    SettingsHighlightPaint.setStrokeWidth(GenericInterSpace * 1.5f);
    SettingsHighlightPaint.setColor(CouleurSombre);
    SettingsHighlightPaint.setAlpha(224);
    SettingsHighlightPaint.setShadowLayer(GenericInterSpace, GenericInterSpace, GenericInterSpace, Color.argb(140, 0, 0, 0));
*/

    VideoSubBgPaint.setAntiAlias(true);
    VideoSubBgPaint.setStyle(Paint.Style.FILL);
    VideoSubBgPaint.setColor(Gallery.CouleurTresSombre);
    VideoSubBgPaint.setAlpha(142);
    VideoSubPaint.setAntiAlias(true);
    VideoSubPaint.setTextAlign(Paint.Align.CENTER);
    VideoSubPaint.setColor(Gallery.CouleurTresTresClaire);
    VideoSubPaint.setTextSize((GenericTextH * 2.0f * videosubfontsize) / 55.0f);
    VideoSubPaint.setShadowLayer(GenericInterSpace, GenericInterSpace, GenericInterSpace, Color.argb(255, 0, 0, 0));


    VideoTextBgPaint.setAntiAlias(true);
    VideoTextBgPaint.setStyle(Paint.Style.FILL);
    VideoTextBgPaint.setColor(Gallery.CouleurTresSombre);
    VideoTextBgPaint.setAlpha(142);
    //VideoTextBgPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DARKEN));
    VideoTextPaint.setAntiAlias(true);
    VideoTextPaint.setTextAlign(Paint.Align.CENTER);
    VideoTextPaint.setColor(Gallery.CouleurTresTresClaire);
    VideoTextPaint.setTextSize(GenericTextH);
    VideoTextPaintRight.setAntiAlias(true);
    VideoTextPaintRight.setTextAlign(Paint.Align.LEFT);
    VideoTextPaintRight.setColor(Gallery.CouleurTresTresClaire);
    VideoTextPaintRight.setTextSize(GenericTextH);
    VideoBottomBarPaint.setStyle(Paint.Style.FILL);
    VideoBottomBarPaint.setColor(Gallery.CouleurVideoBottomBar);
    VideoBottomBarPaint.setAlpha(215);
    VideoBottomBarPaint.setAntiAlias(true);

    KeyboardFocus.setStyle(Paint.Style.FILL);
    KeyboardFocus.setColor(Gallery.CouleurSouligneMenu);
    KeyboardFocus.setAntiAlias(true);
    //KeyboardFocus.setStrokeWidth(strokewidth * 2.0f);

    BitmapSelectedBgPaint.setStyle(Paint.Style.STROKE);
    BitmapSelectedBgPaint.setColor(Gallery.CouleurContourMessage);
    BitmapSelectedBgPaint.setAntiAlias(true);
    BitmapSelectedBgPaint.setStrokeWidth(strokewidth * 2.0f);
    BitmapSelectedPaint.setStyle(Paint.Style.STROKE);
    BitmapSelectedPaint.setColor(Gallery.CouleurContourMessage);
    BitmapSelectedPaint.setAntiAlias(true);
    BitmapSelectedPaint.setStrokeWidth(strokewidth * 2.0f);

    AppinfoActive.setStyle(Paint.Style.STROKE);
    AppinfoActive.setStrokeWidth(strokewidth * 2.0f);
    AppinfoActive.setColor(Color.parseColor("#009999"));
    AppinfoActive.setAntiAlias(true);
    AppinfoDisabled.setStyle(Paint.Style.STROKE);
    AppinfoDisabled.setStrokeWidth(strokewidth * 2.0f);
    AppinfoDisabled.setColor(Color.parseColor("#ff9300"));
    AppinfoDisabled.setAntiAlias(true);
    AppinfoUninstalled.setStyle(Paint.Style.STROKE);
    AppinfoUninstalled.setStrokeWidth(strokewidth * 2.0f);
    AppinfoUninstalled.setColor(Color.parseColor("#ff3b00"));
    AppinfoUninstalled.setAntiAlias(true);
    AppinfoTextPaint.setTextSize(GenericTextH * 2.0f);
    AppinfoTextPaint.setTextAlign(Paint.Align.RIGHT);
    AppinfoTextPaint.setTypeface(Typeface.MONOSPACE);
    AppinfoTextPaint.setColor(Color.parseColor("#ffff00"));
    AppinfoTextPaint.setShadowLayer(GenericInterSpace, GenericInterSpace, 0.0f, Color.rgb(0, 0, 0));
    AppinfoTextPaint.setAntiAlias(true);

    /**
     *     cases en bordure : suivant précédent options
     */
    PaintMenuButton.setStyle(Paint.Style.FILL);
    PaintMenuButton.setStrokeWidth(strokewidth);
    PaintMenuButton.setColor(Gallery.CouleurBgQuickAccessButton);
    PaintMenuButton.setAntiAlias(true);

    /**
     *     settings
     */
    Menu1TextePaint.setTextSize(GenericTextH);
    Menu1TextePaint.setTextAlign(Paint.Align.RIGHT);
    Menu1TextePaint.setColor(Gallery.CouleurTexteMenu1);
    Menu1TextePaint.setAntiAlias(true);
    //SettingsHighlightPaint.setTypeface(Typeface.MONOSPACE);
    SettingsHighlightPaint.setTextSize(GenericTextH);
    SettingsHighlightPaint.setTextAlign(Paint.Align.RIGHT);
    SettingsHighlightPaint.setColor(Gallery.CouleurTresSombre);
    SettingsHighlightPaint.setAntiAlias(true);
    SettingsHighlightPaint.setTypeface(Typeface.DEFAULT_BOLD);
    //SettingsBgPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.LIGHTEN));
    //SettingsBgPaint.setShadowLayer(GenericInterSpace, GenericInterSpace, GenericInterSpace, Color.argb(140, 0, 0, 0));

    float shadowwitdh = GenericInterSpace * 0.60f;
    Menu1BgPainta.setColor(Gallery.CouleurBgMenu1);
    Menu1BgPainta.setAlpha((int)(255 * 0.75f));
    Menu1BgPainta.setAntiAlias(true);
    Menu1BgPainto.setColor(Gallery.CouleurBgMenu1);
    Menu1BgPainto.setAlpha((int)(255 * 0.84f));
    Menu1BgPainto.setShadowLayer(shadowwitdh, -shadowwitdh, -shadowwitdh, Color.argb(55, 0, 0, 0));
    Menu1BgPainto.setAntiAlias(true);

    Menu2BgPainta.setColor(Gallery.CouleurBgMenu2);
    Menu2BgPainta.setAlpha((int)(255 * 0.75));
    Menu2BgPainta.setAntiAlias(true);
    Menu2BgPainto.setColor(Gallery.CouleurBgMenu2);
    Menu2BgPainto.setAlpha((int)(255 * 0.84f));
    Menu2BgPainto.setShadowLayer(shadowwitdh, -shadowwitdh, -shadowwitdh, Color.argb(55, 0, 0, 0));
    Menu2BgPainto.setAntiAlias(true);

    MenuStatusPaint.setAntiAlias(true);
    MenuStatusPaint.setStyle(Paint.Style.STROKE);
    MenuStatusPaint.setStrokeWidth(GenericInterSpace);

    SettingsSelPaint.setStyle(Paint.Style.FILL);
    SettingsSelPaint.setColor(Gallery.CouleurImageSelection);
    SettingsSelPaint.setAlpha(215);
    SettingsSelPaint.setAntiAlias(true);

    /**
     *      folder names
     */
    FolderNamePaint.setTextSize(GenericTextH);
    FolderNamePaint.setTextAlign(Paint.Align.LEFT);
    FolderNamePaint.setColor(Gallery.CouleurTexteFolder);
    FolderNamePaint.setAntiAlias(true);
    FolderNameBgPaint.setStrokeWidth(strokewidth);
    FolderNameBgPaint.setStyle(Paint.Style.FILL);
    FolderNameBgPaint.setColor(Gallery.CouleurBgFolder);
    FolderNameBgPaint.setAntiAlias(true);
    FolderNameBgPaint.setAlpha((int)(255.0f * 0.80f));
    // inutilisé
    BackgroundColorPaint.setStyle(Paint.Style.FILL);
    BackgroundColorPaint.setColor(Gallery.CouleurBackground);
    BackgroundColorPaint.setAntiAlias(true);
    BackgroundColorPaint.setAlpha((int)(255.0f * 0.80f));

    /**
     *      miniature label + blank
     */
    MiniatureBgPaint.setStrokeWidth(strokewidth);
    MiniatureBgPaint.setStyle(Paint.Style.FILL);
    MiniatureBgPaint.setColor(Gallery.CouleurBgNoMiniature);
    MiniatureBgPaint.setAlpha((int)(255.0f * 0.80f));
    MiniatureBgPaint.setAntiAlias(true);

    PictureLabel.setTextSize(GenericTextH * 1.00f);
    PictureLabel.setTextAlign(Paint.Align.LEFT);
    PictureLabel.setColor(Gallery.CouleurTextLabel);
    PictureLabel.setAntiAlias(true);
    PictureLabelSmall.setTextSize(GenericTextH * 1.00f);
    PictureLabelSmall.setTextAlign(Paint.Align.LEFT);
    PictureLabelSmall.setColor(Gallery.CouleurTextLabelSmall);
    PictureLabelSmall.setAntiAlias(true);

    PictureLabelThumb.setTextSize(GenericTextH * 0.80f);
    PictureLabelThumb.setTextAlign(Paint.Align.LEFT);
    PictureLabelThumb.setColor(Gallery.CouleurTextLabel);
    PictureLabelThumb.setAntiAlias(true);
    PictureLabelSmallThumb.setTextSize(GenericTextH * 0.70f);
    PictureLabelSmallThumb.setTextAlign(Paint.Align.LEFT);
    PictureLabelSmallThumb.setColor(Gallery.CouleurTextLabelSmall);
    PictureLabelSmallThumb.setAntiAlias(true);

    /**
     *      autres (traces de dessin, ...)
     */
    TracePaint.setAntiAlias(true);
    //TracePaint.setDither(true);
    TracePaint.setStyle(Paint.Style.STROKE);
    TracePaint.setStrokeJoin(Paint.Join.ROUND);
    TracePaint.setStrokeCap(Paint.Cap.ROUND);

    int surfl = surf.size();
    for (int i = 0; i < surfl ; i++) {
      Surf mysurf = surf.get(i);

      mysurf.GraphWidth = GraphWidth;
      if (mysurf.GraphWidth > graphwidthmax * mysurf.mywidth && !iswatch) {
        mysurf.GraphWidth = (int)(graphwidthmax * mysurf.mywidth);
      }
      mysurf.SettingsWidth = SettingsWidth;
      if (mysurf.SettingsWidth > settingswidthmax * mysurf.mywidth && !iswatch) {
        mysurf.SettingsWidth = (int)(settingswidthmax * mysurf.mywidth);
      }

      mysurf.SettingsXmin = mysurf.mywidth - mysurf.SettingsWidth;
      mysurf.SettingsYmax = GenericCaseH;

      mysurf.CaseInvisibleW = (int) (GenericCaseH * 1.50f);
      mysurf.CaseInvisiblePrecXmax = mysurf.CaseInvisibleW;
      mysurf.CaseInvisibleSuivXmin = mysurf.mywidth - mysurf.CaseInvisibleW;
      mysurf.CaseInvisiblePrecSuivYmin = mysurf.myheight - mysurf.CaseInvisibleW;
      mysurf.CaseInvisibleOptionXmax = mysurf.CaseInvisibleW;
      mysurf.CaseInvisibleOptionYmax = mysurf.CaseInvisibleW;

      mysurf.drawtracecurrcolorh = 68.0f;
      mysurf.drawtracecurrcolors = 0.8f;
      mysurf.drawtracecurrcolorv = 0.8f;
      mysurf.drawtracecurrcolora = 255.0f;
      mysurf.drawtracecurrsizemin = unmillimetre * 0.20f;
      mysurf.drawtracecurrsizemax = unmillimetre * 0.20f;
      mysurf.drawtracemaximumadd = unmillimetre * 0.8f;

    }

  }

  public void message(){
    if (activitycontext != null) {
      Intent mintent = new Intent();
      mintent.setAction(broadcastname);
      mintent.putExtra("goal", "status");
      mintent.putExtra("hide", true);
      LocalBroadcastManager.getInstance(activitycontext).sendBroadcast(mintent);
    }
  }
  
  public void message(String message){
    if (activitycontext != null) {
      Intent mintent = new Intent();
      mintent.setAction(broadcastname);
      mintent.putExtra("goal", "status");
      mintent.putExtra("status", message);
      LocalBroadcastManager.getInstance(activitycontext).sendBroadcast(mintent);
    }
  }
  
  public void message(String message, float positionx, float positiony, String align){
    if (activitycontext != null) {
      Intent mintent = new Intent();
      mintent.setAction(broadcastname);
      mintent.putExtra("goal", "status");
      mintent.putExtra("status", message);
      mintent.putExtra("positionx", positionx);
      mintent.putExtra("positiony", positiony);
      mintent.putExtra("align", align);
      LocalBroadcastManager.getInstance(activitycontext).sendBroadcast(mintent);
    }
  }

  public String internalStorageDir = null;
  public long lastgraph = System.currentTimeMillis();
  public float monitorwx = 0, monitorwy = 0, monitorww = 1.0f, monitorwh = 1.0f;
  public int monitormaxtime = 0;

}





































