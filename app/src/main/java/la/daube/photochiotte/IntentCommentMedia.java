package la.daube.photochiotte;

import android.content.Context;
import android.content.Intent;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public class IntentCommentMedia {
  private final static String TAG = "YYYis";

  public IntentCommentMedia(Context context, Intent intent, Gallery model, RelativeLayout mainlayout, EditText input, Button inputvalidate){
    model.deactivateactivitykeydown = true;
    final int id = intent.getIntExtra("id", -1);
    final int d = intent.getIntExtra("d", -1);
    final int f = intent.getIntExtra("f", -1);
    String ttype = intent.getStringExtra("type");
    final String type;
    if (ttype != null)
      type = ttype;
    else
      type = "printName";

    input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_WEB_EDIT_TEXT);
    input.setX(0);
    input.setY(100);
    input.setBackgroundColor(Gallery.CouleurTresSombre);
    //input.setHint("regular expressions, case insensitive (e.g. this.*named == ThisFolderIsNamed)");
    input.setHintTextColor(Gallery.CouleurClaire);
    input.setTextColor(Gallery.CouleurTresClaire);
    input.setFocusableInTouchMode(true);
    input.setImeOptions(EditorInfo.IME_ACTION_DONE);
    if (d >= 0 && f >= 0) {
      String settext = null;
      if (type.equals("printName"))
        settext = model.getMediaPrintName(d, f);
      else if (type.equals("printDetails"))
        settext = model.getMediaPrintDetails(d, f);
      else if (type.equals("printFooter"))
        settext = model.getMediaPrintFooter(d, f);
      if (settext == null)
        input.setHint("This is a comment.");
      else if (settext.length() > 0)
        input.setText(settext);
      else
        input.setHint("This is a comment.");
    } else
      input.setHint("This is a comment.");

    //int buttonwidth = (int) (model.bigScreenWidth * 0.075f);

    mainlayout.addView(input);
    ViewGroup.LayoutParams parms = input.getLayoutParams();
    parms.width = model.bigScreenWidth;
    input.setLayoutParams(parms);

    if (type.equals("printName")) {
      model.message("Please add a comment to your media."
          + "\nprintName will shadow its filename."
          , 0, 100, "left");
    } else if (type.equals("printDetails")) {
      model.message("Please add a comment to your media."
          + "\nprintDetails"
          , 0, 100, "left");
    } else if (type.equals("printFooter")) {
      model.message("Please add a comment to your media."
          + "\nprintFooter"
          , 0, 100, "left");
    }

    //inputvalidate.setX(model.bigScreenWidth - buttonwidth);
    //inputvalidate.setY(0);
    /*ViewGroup.LayoutParams parms2 = inputvalidate.getLayoutParams();
    parms2.width = buttonwidth;
    inputvalidate.setLayoutParams(parms2);*/

    mainlayout.requestLayout();
  
    input.setOnFocusChangeListener(new View.OnFocusChangeListener() {
      @Override
      public void onFocusChange(View view, boolean focused) {
        InputMethodManager keyboard = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (focused)
          keyboard.showSoftInput(input, 0);
        else
          keyboard.hideSoftInputFromWindow(input.getWindowToken(), 0);
      }
    });
  
    input.setOnEditorActionListener(new EditText.OnEditorActionListener() {
      @Override
      public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
        if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_UNSPECIFIED) {
          llog.d(TAG, "actionId " + actionId);
          String searchthis = input.getText().toString();
          int searchthisl = searchthis.length();
          //if (searchthisl >= 0) { // on autorise 0 pour set printName=null
          try {
            model.commandethreaddatabase.put(new String[]{"commentmedia", String.valueOf(id), searchthis, String.valueOf(d), String.valueOf(f), type});
          } catch (InterruptedException e) {
          }
          //}
          input.clearFocus();
          mainlayout.removeView(input);
          mainlayout.requestLayout();
          model.deactivateactivitykeydown = false;
          return true;
        } else {
          llog.d(TAG, "unused actionId " + actionId);
        }
        return false;
      }
    });
    
    input.requestFocus();
  }
}



















