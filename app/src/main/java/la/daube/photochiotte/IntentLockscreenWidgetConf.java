package la.daube.photochiotte;

import android.content.Context;
import android.content.Intent;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class IntentLockscreenWidgetConf {
  private final static String TAG = "YYYis";

  public IntentLockscreenWidgetConf(Context context, Intent intent, Gallery model,
                                    RelativeLayout mainlayout, EditText input, Button inputvalidate
  ){
    model.deactivateactivitykeydown = true;
    final int id = intent.getIntExtra("id", -1);
    final int type = intent.getIntExtra("type", Gallery.lockscreencalendarwidget);

    input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_WEB_EDIT_TEXT);
    input.setX(0);
    input.setY(100);
    input.setBackgroundColor(Gallery.CouleurTresSombre);
    //input.setHint("");
    String settings = "";
    if (type == Gallery.lockscreencalendarwidget) {
      settings = model.preferences.getString("lockscreencalendarwidgetset", Gallery.lockscreencalendarwidgetset);
    } else if (type == Gallery.lockscreencountdownwidget) {
      settings = model.preferences.getString("lockscreencountdownwidgetset", Gallery.lockscreencountdownwidgetset);
    } else if (type == Gallery.lockscreenmonitorwidget) {
      settings = model.preferences.getString("lockscreenmonitorwidgetset", Gallery.lockscreenmonitorwidgetset);
    } else if (type == Gallery.screenmonitorwidget) {
      settings = model.preferences.getString("screenmonitorwidgetset", Gallery.lockscreenmonitorwidgetset);
    }
    input.setText(settings);
    input.setHintTextColor(Gallery.CouleurClaire);
    input.setTextColor(Gallery.CouleurTresClaire);
    input.setFocusableInTouchMode(true);
    input.setImeOptions(EditorInfo.IME_ACTION_DONE);

    //int buttonwidth = (int) (model.bigScreenWidth * 0.075f);

    mainlayout.addView(input);
    ViewGroup.LayoutParams parms = input.getLayoutParams();
    parms.width = model.bigScreenWidth;
    input.setLayoutParams(parms);

    //inputvalidate.setX(model.bigScreenWidth - buttonwidth);
    //inputvalidate.setY(0);
    /*ViewGroup.LayoutParams parms2 = inputvalidate.getLayoutParams();
    parms2.width = buttonwidth;
    inputvalidate.setLayoutParams(parms2);*/

    mainlayout.requestLayout();

    String defdefault = "";
    if (type == Gallery.lockscreencalendarwidget) {
      defdefault = Gallery.lockscreencalendarwidgetset
      + "\n" + Gallery.lockscreencalendarwidgetsets;
    } else if (type == Gallery.lockscreencountdownwidget) {
      defdefault = Gallery.lockscreencountdownwidgetset
      + "\n" + Gallery.lockscreencountdownwidgetsets;
    } else if (type == Gallery.lockscreenmonitorwidget) {
      defdefault = Gallery.lockscreenmonitorwidgetset
      + "\n" + Gallery.lockscreenmonitorwidgetsets;
    } else if (type == Gallery.screenmonitorwidget) {
      defdefault = Gallery.lockscreenmonitorwidgetset
      + "\n" + Gallery.lockscreenmonitorwidgetsets;
    }
    model.message( defdefault
        + "\nPlease fill in wanted values,"
        + "\nrepeat sequence to add other similar widgets."
        , 0, 100, "left"
    );
  
    input.setOnFocusChangeListener(new View.OnFocusChangeListener() {
      @Override
      public void onFocusChange(View view, boolean focused) {
        InputMethodManager keyboard = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (focused)
          keyboard.showSoftInput(input, 0);
        else
          keyboard.hideSoftInputFromWindow(input.getWindowToken(), 0);
      }
    });
  
    input.setOnEditorActionListener(new EditText.OnEditorActionListener() {
      @Override
      public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
        if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_UNSPECIFIED) {
          llog.d(TAG, "actionId " + actionId);
          String searchthis = input.getText().toString();
          if (searchthis.length() > 0) {
            if (type == Gallery.lockscreencalendarwidget) {
              model.preferences.edit().putString("lockscreencalendarwidgetset", searchthis).commit();
            } else if (type == Gallery.lockscreencountdownwidget) {
              model.preferences.edit().putString("lockscreencountdownwidgetset", searchthis).commit();
            } else if (type == Gallery.lockscreenmonitorwidget) {
              model.preferences.edit().putString("lockscreenmonitorwidgetset", searchthis).commit();
            } else if (type == Gallery.screenmonitorwidget) {
              model.preferences.edit().putString("screenmonitorwidgetset", searchthis).commit();
            }
          }
          input.clearFocus();
          mainlayout.removeView(input);
          mainlayout.requestLayout();
          model.deactivateactivitykeydown = false;
          return true;
        } else {
          llog.d(TAG, "unused actionId " + actionId);
        }
        return false;
      }
    });
    
    input.requestFocus();
  }
}
