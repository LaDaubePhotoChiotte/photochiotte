package la.daube.photochiotte;

import android.util.Base64;
import android.webkit.CookieManager;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HttpsURLConnection;

public class InternetSession {
  private static final String TAG = "YYYinet";

  public final static String USER_AGENT = "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/109.0";
  public final static int connecttimeout = 3000;
  public final static int readtimeout = 3000;

  public void savehtml(Gallery model, String data){
    // this is only for debugging
    llog.d(TAG, "saved html to "+model.dossierminiature + "/internetSession.log");
    FileWriter fos;
    try{
      fos = new FileWriter(model.dossierminiature + "/internetSession.log", true);
      fos.write(data);
      fos.flush();
      fos.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
  
  public static String cleanupurl(String url){
    if (url != null) {
      url = url.replaceAll(" ", "+");
      url = url.replaceAll("&amp;", "&");
    }
    return url;
  }
  
  public static String getRedirection(Gallery model, String url, String referer) {
    //initcookiemanager();
    if (url == null) {
      llog.d(TAG, "getRedirection url = null");
      return null;
    }
    url = cleanupurl(url);
    int responseCode = 0;
    String headerfields = "";
    String redirectUrl = "";
    URL obj;
    HttpURLConnection con = null;
    int onreessaie = 1;
    while (0 < onreessaie && onreessaie <= 2) {
      try {
        obj = new URL(url);
        con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET"); // avant head
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setConnectTimeout(connecttimeout);
        con.setReadTimeout(readtimeout);
        con.setRequestProperty("Referer", referer);
        con.setInstanceFollowRedirects(false);
        String cookies = model.getcookie(url);
        if (cookies != null)
          con.setRequestProperty("Cookie", cookies);
        responseCode = con.getResponseCode();
        // headerfields que pour erreurs
        for (Map.Entry<String, List<String>> entries : con.getHeaderFields().entrySet()) {
          String mykey = entries.getKey();
          if (mykey != null) {
            for (String value : entries.getValue()) {
              headerfields += " " + mykey + "=" + value;
            }
          }
        }
        redirectUrl = con.getHeaderField("Location");
        
        onreessaie = 0;
      } catch (javax.net.ssl.SSLHandshakeException e) {
        model.message(e.toString() + "\n" + url);
        //llog.d(TAG, "javax.net.ssl.SSLHandshakeException " + e.toString() + " : getRedirection : " + url + " : referer " + referer);
        e.printStackTrace();
        onreessaie += 1;
      } catch (java.net.SocketTimeoutException e) {
        model.message(e.toString() + "\n" + url);
        //llog.d(TAG, "java.net.SocketTimeoutException " + e.toString() + " : getRedirection : " + url + " : referer " + referer);
        e.printStackTrace();
        onreessaie += 1;
      } catch (Exception e) {
        //llog.d(TAG, "Exception " + e.toString() + " : getRedirection : " + url + " : referer " + referer);
        e.printStackTrace();
        onreessaie = 0;
      } finally {
        if (con != null) {
          con.disconnect();
        }
      }
    }
    if (responseCode == HttpURLConnection.HTTP_MOVED_PERM || responseCode == HttpURLConnection.HTTP_MOVED_TEMP) {
      if (!redirectUrl.startsWith("http")) {
        Pattern findpattern = Pattern.compile("https?://[^/]+");
        Matcher matcher = findpattern.matcher(url);
        if (matcher.find()) {
          // relative path
          redirectUrl = matcher.group(0) + "/" + redirectUrl;
        } else {
          // ok
          //llog.d(TAG, "getredirection "+url+" -> "+redirectUrl);
        }
      }
      return getRedirection(model, redirectUrl, url);
    } else if (responseCode == HttpURLConnection.HTTP_OK) {
      // c'est tout bon on envoie
      return url;
    }
    
    //llog.d(TAG, responseCode + " : getRedirection : " + url + " : referer " + referer + " : " + headerfields);
    return null;
  }

  public static String gethtml(Gallery model, String url, String referer) {
    //initcookiemanager();
    if (url == null) {
      llog.d(TAG, "gethtml url is null");
      return null;
    }
    url = cleanupurl(url);
    int responseCode = 0;
    String response = null;
    URL obj;
    HttpURLConnection con = null;
    try {
      obj = new URL(url);
      con = (HttpURLConnection) obj.openConnection();
      con.setRequestMethod("GET");
      con.setRequestProperty("User-Agent", USER_AGENT);
      con.setConnectTimeout(connecttimeout);
      con.setReadTimeout(readtimeout);
      con.setRequestProperty("Referer", referer);
      con.setInstanceFollowRedirects(true);
      String cookies = model.getcookie(url);
      if (cookies != null)
        con.setRequestProperty("Cookie", cookies);
      if (cookies != null)
        llog.d(TAG, "gethtml url : "+url+" cookies : "+cookies);
      
      responseCode = con.getResponseCode();
      if (responseCode == HttpURLConnection.HTTP_OK) {
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer myresponse = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
          myresponse.append(inputLine);
        }
        in.close();
        response = myresponse.toString();
      } else {
        StringBuilder headerfields = new StringBuilder();
        for (Map.Entry<String, List<String>> entries : con.getHeaderFields().entrySet()) {
          String mykey = entries.getKey();
          if (mykey != null) {
            for (String value : entries.getValue()) {
              headerfields.append(" ").append(mykey).append("=").append(value);
            }
          }
        }
        llog.d(TAG, responseCode+" : getHtml : "+url+" : referer "+referer+" : "+headerfields);
      }
      
    } catch (Exception e) {
      llog.d(TAG, "Exception "+e.toString()+" : getHtml : "+url+" : referer "+referer);
      e.printStackTrace();
    } finally {
      if (con != null) {
        con.disconnect();
      }
    }
    return response;
  }
  
  public static String posthtml(Gallery model, String url, String referer, String urlParameters) {
    //initcookiemanager();
    if (url == null) {
      return null;
    }
    url = cleanupurl(url);
    int responseCode = 0;
    String myresponse = null;
    HttpsURLConnection con = null;
    try {
      URL obj = new URL(url);
      con = (HttpsURLConnection) obj.openConnection();
      con.setRequestMethod("POST");
      con.setRequestProperty("User-Agent", USER_AGENT);
      con.setConnectTimeout(connecttimeout);
      con.setReadTimeout(readtimeout);
      con.setRequestProperty("Referer", referer);
      con.setInstanceFollowRedirects(true);
      String cookies = model.getcookie(url);
      if (cookies != null)
        con.setRequestProperty("Cookie", cookies);
      //llog.d(TAG, "posthtml "+url+" "+urlParameters+" "+cookies);
      con.setDoOutput(true);
      DataOutputStream wr = new DataOutputStream(con.getOutputStream());
      wr.writeBytes(urlParameters);
      wr.flush();
      wr.close();
      
      responseCode = con.getResponseCode();
      if (responseCode == HttpURLConnection.HTTP_OK) {
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
          response.append(inputLine);
        }
        in.close();
        myresponse = response.toString();
      } else {
        StringBuilder headerfields = new StringBuilder();
        for (Map.Entry<String, List<String>> entries : con.getHeaderFields().entrySet()) {
          String mykey = entries.getKey();
          if (mykey != null) {
            for (String value : entries.getValue()) {
              headerfields.append(" ").append(mykey).append("=").append(value);
            }
          }
        }
        llog.d(TAG, responseCode+" : sendPost : "+url+" : referer "+referer+" : "+headerfields);
      }
      
    } catch (Exception e) {
      llog.d(TAG, "Exception "+e.toString()+" : sendPost : "+url+" : referer "+referer);
      e.printStackTrace();
    } finally {
      if(con != null) {
        con.disconnect();
      }
    }
    return myresponse;
  }

  public static String[] gethtmlheaders(Gallery model, String url) {
    ArrayList<String> response = new ArrayList<>();
    if (url == null) {
      llog.d(TAG, "gethtml url is null");
      return null;
    }
    url = cleanupurl(url);
    int responseCode = 0;
    URL obj;
    HttpURLConnection con = null;
    try {
      obj = new URL(url);
      con = (HttpURLConnection) obj.openConnection();
      con.setRequestMethod("GET");
      con.setRequestProperty("User-Agent", USER_AGENT);
      con.setConnectTimeout(connecttimeout);
      con.setReadTimeout(readtimeout);
      con.setInstanceFollowRedirects(false);
      String cookies = model.getcookie(url);
      if (cookies != null)
        con.setRequestProperty("Cookie", cookies);

      responseCode = con.getResponseCode();
      if (responseCode == HttpURLConnection.HTTP_OK) {
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuilder myresponse = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
          myresponse.append(inputLine);
        }
        in.close();
      } else {
        for (Map.Entry<String, List<String>> entries : con.getHeaderFields().entrySet()) {
          String mykey = entries.getKey();
          if (mykey != null) {
            if (mykey.equals("Location")) {
              for (String value : entries.getValue()) {
                response.add(0, value);
                //llog.d(TAG, " got location -> " + value);
              }
            } else if (mykey.equals("Subtitles")) {
              //llog.d(TAG, "Pprint ");
              for (String value : entries.getValue()) {
                byte[] data = Base64.decode(value, Base64.DEFAULT);
                String info = new String(data, "UTF-8");
                String[] infoo = info.split("http://");
                for (int i = 1; i < infoo.length; i++) {
                  //llog.d(TAG, i + " : " + infoo[i]);
                  response.add("http://" + infoo[i]);
                }
                //llog.d(TAG, media.fichierprettyprint);
              }
            }
            /*for (String value : entries.getValue()) {
              headerfields += " " + mykey + "=" + value;
            }*/
          }
        }
        //llog.d(TAG, responseCode+" : getHtml : "+url+" : "+headerfields);
      }

    } catch (Exception e) {
      llog.d(TAG, "Exception "+e.toString()+" : getHtml : "+url);
      e.printStackTrace();
    } finally {
      if(con != null) {
        con.disconnect();
      }
    }
    /*int responsel = response.size();
    String []responsearr = new String[responsel];
    responsearr[0] = ;
    for (int i = 1 ; i < responsel ; i++) {

    }*/
    return response.toArray(new String[0]);
  }

  public static boolean downloadToFile(
      Gallery model,
      String url,
      String referer,
      String cookies,
      String destination
  ) {
    if (url == null)
      return false;
    if (!Gallery.couldBeOnline(url))
      return false;
    boolean temporarymessage = false;
    if (referer == null)
      referer = url;
    int responseCode = 0;
    URL obj;
    HttpURLConnection con = null;
    try {
      obj = new URL(url);
      con = (HttpURLConnection) obj.openConnection();
      con.setRequestMethod("GET");
      con.setRequestProperty("User-Agent", USER_AGENT);
      con.setConnectTimeout(connecttimeout);
      con.setReadTimeout(readtimeout);
      con.setRequestProperty("Referer", referer);
      con.setInstanceFollowRedirects(true); // true nécessaire pour certains sites
      if (cookies != null)
        con.setRequestProperty("Cookie", cookies);

      boolean bailout = false;
      String redirige = null;
      int tailletotale = 0;
      if (!bailout) {
        responseCode = con.getResponseCode();
        if (responseCode == HttpURLConnection.HTTP_OK) {
          InputStream is = con.getInputStream();
          FileOutputStream babuffer = new FileOutputStream(destination);
          long temps = System.currentTimeMillis();
          int downloaded = 0;
          int nRead;
          int datal = 32768;
          byte[] data = new byte[datal];
          try {
            while ((nRead = is.read(data, 0, datal)) != -1) {
              babuffer.write(data, 0, nRead);
              downloaded += nRead;
              if (model != null) {
                if (System.currentTimeMillis() - temps > 500) {
                  model.message(String.format("%.2f / %.2f Mo", (float) downloaded / 1000000.0f, (float) tailletotale / 1000000.0f));
                  temporarymessage = true;
                  temps = System.currentTimeMillis();
                }
              }
            }
            babuffer.flush();
          } catch (OutOfMemoryError e) {
            e.printStackTrace();
          } catch (IOException e) {
            e.printStackTrace();
          }
          babuffer.close();
          is.close();
        } else if (responseCode == HttpURLConnection.HTTP_MOVED_PERM && redirige != null) {
          return downloadToFile(model, redirige, url, cookies, destination);
        } else {
          StringBuilder headerfields = new StringBuilder();
          for (Map.Entry<String, List<String>> entries : con.getHeaderFields().entrySet()) {
            String mykey = entries.getKey();
            if (mykey != null) {
              for (String value : entries.getValue()) {
                headerfields.append(" ").append(mykey).append("=").append(value);
              }
            }
          }
          llog.d(TAG, responseCode + " : getPicture : " + url);// + " : referer " + referer + " : " + headerfields);
          if (model != null) {
            if (responseCode < 300 || responseCode > 306) { // évite messages zdf redirection
              if (!url.contains(".thmb_") && !url.contains(".thmb.") && !url.endsWith(".thmb")) {
                temporarymessage = false;
                model.message("Http responseCode " + responseCode + "\n" + url);
              }
            }
          }
        }
      }
    } catch (Exception e) {
      llog.d(TAG, "Exception "+e.toString()+" : getPicture : "+url+" : referer "+referer);
      if (model != null) {
        temporarymessage = false;
        model.message(e.toString() + "\n" + url);
      }
    } finally {
      if(con != null) {
        con.disconnect();
      }
    }
    if (model != null) {
      if (temporarymessage) {
        llog.d(TAG, "fdgdfgdg");
        model.message();
      }
    }
    return true;
  }

  public static byte[] getPicture(
      Gallery model,
      String url,
      String referer,
      String cookies,
      Media media
  ) {
    if (url == null)
      return null;
    if (!Gallery.couldBeOnline(url))
      return null;
    boolean temporarymessage = false;
    if (referer == null)
      referer = url;
    int responseCode = 0;
    byte[] buffer = null;
    URL obj;
    HttpURLConnection con = null;
    try {
      obj = new URL(url);
      con = (HttpURLConnection) obj.openConnection();
      con.setRequestMethod("GET");
      con.setRequestProperty("User-Agent", USER_AGENT);
      con.setConnectTimeout(connecttimeout);
      con.setReadTimeout(readtimeout);
      con.setRequestProperty("Referer", referer);
      con.setInstanceFollowRedirects(true); // true nécessaire pour certains sites
      if (cookies != null)
        con.setRequestProperty("Cookie", cookies);

      boolean bailout = false;
      String redirige = null;
      int tailletotale = 0;
      for (Map.Entry<String, List<String>> entries : con.getHeaderFields().entrySet()) {
        String mykey = entries.getKey();
        if (mykey != null) {
          mykey = mykey.toLowerCase(Locale.getDefault());
          //for (String value : entries.getValue()) {
          //  llog.d(TAG, mykey + " = " + value);
          //}
          if (mykey.equals("content-length")) {
            for (String value : entries.getValue()) {
              tailletotale = Integer.parseInt(value);
            }
          } else if (mykey.equals("location")) {
            for (String value : entries.getValue()) {
              redirige = value;
            }
          } else if (mykey.equals("content-type")) {
            /**
             * inutilisable
             *
             * nginx  : .mkv.thmb_13   video/x-matroska
             * apache : .jpg.thmb      application/octet-stream
             * json   : &t=y           text/html; charset=UTF-8
             *
             */
            /*for (String value : entries.getValue()) {
              if (value != null) {
                if (!value.toLowerCase(Locale.getDefault()).startsWith("image/")) {
                  llog.d(TAG, "error content-type this is not an image " + url + " " + value);
                  bailout = true;
                }
              }
            }*/
          } else if (mykey.equals("pprint") && media != null && model != null) {
            //llog.d(TAG, "Pprint ");
            for (String value : entries.getValue()) {
              byte[] data = Base64.decode(value, Base64.DEFAULT);
              String info = new String(data, "UTF-8");
              String[] infoo = info.split("\t");
              String[] pprint = new String[]{null, null, null};
              for (int i = 0 ; i < infoo.length ; i++) {
                //llog.d(TAG, i + " -------- " + infoo[i]);
                if (i == 0) {
                  if (infoo[i].length() > 0) {
                    pprint[0] = infoo[i];
                  }
                } else if (i == 2) {
                  if (infoo[i].length() > 0) {
                    pprint[1] = infoo[i];
                  }
                } else {
                  if (infoo[i].length() > 0) {
                    pprint[2] = infoo[i];
                  }
                }
              }
              //llog.d(TAG, media.fichierprettyprint);
              media.printName = pprint[0];
              media.printFooter = pprint[1];
              media.printDetails = pprint[2];
              model.setMediaPrintNameFooterDetails(media, pprint);
            }
          }
        }
      }
      if (!bailout) {
        responseCode = con.getResponseCode();
        if (responseCode == HttpURLConnection.HTTP_OK) {
          InputStream is = con.getInputStream();
          ByteArrayOutputStream babuffer = new ByteArrayOutputStream();
          long temps = System.currentTimeMillis();
          int downloaded = 0;
          int nRead;
          int datal = 32768;
          byte[] data = new byte[datal];
          try {
            while ((nRead = is.read(data, 0, datal)) != -1) {
              babuffer.write(data, 0, nRead);
              downloaded += nRead;
              if (model != null) {
                if (System.currentTimeMillis() - temps > 500) {
                  model.message(String.format("%.2f / %.2f Mo", (float) downloaded / 1000000.0f, (float) tailletotale / 1000000.0f));
                  temporarymessage = true;
                  temps = System.currentTimeMillis();
                }
              }
            }
            babuffer.flush();
            buffer = babuffer.toByteArray();
          } catch (OutOfMemoryError e) {
            buffer = null;
            e.printStackTrace();
          } catch (IOException e) {
            e.printStackTrace();
          }
          babuffer.close();
          is.close();
        } else if (responseCode == HttpURLConnection.HTTP_MOVED_PERM && redirige != null) {
          return getPicture(model, redirige, url, cookies, media);
        } else {
          StringBuilder headerfields = new StringBuilder();
          for (Map.Entry<String, List<String>> entries : con.getHeaderFields().entrySet()) {
            String mykey = entries.getKey();
            if (mykey != null) {
              for (String value : entries.getValue()) {
                headerfields.append(" ").append(mykey).append("=").append(value);
              }
            }
          }
          llog.d(TAG, responseCode + " : getPicture : " + url);// + " : referer " + referer + " : " + headerfields);
          if (model != null) {
            if (responseCode < 300 || responseCode > 306) { // évite messages zdf redirection
              if (!url.contains(".thmb_") && !url.contains(".thmb.") && !url.endsWith(".thmb")) {
                temporarymessage = false;
                model.message("Http responseCode " + responseCode + "\n" + url);
              }
            }
          }
        }
      }
    } catch (Exception e) {
      llog.d(TAG, "Exception "+e.toString()+" : getPicture : "+url+" : referer "+referer);
      if (model != null) {
        temporarymessage = false;
        model.message(e.toString() + "\n" + url);
      }
    } finally {
      if(con != null) {
        con.disconnect();
      }
    }
    if (model != null) {
      if (temporarymessage) {
        llog.d(TAG, "fdgdfgdhfhgg");
        model.message();
      }
    }
    return buffer;
  }

  public static byte[] getPicture(Gallery model, String url, Media media, boolean changeminiaturelink) {
    //llog.d(TAG, "getPicture " + url);
    //initcookiemanager();
    if (url == null)
      return null;
    if (!Gallery.couldBeOnline(url))
      return null;
    String referer = media.address;
    int responseCode = 0;
    url = cleanupurl(url);
    // les grandes images peuvent avoir une redirection qu'il ne faut pas prendre
    if (changeminiaturelink) {
      //llog.d(TAG, url + " -> " + url);
      url = getRedirection(model, url, referer);
      if (url == null)
        return null;
      if (!url.startsWith("http") && !url.startsWith("ftp"))
        return null;
      //llog.d(TAG, url + " -> " + url);
      model.setMediaAddressToGetThumbnail(media, url);
    }
    String cookies = model.getcookie(url);
    return getPicture(model, url, referer, cookies, media);
  }

  public static String extension(String monfouhc, String monfouhcsrc){
    if (monfouhcsrc == null || monfouhc == null) {
      return null;
    }
    String monfouhchref = monfouhc.replaceAll("\\.([Hh][tT][mM][lL]?|[Pp][hH][Pp])$", "");
    Matcher extou = Pattern.compile("(\\.\\w{2,4})$").matcher(monfouhchref);
    if (extou.find()){
      return extou.group(1);
    } else {
      extou = Pattern.compile(".*(\\.\\w{2,4})$").matcher(monfouhcsrc);
      if (extou.find()){
        return extou.group(1);
      }
    }
    llog.d(TAG, "pas d'extension trouvée pour : " + monfouhc + " et " + monfouhcsrc);
    return null;
  }

  public static String getDirectPictureAddressBypassAds(Gallery model, String lienhref, String miniature){
    //initcookiemanager();
    
    String exten = extension(lienhref, miniature);
    
    /*
    
    */
    
    // cleanup needed here
    if (Pattern.compile("^https?://thumbs\\d+\\.imgbox\\.com/").matcher(miniature).find()) {
      return miniature.replaceAll("(https?://)thumbs(.+)_t\\..+$", "$1images$2_o")+exten;
    }
    else if (Pattern.compile("^https?://t\\.imgbox\\.com/").matcher(miniature).find()) {
      return miniature.replaceAll("(https?://)t(.+)$", "$1i$2")+exten;
    }
    else if (Pattern.compile("^https?://t\\d+\\.pixhost\\.\\w+/thumbs/").matcher(miniature).find()) {
      return miniature.replaceAll("(https?://)t(.+)/thumbs/(.+)\\.\\w+$", "$1img$2/images/$3")+exten;
    }
    
    // la redirection comprend le bon serveur mais pas la miniature d'origine
    else if (Pattern.compile("^https?://[^/]*imx\\.to/").matcher(miniature).find()) {
      miniature = getRedirection(model, miniature, lienhref);
      if (miniature != null) {
        return miniature.replaceAll("^(https?://)[ti]([^/]*\\.imx\\.to)/.*?t/", "$1i$2/i/");
      }
    }

    // extension surtout pas
    else if (Pattern.compile("\\.imgspice\\.com.*_[a-z]\\.\\w{2,4}$").matcher(miniature).find()) {
      return miniature.replaceAll("_[a-z](\\.\\w{2,4}$)", "$1");
    }
    
    // Extension indispensable
    else if (Pattern.compile("^https?://.+\\.fastpic\\.ru/thumb/.+\\.\\w+$").matcher(miniature).find()) {
      return miniature.replaceAll("(.+/)thumb(/.+)\\.\\w+$", "$1big$2")+exten;
    }
    
    // GET HTML FIRST
    else if (Pattern.compile("^https?://thumb(nail)?s\\w+\\.imagebam\\.com/").matcher(miniature).find()) {
      String html = gethtml(model, lienhref, miniature);
      if (html != null) {
        Pattern pattern = Pattern.compile("content=.https?://\\w+\\.imagebam\\.com/[^\"\']+\\.(\\w{2,4})", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(html);
        if (matcher.find()) {
          return matcher.group(0).substring(9);
        }
      }
    }
    else if (Pattern.compile("^https?://(.+\\.imagetwist\\.com|vipr.im)/").matcher(miniature).find()) {
      String html = gethtml(model, lienhref, miniature);
      if (html != null) {
        Pattern pattern = Pattern.compile("<img src=.(https?://[^\"\']+). class=.pic img", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(html);
        if (matcher.find()) {
          return matcher.group(1);
        }
      }
    }
    else if (Pattern.compile("^https?://.+\\.postimg\\.\\w+/").matcher(miniature).find()) {
      String html = gethtml(model, lienhref, miniature);
      if (html != null) {
        Pattern pattern = Pattern.compile("<img id=.main-image. src=.(https?://[^\'\"]+)", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(html);
        if (matcher.find()) {
          return matcher.group(1);
        }
      }
    }
  
      /**
           les plus courants
       */
  
    //       /t/           /i/
    //       /th/          /i/
    else if (Pattern.compile("^https?://[^/]+/t/").matcher(miniature).find()) {
      return miniature.replaceAll("(^https?://[^/]+)/t/", "$1/i/");
    } else if (Pattern.compile("^https?://[^/]+/th/").matcher(miniature).find()) {
      return miniature.replaceAll("(^https?://[^/]+)/th/", "$1/i/");
    //       /aaa/upload/small/          /aaa/upload/big/
    //       /upload/small/              /upload/big/
    //       /upload/small-medium/       /upload/big/
    } else if (Pattern.compile("^https?://.+/upload/small(-medium)?/").matcher(miniature).find()) {
      return miniature.replaceAll("/upload/small(-medium)?/", "/upload/big/");
    } else if (Pattern.compile("^https?://.+/images/small(-medium)?/").matcher(miniature).find()) {
      return miniature.replaceAll("/images/small(-medium)?/", "/images/big/");
    } else if (Pattern.compile("^https?://.+/small(-medium)?/").matcher(miniature).find()) {
      return miniature.replaceAll("/small(-medium)?/", "/big/");
    //       /thumb/       /big/
    } else if (Pattern.compile("^https?://[^/]+/thumb/").matcher(miniature).find()) {
      return miniature.replaceAll("/thumb/", "/big/");
    //       _m.jpg           .exten
    //       _t.jpg           .exten (.jpg -> .jpeg, ...)
    //       _l.jpg           .exten
    //       ... et on en profite pour prendre l'extension -nécessaire- pour certains sites
    } else if (Pattern.compile("_[a-z]\\.\\w{2,4}$").matcher(miniature).find()) {
      return miniature.replaceAll("_[a-z]\\.\\w{2,4}$", exten);
    }
    
    // TODO : this is only for debugging
    //savehtml(lienhref+"\n"+miniature+"\n\n");
    llog.d(TAG, "RULE FAILED : \n"+lienhref+"\n"+miniature);
    return null;
  }

  private final static Pattern findpattern3 = Pattern.compile("<a href=\"([^\"]+)");
  public static ArrayList<Media> getApache(String source, boolean ignoreFolders){
    ArrayList<Media> listmedia = null;
    llog.d(TAG, "getApache : " + source + " : " + ignoreFolders);


    InputStreamReader ins;
    BufferedReader in;
    try {
      URL urlurl = new URL(source);
      HttpURLConnection c = (HttpURLConnection) urlurl.openConnection();
      c.setRequestProperty("Connection", "Keep-Alive");
      c.setRequestMethod("GET");
      c.setConnectTimeout(InternetSession.connecttimeout);
      int responsecode = c.getResponseCode();
      if (responsecode >= 200 && responsecode <= 310) {


        listmedia = new ArrayList<>();
        c.connect();
        ins = new InputStreamReader(c.getInputStream());
        in = new BufferedReader(ins);
        String line;
        Matcher matcher;
        while ((line = in.readLine()) != null) {
          matcher = findpattern3.matcher(line);
          if (matcher.find()) {

            String address = matcher.group(1);

            if (address.equals("..")
                || address.startsWith("?")
                || address.startsWith("../")
                || address.startsWith("#")
                || address.startsWith("/")
            ) {
              continue;
            }

            if (source.endsWith("/"))
              address = source + address;
            else
              address = source + "/" + address;

            // nécessaire : virer les &amp; etc
            address = corrigelienapache(address);

            boolean isafolder = false;
            if (address.endsWith("/")) {
              isafolder = true;
            }

            if (isafolder && !ignoreFolders) {
              String escaped = null;
              try {
                escaped = URLDecoder.decode(address, "utf-8");
              } catch (UnsupportedEncodingException e) {}

              Media media = new Media();
              media.address = address;
              media.addressescaped = escaped;
              media.printFooter = "./";
              media.ordnerAddress = source;
              //media.ordnerPrintName = printfolder;
              media.isOnline = Media.online_apache;
              media.isALinkThatCreatesAnOrdner = true;
              listmedia.add(media);

            } else if (!isafolder) {
              int type = Media.getType(address); // pas l'autre car il est peut-être coupé
              if (type != Media.media_refused) {
                String escaped = null;
                try {
                  escaped = URLDecoder.decode(address, "utf-8");
                } catch (UnsupportedEncodingException e) {
                  e.printStackTrace();
                }

                Media media = new Media();
                media.address = address;
                media.addressescaped = escaped;
                media.ordnerAddress = source;
                media.isOnline = Media.online_apache;
                media.type = type;
                listmedia.add(media);
              }
            }
          }
        }
        in.close();
        ins.close();
      }
      Gallery.isConnectedToInternet = true;
    } catch (IOException e) {
      Gallery.isConnectedToInternet = false;
      llog.d(TAG, "getApache error " + e.getMessage());
      e.printStackTrace();
      //model.message("ERR" + (Gallery.isConnectedToInternetRetry+1) + " " + e.getMessage() + "\n" + source);
    }

    /* don't retry if not run in a separate thread otherwise it will lock threaddatabase for too long
    if (!Gallery.isConnectedToInternet && Gallery.isConnectedToInternetRetry < Gallery.isConnectedToInternetRetryMax) {
      Gallery.isConnectedToInternetRetry += 1;
      try {
        Thread.sleep(Gallery.isConnectedToInternetRetryDeltaTime);
      } catch (InterruptedException e) {
      }
      return getApache(source, ignoreFolders);
    }*/

    return listmedia;
  }

  public static boolean putApacheFromApache(Gallery model, String source, String destination, boolean copyoverwritenosync, String additionalMetadata){
    llog.d(TAG, "putApacheFromApache : " + source +" -> " + destination);
    boolean success = false;
    HttpURLConnection c;

    InputStream in;
    BufferedInputStream fis;
    try {
      URL urlurlin = new URL(source);
      long destinationlength = 0;
      long sourcelength = -1;

      HttpURLConnection cin = (HttpURLConnection) urlurlin.openConnection();
      cin.setRequestProperty("Connection", "Keep-Alive");
      cin.setRequestMethod("GET");
      cin.setConnectTimeout(InternetSession.connecttimeout);
      cin.setReadTimeout(InternetSession.readtimeout);

      for (Map.Entry<String, List<String>> entries : cin.getHeaderFields().entrySet()) {
        String mykey = entries.getKey();
        if (mykey != null) {
          if (mykey.equalsIgnoreCase("content-length")) {
            for (String value : entries.getValue()) {
              try {
                sourcelength = Long.parseLong(value);
              } catch (Exception e) {
              }
            }
            break;
          }
        }
      }
      byte[] addbytes = null;
      int addbytesl = 0;
      if (additionalMetadata != null) {
        addbytes = additionalMetadata.getBytes(StandardCharsets.UTF_8);
        addbytesl = addbytes.length;
        sourcelength += addbytesl;
      }

      int responsecode = cin.getResponseCode();
      if (responsecode >= 200 && responsecode <= 310) {

        URL urlurl = new URL(destination);

        if (!copyoverwritenosync) {
          c = (HttpURLConnection) urlurl.openConnection();
          c.setRequestMethod("HEAD");
          c.setConnectTimeout(InternetSession.connecttimeout);
          c.setReadTimeout(InternetSession.readtimeout);

          for (Map.Entry<String, List<String>> entries : c.getHeaderFields().entrySet()) {
            String mykey = entries.getKey();
            if (mykey != null) {
              if (mykey.equalsIgnoreCase("content-length")) {
                for (String value : entries.getValue()) {
                  try {
                    destinationlength = Long.parseLong(value);
                  } catch (Exception e) {
                  }
                }
                break;
              }
            }
          }
          c.disconnect();
        }

        if (destinationlength > 0 && destinationlength == sourcelength) {
          llog.d(TAG, "putApacheFromApache no overwrite drop connection " + source);
          model.uploadthreadbailalreadypresent = true;
          success = true;
        } else {
          model.uploadthreadbailalreadypresent = false;

          c = (HttpURLConnection) urlurl.openConnection();
          c.setRequestProperty("Connection", "Keep-Alive");
          c.setRequestMethod("PUT");
          c.setRequestProperty("Content-Length", String.valueOf(sourcelength));
          c.setDoOutput(true);
          c.setUseCaches(false);
          c.setFixedLengthStreamingMode(sourcelength);
          //c.setChunkedStreamingMode(1024); // HTTP Transfer-Encoding header to chunked which will force the request body being sent in chunks.
          c.setConnectTimeout(InternetSession.connecttimeout);
          c.setReadTimeout(InternetSession.readtimeout);

          cin.connect();
          c.connect();

          in = cin.getInputStream();
          OutputStream outputStream = c.getOutputStream();
          fis = new BufferedInputStream(in);
          byte[] streamFileBytes = new byte[32768];
          int bytesRead = 0;
          int totalBytesRead = 0;
          while ((bytesRead = fis.read(streamFileBytes)) > 0) {
            outputStream.write(streamFileBytes, 0, bytesRead);
            outputStream.flush();
            model.uploadthreadbytes += bytesRead;
            model.uploadthreadbytesin += bytesRead;
            displayprogress(model);
            totalBytesRead += bytesRead;
          }
          if (addbytesl > 0) {
            outputStream.write(addbytes, 0, addbytesl);
            outputStream.flush();
            totalBytesRead += addbytesl;
          }
          outputStream.close();
          fis.close();
          in.close();

          responsecode = c.getResponseCode();
          if (responsecode >= 200 && responsecode <= 310 && totalBytesRead == sourcelength) {
            success = true;
          } else {
            StringBuilder headerfields = new StringBuilder();
            for (Map.Entry<String, List<String>> entries : c.getHeaderFields().entrySet()) {
              String mykey = entries.getKey();
              if (mykey != null) {
                for (String value : entries.getValue()) {
                  headerfields.append(" ").append(mykey).append("=").append(value);
                }
              }
            }
            llog.d(TAG, "putApacheFromApache " + destination + " " + responsecode + " : put : " + headerfields + " " + totalBytesRead + "/" + sourcelength);
          }
        }

      } else {
        StringBuilder headerfields = new StringBuilder();
        for (Map.Entry<String, List<String>> entries : cin.getHeaderFields().entrySet()) {
          String mykey = entries.getKey();
          if (mykey != null) {
            for (String value : entries.getValue()) {
              headerfields.append(" ").append(mykey).append("=").append(value);
            }
          }
        }
        llog.d(TAG, "putApacheFromApache " + source + " " + responsecode + " : get : " + headerfields);
      }

      Gallery.isConnectedToInternet = true;
    } catch (IOException e) {
      Gallery.isConnectedToInternet = false;
      llog.d(TAG, "putApacheFromApache error! " + e.getMessage());
      e.printStackTrace();
      model.message("ERR" + (Gallery.isConnectedToInternetRetry+1) + " " + e.getMessage() + "\n" + source + "\n" + destination);
    }

    if (!Gallery.isConnectedToInternet && Gallery.isConnectedToInternetRetry < Gallery.isConnectedToInternetRetryMax) {
      Gallery.isConnectedToInternetRetry += 1;
      try {
        Thread.sleep(Gallery.isConnectedToInternetRetryDeltaTime);
      } catch (InterruptedException e) {
      }
      return putApacheFromApache(model, source, destination, copyoverwritenosync, additionalMetadata);
    }

    return success;
  }

  public static boolean putApache(Gallery model, File file, String source, boolean copyoverwritenosync, String additionalMetadata){
    llog.d(TAG, "putApache : " + source);
    boolean success = false;
    HttpURLConnection c;

    FileInputStream fi;
    BufferedInputStream fis;
    try {
      URL urlurl = new URL(source);
      long destinationlength = 0;
      long sourcelength = -1;
      if (file.exists())
        sourcelength = file.length();
      byte[] addbytes = null;
      int addbytesl = 0;
      if (additionalMetadata != null) {
        addbytes = additionalMetadata.getBytes(StandardCharsets.UTF_8);
        addbytesl = addbytes.length;
        sourcelength += addbytesl;
      }

      if (!copyoverwritenosync) {
        c = (HttpURLConnection) urlurl.openConnection();
        c.setRequestMethod("HEAD");
        c.setConnectTimeout(InternetSession.connecttimeout);
        c.setReadTimeout(InternetSession.readtimeout);

        for (Map.Entry<String, List<String>> entries : c.getHeaderFields().entrySet()) {
          String mykey = entries.getKey();
          if (mykey != null) {
            if (mykey.equalsIgnoreCase("content-length")) {
              for (String value : entries.getValue()) {
                try {
                  destinationlength = Long.parseLong(value);
                } catch (Exception e) {
                }
              }
              break;
            }
          }
        }
        c.disconnect();
      }

      if (destinationlength > 0 && destinationlength == sourcelength) {
        llog.d(TAG, "putApache no overwrite drop connection " + source);
        model.uploadthreadbailalreadypresent = true;
        success = true;
      } else {
        model.uploadthreadbailalreadypresent = false;

        c = (HttpURLConnection) urlurl.openConnection();
        c.setRequestProperty("Connection", "Keep-Alive");
        c.setRequestMethod("PUT");
        c.setRequestProperty("Content-Length", String.valueOf(sourcelength));
        c.setDoOutput(true);
        c.setUseCaches(false);
        c.setFixedLengthStreamingMode(sourcelength);
        //c.setChunkedStreamingMode(1024); // HTTP Transfer-Encoding header to chunked which will force the request body being sent in chunks.
        c.setConnectTimeout(InternetSession.connecttimeout);
        c.setReadTimeout(InternetSession.readtimeout);

        c.connect();

        OutputStream outputStream = c.getOutputStream();
        fi = new FileInputStream(file);
        fis = new BufferedInputStream(fi);
        byte[] streamFileBytes = new byte[32768];
        int bytesRead = 0;
        int totalBytesRead = 0;
        while ((bytesRead = fis.read(streamFileBytes)) > 0) {
          outputStream.write(streamFileBytes, 0, bytesRead);
          outputStream.flush();
          model.uploadthreadbytes += bytesRead;
          model.uploadthreadbytesin += bytesRead;
          displayprogress(model);
          totalBytesRead += bytesRead;
        }
        if (addbytesl > 0) {
          outputStream.write(addbytes, 0, addbytesl);
          outputStream.flush();
          totalBytesRead += addbytesl;
        }
        outputStream.close();
        fis.close();
        fi.close();

        int responsecode = c.getResponseCode();
        if (responsecode >= 200 && responsecode <= 310 && totalBytesRead == sourcelength) {
          success = true;
        } else {
          StringBuilder headerfields = new StringBuilder();
          for (Map.Entry<String, List<String>> entries : c.getHeaderFields().entrySet()) {
            String mykey = entries.getKey();
            if (mykey != null) {
              for (String value : entries.getValue()) {
                headerfields.append(" ").append(mykey).append("=").append(value);
              }
            }
          }
          llog.d(TAG, "putApache " + source + " " + responsecode + " : put : " + headerfields + " " + totalBytesRead + "/" + sourcelength);
        }
      }

      Gallery.isConnectedToInternet = true;
    } catch (IOException e) {
      Gallery.isConnectedToInternet = false;
      llog.d(TAG, "putApache error! " + e.getMessage());
      e.printStackTrace();
      model.message("ERR" + (Gallery.isConnectedToInternetRetry+1) + " " + e.getMessage() + "\n" + source);
    }

    if (!Gallery.isConnectedToInternet && Gallery.isConnectedToInternetRetry < Gallery.isConnectedToInternetRetryMax) {
      Gallery.isConnectedToInternetRetry += 1;
      try {
        Thread.sleep(Gallery.isConnectedToInternetRetryDeltaTime);
      } catch (InterruptedException e) {
      }
      return putApache(model, file, source, copyoverwritenosync, additionalMetadata);
    }

    return success;
  }

  public static boolean getApacheSaveToFile(Gallery model, String source, File file, boolean copyoverwritenosync){
    llog.d(TAG, "getApacheSaveToFile : " + source);
    boolean success = false;

    FileOutputStream fi;
    BufferedOutputStream fis;
    try {
      URL urlurl = new URL(source);
      HttpURLConnection c = (HttpURLConnection) urlurl.openConnection();
      c.setRequestProperty("Connection", "Keep-Alive");
      c.setRequestMethod("GET");
      c.setConnectTimeout(InternetSession.connecttimeout);
      c.setReadTimeout(InternetSession.readtimeout);

      long contentlength = 0;
      long filelength = -1;
      if (!copyoverwritenosync) {
        if (file.exists())
          filelength = file.length();
      }
      for (Map.Entry<String, List<String>> entries : c.getHeaderFields().entrySet()) {
        String mykey = entries.getKey();
        if (mykey != null) {
          if (mykey.equalsIgnoreCase("content-length")) {
            for (String value : entries.getValue()) {
              try {
                contentlength = Long.parseLong(value);
              } catch (Exception e) {
              }
            }
            break;
          }
        }
      }

      int responsecode = c.getResponseCode();
      if (filelength > 0 && contentlength == filelength) {
        llog.d(TAG, "getApacheSaveToFile no overwrite drop connection fl " + filelength + " cl " + contentlength + " " + source);
        model.uploadthreadbailalreadypresent = true;
        success = true;
      } else if (responsecode >= 200 && responsecode <= 310 && contentlength != filelength) {
        model.uploadthreadbailalreadypresent = false;
        c.connect();

        InputStream outputStream = c.getInputStream();
        fi = new FileOutputStream(file);
        fis = new BufferedOutputStream(fi);
        byte[] streamFileBytes = new byte[32768];
        int bytesRead = 0;
        int totalBytesRead = 0;
        while ((bytesRead = outputStream.read(streamFileBytes)) > 0) {
          fis.write(streamFileBytes, 0, bytesRead);
          fis.flush();
          model.uploadthreadbytes += bytesRead;
          model.uploadthreadbytesin += bytesRead;
          displayprogress(model);
          totalBytesRead += bytesRead;
        }
        fis.close();
        fi.close();
        outputStream.close();
        if (totalBytesRead != contentlength) {
          llog.d(TAG, "getApacheSaveToFile error received length " + totalBytesRead + " content length " + contentlength + " " + source);
        } else {
          success = true;
        }
      } else { // ignore bad responsecodes
        StringBuilder headerfields = new StringBuilder();
        for (Map.Entry<String, List<String>> entries : c.getHeaderFields().entrySet()) {
          String mykey = entries.getKey();
          if (mykey != null) {
            for (String value : entries.getValue()) {
              headerfields.append(" ").append(mykey).append("=").append(value);
            }
          }
        }
        llog.d(TAG, "getApacheSaveToFile " + source + " " + responsecode + " : put : " + headerfields);
      }

      Gallery.isConnectedToInternet = true;
    } catch (IOException e) {
      Gallery.isConnectedToInternet = false;
      llog.d(TAG, "getApacheSaveToFile error " + e.getMessage());
      e.printStackTrace();
      model.message("ERR" + (Gallery.isConnectedToInternetRetry+1) + " " + e.getMessage() + "\n" + source);
    }

    if (!Gallery.isConnectedToInternet && Gallery.isConnectedToInternetRetry < Gallery.isConnectedToInternetRetryMax) {
      Gallery.isConnectedToInternetRetry += 1;
      try {
        Thread.sleep(Gallery.isConnectedToInternetRetryDeltaTime);
      } catch (InterruptedException e) {
      }
      return getApacheSaveToFile(model, source, file, copyoverwritenosync);
    }

    return success;
  }

  public static boolean deleteApache(Gallery model, String source){
    llog.d(TAG, "deleteApache : " + source);
    boolean success = false;

    try {
      URL urlurl = new URL(source);
      HttpURLConnection c = (HttpURLConnection) urlurl.openConnection();
      c.setRequestProperty("Connection", "Keep-Alive");
      c.setRequestMethod("DELETE");
      c.setDoOutput(false);
      c.setUseCaches(false);
      c.setConnectTimeout(InternetSession.connecttimeout);
      c.setReadTimeout(InternetSession.readtimeout);

      c.connect();

      int responsecode = c.getResponseCode();
      if (responsecode >= 200 && responsecode <= 310) {
        success = true;
      } else {
        StringBuilder headerfields = new StringBuilder();
        for (Map.Entry<String, List<String>> entries : c.getHeaderFields().entrySet()) {
          String mykey = entries.getKey();
          if (mykey != null) {
            for (String value : entries.getValue()) {
              headerfields.append(" ").append(mykey).append("=").append(value);
            }
          }
        }
        llog.d(TAG, "deleteApache " + source + " " + responsecode + " : delete : " + headerfields);
      }

      Gallery.isConnectedToInternet = true;
    } catch (IOException e) {
      Gallery.isConnectedToInternet = false;
      llog.d(TAG, "deleteApache error! " + e.getMessage());
      e.printStackTrace();
      model.message("ERR" + (Gallery.isConnectedToInternetRetry+1) + " " + e.getMessage() + "\n" + source);
    }

    if (!Gallery.isConnectedToInternet && Gallery.isConnectedToInternetRetry < Gallery.isConnectedToInternetRetryMax) {
      Gallery.isConnectedToInternetRetry += 1;
      try {
        Thread.sleep(Gallery.isConnectedToInternetRetryDeltaTime);
      } catch (InterruptedException e) {
      }
      return deleteApache(model, source);
    }

    return success;
  }

  public static void displayprogress(Gallery model){
    if (model.uploadthreadrunning) {
      double currtime = System.currentTimeMillis();
      double dtime = currtime - model.uploadthreadtimein;
      if (dtime < 1000)
        return;
      double dtimetot = currtime - model.uploadthreadtime;
      if (dtimetot < 1000)
        return;
      double speedtot = model.uploadthreadbytes / dtimetot;
      double speed = model.uploadthreadbytesin / dtime;
      model.uploadthreadstatus = String.format("%.0fMB  %.0fkB/s(%.0f)  %d/%d  (%d failed, %d id)",
          model.uploadthreadbytes / 1000000.0f, speedtot, speed,
          model.uploadthreadsuccess, model.uploadthreadtotal, model.uploadthreadfail, model.uploadthreadbailalreadypresentcount);
      model.uploadthreadbytesin = 0;
      model.uploadthreadtimein = currtime;
    }
  }

  public static String bytetohex(char c){
    String o = "";
    byte[] bytes;
      bytes = String.format("%c", c).getBytes(StandardCharsets.UTF_8);
      for (int j = 0 ; j < bytes.length ; j++)
        o += "%" + String.format("%02x", bytes[j]);
      return o;
  }

  public static String corrigelienapache(String s){
    StringBuilder o = new StringBuilder();
    int sl = s.length();
    int i = 0;
    while (i < sl) {
      char c = s.charAt(i);
      boolean modif = false;
      if (i+4 < sl && c == '&') {
        if (s.charAt(i + 1) == 'a'
                && s.charAt(i + 2) == 'm'
                && s.charAt(i + 3) == 'p'
                && s.charAt(i + 4) == ';'
        ) {
          o.append('&');
          i += 5;
          modif = true;
        }
      }
      if (!modif) {
        if (
               c == '\''  // nécessaire
            || c == ';'   // pour les escape &amp; ne pas refaire pour dossier puis pour fichier sinon dossier faux
        ) {
          o.append(bytetohex(c));
          i += 1;
        } else {
          o.append(c);
          i += 1;
        }
      }
    }
    return o.toString();
  }

  // UTF-16 (hex) 	0x2019 (2019)
  // UTF-8 (hex) 	0xE2 0x80 0x99 (e28099)

  public static String corrigepathenlien(String s) {
    StringBuilder o = new StringBuilder();
    for (int i = 0 ; i < s.length() ; i++) {
      char c = s.charAt(i);
      if (( 0x30 <= c && c <= 0x39) || ( 0x41 <= c && c <= 0x5a) || ( 0x61 <= c && c <= 0x7a)
          || c == '/'
          || c == ':'
          || c == '+'
          || c == '-'
          || c == '='
          || c == '_'
          || c == '&'
          || c == '?'
          || c == '.'
          || c == ','
      ) {
        o.append(c);
      } else if (c == 32) {
        o.append("%20");
      } else {
        if (c > 255)
          llog.d(TAG, s);
        o.append(bytetohex(c));
      }
    }
    return o.toString();
  }

}















