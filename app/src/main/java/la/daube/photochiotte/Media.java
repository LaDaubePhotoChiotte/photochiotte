package la.daube.photochiotte;

import android.graphics.Bitmap;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.regex.Matcher;

class Media {

  // reserved for backward compatibility old MEDIA_ADDRESS = 9;
  public volatile String address = null; public final static int MEDIA_ADDRESS = 1; // or addressEncode to ask the app for url encoding
  public volatile String printName = null; public final static int MEDIA_PRINTNAME = 2;
  public volatile String printDetails = null; public final static int MEDIA_PRINTDETAILS = 3;
  public volatile String printFooter = null; public final static int MEDIA_PRINTFOOTER = 4;
  public volatile String ordnerAddress = null; public final static int MEDIA_ORDNERADDRESS = 5; // or ordnerAddressEncode to ask the app for url encoding
  public volatile String ordnerPrintName = null; public final static int MEDIA_ORDNERPRINTNAME = 6;
  public volatile String addressToGetPreviewFullSize = null; public final static int MEDIA_ADDRESSTOGETPICTUREFULLSIZE = 7; // or addressToGetPreviewFullSizeEncode to ask the app for url encoding
  public volatile String addressToGetThumbnail = null; public final static int MEDIA_ADDRESSTOGETPICTURETHUMBNAILSIZE = 8; // or addressToGetThumbnailEncode to ask the app for url encoding
  public volatile String bookmarkToOrdner = null; public final static int MEDIA_BOORKMARKTOORDNER = 24; // or bookmarkToOrdnerEncode to ask the app for url encoding
  public volatile boolean isALinkThatCreatesAnOrdner = false; public final static int MEDIA_ISALINKTHATCREATESANORDNER = 17;

  public volatile int addressToGetLibextractorsThumbnail = -1; public final static int MEDIA_ADDRESSTOGETLIBEXTRACTORSTHUMBNAIL = 10;
  public volatile int type = media_picture; public final static int MEDIA_TYPE = 11;
  public volatile int isOnline = online_no; public final static int MEDIA_ISONLINE = 12;
  public volatile boolean isInsideAnArchive = false; public final static int MEDIA_ISINSIDEANARCHIVE = 13;

  public volatile boolean isSelected = false; public final static int MEDIA_ISSELECTED = 14;

  public volatile int filmstripCount = filmstripNotCheckedYet; public final static int MEDIA_FILMSTRIPCOUNT = 15;

  public volatile boolean getDirectPictureAddressBypassAds = false; public final static int getDirectPictureAddressBypassAds_ = 16;
  public volatile int isonlinelevel = 0; public final static int isonlinelevel_ = 18;
  public volatile int isonlineforumlevel = -1; public final static int isonlineforumlevel_ = 19;
  public volatile int isonlinelinktonextpagei = 1; public final static int isonlinelinktonextpagei_ = 20;
  public volatile int isonlinelinktonextpagetot = 1; public final static int isonlinelinktonextpagetot_ = 21;
  public volatile String isonlineparentlink = null; public final static int isonlineparentlink_ = 22; // or isonlineparentlinkEncode to ask the app for url encoding

  public volatile boolean playInSequence = false; public final static int MEDIA_PLAYINSEQUENCE = 25;
  public volatile int playStartAtPosition = 0; public final static int MEDIA_PLAYSTARTATPOSITION = 26;
  public volatile ArrayList<String> subtitleAddress = null; public final static int MEDIA_SUBTITLEADDRESS = 27; // or subtitleAddressEncode to ask the app for url encoding

  /** generated **/
  private final static String TAG = "YYYff";

  public volatile int index = -1;
  public volatile int ordnerIndex = -1;

  public volatile int width = 0;
  public volatile int height = 0;
  public volatile float offsetX = 0.0f;
  public volatile float offsetY = 0.0f;

  public volatile String addressescaped = null;
  public volatile int metadatawidth = 0;
  public volatile int metadataheight = 0;
  public volatile int metadatamaxwidth = 0;
  public volatile int metadatamaxheight = 0;
  public volatile int metadatarotation = 0;
  public volatile int metadataaudio = 0;
  public volatile int metadatasubtitle = 0;
  public volatile String metadatacreationtime = null;
  public volatile long metadataduration = -1;

  public Bitmap[] bitmap = null;

  public volatile boolean selectedchecked = false;
  public volatile boolean bitmapaskednext = false;
  public volatile long filmstriplastchange = 0;
  public volatile int filmstripcurrent = -1;

  public String[] buttonaction = null;

  /** static **/
  public static final int online_no = 0;
  public static final int online = 5;
  public static final int online_apache = 1; // tries to grab .thmb_0 for addressToGetThumbnail and addressToGetPreviewFullSize
  public static final int online_json = 2;
  public static final int online_forum = 3;
  public static final int online_tor = 4;

  public static final int filmstripThumbnailFound = -5;
  public static final int filmstripNoThumbnailNoFilmstripcount = -4;
  public static final int filmstripNoDirectBitmap = -3;
  public static final int filmstripCreateFakeBitmap = -2;
  public static final int filmstripNotCheckedYet = -1;

  public static final int media_refused = 0;
  public static final int media_picture = 1;
  public static final int media_video = 2;
  public static final int media_music = 3;
  public static final int media_eps = 4;
  public static final int media_app = 5;
  public static final int media_button = 6;
  public static final int media_collection = 7;


  public void mediaToBuffer(ByteBuffer selected) {
    addstring(address, MEDIA_ADDRESS, selected);
    addstring(printName, MEDIA_PRINTNAME, selected);
    addstring(printDetails, MEDIA_PRINTDETAILS, selected);
    addstring(printFooter, MEDIA_PRINTFOOTER, selected);
    addstring(ordnerAddress, MEDIA_ORDNERADDRESS, selected);
    addstring(ordnerPrintName, MEDIA_ORDNERPRINTNAME, selected);
    addstring(addressToGetPreviewFullSize, MEDIA_ADDRESSTOGETPICTUREFULLSIZE, selected);
    addstring(addressToGetThumbnail, MEDIA_ADDRESSTOGETPICTURETHUMBNAILSIZE, selected);
    addstring(bookmarkToOrdner, MEDIA_BOORKMARKTOORDNER, selected);
    if (isALinkThatCreatesAnOrdner) {
      selected.put((byte) MEDIA_ISALINKTHATCREATESANORDNER); selected.put((byte) 1);
    }

    selected.put((byte) MEDIA_ADDRESSTOGETLIBEXTRACTORSTHUMBNAIL); selected.putInt(addressToGetLibextractorsThumbnail).order(ByteOrder.BIG_ENDIAN);
    selected.put((byte) MEDIA_TYPE); selected.putInt(type).order(ByteOrder.BIG_ENDIAN);
    selected.put((byte) MEDIA_ISONLINE); selected.putInt(isOnline).order(ByteOrder.BIG_ENDIAN);
    if (isInsideAnArchive) {
      selected.put((byte) MEDIA_ISINSIDEANARCHIVE); selected.put((byte) 1);
    }

    if (isSelected) {
      selected.put((byte) MEDIA_ISSELECTED); selected.put((byte) 1);
    }

    selected.put((byte) MEDIA_FILMSTRIPCOUNT); selected.putInt(filmstripCount).order(ByteOrder.BIG_ENDIAN);

    if (type == online_forum) {
      if (getDirectPictureAddressBypassAds) {
        selected.put((byte) getDirectPictureAddressBypassAds_); selected.put((byte) 1);
      }
      selected.put((byte) isonlinelevel_); selected.putInt(isonlinelevel).order(ByteOrder.BIG_ENDIAN);
      selected.put((byte) isonlineforumlevel_); selected.putInt(isonlineforumlevel).order(ByteOrder.BIG_ENDIAN);
      selected.put((byte) isonlinelinktonextpagei_); selected.putInt(isonlinelinktonextpagei).order(ByteOrder.BIG_ENDIAN);
      selected.put((byte) isonlinelinktonextpagetot_); selected.putInt(isonlinelinktonextpagetot).order(ByteOrder.BIG_ENDIAN);
      addstring(isonlineparentlink, isonlineparentlink_, selected);
    }

    if (playInSequence) {
      selected.put((byte) MEDIA_PLAYINSEQUENCE); selected.put((byte) 1);
    }
    if (playStartAtPosition > 0) {
      selected.put((byte) MEDIA_PLAYSTARTATPOSITION); selected.putInt(playStartAtPosition).order(ByteOrder.BIG_ENDIAN);
    }
    if (subtitleAddress != null) {
      int subtitleAddressl = subtitleAddress.size();
      if (subtitleAddressl > 0) {
        for (int i = 0 ; i < subtitleAddressl ; i++) {
          addstring(subtitleAddress.get(i), MEDIA_SUBTITLEADDRESS, selected);
        }
      }
    }


  }

  /**
   *  deep copy except bitmaps
   */
  public Media copyMedia(){
    Media media = new Media();

    media.index = index;
    media.ordnerIndex = ordnerIndex;

    media.width = width;
    media.height = height;
    media.offsetX = offsetX;
    media.offsetY = offsetY;

    media.address = address;
    media.printName = printName;
    media.printDetails = printDetails;
    media.printFooter = printFooter;
    media.ordnerAddress = ordnerAddress;
    media.ordnerPrintName = ordnerPrintName;
    media.addressToGetPreviewFullSize = addressToGetPreviewFullSize;
    media.addressToGetThumbnail = addressToGetThumbnail;
    media.bookmarkToOrdner = bookmarkToOrdner;

    media.addressToGetLibextractorsThumbnail = addressToGetLibextractorsThumbnail;
    media.type = type;
    media.isOnline = isOnline;
    media.isInsideAnArchive = isInsideAnArchive;

    media.selectedchecked = selectedchecked;
    media.isSelected = isSelected;
    media.filmstriplastchange = filmstriplastchange;
    media.bitmapaskednext = bitmapaskednext;

    media.filmstripcurrent = filmstripcurrent;
    media.filmstripCount = filmstripCount;

    media.addressescaped = addressescaped;
    media.metadatawidth = metadatawidth;
    media.metadataheight = metadataheight;
    media.metadatamaxwidth = metadatamaxwidth;
    media.metadatamaxheight = metadatamaxheight;
    media.metadatarotation = metadatarotation;
    media.metadataaudio = metadataaudio;
    media.metadatasubtitle = metadatasubtitle;
    media.metadatacreationtime = metadatacreationtime;
    media.metadataduration = metadataduration;

    media.bitmap = bitmap;

    media.buttonaction = buttonaction;

    media.getDirectPictureAddressBypassAds = getDirectPictureAddressBypassAds;
    media.isALinkThatCreatesAnOrdner = isALinkThatCreatesAnOrdner;
    media.isonlinelevel = isonlinelevel;
    media.isonlineforumlevel = isonlineforumlevel;
    media.isonlinelinktonextpagei = isonlinelinktonextpagei;
    media.isonlinelinktonextpagetot = isonlinelinktonextpagetot;
    media.isonlineparentlink = isonlineparentlink;

    media.playInSequence = playInSequence;
    media.playStartAtPosition = playStartAtPosition;
    if (subtitleAddress != null) {
      int subtitleAddressl = subtitleAddress.size();
      if (subtitleAddressl > 0) {
        media.subtitleAddress = new ArrayList<>();
        for (int i = 0; i < subtitleAddressl; i++) {
          media.subtitleAddress.add(subtitleAddress.get(i));
        }
      }
    }

    return media;
  }

  public static boolean getArchived(String dossier) {
    Matcher matcher = Gallery.archivepattern.matcher(dossier);
    if (matcher.find()) {
      return true;
    }
    return false;
  }

  public static boolean typeAcceptedForMusic(String fichiernom) {
    int ttype = getType(fichiernom);
    if (ttype == media_video || ttype == media_music)
      return true;
    return false;
  }

  public static int getType(String fichiernom) {
    Matcher matcher = Gallery.imagepattern.matcher(fichiernom);
    if (matcher.find()) {
      return media_picture;
    }
    matcher = Gallery.videoppattern.matcher(fichiernom);
    if (matcher.find()) {
      return media_video;
    }
    matcher = Gallery.audiopattern.matcher(fichiernom);
    if (matcher.find()) {
      return media_music;
    }
    matcher = Gallery.collectionpattern.matcher(fichiernom);
    if (matcher.find()) {
      return media_collection;
    }
    matcher = Gallery.externalviewerpattern.matcher(fichiernom);
    if (matcher.find()) {
      return media_eps;
    }
    return media_refused;
  }

  public void addstring(String string, final int key, ByteBuffer selected) {
    if (string != null) {
      selected.put((byte) key);
      byte[] bbyte = string.getBytes(StandardCharsets.UTF_8);
      selected.putInt(bbyte.length).order(ByteOrder.BIG_ENDIAN);
      selected.put(bbyte);
    }
  }

}