package la.daube.photochiotte;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ShellExecuter {

  private static final String TAG = "YYYsh";
  public static final String magiskfolder = "/data/adb/modules";
  public static final String magiskmodule = magiskfolder + "/photochiotte";
  public Gallery model;
  public static final int isUNKNOWN = 0;
  public static final int isENABLED = 1;
  public static final int isMAGISKMODDED = 2;
  public static final int isDISABLED = 3;
  public static final int isMAGISKUNINSTALLED = 4;
  public static final int isUNINSTALLED = 5;
  public static final int isSD = 6;

  public ShellExecuter(Gallery mmodel) {
    model = mmodel;
  }

  private volatile boolean travaille = false;

  public void executethread(final List<String> command, final String displaywhilewaiting) {
    travaille = true;
    if (model != null) {
      new Thread(new Runnable() {
        @Override
        public void run() {
          long temps = System.currentTimeMillis();
          while (travaille) {
            model.message(displaywhilewaiting + String.format(" - Please wait (%.1fs)", ((System.currentTimeMillis() - temps)) / 1000.0f));
            try {
              Thread.sleep(500);
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          }
          getrunningservices();
        }
      }).start();
    }
    new Thread(new Runnable() {
      @Override
      public void run() {
        execute(command, true, false);
        travaille = false;
      }
    }).start();
  }

  public String[] executethreadresults(final List<String> command, final String displaywhilewaiting) {
    return execute(command, true, false);
  }

  public void getrunningservices() {
    llog.d(TAG, "getrunningservices");
    List<String> command = new ArrayList<>();
    String[] resultline;
    int allappinfol;
    String[] result;
    int packagel;

    command.clear();
    command.add("pm list packages -f");
    result = execute(command, false, false);
    if (result != null) {
      resultline = result[1].split("\n");
      packagel = " > package:".length();
      allappinfol = model.allappinfo.size();
      for (int j = 0; j < allappinfol; j++) {
        model.allappinfo.get(j).statuscurrent = isUNINSTALLED;
      }
      for (int i = 0; i < resultline.length; i++) {
        if (resultline[i].length() > packagel) {
          resultline[i] = resultline[i].substring(packagel);
          int last = resultline[i].lastIndexOf("=");
          String sourcedir = resultline[i].substring(0, last);
          resultline[i] = resultline[i].substring(last + 1);
          boolean alreadyhave = false;
          for (int j = 0; j < allappinfol; j++) {
            if (model.allappinfo.get(j).packagename.equals(resultline[i])) {
              model.allappinfo.get(j).sourcedir = sourcedir;
              model.allappinfo.get(j).statuscurrent = isENABLED;
              alreadyhave = true;
              break;
            }
          }
          if (!alreadyhave) {
            llog.d(TAG, "package not found by packagemanager " + resultline[i]);
            AppInfo app = new AppInfo();
            app.appname = resultline[i];
            app.packagename = resultline[i];
            app.sourcedir = sourcedir;
            app.statuscurrent = isENABLED;
            model.allappinfo.add(app);
          }
        }
      }
      for (int j = 0; j < allappinfol; j++) {
        if (model.allappinfo.get(j).statuscurrent == isUNINSTALLED)
          llog.d(TAG, "package uninstalled " + model.allappinfo.get(j).packagename);
      }
    }

    command.clear();
    command.add("dumpsys activity services | grep ServiceRecord");
    result = execute(command, false, false);
    if (result != null) {
      resultline = result[1].split("\n");
      allappinfol = model.allappinfo.size();
      for (int i = 0; i < allappinfol; i++) {
        model.allappinfo.get(i).servicerunningl = 0;
      }
      Pattern packagen1 = Pattern.compile(" ([^ ]+)/([^ ]+) c:([^ ]+).$");
      Pattern packagen2 = Pattern.compile(" ([^ ]+)/([^ ]+).$");
      Matcher matcher;
      for (int i = 0; i < resultline.length; i++) {
        String []serviceinfo = null;
        matcher = packagen1.matcher(resultline[i]);
        if (matcher.find()) {
          serviceinfo = new String[]{matcher.group(1), matcher.group(2), matcher.group(3)};
        } else {
          matcher = packagen2.matcher(resultline[i]);
          if (matcher.find()) {
            serviceinfo = new String[]{matcher.group(1), matcher.group(2)};
          }
        }
        if (serviceinfo != null) {
          boolean packagenotfound = true;
          allappinfol = model.allappinfo.size();
          for (int j = 0; j < allappinfol; j++) {
            boolean pickit = false;
            /*if (serviceinfo.length == 3)
              if (model.allappinfo.get(j).packagename.equals(serviceinfo[2]))
                pickit = true;*/
            if (!pickit)
              if (model.allappinfo.get(j).packagename.equals(serviceinfo[0]))
                pickit = true;
            if (pickit) {
              boolean found = false;
              for (int k = 0; k < model.allappinfo.get(j).servicel; k++) {
                //llog.d(TAG, "  look at " + serviceinfo[1] + " in " + model.allappinfo.get(j).servicename[k]);
                if (model.allappinfo.get(j).servicename[k].endsWith(serviceinfo[1])) {
                  model.allappinfo.get(j).servicestatuscurrent[k] = isENABLED;
                  model.allappinfo.get(j).servicerunningl += 1;
                  found = true;
                  break;
                }
              }
              if (!found) {
                llog.d(TAG, "service not found for package : " + resultline[i]);
              }
              packagenotfound = false;
              break;
            }
          }
          if (packagenotfound) {
            AppInfo app = new AppInfo();
            app.appname = resultline[i];
            app.packagename = resultline[i];
            app.sourcedir = "unknown service";
            model.allappinfo.add(app);
            llog.d(TAG, "package not found for service : " + resultline[i]);
          }
        } else {
          llog.d(TAG, "pattern not found for service : " + resultline[i]);
        }
      }
    }

    command = new ArrayList<>();
    command.add("pm list packages -d");
    result = execute(command, false, false);
    if (result != null) {
      resultline = result[1].split("\n");
      packagel = " > package:".length();
      allappinfol = model.allappinfo.size();
      for (int i = 0; i < resultline.length; i++) {
        if (resultline[i].length() > packagel) {
          resultline[i] = resultline[i].substring(packagel);
          for (int j = 0; j < allappinfol; j++) {
            if (model.allappinfo.get(j).packagename.equals(resultline[i])) {
              model.allappinfo.get(j).statuscurrent = isDISABLED;
              //Log.d(TAG, "disabled : "+resultline[i]+" "+model.allappinfo.get(j).statuscurrent);
              break;
            }
          }
        }
      }
    }

    try {
      model.commandethreadbrowser.put(new String[]{String.valueOf(0), "update"});
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

  }

  public String[] execute(List<String> command, boolean showresults, boolean onlystdout) {

    Runtime runtime = Runtime.getRuntime();
    Process proc = null;
    OutputStreamWriter osw = null;
    String sbstdOut = "";
    String sbstdErr = "";
    int exitvalue = -128;
    int commandl = command.size();
    StringBuilder macommande = new StringBuilder();
    for (int i = 0; i < commandl; i++) {
      macommande.append(command.get(i) + " ;\n");
    }
    ArrayList<String> stdouts = new ArrayList<>();

    try { // Run Script
      proc = runtime.exec("su");
      //llog.d(TAG, "su");
      osw = new OutputStreamWriter(proc.getOutputStream());
      for (int i = 0; i < commandl; i++) {
        //llog.d(TAG, command.get(i) + " ;");
        osw.write(command.get(i) + " ;");
      }
      osw.flush();
      osw.close();
    } catch (IOException ex) {
      ex.printStackTrace();
    } finally {
      if (osw != null) {
        try {
          osw.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }

    try {
      if (proc != null) {
        proc.waitFor();
      } else {
        llog.d(TAG, "proc null");
        if (model != null && showresults)
          model.message("Error could not execute command.");
        return null;
      }
    } catch (InterruptedException e) {
      Log.d(TAG, "InterruptedException e");
      e.printStackTrace();
    }

    try {
      InputStreamReader is = new InputStreamReader(proc.getInputStream());
      BufferedReader reader = new BufferedReader(is);
      StringBuilder stdout = new StringBuilder();
      String line;
      try {
        while ((line = reader.readLine()) != null) {
          if (onlystdout)
            stdouts.add(line);
          else
            stdout.append(" > " + line + "\n");
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
      sbstdOut = stdout.toString();
      is.close();
    } catch (IOException e) {
      e.printStackTrace();
    }

    try {
      InputStreamReader is = new InputStreamReader(proc.getErrorStream());
      BufferedReader reader = new BufferedReader(is);
      StringBuilder stdout = new StringBuilder();
      String line;
      try {
        while ((line = reader.readLine()) != null) {
          stdout.append("E> " + line + "\n");
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
      sbstdErr = stdout.toString();
      is.close();
    } catch (IOException e) {
      e.printStackTrace();
    }

    exitvalue = proc.exitValue();

    if (model != null && showresults) {
      //if (exitvalue == -128) { model.message("Device is not rooted.");
      model.message(macommande + "Return code = " + exitvalue + "\n" + sbstdOut + "\n" + sbstdErr);
    }
    llog.d(TAG, macommande + "Return code = " + exitvalue + "\n" + sbstdOut + "\n" + sbstdErr);

    //Log.d(TAG, sbstdOut);

    if (onlystdout)
      return stdouts.toArray(new String[0]);
    else
      return new String[]{String.valueOf(exitvalue), sbstdOut, sbstdErr};
  }

}
















































