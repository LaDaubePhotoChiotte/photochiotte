package la.daube.photochiotte;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PermissionInfo;
import android.content.pm.ServiceInfo;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.Base64;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.room.Room;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.CodingErrorAction;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class ThreadDatabase implements Runnable {
  private static final String TAG = "YYYtdb";

  private final Gallery model;

  //private volatile boolean wearelocked = false;
  
  private final List<String> dinserers = new ArrayList<>();
  private final List<Fichier> finserer = new ArrayList<>();
  private final ArrayList<HashMap<String, String>> onlinedata = new ArrayList<>();

  protected ThreadDatabase(Gallery mmodel){
    model = mmodel;
  }
  
  void deletefiles(File fileOrDirectory) {
    if (fileOrDirectory == null)
      return;
    if (fileOrDirectory.isDirectory()) {
      File[] childs = fileOrDirectory.listFiles();
      if (childs != null) {
        for (File child : childs)
          if (child != null)
            deletefiles(child);
      }
    } else {
      fileOrDirectory.delete();
    }
  }

  public static ArrayList<String> findfiles(String dir, String ext) {
    ArrayList<String> found = null;
    File dirfile = new File(dir);
    if (!dirfile.exists())
      return found;
    File[] directory = dirfile.listFiles();
    if (directory != null) {
      found = new ArrayList<>();
      for (File f : directory) {
        String fnom = f.getName();
        int extp = fnom.lastIndexOf(ext);
        if (extp == fnom.length() - ext.length()) {
          found.add(dir + fnom);
          llog.d(TAG, "found collection <" + fnom + ">");
        }
      }
    }
    return found;
  }

  public static boolean copyFile(File sourceFile, File destFile) throws IOException {
    boolean success = false;
    if (!destFile.getParentFile().exists())
      destFile.getParentFile().mkdirs();
    if (!destFile.exists())
      destFile.createNewFile();
    FileChannel source = null;
    FileChannel destination = null;
    try {
      source = new FileInputStream(sourceFile).getChannel();
      destination = new FileOutputStream(destFile).getChannel();
      long sourcesize = source.size();
      destination.transferFrom(source, 0, sourcesize);
      success = true;
    } finally {
      if (source != null)
        source.close();
      if (destination != null)
        destination.close();
    }
    return success;
  }

  /*public void deletethemall(String basedir, DocumentFile pickedDir) {
    for (DocumentFile file : pickedDir.listFiles()) {
      String fullpath = basedir+"/"+file.getName();
      if (file.isDirectory()) {
        deletethemall(fullpath, file);
      } else {
        int fn = model.deletefiles.size();
        for (int i = 0 ; i < fn ; i++) {
          String thisfile = model.deletefiles.get(i)[1];
          if (thisfile.endsWith(fullpath)) {
            //llog.d(TAG,   "deleting " + thisfile);
            boolean deleted = file.delete();
            if (deleted) {
              deletefilefromdbmini(model.deletefiles.get(i)[0], thisfile);
              //llog.d(TAG, "deleted  " + thisfile);
            }
          }
        }
      }
    }
  }*/

  private void removeMediaFromDatabase(String ordnerAddress, String mediaAddress) {
    model.databasecurrentlyremovingfiles = true;
    int d = model.findFolder(ordnerAddress);
    if (d == -1) {
      llog.d(TAG, "removeMediaFromDatabase folder not found " + ordnerAddress);
    } else {
      int f = model.findFileInFolder(d, mediaAddress);
      if (f == -1) {
        llog.d(TAG, "removeMediaFromDatabase file not found " + ordnerAddress + " " + mediaAddress);
      } else {
        int addressToGetLibextractorsThumbnail = model.getMediaAddressToGetLibextractorsThumbnail(d, f);
        if (addressToGetLibextractorsThumbnail >= 0) {
          String cefichier;
          File ffichier;
          for (int i = 0 ; i < 1000 ; i++) {
            cefichier = model.dossierminiature + addressToGetLibextractorsThumbnail + "_" + i;
            ffichier = new File(cefichier);
            if (!ffichier.exists())
              break;
            ffichier.delete();
            cefichier = model.dossierminiature + addressToGetLibextractorsThumbnail + "." + i;
            ffichier = new File(cefichier);
            if (!ffichier.exists())
              break;
            ffichier.delete();
          }
          model.db.dbFichier().deleteFichierByName(mediaAddress);
        }
        model.removeMediaFromGallery(ordnerAddress, mediaAddress, d, f);
        int mtouslesfichiersli = model.getFolderFileCount(d);
        if (mtouslesfichiersli == 0) {
          model.db.dbDossier().deleteFolderOrZip(ordnerAddress);
          model.removeOrdnerFromGallery(ordnerAddress, d);
        }
      }
    }
    model.databasecurrentlyremovingfiles = false;
  }

  private void removeOrdnerFromDatabase(String address) {
    int d = model.findFolder(address);
    if (d == -1) {
      llog.d(TAG, "removeOrdnerFromDatabase folder not found " + address);
    } else {
      //if (model.getFolderNotLoadedYet(idossier)) rechargetoutledossier(idossier, false); // pas ici
      String[] medialist = model.getOrdnerMediaList(address, d);
      if (medialist == null)
        llog.d(TAG, "removeOrdnerFromDatabase medialist null " + address);
      else {
        int n = medialist.length;
        for (int f = 0; f < n; f++)
          removeMediaFromDatabase(address, medialist[f]);
      }
      model.db.dbDossier().deleteFolderOrZip(address);
      model.removeOrdnerFromGallery(address, d);
    }
  }

  public void execshell(String filename, String args, ShellExecuter exe, boolean showresults) {
    String destname = model.internalStorageDir + "/" + filename;
    File outFile = new File(destname);
    if (!outFile.exists() || BuildConfig.DEBUG) { // TODO : do not overwrite if no new version detected
      InputStream ins;
      OutputStream out;
      final AssetManager assetManager = model.activitycontext.getAssets();
      try {
        ins = assetManager.open(filename, AssetManager.ACCESS_STREAMING);
        out = new FileOutputStream(outFile);
        int read;
        byte[] bytes = new byte[1024];
        while ((read = ins.read(bytes)) != -1) {
          out.write(bytes, 0, read);
        }
        ins.close();
        out.close();
        llog.d(TAG, "Copied asset file to " + destname);
      } catch (IOException e) {
        llog.d(TAG, "Failed to copy asset file " + e);
      }
    }
    ArrayList<String> command = new ArrayList<>();
    command.add("cd  " + model.internalStorageDir);
    command.add("chmod +x " + filename);
    command.add("/system/bin/sh " + filename + " " + args);
    llog.d(TAG, "execute sh " + filename + " " + args);
    String[] result = exe.execute(command, showresults, false);
    llog.d(TAG, "sh " + filename + " " + args + " executed " + args);
  }

  @Override
  public void run(){
    model.threaddatabaserunning = true;
    llog.d(TAG, "+++++++++++++++++++++ ThreadDatabase");

    while (model.dossierminiature == null) {
      try {
        Thread.sleep(10);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }

    model.db = Room.databaseBuilder(model.activitycontext, AppDatabase.class, "database-name").fallbackToDestructiveMigration().build();

    //initializeonline();
    readfile();

    Gallery.isConnectedToInternetRetry = 0;

    while(true){

      boolean encore = true;
      String[] commande = null;
      int metenmemoiren = 0;
  
      /*if (model.commandethreaddatabase.isEmpty()) {
        nettoiedeldossier();
      }*/

      while (encore) {

        try {
          commande = model.commandethreaddatabase.take();
        } catch (InterruptedException e) {
          e.printStackTrace();
        }

        if (commande[0].equals("quit")) {
          if (model.threaddatabaseon) {
            llog.d(TAG, "////////////////////// thread database keep running");
          } else {
            model.threaddatabaserunning = false;
            llog.d(TAG, "----------------------- ThreadDatabase");
            if (model.db.isOpen())
              model.db.close();
            return;
          }

        /*} else if (wearelocked) {

          if (commande[0].equals("unlock")) {
            wearelocked = false;
            try {
              model.commandethreadminiature.put(new String[]{"unlock"});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          } else {
            // on remet à la fin les autres ordres le temps de la pause, on garde le même ordre
            if (model.commandethreaddatabase.isEmpty()) {
              try {
                Thread.sleep(30);
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
            }
            if (!commande[0].equals("rechargetoutledossier")) {
              try {
                model.commandethreaddatabase.put(commande);
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
            }
          }*/

        } else if (commande[0].equals("copyassets")) {
          boolean newversiondetected = false;
          if (commande[1].equals("newversion"))
            newversiondetected = true;
          final AssetManager assetManager = model.activitycontext.getAssets();
          String[] files = new String[]{
                  // do not overwrite
                  "onlinesites.txt",
                  "mpv.conf",
                  "input.conf",
                  // update every time
                  "cacert.pem",
                  "subfont.ttf",
          };
          int filesl = files.length;
          for (int i = 0 ; i < filesl ; i++) {
            String filename = files[i];
            String destname = model.dossierconfig + "/" + filename;
            File outFile = new File(destname);
            if (outFile.exists() && i < 3) // do not overwrite
              continue;
            else if (outFile.exists() && !newversiondetected) // overwrite if new version
              continue;
            InputStream ins;
            OutputStream out;
            try {
              ins = assetManager.open(filename, AssetManager.ACCESS_STREAMING);
              out = new FileOutputStream(outFile);
              int read;
              byte[] bytes = new byte[1024];
              while ((read = ins.read(bytes)) != -1) {
                out.write(bytes, 0, read);
              }
              ins.close();
              out.close();
              llog.d(TAG, "Copied asset file to " + destname);
            } catch (IOException e) {
              llog.d(TAG, "Failed to copy asset file " + e);
            }
          }

        } else if (commande[0].equals("scanapps")) {

          ShellExecuter exe = new ShellExecuter(model);
          int currid = Integer.parseInt(commande[1]);
          if (currid < 0)
            currid = 0;
          if (commande[2].equals("reinitialize")) {
            reinitialize();
            exe.getrunningservices();
          } else if (commande[2].equals("runcmd")) {
            ArrayList<String> command = new ArrayList<>();
            command.add(commande[3]);
            llog.d(TAG, "execute cmd " + commande[3]);
            String[] result = exe.execute(command, true, false);
            llog.d(TAG, "cmd executed " + commande[3]);
          } else {
            execshell(commande[2], commande[3], exe, true);
            if (commande[4].equals("refreshappsthumbs")) {
              try {
                Thread.sleep(2000);
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
              reinitialize();
              exe.getrunningservices();
              try {
                model.commandethreadminiature.put(new String[]{"cleanup", "force"});
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
              model.surf.get(currid).removekeysfromcache();
              model.surf.get(currid).removefromcache();
            }
          }

        } else if (commande[0].equals("findMediaInAnotherOrdner")) {
          int currid = Integer.parseInt(commande[1]);
          Surf mysurf = model.surf.get(currid);
          String mondossier = commande[2];
          if (mondossier != null) // happens if we click a second time away from collection
            if (mondossier.length() == 0)
              mondossier = null;
          String monfichier = commande[3];
          if (monfichier != null)
            if (monfichier.length() == 0)
              monfichier = null;
          if (monfichier == null) {
            model.message("media address missing");
          } else {
            llog.d(TAG, "findMediaInAnotherOrdner " + mondossier + " " + monfichier);
            int d = -1;
            int f = -1;
            if (monfichier.startsWith("/")) {
              String dossierfound = model.db.dbFichier().fichierDejaDansDB(monfichier);
              if (dossierfound == null) {
                llog.d(TAG, "file not found in the database check if moved " + monfichier);
                int lastslash = monfichier.lastIndexOf('/');
                if (lastslash == -1 || lastslash >= monfichier.length() - 1) {
                  llog.d(TAG, "filename without slash " + monfichier);
                } else {
                  String fname = monfichier.substring(lastslash); // keep slash
                  List<Fichier> fichier = model.db.dbFichier().fichierNomDejaDansDB(fname);
                  if (fichier == null) {
                    llog.d(TAG, "not even small fname found");
                  } else {
                    int fichierl = fichier.size();
                    if (fichierl == 0) {
                      llog.d(TAG, "not even small fname found");
                    } else {
                      for (int i = 0 ; i < fichierl ; i++) {
                        d = model.findFolder(fichier.get(i).dossier);
                        if (d > mysurf.ordnerIndex) { // this check is enough because there is not twice the same file name in the same folder
                          mondossier = fichier.get(i).dossier;
                          monfichier = fichier.get(i).fichier;
                          llog.d(TAG, "file moved to " + mondossier + " " + monfichier);
                          break;
                        }
                      }
                    }
                  }
                }
              } else {
                if (mondossier != null)
                  if (!dossierfound.equals(mondossier))
                    llog.d(TAG, "different folders found " + dossierfound + " " + mondossier);
              }
            }
            if (mondossier != null && d < 0) {
              d = model.findFolder(mondossier);
              if (d < 0)
                if (!mondossier.endsWith("/"))
                  d = model.findFolder(mondossier + "/");
              llog.d(TAG, "findfolder " + d);
              if (d >= 0) { // folder exists
                f = model.findMediaAddress(d, monfichier);
                llog.d(TAG, "findMediaAddress " + d + " " + f);
                if (f == -2) {
                  llog.d(TAG, "findMediaInAnotherOrdner folder has already been loaded but file has not been found ");
                  d = -1; // look if file has been moved to another folder
                }
              }
            }
            if (d < 0) {
              llog.d(TAG, "findMediaInAnotherOrdner check file moved to another folder");
              int[] df = model.findFileNameInAllBut(mysurf.ordnerIndex, mysurf.mediaIndex, monfichier);
              if (df != null) {
                d = df[0];
                f = df[1];
              }
            }
            if (d < 0 && mondossier != null) {
              llog.d(TAG, "findMediaInAnotherOrdner server moved but folder structure was kept");
              d = model.findFolderEndsSame(mondossier);
            }
            if (d < 0) {
              llog.d(TAG, "ordner not found folder probably not loaded yet " + mondossier + " " + monfichier);
              model.message(mondossier + "\nnot found");
            } else {
              llog.d(TAG, currid + " found ordner " + d + " : " + mondossier + " on cherche fichier " + monfichier);
              int bookmarkordneri = model.findFolder(Gallery.virtualBookmarkFolder);
              if (bookmarkordneri != -1) {
                llog.d(TAG, "virtualBookmarkFolder found " + bookmarkordneri + " " + d);
                model.removeOrdnerFromGallery(Gallery.virtualBookmarkFolder, bookmarkordneri);
                if (bookmarkordneri <= d) {
                  llog.d(TAG, "virtualBookmarkFolder found " + bookmarkordneri + " <= " + d + " d-=1");
                  d -= 1;
                }
              }

              if (f < 0)
                f = model.findMediaAddress(d, monfichier);
              llog.d(TAG, "findMediaAddress " + d + " : " + f);
              if (f < 0) {
                llog.d(TAG, "file not found yet " + d);
                model.bookmarkshowthisfilewhenfolderready = monfichier;
                model.changeBigPicture(currid, d, 0, -1, 0, true, false);
                try {
                  model.commandethreadbrowser.put(new String[]{String.valueOf(currid), "dontmissupdate"});
                } catch (InterruptedException e) {
                  e.printStackTrace();
                }
              } else {
                model.changeBigPicture(currid, d, 0, f, 0, true, false);
              }

              model.optionshowbookmarkeddisplay = false;
            }
          }

        } else if (commande[0].equals("loadAutoStartOnlineMedia")) {

          String lastbrowsedonline = null;
          String lastbrowsedonlined = null;
          String lastbrowsedonlinef = null;
          if (!model.autoloadlastonlinemediaviewed) {
            llog.d(TAG, "do not restore last session");
          } else {
            lastbrowsedonline = model.preferences.getString("lastbrowsedonline", null);
            if (lastbrowsedonline != null) {
              lastbrowsedonlined = model.preferences.getString("lastbrowsedonlined", null);
              lastbrowsedonlinef = model.preferences.getString("lastbrowsedonlinef", null);
              model.message("Restore\n" + lastbrowsedonlinef);
            }
          }

          int nombredossierbookmarked = model.preferences.getInt("nombreonline", 0);
          for (int i = 0; i < nombredossierbookmarked; i++) {
            boolean autoload = model.preferences.getBoolean("autoload" + i, false);
            if (autoload) {
              String onaffichecebookmark = model.preferences.getString("mediaonline" + i, null);
              if (onaffichecebookmark != null) {
                if (lastbrowsedonline != null) {
                  if (onaffichecebookmark.equals(lastbrowsedonline)) {
                    lastbrowsedonline = null;
                  }
                }
                try {
                  model.commandethreaddatabase.put(new String[]{"browseonline", "0", onaffichecebookmark});
                } catch (InterruptedException e) {
                  e.printStackTrace();
                }
              }
            }
          }

          if (lastbrowsedonline != null) {
            llog.d(TAG, "reload last time collection " + lastbrowsedonline);
            try {
              model.commandethreaddatabase.put(new String[]{"browseonline", "0", lastbrowsedonline});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          }
          if (lastbrowsedonlinef != null) {
            model.surf.get(0).zoommode = model.preferences.getInt("lastbrowsedonlinez", Gallery.ZoomKeep);
            llog.d(TAG, "lookup last time file " + lastbrowsedonlined + " " + lastbrowsedonlinef + " zoommode " + model.surf.get(0).zoommode);
            try {
              model.commandethreaddatabase.put(new String[]{"findMediaInAnotherOrdner", String.valueOf(0), lastbrowsedonlined, lastbrowsedonlinef});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          }

        } else if (commande[0].equals("chercheFichierDansDatabaseSinonRescanSonDossier")) {
          
          int mtouslesdossiersl = model.folderCount;
          if (mtouslesdossiersl <= 0) {
            llog.d(TAG, commande[0] + " : on charge tous les dossiers depuis la database");
            model.setFoldersFromDB(model.db.dbDossier().getAll());
            mtouslesdossiersl = model.folderCount;
            if (mtouslesdossiersl <= 0) {
              llog.d(TAG, commande[0] + " : on n'a aucun dossier dans la database, on refait un scan.");
              //deletefiles(new File(model.dossierminiature));
              //model.db.clearAllTables();

              int basefolderl = model.basefolder.size();
              for (int i = 0 ; i < basefolderl ; i++) {
                addSubFoldersToDatabaseAndGallery(model.basefolder.get(i));
              }

              if (model.folderCount <= 0) {
                llog.d(TAG, commande[0] + " : on n'a toujours aucun dossier dans la database, on abandonne.");
              } else {
                llog.d(TAG, commande[0] + " : on a ajouté " + model.folderCount + " dossiers dans la database.");
              }
              try {
                model.commandethreadbrowser.put(new String[]{"-1", "dontmissupdate"});
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
              continue;
            } else {
              llog.d(TAG, "chercheFichierDansDatabaseSinonRescanSonDossier : check new files in mediastore");
              Intent intent = new Intent();
              intent.setAction(Gallery.broadcastname);
              intent.putExtra("goal", "checknewfilesinmediastore");
              LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
            }
            String cefichier = commande[2];
            if (cefichier == null) {
              llog.d(TAG, commande[0] + " : on a chargé la database.");
              try {
                model.commandethreadbrowser.put(new String[]{"-1", "dontmissupdate"});
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
              continue;
            }
          }
          
          String cedossier;
          String cefichier = commande[2];
          boolean forcerescan = false;
          if (cefichier == null) {
            cedossier = commande[1];
            cefichier = commande[1] + "/barbabulle.gif";
            forcerescan = true;
            llog.d(TAG, commande[0] + " : on veut rescanner le dossier "+cedossier);
          } else {
            llog.d(TAG, commande[0] + " : on cherche le fichier " + cefichier);
            cedossier = model.db.dbFichier().fichierDejaDansDB(cefichier);
          }
          
          if (cedossier == null || forcerescan) {
            cedossier = new File(cefichier).getParent();
            if (cedossier != null) {
              if (!cedossier.endsWith("/"))
                cedossier += "/";
              cedossier = model.db.dbDossier().dossierDejaDansDB(cedossier);
              if (cedossier == null || forcerescan) {
                if (cedossier == null) {
                  cedossier = new File(cefichier).getParent();
                  if (cedossier != null) {
                    if (!cedossier.endsWith("/"))
                      cedossier += "/";
                    llog.d(TAG, commande[0] + " : " + cedossier + " n'est pas encore dans la database, on le scanne.");
                  }
                } else {
                  llog.d(TAG, commande[0] + " : forcerescan " + cedossier);
                }
                if (cedossier != null) {
                  addSubFoldersToDatabaseAndGallery(cedossier);
                  cedossier = model.db.dbDossier().dossierDejaDansDB(cedossier);
                }
                if (cedossier == null) {
                  llog.d(TAG, commande[0] + " : son dossier parent n'est toujours pas dans la database. On annule la recherche de fichier.");
                  continue;
                }
              }
              llog.d(TAG, commande[0] + " : le dossier parent est dans la database mais pas le fichier : il faut scanner le dossier " + cedossier);
              forcerescan = true;
              int idossier = model.findFolder(cedossier);
              if (idossier == -1) {
                llog.d(TAG, commande[0] + " : ERREUR dossier pas trouvé " + cedossier);
              } else {
                reloadFolderOrRemoveFromGallery(idossier, forcerescan);
              }
            } else {
              llog.d(TAG, commande[0] + " : son dossier parent n'est toujours pas dans la database. On annule la recherche de fichier.");
              continue;
            }
          }

          int onaffichelefichierdirectement = Integer.parseInt(commande[3]);
          llog.d(TAG, commande[0] + " : fichier déjà trouvé dans la database on le cherche et on l'affiche si " + onaffichelefichierdirectement);
          int thisfolder = model.findFolder(cedossier);
          int thisfile = -1;
          if (thisfolder == -1) {
            llog.d(TAG, commande[0] + " : ERREUR fichier " + cefichier + " trouvé mais pas le dossier " + cedossier);
          } else {
            List<Media> listmedia = new ArrayList<>();
            String dossiername = model.getOrnderAddress(thisfolder);
            List<Fichier> listfichier;
            if (dossiername != null)
              listfichier = model.db.dbFichier().getAll(dossiername);
            else
              listfichier = new ArrayList<>();
            int listfichierl = listfichier.size();
            for (int i = 0; i < listfichierl; i++) {
              Fichier dbfichier = listfichier.get(i);
              Media media = new Media();
              media.addressToGetLibextractorsThumbnail = dbfichier.uid;
              media.address = dbfichier.fichier;
              media.ordnerAddress = dbfichier.dossier;
              media.isInsideAnArchive = Media.getArchived(dbfichier.dossier);
              media.type = Media.getType(dbfichier.fichier);
              listmedia.add(media);
              //llog.d(TAG, "+ add file " + media.fichier);
            }
            model.setFilesOrSetNotLoadedYet(thisfolder, listmedia);
            listfichier.clear();
            thisfile = model.findFileInFolder(thisfolder, cefichier);
            if (thisfile == -1)
              llog.d(TAG, commande[0] + " : ERREUR fichier " + cefichier + " pas trouvé mais le dossier a été trouvé " + cedossier);
          }
          /*
          llog.d(TAG, commande[0] + " : on met à jour la miniature (ah ouais et pourquoi ?) folder " + thisfolder + " file " + thisfile);
          int uidfichier = model.getFileFreeBitmap(thisfolder, thisfile);
          String ccefichier = model.dossierminiature + uidfichier;
          File ffichier = new File(ccefichier);
          if (ffichier.exists()) {
            boolean deleted = ffichier.delete();
            llog.d(TAG, deleted + " deleted miniature "+ccefichier);
          }
          */
          if (onaffichelefichierdirectement != -1) {
            try {
              model.commandethreadbrowser.put(new String[]{"-1", "ChangeCurrentDisplayedPicture", String.valueOf(thisfolder), String.valueOf(thisfile), String.valueOf(onaffichelefichierdirectement)});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          }

          // on cherche s'il y a de nouveaux fichiers dans le media store
          /*
          for (String folder : model.autorescan) {
            if (model.findFolder(folder) == -1) {
              llog.d(TAG, "on doit ajouter le dossier par défaut " + folder);
              dossier.dossier = folder;
              model.addFolder(dossier, null, false, false, false);
              try {
                model.commandethreaddatabase.put(new String[]{"chercheFichierDansDatabaseSinonRescanSonDossier", folder, null, "-1"});
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
            }
          }
          */
          /*
          if (model.findFolder(model.dossiercachefiles) == -1) {
            llog.d(TAG, "on doit ajouter le dossier par défaut " + model.dossiercachefiles);
            dossier.dossier = model.dossiercachefiles;
            model.addFolder(dossier, null, false, false, false);
            try {
              model.commandethreaddatabase.put(new String[]{"chercheFichierDansDatabaseSinonRescanSonDossier", model.dossiercachefiles, null, "-1"});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          }
          */

          //model.message();
          encore = false;


        } else if (commande[0].equals("foldthis")) {
          String foldthis = commande[1];
          List<String> folderstartswith = model.findFoldersStartsWith(foldthis);
          for (int i = 0 ; i < folderstartswith.size() ; i++)
            removeOrdnerFromDatabase(folderstartswith.get(i));
          llog.d(TAG, "foldthis finished handling "+foldthis);
          encore = false;

        } else if (commande[0].equals("closeonlinefolders")) {
          model.removeOnlineFolders();
          model.redrawsplitviewbitmapm = true;
          model.forgetlastbrowsedonline();
          if (model.showthisfile != null) {
            llog.d(TAG, "closed online folders, goto " +  model.showthisfolder + " " + model.showthisfile);
            try {
              model.commandethreaddatabase.put(new String[]{"chercheFichierDansDatabaseSinonRescanSonDossier", model.showthisfolder, model.showthisfile, "0"});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          }
          try {
            model.commandethreadbrowser.put(new String[]{"-1", "update"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
  
  
  
  
  
  
  


        } else if (commande[0].equals("commentmedia")) {
          int id = Integer.parseInt(commande[1]);
          int d = Integer.parseInt(commande[3]);
          int f = Integer.parseInt(commande[4]);
          String type = commande[5];

          String addinfo = commande[2];

          ArrayList<String> s = new ArrayList<>();
          String additionalMetadata = "";
          if (type.equals("printName")) {
            additionalMetadata = "printName=" + addinfo + "\n";
            s.add(additionalMetadata);
            model.setMediaPrintName(d, f, addinfo);
          } else if (type.equals("printDetails")) {
            additionalMetadata = "printDetails=" + addinfo + "\n";
            s.add(additionalMetadata);
            model.setMediaPrintDetails(d, f, addinfo);
          } else if (type.equals("printFooter")) {
            additionalMetadata = "printFooter=" + addinfo + "\n";
            s.add(additionalMetadata);
            model.setMediaPrintFooter(d, f, addinfo);
          }

          int isonline = model.getMediaIsOnline(d, f);
          if (isonline == Media.online_no) {
            int addressToGetLibextractorsThumbnail = model.getMediaAddressToGetLibextractorsThumbnail(d, f);
            File file = new File(model.dossierminiature + addressToGetLibextractorsThumbnail + "_0");
            Gallery.saveStringsToFile(s, file, true);
            llog.d(TAG, "appending " + s.get(0) + " to " + model.dossierminiature + addressToGetLibextractorsThumbnail + "_0");
          } else if (isonline == Media.online_apache) {
            String miniature = model.getMediaAddress(d, f);
            String tempfile = model.dossierminiature + "dummy.temp";
            File fichier = new File(tempfile);
            boolean grabbed = InternetSession.getApacheSaveToFile(model, miniature + ".thmb_0", fichier, true);
            boolean updated = false;
            if (grabbed) {
              updated = InternetSession.putApache(model, fichier, miniature + ".thmb_0", model.copyoverwritenosync, additionalMetadata);
            }
            llog.d(TAG, "updated metadata for " + miniature + ".thmb_0 grabbed " + grabbed + " updated " + updated);
          }

          encore = false;
  
  
  



        } else if (commande[0].equals("updateapachefolderlist")) {

          model.updateApacheFolderList(commande[2]);

          encore = false;
  
  
  
  
        } else if (commande[0].equals("browseonline")) {
          int surfi = Integer.parseInt(commande[1]);
          int lastfolder = model.folderCount;
          model.showonlinesite = commande[2];
          model.showthisfilelockjump = true;
          boolean success = browseonlinesite(surfi, commande[2]);
          if (success) {
            model.changeBigPicture(surfi, lastfolder, 0, -1, 0, true, false);
            model.lastbrowsedonline = commande[2];
          } else
            llog.d(TAG, "error failed to fetch " + commande[2]);
          try {
            model.commandethreadbrowser.put(new String[]{"-1", "update"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }

          encore = false;
  
        } else if (commande[0].equals("browseonlinefolder")) {
          int surfi = Integer.parseInt(commande[1]);
          int currentfolder = model.surf.get(surfi).ordnerIndex;
          boolean success = browseonlinefolder(surfi, commande[2], commande[3]);
          if (success)
            model.changeBigPicture(surfi, currentfolder + 1, 0, -1, 0, true, false);
          else
            model.message("error failed to fetch " + commande[2]);
          try {
            model.commandethreadbrowser.put(new String[]{"-1", "update"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
  
          encore = false;

        } else if (commande[0].equals("updatecollection")) {
          llog.d(TAG, "updatecollection " + commande[2]);
          int surfi = Integer.parseInt(commande[1]);
          boolean success = retrieveonlineindexcollection(commande[2], commande[2], -1, -1);
          if (!success)
            llog.d(TAG, "error failed to updatecollection " + commande[2]);
          try {
            model.commandethreadbrowser.put(new String[]{"-1", "update"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
  
        } else if (commande[0].equals("addonline")) {
  
          String pathonline = commande[1];
          if (pathonline.contains(":")) {

            //if (pathonline.endsWith("/")) {
            //  pathonline = pathonline.substring(0, pathonline.length()-1);
            //}
            int nombreonline = model.preferences.getInt("nombreonline", 0);
            SharedPreferences.Editor editor = model.preferences.edit();
            editor.putString("mediaonline" + nombreonline, pathonline);
            if (commande[2].equals("autoload")) {
              editor.putBoolean("autoload" + nombreonline, true);
            } else {
              editor.putBoolean("autoload" + nombreonline, false);
            }
            editor.putInt("nombreonline", nombreonline + 1);
            editor.apply();
            model.nombreonline = nombreonline + 1;
            llog.d(TAG, "addonline (" + pathonline + " : " + nombreonline + " total)");

          } else {

            String destname = model.dossierconfig + "/onlinesites.txt";
            File outFile = new File(destname);
            OutputStream out;
            try {
              byte[] data = Base64.decode(pathonline, Base64.DEFAULT);
              out = new FileOutputStream(outFile, true);
              out.write(data, 0, data.length);
              out.close();
              llog.d(TAG, "Wrote data to " + destname);
            } catch (IOException e) {
              llog.d(TAG, "Failed to write data " + e);
            } catch (IllegalArgumentException e) {
              model.message("could not decode base64 data");
              llog.d(TAG, "Failed to decode data " + e);
            }

          }
  
          try {
            model.commandethreadbrowser.put(new String[]{"-1", "update"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
  
          encore = false;
  
        } else if (commande[0].equals("deleteonline")) {
  
          int bookmarkthisfolder = Integer.parseInt(commande[1]);
  
          int nombreonline = model.preferences.getInt("nombreonline", 0);
          model.nombreonline = nombreonline - 1;
          SharedPreferences.Editor editor = model.preferences.edit();
          for(int i = bookmarkthisfolder ; i < nombreonline-1 ; i++) {
            editor.putBoolean("autoload"+i, model.preferences.getBoolean("autoload"+String.valueOf(i+1), false));
            editor.putString("mediaonline"+i, model.preferences.getString("mediaonline"+String.valueOf(i+1), null));
          }
          editor.remove("autoload"+String.valueOf(nombreonline-1));
          editor.remove("mediaonline"+String.valueOf(nombreonline-1));
          editor.putInt("nombreonline",nombreonline-1);
          editor.apply();
          llog.d(TAG, "deleteonline (" + commande[1] + " : " + bookmarkthisfolder + " total)");
  
          try {
            model.commandethreadbrowser.put(new String[]{"-1", "update"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
  
          encore = false;


        } else if (commande[0].equals("addautorescan")) {
          int nombredossierbookmarked = model.preferences.getInt("nombredossierautorescan", 0);
          boolean onadeja = false;
          for (int i = 0; i < nombredossierbookmarked; i++) {
            String monoption = model.preferences.getString("dossierautorescan" + i, "");
            if (monoption.equals(commande[1]))
              onadeja = true;
          }
          if (!onadeja) {
            SharedPreferences.Editor editor = model.preferences.edit();
            editor.putString("dossierautorescan" + nombredossierbookmarked, commande[1]);
            editor.putInt("nombredossierautorescan", nombredossierbookmarked + 1);
            editor.apply();
            try {
              model.commandethreaddatabase.put(new String[]{"chercheFichierDansDatabaseSinonRescanSonDossier", commande[1], null, "-1"});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
            llog.d(TAG, "addautorescan (" + commande[1] + ")");
            try {
              model.commandethreadbrowser.put(new String[]{"-1", "update"});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          }
          encore = false;

        } else if (commande[0].equals("deleteautorescan")) {
          int bookmarkthisfolder = Integer.parseInt(commande[1]);
          int nombredossierbookmarked = model.preferences.getInt("nombredossierautorescan", 0);
          SharedPreferences.Editor editor = model.preferences.edit();
          for(int i = bookmarkthisfolder; i < nombredossierbookmarked-1; i++){
            editor.putString("dossierautorescan"+String.valueOf(i), model.preferences.getString("dossierautorescan"+String.valueOf(i+1), null));
          }
          editor.remove("dossierautorescan"+String.valueOf(nombredossierbookmarked-1));
          editor.putInt("nombredossierautorescan",nombredossierbookmarked-1);
          editor.apply();
          llog.d(TAG, "deleteautorescan (" + bookmarkthisfolder + ")");
          try {
            model.commandethreadbrowser.put(new String[]{"-1", "update"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          encore = false;


        } else if (commande[0].equals("adddestinationsaved")) {
          int nombredestinationsaved = model.preferences.getInt("nombredestinationsaved", 0);
          SharedPreferences.Editor editor = model.preferences.edit();
          editor.putString("destinationsaved"+String.valueOf(nombredestinationsaved), commande[1]);
          editor.putInt("nombredestinationsaved",nombredestinationsaved+1);
          editor.apply();
          llog.d(TAG, "adddestinationsaved (" + commande[1] + ")");
          model.nombredestinationsaved = nombredestinationsaved + 1;
          model.optiondestinationmemory = nombredestinationsaved;
          model.preferences.edit().putInt("optiondestinationmemory", model.optiondestinationmemory).commit();
          model.odestinationfolder = "sgdsdf";
          try {
            model.commandethreadbrowser.put(new String[]{"-1", "update"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          encore = false;

        } else if (commande[0].equals("deletedestinationsaved")) {
          int bookmarkthisfolder = Integer.parseInt(commande[1]);
          int nombredestinationsaved = model.preferences.getInt("nombredestinationsaved", 0);
          model.nombredestinationsaved = nombredestinationsaved-1;
          model.optiondestinationmemory = -1;
          model.preferences.edit().putInt("optiondestinationmemory", model.optiondestinationmemory).commit();
          SharedPreferences.Editor editor = model.preferences.edit();
          for(int i = bookmarkthisfolder; i < nombredestinationsaved-1; i++){
            editor.putString("destinationsaved"+String.valueOf(i), model.preferences.getString("destinationsaved"+String.valueOf(i+1), null));
          }
          editor.remove("destinationsaved"+String.valueOf(nombredestinationsaved-1));
          editor.putInt("nombredestinationsaved",nombredestinationsaved-1);
          editor.apply();
          model.odestinationfolder = "sgdgfdfgdsdf";
          llog.d(TAG, "deletedestinationsaved (" + bookmarkthisfolder + ")");
          try {
            model.commandethreadbrowser.put(new String[]{"-1", "update"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          encore = false;
  
          
        } else if (commande[0].equals("addbookmark")) {
          int nombredossierbookmarked = model.preferences.getInt("nombredossierbookmarked", 0);
          SharedPreferences.Editor editor = model.preferences.edit();
          editor.putString("dossierbookmarked"+String.valueOf(nombredossierbookmarked), commande[1]);
          editor.putString("fichierbookmarked"+String.valueOf(nombredossierbookmarked), commande[2]);
          editor.putInt("nombredossierbookmarked",nombredossierbookmarked+1);
          editor.apply();
          llog.d(TAG, "addbookmark (" + commande[1] + " : " + commande[2] + " dossiers)");
          model.nombredossierbookmarked = nombredossierbookmarked + 1;
          try {
            model.commandethreadbrowser.put(new String[]{"-1", "update"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          encore = false;

        } else if (commande[0].equals("deletebookmark")) {
          int bookmarkthisfolder = Integer.parseInt(commande[1]);
          int nombredossierbookmarked = model.preferences.getInt("nombredossierbookmarked", 0);
          model.nombredossierbookmarked = nombredossierbookmarked-1;
          SharedPreferences.Editor editor = model.preferences.edit();
          for(int i = bookmarkthisfolder; i < nombredossierbookmarked-1; i++){
            editor.putString("dossierbookmarked"+String.valueOf(i), model.preferences.getString("dossierbookmarked"+String.valueOf(i+1), null));
            editor.putString("fichierbookmarked"+String.valueOf(i), model.preferences.getString("fichierbookmarked"+String.valueOf(i+1), null));
          }
          editor.remove("dossierbookmarked"+String.valueOf(nombredossierbookmarked-1));
          editor.remove("fichierbookmarked"+String.valueOf(nombredossierbookmarked-1));
          editor.putInt("nombredossierbookmarked",nombredossierbookmarked-1);
          editor.apply();
          llog.d(TAG, "deletebookmark (" + bookmarkthisfolder + ")");
          try {
            model.commandethreadbrowser.put(new String[]{"-1", "update"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          encore = false;



        } else if (commande[0].equals("addservicedisabled")) {
          SharedPreferences.Editor editor = model.preferences.edit();
          editor.putString("servicedisabled" + commande[1], commande[1]);
          editor.apply();
          llog.d(TAG, "addservicedisabled (" + commande[1]);
          try {
            model.commandethreadbrowser.put(new String[]{"-1", "update"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          encore = false;

        } else if (commande[0].equals("deleteservicedisabled")) {
          int bookmarkthisfolder = Integer.parseInt(commande[1]);
          if (model.preferences.contains("servicedisabled" + commande[1])) {
            SharedPreferences.Editor editor = model.preferences.edit();
            editor.remove("servicedisabled" + commande[1]);
            editor.apply();
          }
          llog.d(TAG, "deletebookmark (" + bookmarkthisfolder + ")");
          try {
            model.commandethreadbrowser.put(new String[]{"-1", "update"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          encore = false;


































        } else if (commande[0].equals("nouveaufichierpardefaut")) {

          int cedossier = Integer.parseInt(commande[1]);
          int cefichier = Integer.parseInt(commande[2]);
          if(cedossier >= model.folderCount){
            llog.d(TAG, "ERREUR nouveaufichierpardefaut < model.getFolderCount() on rescanne tout le disque");
            continue;
          }
          if(cefichier >= model.getFolderFileCount(cedossier)){
            llog.d(TAG, "ERREUR nouveaufichierpardefaut < model.mtouslesfichiersl on rescanne tout le disque");
            continue;
          }
          String dossiername = model.getOrnderAddress(cedossier);
          if (dossiername != null) {
            model.setFolderDefaultFile(cedossier, cefichier);
            model.db.dbDossier().updateFichierParDefaut(dossiername, cefichier);
          }

          

        } else if (commande[0].equals("copyfiles")) {
          if (model.uploadthreadrunning) {
            model.message("another task is still runnning");
          } else {
            final String destinationOrdnerAddress;
            if (!commande[1].endsWith("/"))
              destinationOrdnerAddress = commande[1] + "/";
            else
              destinationOrdnerAddress = commande[1];
            final boolean move;
            final String renameprefix;
            final String dstfold2;
            final String dstname;
            final int isonline;
            final int addresstogetlibextractorthumb;
            if (commande[2].equals("copyonly")) {
              move = false;
              renameprefix = null;
            } else if (commande[2].equals("remove")) {
              move = true;
              renameprefix = null;
            } else {
              move = true;
              renameprefix = commande[2];
            }
            dstfold2 = commande[3];
            dstname = commande[4];
            isonline = Integer.parseInt(commande[5]);
            addresstogetlibextractorthumb = Integer.parseInt(commande[6]);
            new Thread(new Runnable() {
              @Override
              public void run() {
                Gallery.isConnectedToInternetRetry = 0;
                model.uploadthreadrunning = true;
                model.uploadthreadstatus = "copying...";
                model.uploadthreadsuccess = 0;
                model.uploadthreadbailalreadypresentcount = 0;
                model.uploadthreadfail = 0;
                model.uploadthreadtotal = 0;
                model.uploadthreadbytes = 0;
                model.uploadthreadtime = System.currentTimeMillis();
                model.uploadthreadbytesin = 0;
                model.uploadthreadtimein = System.currentTimeMillis();
                int renameprefixindex = 0;
                boolean firstcheckfolderexists = true;
                String destinationMediaAddress = destinationOrdnerAddress + "bulle.gif";
                boolean destinationonline = false;
                if (Gallery.couldBeOnline(destinationOrdnerAddress))
                  destinationonline = true;
                int folderfoundingalleryatpos = -1;
                if (!destinationonline)
                  folderfoundingalleryatpos = model.findFolder(destinationOrdnerAddress);
                model.deletefiles.clear();
                ArrayList<Ordner> tempgallery;
                if (model.currCollectionAddress == null) {
                  tempgallery = new ArrayList<>();
                  Ordner ordner = new Ordner();
                  ordner.mediaList = new ArrayList<>();
                  Media media = new Media();
                  media.bookmarkToOrdner = dstfold2;
                  media.address = dstname;
                  media.isOnline = isonline;
                  media.addressToGetLibextractorsThumbnail = addresstogetlibextractorthumb;
                  ordner.mediaList.add(media);
                  ordner.mediaListCount = 1;
                  tempgallery.add(ordner);
                } else {
                  tempgallery = model.mergeCollections(model.currCollectionAddress);
                }
                if (tempgallery == null) {
                  llog.d(TAG, "collection <" + model.currCollectionAddress + "> not merged, null");
                } else {
                  int tempgalleryl = tempgallery.size();
                  Ordner ordner;
                  String renameprefixn = null;
                  String renameprefixext = null;
                  int totalprecount = 0;
                  if (renameprefix != null) {
                    for (int i = 0; i < tempgalleryl; i++) {
                      ordner = tempgallery.get(i);
                      if (ordner.mediaList == null) {
                      } else if (ordner.mediaList.size() > 0) {
                        totalprecount += ordner.mediaList.size();
                      }
                    }
                    if (totalprecount > 1) {
                      int rl = renameprefix.length();
                      int lastdot = renameprefix.lastIndexOf('.');
                      if (lastdot >= 0 && lastdot < rl - 1) {
                        renameprefixn = renameprefix.substring(0, lastdot);
                        renameprefixext = renameprefix.substring(lastdot);
                      }
                    }
                  }
                  for (int i = 0; i < tempgalleryl; i++) {
                    ordner = tempgallery.get(i);
                    if (ordner.mediaList == null) {
                    } else if (ordner.mediaList.size() > 0) {
                      int mediaListCount = ordner.mediaList.size();
                      model.uploadthreadtotal += mediaListCount;
                      for (int j = 0; j < mediaListCount; j++) {
                        InternetSession.displayprogress(model);
                        Media media = ordner.mediaList.get(j);
                        String dstfold = media.bookmarkToOrdner; // achtung ordneraddress a été changé dans la collection
                        String dstname = media.address;
                        File sourceFile = null;
                        String destinationFileName = null;
                        String destinationFileNameEncoded = null;
                        String renamename = renameprefix;
                        if (renameprefixn != null && renameprefixext != null) {
                          renameprefixindex += 1;
                          if (totalprecount < 100000)
                            renamename = String.format("%s%05d%s", renameprefixn, renameprefixindex, renameprefixext);
                          else
                            renamename = String.format("%s%08d%s", renameprefixn, renameprefixindex, renameprefixext);
                        }
                        if (media.isOnline == Media.online_no) {
                          sourceFile = new File(dstname);
                          if (!sourceFile.exists()) {
                            sourceFile = null;
                          } else {
                            if (renameprefix == null)
                              destinationFileName = sourceFile.getName();
                            else
                              destinationFileName = renamename;
                            destinationFileNameEncoded = Gallery.urlEncode(destinationFileName);
                          }
                        } else {
                          if (renameprefix == null) {
                            destinationFileNameEncoded = dstname.replaceAll("^.*/", "");
                            destinationFileName = destinationFileNameEncoded;
                            try {
                              destinationFileName = URLDecoder.decode(destinationFileNameEncoded, "utf-8");
                            } catch (UnsupportedEncodingException e) {
                              e.printStackTrace();
                            }
                          } else {
                            destinationFileName = renamename;
                            destinationFileNameEncoded = Gallery.urlEncode(destinationFileName);
                          }
                        }
                        if (!destinationonline) {
                          destinationMediaAddress = destinationOrdnerAddress + destinationFileName;
                        } else {
                          destinationMediaAddress = destinationOrdnerAddress + destinationFileNameEncoded;
                        }
                        if (dstname.equals(destinationMediaAddress)) {
                          llog.d(TAG, "bail out do not overwrite same file " + dstname + " = " + destinationMediaAddress);
                        } else if (sourceFile == null && media.isOnline == Media.online_no) {
                          llog.d(TAG, "file does not exist " + dstname);
                        } else {
                          if (!destinationonline) {
                            if (model.usesaf) {
                              model.deletefiles.add(dstname);
                            }
                            File folder = new File(destinationOrdnerAddress);
                            if (!folder.exists())
                              folder.mkdirs();
                            File destinationFile = new File(destinationMediaAddress);
                            boolean alreadyexists = false;
                            if (destinationFile.exists()) {
                              alreadyexists = true;
                              llog.d(TAG, "there is already a file named\n" + destinationMediaAddress);
                            }
                            boolean success = false;
                            if (media.isOnline == Media.online_no) {
                              try {
                                success = copyFile(sourceFile, destinationFile);
                              } catch (IOException e) {
                                llog.d(TAG, "ERREUR exception " + e.toString());
                              }
                              if (success)
                                model.uploadthreadsuccess += 1;
                            } else {
                              success = InternetSession.getApacheSaveToFile(model, dstname, destinationFile, model.copyoverwritenosync);
                              if (model.uploadthreadbailalreadypresent)
                                model.uploadthreadbailalreadypresentcount += 1;
                              else
                                model.uploadthreadsuccess += 1;
                            }
                            if (success) {
                              // on ajoute ici à la gallerie, récupérer son UID d'abord
                              if (firstcheckfolderexists) {
                                firstcheckfolderexists = false;
                                if (folderfoundingalleryatpos == -1) {
                                  Dossier dd = new Dossier();
                                  dd.dossier = destinationOrdnerAddress;
                                  model.db.dbDossier().insert(dd);
                                  model.addFolder(-1, dd.dossier,  false, 0, null, Media.online_no);
                                }
                              }
                              if (!alreadyexists) {
                                Fichier fichier = new Fichier();
                                fichier.fichier = destinationMediaAddress;
                                fichier.dossier = destinationOrdnerAddress;
                                model.db.dbFichier().insert(fichier);
                              }
                              int uid = model.db.dbFichier().fichierGetUID(destinationMediaAddress);
                              //llog.d(TAG, "inserted file " + uid + " : " + destinationMediaAddress);
                              if (uid >= 1) {
                                success = false;
                                String destinationMiniatureAddress = model.dossierminiature + uid;
                                if (media.isOnline == Media.online_no) {
                                  if (media.addressToGetLibextractorsThumbnail != -1) {
                                    String sourceMiniatureAddress = model.dossierminiature + media.addressToGetLibextractorsThumbnail;
                                    File sourceMiniFile;
                                    int thmbl = 1;
                                    int l;
                                    for (l = 0; l < 1000; l++) {
                                      sourceMiniFile = new File(sourceMiniatureAddress + "_" + l);
                                      if (!sourceMiniFile.exists())
                                        break;
                                      destinationFile = new File(destinationMiniatureAddress + "_" + l);
                                      try {
                                        success = copyFile(sourceMiniFile, destinationFile);
                                      } catch (IOException e) {
                                        llog.d(TAG, "ERREUR exception " + e.toString());
                                      }
                                      if (!success)
                                        break;
                                      if (move)
                                        sourceMiniFile.delete();
                                      sourceMiniFile = new File(sourceMiniatureAddress + "." + l);
                                      if (!sourceMiniFile.exists())
                                        break;
                                      destinationFile = new File(destinationMiniatureAddress + "." + l);
                                      try {
                                        success = copyFile(sourceMiniFile, destinationFile);
                                      } catch (IOException e) {
                                        llog.d(TAG, "ERREUR exception " + e.toString());
                                      }
                                      if (!success)
                                        break;
                                      if (move)
                                        sourceMiniFile.delete();
                                      thmbl += 1;
                                    }
                                    llog.d(TAG, thmbl + " copied " + sourceMiniatureAddress + "_" + l + " to " + destinationMiniatureAddress+ "_" + l);
                                    if (move) {
                                      boolean deleted = sourceFile.delete();
                                      if (deleted) {
                                        removeMediaFromDatabase(dstfold, dstname);
                                        llog.d(TAG, "moved " + dstfold + " " + dstname + " to " + destinationMediaAddress);
                                      } else {
                                        llog.d(TAG, "copied " + dstfold + " " + dstname + " to " + destinationMediaAddress);
                                      }
                                      removeMediaFromDatabase(dstfold, dstname);
                                    }
                                  } else {
                                    llog.d(TAG, "failed to copy thumbs no addressToGetLibextractorsThumbnail " + dstname);
                                  }
                                } else {
                                  int thmbl = 1;
                                  int l;
                                  for (l = 0; l < 1000; l++) {
                                    destinationFile = new File(destinationMiniatureAddress + "_" + l);
                                    success = InternetSession.getApacheSaveToFile(model, dstname + ".thmb_" + l, destinationFile, model.copyoverwritenosync);
                                    if (!success)
                                      break;
                                    if (move) {
                                      success = InternetSession.deleteApache(model, dstname + ".thmb_" + l);
                                      if (!success)
                                        llog.d(TAG, "failed to delete " + dstname + ".thmb_" + l);
                                    }
                                    destinationFile = new File(destinationMiniatureAddress + "." + l);
                                    success = InternetSession.getApacheSaveToFile(model, dstname + ".thmb." + l, destinationFile, model.copyoverwritenosync);
                                    if (!success)
                                      break;
                                    if (move) {
                                      success = InternetSession.deleteApache(model, dstname + ".thmb." + l);
                                      if (!success)
                                        llog.d(TAG, "failed to delete " + dstname + ".thmb." + l);
                                    }
                                    thmbl += 1;
                                  }
                                  llog.d(TAG, thmbl + " copied " + dstname + ".thmb_" + l + " to " + destinationMiniatureAddress+ "_" + l);
                                  if (move) {
                                    success = InternetSession.deleteApache(model, dstname);
                                    if (success) {
                                      llog.d(TAG, "moved " + dstname + " to " + destinationMediaAddress);
                                    } else {
                                      llog.d(TAG, "copied " + dstname + " to " + destinationMediaAddress);
                                    }
                                  }
                                }
                              }
                            } else {
                              model.uploadthreadfail += 1;
                              llog.d(TAG, "failed to copy " + dstname + " to " + destinationMediaAddress);
                            }
                          } else {

                            String additionalMetadata = null;
                            /*String additionalMetadata = "";
                            if (media.printName != null)
                              additionalMetadata += "printName=" + media.printName + "\n";
                            if (media.printDetails != null)
                              additionalMetadata += "printDetails=" + media.printDetails + "\n";
                            if (media.printFooter != null)
                              additionalMetadata += "printFooter=" + media.printFooter + "\n";
                            if (media.playInSequence)
                              additionalMetadata += "playInSequence=true\n";
                            if (media.playStartAtPosition != 0)
                              additionalMetadata += "playStartAtPosition=" + media.playStartAtPosition + "\n";
                            if (media.subtitleAddress != null) {
                              int subtitleAddressl = media.subtitleAddress.size();
                              for (int p = 0 ; p < subtitleAddressl ; p++)
                                additionalMetadata += "subtitleAddress=" + media.subtitleAddress.get(p) + "\n";
                            }
                            if (additionalMetadata.length() == 0)
                              additionalMetadata = null;*/

                            // destinationonline
                            boolean success;
                            if (media.isOnline == Media.online_no) {
                              success = InternetSession.putApache(model, sourceFile, destinationMediaAddress, model.copyoverwritenosync, null);
                            } else {
                              success = InternetSession.putApacheFromApache(model, dstname, destinationMediaAddress, model.copyoverwritenosync, null);
                            }
                            if (success) {
                              if (model.uploadthreadbailalreadypresent)
                                model.uploadthreadbailalreadypresentcount += 1;
                              else
                                model.uploadthreadsuccess += 1;
                              if (media.isOnline == Media.online_no) {
                                if (media.addressToGetLibextractorsThumbnail != -1) {
                                  String cefichier = model.dossierminiature + media.addressToGetLibextractorsThumbnail;
                                  File sourceMiniFile;
                                  int thmbl = 1;
                                  int l;
                                  for (l = 0; l < 1000; l++) {
                                    sourceMiniFile = new File(cefichier + "_" + l);
                                    if (!sourceMiniFile.exists())
                                      break;
                                    if (l == 0)
                                      success = InternetSession.putApache(model, sourceMiniFile, destinationMediaAddress + ".thmb_" + l, model.copyoverwritenosync, additionalMetadata);
                                    else
                                      success = InternetSession.putApache(model, sourceMiniFile, destinationMediaAddress + ".thmb_" + l, model.copyoverwritenosync, null);
                                    if (!success)
                                      break;
                                    if (move)
                                      sourceMiniFile.delete();
                                    sourceMiniFile = new File(cefichier + "." + l);
                                    if (!sourceMiniFile.exists())
                                      break;
                                    success = InternetSession.putApache(model, sourceMiniFile, destinationMediaAddress + ".thmb." + l, model.copyoverwritenosync, null);
                                    if (!success)
                                      break;
                                    if (move)
                                      sourceMiniFile.delete();
                                    thmbl += 1;
                                  }
                                  llog.d(TAG, thmbl + " copied " + cefichier + "." + l + " to " + destinationMediaAddress + ".thmb." + l);
                                  if (move) {
                                    boolean deleted = sourceFile.delete();
                                    if (deleted) {
                                      removeMediaFromDatabase(dstfold, dstname);
                                      llog.d(TAG, "moved " + dstfold + " " + dstname + " to " + destinationMediaAddress);
                                    } else {
                                      llog.d(TAG, "copied " + dstfold + " " + dstname + " to " + destinationMediaAddress);
                                    }
                                    removeMediaFromDatabase(dstfold, dstname);
                                  }
                                } else {
                                  llog.d(TAG, "failed to copy thumbs no addressToGetLibextractorsThumbnail " + dstname);
                                }
                              } else {
                                int thmbl = 1;
                                int l;
                                for (l = 0; l < 1000; l++) {
                                  if (l == 0)
                                    success = InternetSession.putApacheFromApache(model, dstname + ".thmb_" + l, destinationMediaAddress + ".thmb_" + l, model.copyoverwritenosync, additionalMetadata);
                                  else
                                    success = InternetSession.putApacheFromApache(model, dstname + ".thmb_" + l, destinationMediaAddress + ".thmb_" + l, model.copyoverwritenosync, null);
                                  if (!success)
                                    break;
                                  if (move) {
                                    success = InternetSession.deleteApache(model, dstname + ".thmb_" + l);
                                    if (!success)
                                      llog.d(TAG, "failed to delete " + dstname + ".thmb_" + l);
                                  }
                                  success = InternetSession.putApacheFromApache(model, dstname + ".thmb." + l, destinationMediaAddress + ".thmb." + l, model.copyoverwritenosync, null);
                                  if (!success)
                                    break;
                                  if (move) {
                                    success = InternetSession.deleteApache(model, dstname + ".thmb." + l);
                                    if (!success)
                                      llog.d(TAG, "failed to delete " + dstname + ".thmb." + l);
                                  }
                                  thmbl += 1;
                                }
                                llog.d(TAG, thmbl + " copied " + dstname + ".thmb" + " to " + destinationMediaAddress + ".thmb." + l);
                                if (move) {
                                  success = InternetSession.deleteApache(model, dstname);
                                  if (success) {
                                    llog.d(TAG, "moved " + dstname + " to " + destinationMediaAddress);
                                  } else {
                                    llog.d(TAG, "copied " + dstname + " to " + destinationMediaAddress);
                                  }
                                }
                              }
                            } else {
                              model.uploadthreadfail += 1;
                              llog.d(TAG, "failed to copy " + dstname + " to " + destinationMediaAddress);
                            }
                          }
                        }
                      }
                    }
                  }
                  tempgallery.clear();
                }
                model.uploadthreadstatus = null;
                if (model.deletefiles.size() > 0) {
                  Intent intent = new Intent();
                  intent.setAction(Gallery.broadcastname);
                  intent.putExtra("goal", "movefiles");
                  //if (mysurf.basisfolderclicked != null) intent.putExtra("destination", mysurf.basisfolderclicked);
                  LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
                  model.uploadthreadrunning = false;
                } else {
                  if (destinationonline) {
                    int idossier = model.findFolder(destinationOrdnerAddress);
                    if (idossier == -1) {
                      llog.d(TAG, "deletefilefromdbmini folder isn't loaded don't force reload " + destinationOrdnerAddress);
                    } else {
                      llog.d(TAG, "deletefilefromdbmini force reload folder " + destinationOrdnerAddress);
                      model.setFilesOrSetNotLoadedYet(idossier, null);
                    }
                  } else {
                    try {
                      model.commandethreaddatabase.put(new String[]{"chercheFichierDansDatabaseSinonRescanSonDossier", destinationOrdnerAddress, destinationMediaAddress, "-1"});
                    } catch (InterruptedException e) {
                      e.printStackTrace();
                    }
                  }
                  model.uploadthreadrunning = false;
                  if (move) {
                    String msg = String.format("%5d/%5d files have been moved", model.uploadthreadsuccess, model.uploadthreadtotal);
                    if (model.uploadthreadbailalreadypresentcount > 0)
                      msg += String.format("\n%5d/%5d were already present", model.uploadthreadbailalreadypresentcount, model.uploadthreadtotal);
                    if (model.uploadthreadfail > 0)
                      msg += String.format("\n%5d/%5d failed", model.uploadthreadfail, model.uploadthreadtotal);
                    else
                      msg += "\nsuccessful transfer";
                    llog.d(TAG, msg);
                    model.message(msg);
                  } else {
                    String msg = String.format("%5d/%5d files have been copied", model.uploadthreadsuccess, model.uploadthreadtotal);
                    if (model.uploadthreadbailalreadypresentcount > 0)
                      msg += String.format("\n%5d/%5d were already present", model.uploadthreadbailalreadypresentcount, model.uploadthreadtotal);
                    if (model.uploadthreadfail > 0)
                      msg += String.format("\n%5d/%5d failed", model.uploadthreadfail, model.uploadthreadtotal);
                    else
                      msg += "\nsuccessful transfer";
                    llog.d(TAG, msg);
                    model.message(msg);
                  }
                }
              }
            }).start();
          }


        } else if (commande[0].equals("sharefiles")) {
          model.deletefiles.clear();
          if (model.currCollectionAddress == null) {
            String fdname = commande[1];
            model.deletefiles.add(fdname);
          } else {
            ArrayList<Ordner> tempgallery = model.mergeCollections(model.currCollectionAddress);
            if (tempgallery == null) {
              llog.d(TAG, model.currCollectionAddress + " null");
            } else {
              int tempgalleryl = tempgallery.size();
              Ordner ordner;
              for (int i = 0; i < tempgalleryl; i++) {
                ordner = tempgallery.get(i);
                if (ordner.mediaList == null) {
                } else if (ordner.mediaList.size() > 0) {
                  int mediaListCount = ordner.mediaList.size();
                  for (int j = 0; j < mediaListCount; j++) {
                    Media media = ordner.mediaList.get(j);
                    String fdname = media.address;
                    model.deletefiles.add(fdname);
                  }
                }
              }
            }
          }
          if (model.deletefiles.size() > 0) {
            Intent intent = new Intent();
            intent.setAction(Gallery.broadcastname);
            intent.putExtra("goal", "sharefiles");
            LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
          } else {
            llog.d(TAG, "error nothing to share");
          }

        } else if (commande[0].equals("deletefiles")) {
          Gallery.isConnectedToInternetRetry = 0;
          int dummydeletedcounter = 0;
          int totalcounter = 0;
          boolean targetonline = false;
          model.databasecurrentlyremovingfiles = true;
          int maxtestagain = 3;
          int testagain = 0;
          // on élimine les /Android/data/la.daube d'abord
          model.deletefiles.clear();
          ArrayList<Ordner> tempgallery;
          if (model.currCollectionAddress == null) {
            String dstfold = commande[1];
            String dstname = commande[2];
            tempgallery = new ArrayList<>();
            Ordner ordner = new Ordner();
            ordner.mediaList = new ArrayList<>();
            Media media = new Media();
            media.bookmarkToOrdner = dstfold;
            media.address = dstname;
            ordner.mediaList.add(media);
            ordner.mediaListCount = 1;
            tempgallery.add(ordner);
          } else {
            tempgallery = model.mergeCollections(model.currCollectionAddress);
          }
          if (tempgallery == null) {
            llog.d(TAG, "collection <" + model.currCollectionAddress + "> not merged, null");
          } else {
            int tempgalleryl = tempgallery.size();
            Ordner ordner;
            for (int i = 0; i < tempgalleryl; i++) {
              ordner = tempgallery.get(i);
              if (ordner.mediaList == null) {
              } else if (ordner.mediaList.size() > 0) {
                int mediaListCount = ordner.mediaList.size();
                totalcounter += mediaListCount;
                for (int j = 0; j < mediaListCount; j++) {
                  Media media = ordner.mediaList.get(j);
                  String dstfold = media.bookmarkToOrdner; // achtung ordneraddress a été changé dans la collection
                  String dstname = media.address;
                  if (Gallery.couldBeOnline(dstname))
                    targetonline = true;
                  if (!targetonline) {
                    if (model.usesaf) { // always try to delete
                      model.deletefiles.add(dstname);
                    }
                    File file = new File(dstname);
                    if (!file.exists()) {
                      llog.d(TAG, "file does not exist " + dstname);
                    } else {
                      String filename = file.getName();
                      String filepath = file.getAbsolutePath();
                      boolean deleted = file.delete();
                      if (deleted) {
                        dummydeletedcounter += 1;
                        removeMediaFromDatabase(dstfold, dstname);
                        llog.d(TAG, "deleted\n" + dstname + "\n" + dummydeletedcounter + "/" + totalcounter);
                        model.message("deleted\n" + dstname + "\n" + dummydeletedcounter + "/" + totalcounter);
                      } else {
                        llog.d(TAG, "failed to delete\n" + dstname + "\n" + dummydeletedcounter + "/" + totalcounter);
                        model.message("failed to delete\n" + dstname + "\n" + dummydeletedcounter + "/" + totalcounter);
                      }
                    }
                  } else {
                    boolean success = InternetSession.deleteApache(model, dstname);
                    if (success) {
                      removeMediaFromDatabase(dstfold, dstname);
                      int thmbl = 0;
                      for (int l = 0 ; l < 1000 ; l++) {
                        success = InternetSession.deleteApache(model, dstname + ".thmb_" + l);
                        if (!success)
                          break;
                        success = InternetSession.deleteApache(model, dstname + ".thmb." + l);
                        if (!success)
                          break;
                        thmbl += 1;
                      }
                      dummydeletedcounter += 1;
                      if (thmbl > 0) {
                        llog.d(TAG, "deleted " + thmbl + "\n" + dstname + "\n" + dummydeletedcounter + "/" + totalcounter);
                        model.message("deleted " + thmbl + "\n" + dstname + "\n" + dummydeletedcounter + "/" + totalcounter);
                      } else {
                        llog.d(TAG, "deleted\n" + dstname + "\n" + dummydeletedcounter + "/" + totalcounter);
                        model.message("deleted\n" + dstname + "\n" + dummydeletedcounter + "/" + totalcounter);
                      }
                    } else {
                      llog.d(TAG, "failed to delete\n" + dstname + "\n" + dummydeletedcounter + "/" + totalcounter);
                      model.message("failed to delete\n" + dstname + "\n" + dummydeletedcounter + "/" + totalcounter);
                    }
                  }
                }
              }
            }
          }
          /*int filesdeletedfromfolderl = filesdeletedfromfolder.size();
          for (int f = 0 ; f < filesdeletedfromfolderl ; f++) {
            String dossier = filesdeletedfromfolder.get(f);
            int idossier = model.findFolder(dossier);
            if (idossier == -1) {
              llog.d(TAG, "folder not found " + dossier);
            } else {
              model.setFolderNotLoadedYet(idossier, true);
              rechargetoutledossier(idossier, false);
            }
          }*/
          model.redrawsplitviewbitmapm = true;
          model.databasecurrentlyremovingfiles = false;
          if (model.deletefiles.size() > 0) {
            Intent intent = new Intent();
            intent.setAction(Gallery.broadcastname);
            intent.putExtra("goal", "deletefiles");
            LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
          } else {
            llog.d(TAG, dummydeletedcounter + "/" + totalcounter + " files have been deleted");
            model.message( dummydeletedcounter + "/" + totalcounter + " files have been deleted");
            try {
              model.commandethreadbrowser.put(new String[]{"-1", "update"});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          }












        } else if (commande[0].equals("deletefilefromdatabase")) {
          llog.d(TAG, "deletefilefromdatabase " + commande[1] + " " + commande[2]);
          removeMediaFromDatabase(commande[1], commande[2]);
          try {
            model.commandethreadbrowser.put(new String[]{"-1", "update"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }











        } else if (commande[0].equals("clearselection")) {
          llog.d(TAG, "clearselection " + commande[1]); // wa already hid it
          model.clearAllSelected();
          Collection collection = model.getCollection(commande[1]);
          if (collection == null) {
            llog.d (TAG, "collection " + commande[1] + " null");
          } else  {
            collection.emptyCollectionRemoveAllMedia();
            collection.deleteDataStoredOnDisc();
            // clearselection but keep this collection active (keep name, address)
            model.currCollectionSelectedCount = 0;
            model.currCollectionAddToFolderi = 0;
            model.setCurrCollectionOrdnerList(null);
            try {
              model.commandethreadbrowser.put(new String[]{"-1", "update"});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          }

        } else if (commande[0].equals("closecollection")) {
          llog.d(TAG, "closecollection " + commande[1] + " currcoll " + model.currCollectionAddress);
          model.clearAllSelected();
          if (model.currCollectionAddress != null) {
            if (model.currCollectionAddress.equals(commande[1])) {
              model.currCollectionAddress = null;
              model.currCollectionPrintName = null;
              model.currCollectionSelectedCount = 0;
              model.currCollectionAddToFolderi = 0;
              model.setCurrCollectionOrdnerList(null);
            }
          }
          Collection collection = model.getCollection(commande[1]);
          if (collection == null) { llog.d (TAG, "collection " + commande[1] + " null");
          } else  {
            collection.setNull();
          }
          try {
            model.commandethreadbrowser.put(new String[]{"-1", "update"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }

        } else if (commande[0].equals("DeleteCollectionFromDisk")) {
          llog.d(TAG, "DeleteCollectionFromDisk " + commande[1]);
          model.clearAllSelected();
          if (model.currCollectionAddress != null) {
            if (model.currCollectionAddress.equals(commande[1])) {
              model.currCollectionAddress = null;
              model.currCollectionPrintName = null;
              model.currCollectionSelectedCount = 0;
              model.currCollectionAddToFolderi = 0;
              model.setCurrCollectionOrdnerList(null);
            }
          }
          Collection collection = model.getCollection(commande[1]);
          if (collection != null) {
            collection.setNull();
            collection.deleteDataStoredOnDisc();
          } else {
            Collection.deleteCollection(commande[1]);
          }
          try {
            model.commandethreaddatabase.put(new String[]{"rescancollectionsondisk"});
            model.commandethreadbrowser.put(new String[]{"-1", "update"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }

        } else if (commande[0].equals("LoadCollectionFromDiskOrTemp")) {
          llog.d(TAG, "LoadCollectionFromDiskOrTemp " + commande[1]);
          String action = commande[1]; // create or load
          String colladdress = commande[2]; // create or load
          Boolean setascurrentactive = Boolean.parseBoolean(commande[3]); // create or load
          int collid = -1;
          String address;
          String printName;
          int collectionsl = model.collections.size();
          if (action.equals("create")) {
            address = model.dossiercollections + colladdress + Gallery.collectionextension;
            printName = colladdress;
            collid = collectionsl;
            model.collections.add(new Collection(address, printName, model));
            model.addAvailCollectionsOnDisk(address);
          } else if (colladdress.equals(Gallery.virtualBookmarkFolder)) {
            address = Gallery.virtualBookmarkFolder;
            printName = "bookmark";
            int nullplaceholder = -1;
            for (int i = 0 ; i < collectionsl ; i++) {
              if (model.collections.get(i).address == null) {
                if (nullplaceholder == -1) {
                  nullplaceholder = i;
                }
              } else if (model.collections.get(i).address.equals(address)) {
                collid = i;
                break;
              }
            }
            if (collid == -1) {
              if (nullplaceholder != -1) {
                collid = nullplaceholder;
                model.collections.set(nullplaceholder, new Collection(address, printName, model));
              } else {
                collid = collectionsl;
                model.collections.add(new Collection(address, printName, model));
              }
            } else {
              //model.clearAllSelected();
              model.collections.get(collid).setNull();
              model.collections.get(collid).address = address;
              model.collections.get(collid).printName = printName;
            }
            model.collections.get(collid).reloadBookmarksFromPreferences(model.preferences, model.db);
          } else if (colladdress.equals(Gallery.virtualAppFolder)) {
            address = Gallery.virtualAppFolder;
            printName = "apps";
            int nullplaceholder = -1;
            for (int i = 0 ; i < collectionsl ; i++) {
              if (model.collections.get(i).address == null) {
                if (nullplaceholder == -1) {
                  nullplaceholder = i;
                }
              } else if (model.collections.get(i).address.equals(address)) {
                collid = i;
                break;
              }
            }
            if (collid == -1) {
              if (nullplaceholder != -1) {
                collid = nullplaceholder;
                model.collections.set(nullplaceholder, new Collection(address, printName, model));
              } else {
                collid = collectionsl;
                model.collections.add(new Collection(address, printName, model));
              }
            } else {
              //model.clearAllSelected();
              model.collections.get(collid).setNull();
              model.collections.get(collid).address = address;
              model.collections.get(collid).printName = printName;
            }
            model.collections.get(collid).reloadApps(model);
          } else if (colladdress.equals(Gallery.virtualWidgetFolder)) {
            address = Gallery.virtualWidgetFolder;
            printName = "widgets";
            int nullplaceholder = -1;
            for (int i = 0 ; i < collectionsl ; i++) {
              if (model.collections.get(i).address == null) {
                if (nullplaceholder == -1) {
                  nullplaceholder = i;
                }
              } else if (model.collections.get(i).address.equals(address)) {
                collid = i;
                break;
              }
            }
            if (collid == -1) {
              if (nullplaceholder != -1) {
                collid = nullplaceholder;
                model.collections.set(nullplaceholder, new Collection(address, printName, model));
              } else {
                collid = collectionsl;
                model.collections.add(new Collection(address, printName, model));
              }
            } else {
              //model.clearAllSelected();
              model.collections.get(collid).setNull();
              model.collections.get(collid).address = address;
              model.collections.get(collid).printName = printName;
            }
            model.collections.get(collid).reloadWidgets(model.preferences, model);
          } else if (colladdress.equals("temp")) {
            address = "temp";
            printName = "temp";
            int nullplaceholder = -1;
            for (int i = 0 ; i < collectionsl ; i++) {
              if (model.collections.get(i).address == null) {
                if (nullplaceholder == -1) {
                  nullplaceholder = i;
                }
              } else if (model.collections.get(i).address.equals(address)) {
                collid = i;
                break;
              }
            }
            if (collid == -1) {
              if (nullplaceholder != -1) {
                collid = nullplaceholder;
                model.collections.set(nullplaceholder, new Collection(address, printName, model));
              } else {
                collid = collectionsl;
                model.collections.add(new Collection(address, printName, model));
              }
            } else {
              //model.clearAllSelected();
              model.collections.get(collid).setNull();
              model.collections.get(collid).address = address;
              model.collections.get(collid).printName = printName;
            }
          } else {
            address = colladdress;
            printName = Gallery.colladdresstoname(address);
            if (printName == null) {
              printName = "error";
              llog.d(TAG, "error no printname for " + address);
            }
            int nullplaceholder = -1;
            for (int i = 0 ; i < collectionsl ; i++) {
              if (model.collections.get(i).address == null) {
                if (nullplaceholder == -1) {
                  nullplaceholder = i;
                }
              } else if (model.collections.get(i).address.equals(address)) {
                collid = i;
                break;
              }
            }
            if (collid == -1) {
              if (nullplaceholder != -1) {
                collid = nullplaceholder;
                model.collections.set(nullplaceholder, new Collection(address, printName, model));
              } else {
                collid = collectionsl;
                model.collections.add(new Collection(address, printName, model));
              }
            }
          }
          if (setascurrentactive) {
            model.currCollectionAddress = address;
            model.currCollectionPrintName = printName;
            model.currCollectionSelectedCount = model.collections.get(collid).selectedCount;
            model.currCollectionAddToFolderi = 0;
            //llog.d(TAG, "setCurrCollectionOrdnerList LoadCollectionFromDiskOrTemp " + printName);
            model.setCurrCollectionOrdnerList(model.collections.get(collid).displayedOrdnerList);
            if (model.currCollectionGallery != null) {
              model.currCollectionGallery.clear();
            }
            model.clearAllSelected();
            model.currCollectionGallery = model.mergeCollections(address);
          }
          try {
            model.commandethreadbrowser.put(new String[]{"-1", "update"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }

        } else if (commande[0].equals("rescancollectionsondisk")) {
          llog.d(TAG, "rescancollectionsondisk");
          ArrayList<String> collondisk = findfiles(model.dossiercollections, Gallery.collectionextension);
          model.setAvailCollectionsOnDisk(collondisk);
          try {
            model.commandethreadbrowser.put(new String[]{"-1", "update"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }

        } else if (commande[0].equals("addcollectionfolderstodisplay")) {
          llog.d(TAG, "addcollectionfolderstodisplay " + commande[1] + " " + commande[2]);
          int currid = Integer.parseInt(commande[1]);
          Collection collection = model.getCollection(commande[2]);
          if (collection != null) {
            int posadd = model.folderCount;
            if (commande.length > 3)
              posadd = Integer.parseInt(commande[3]);
            model.collectionsToDisplay(commande[2], posadd);
            model.changeBigPicture(currid, posadd, 0, -1, 0, true, false);
            try {
              model.commandethreadbrowser.put(new String[]{"-1", "update"});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          }

        } else if (commande[0].equals("removedisplayedcollectionfolders")) {
          llog.d(TAG, "removedisplayedcollectionfolders " + commande[1]);
          Collection collection = model.getCollection(commande[1]);
          if (collection != null) {
            if (collection.address != null) {
              if (!collection.address.equals("temp")) {
                List<String> displayerdOrdnerList = collection.displayedOrdnerList;
                for (String folder : displayerdOrdnerList) {
                  llog.d(TAG, " - ornder " + folder);
                  int found = model.findFolder(folder);
                  if (found != -1) {
                    model.removeOrdnerFromGallery(folder, found);
                  }
                }
                try {
                  model.commandethreadbrowser.put(new String[]{"-1", "update"});
                } catch (InterruptedException e) {
                  e.printStackTrace();
                }
              }
            }
          }

        } else if (commande[0].equals("isbitmapselectedcheck")) {
          if (model.currCollectionAddress != null) {
            int idossier = Integer.parseInt(commande[1]);
            int ifichier = Integer.parseInt(commande[2]);
            String dossier = commande[3];
            String fichier = commande[4];
            boolean isselected = false;
            Collection collection = model.getCollection(model.currCollectionAddress);
            if (collection != null) {
              int selfichl = collection.elementList.size();
              for (int i = 0; i < selfichl; i++) {
                Element entry = collection.elementList.get(i);
                if (entry.type == Element.element_ordner) {
                  if (dossier.equals(entry.ordner.address)) {
                    isselected = entry.add;
                    //llog.d(TAG, "getFileIsBitmapSelected folder file is selected " + isselected);
                  }
                } else if (entry.type == Element.element_media) {
                  if (fichier.equals(entry.media.address)) {
                    isselected = entry.add;
                    //llog.d(TAG, "getFileIsBitmapSelected file is selected " + isselected);
                  }
                }
              }
              //llog.d(TAG, "isbitmapselectedcheck file is selected " + isselected);
              model.setFileIsBitmapSelected(idossier, ifichier, isselected);
            }
          }

        } else if (commande[0].equals("createnewfolder")) {
          String dossier = commande[1];
          Collection collection = model.getCollection(model.currCollectionAddress);
          if (collection != null) {
            Ordner ordner = new Ordner(dossier, 0, null);
            collection.copyOrdnerToData(ordner, true);
            //llog.d(TAG, "setCurrCollectionOrdnerList createnewfolder " + dossier);
            model.setCurrCollectionOrdnerList(collection.displayedOrdnerList);
          }

        } else if (commande[0].equals("selectfolder")) {
          String basisfolderclicked = commande[1];
          int dossiercourant = Integer.parseInt(commande[2]);
          boolean selectit = Boolean.parseBoolean(commande[3]);
          String currcolladd = model.currCollectionAddress;

          int[] fif = model.findFolderStartsWith(basisfolderclicked, dossiercourant);

          //llog.d(TAG, "selectfolder " + basisfolderclicked + " " + selectit + " " + dossiercourant + " : " + fif[0] + " -> " + fif[1] + " excluded");

          boolean success = false;
          int count = 0;
          // achtung si un dossier est vide il est supprimé avant, ne pas incrémenter
          int k = fif[0];
          int kp = fif[0];
          while (kp < fif[1]) {
            success = true;
            if (model.getFolderNotLoadedYet(k)) {
              success = reloadFolderOrRemoveFromGallery(k, false);
              if (!success) {
                llog.d(TAG, "reloadFolderOrRemoveFromGallery empty folder or failed " + k);
              }
            }
            if (success) {
              count += 1;
              model.copySelectedToData(k, -1, selectit, currcolladd, true);
              k += 1;
            }
            kp += 1;
          }

          if (!success)
            model.message("error failed to load all the folders");
          else
            model.message(count + " folders added");

          try {
            model.commandethreadbrowser.put(new String[]{"-1", "update"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }

        } else if (commande[0].equals("selectfile")) {
          int idossier = Integer.parseInt(commande[1]);
          int ifichier = Integer.parseInt(commande[2]);
          boolean selectit = Boolean.parseBoolean(commande[3]);
          String fichier = commande[4];

          model.copySelectedToData(idossier, ifichier, selectit, model.currCollectionAddress, false);

          try {
            model.commandethreadbrowser.put(new String[]{"-1", "update"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }

        } else if (commande[0].equals("search")) {
          int di = Integer.parseInt(commande[3]);
          int fi = Integer.parseInt(commande[4]);
          if (di < 0)
            di = 0;
          if (fi < 0)
            fi = 0;

          llog.d(TAG, "search "+commande[1]+" currid "+commande[2]+" depuis "+di + "," + fi);
          final Pattern pattern = Pattern.compile(commande[1], Pattern.CASE_INSENSITIVE);

          int[] trouvedans = model.findFolder(pattern, di, fi);
          if (trouvedans[0] == -1)
            model.message("no match found");
          else {
            try {
              model.commandethreadbrowser.put(new String[]{commande[2], "ChangeCurrentDisplayedPicture", String.valueOf(trouvedans[0]), String.valueOf(trouvedans[1]), commande[2]});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          }

        } else if (commande[0].equals("searchfile")) { // sit on position, only for collections
          llog.d(TAG, "searchfile "+commande[1]+" currid "+commande[2]+" folder " + commande[3] + " essai " + commande[4]);
          int f = Integer.parseInt(commande[3]);
          int onveutcefichier = model.findMediaAddress(f, commande[1]);
          int essai = Integer.parseInt(commande[4]);
          if (onveutcefichier >= 0) {
            try {
              model.commandethreadbrowser.put(new String[]{commande[2], "ChangeCurrentDisplayedPicture",
                      commande[3], String.valueOf(onveutcefichier), commande[2]});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          }
          
          
          
        
          
        } else if (model.commandethreaddatabase.isEmpty()) {
          /**
           toutes les autres commandes :
           on attend que la queue soit vide
           et on ne prend que la dernière
           */
          encore = false;
        }
      }

      /**
       toutes les autres commandes :
       on attend que la queue soit vide
       et on ne prend que la dernière
       */

      if (commande[0].equals("rechargetoutledossier")) {
        int currid = Integer.parseInt(commande[1]);
        int idossier = Integer.parseInt(commande[2]);
        boolean putbigpictureinmemory = Boolean.parseBoolean(commande[3]);
        /*llog.d(TAG, "c rechargetoutledossier " + currid + " dossier " + idossier + " putbigpictureinmemory " + putbigpictureinmemory);
        for (int i = 0 ; i < model.surf.size() ; i++) {
          Surf thissurf = model.surf.get(i);
          if (thissurf.dossierprincipalsave != null) {
            handled = true;
            idossier = model.findFolder(thissurf.dossierprincipalsave);
            if (idossier == -1) {
              idossier = thissurf.dossierprincipal;
              llog.d(TAG, "c rechargetoutledossier principal findfoldersave pas trouvé " + thissurf.dossierprincipalsave + " on prend le surf " + idossier);
            } else {
              if (idossier != thissurf.dossierprincipal) {
                llog.d(TAG, "c rechargetoutledossier principal findfolder " + idossier + " changé != surf " + thissurf.dossierprincipal);
              } else {
                llog.d(TAG, "c rechargetoutledossier principal findfolder " + idossier + " == surf " + thissurf.dossierprincipal);
              }
            }
            thissurf.dossierprincipalsave = null;
            rechargetoutledossier(idossier, false);
          }
        }
        if (!handled) {*/
        if (idossier == -1)
          idossier = model.surf.get(currid).ordnerIndex;
        if (putbigpictureinmemory)
          model.surf.get(currid).putbigpictureinmemory = true;
        //llog.d(TAG, "c rechargetoutledossier principal " + idossier + " putbiginmem " + putbigpictureinmemory);
        reloadFolderOrRemoveFromGallery(idossier, false);
        //}
        /*if (putbigpictureinmemory)
          model.surf.get(currid).putbigpictureinmemory = true;
        rechargetoutledossier(idossier, false);*/

        String dossiername = model.getOrnderAddress(idossier);
        if (dossiername != null) {
          for (int i = 0; i < model.autorescan.size(); i++) {
            if (dossiername.startsWith(model.autorescan.get(i))) {
              llog.d(TAG, currid + " On a un dossier en autorescan : " + model.autorescan.get(i) + " " + dossiername);
              try {
                model.commandethreaddatabase.put(new String[]{"chercheFichierDansDatabaseSinonRescanSonDossier", model.autorescan.get(i), null, "-1"});
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
              model.autorescan.remove(i);
              break;
            }
          }
        }

        try {
          model.commandethreadbrowser.put(new String[]{"-1", "update"});
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }



    }
  }

  
  private boolean reloadFolderOrRemoveFromGallery(int idossier, boolean forcerescan) {
    if (idossier >= model.folderCount) {
      llog.d(TAG, "reloadFolderOrRemoveFromGallery on dépasse le dossiercount");
    } else {
      int tailledudossier = model.getFolderFileCount(idossier);
      String cedossier = model.getOrnderAddress(idossier);
      if (cedossier == null) {
        llog.d(TAG, "reloadFolderOrRemoveFromGallery cedossier == null");
      } else {
        int disonline = model.getFolderIsOnline(idossier);
        boolean foldernotloadedyet = model.getFolderNotLoadedYet(idossier);
        if (disonline != Media.online_no) {
          if (tailledudossier == 1 && disonline == Media.online_forum) {
            if (model.getMediaIsOnlineLevel(idossier, 0) == 2) {
              llog.d(TAG, "reloadFolderOrRemoveFromGallery browse online no folder change " + idossier);
              boolean notemptyfailed = browseonlineforum(model.getFileIsOnlineForumLevel(idossier, 0), idossier, 0);
              if (!notemptyfailed) {
                removeOrdnerFromDatabase(cedossier);
                return false;
              }
              try {
                model.commandethreadbrowser.put(new String[]{"-1", "update"});
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
            }
          } else if (foldernotloadedyet) {
            // on ne supprime jamais les dossiers vides ici
            llog.d(TAG, "reloadFolderOrRemoveFromGallery -1 " + idossier + " " + cedossier);
            if (disonline == Media.online_apache) {
              if (cedossier.endsWith(".sel")) {
                // si le .sel ne contient aucun media on a une endlessloop
                //llog.d(TAG, "rechargetoutledossier -1 " + idossier + " " + cedossier + " load collection");
                retrieveonlineindexcollection(cedossier, cedossier, idossier, -1);
                //llog.d(TAG, "rechargetoutledossier -1 " + idossier + " " + cedossier + "collection loaded");
              } else {
                ArrayList<Media> listmedia = InternetSession.getApache(cedossier, true);
                if (listmedia == null) {
                  llog.d(TAG, "reloadFolderOrRemoveFromGallery failed to retrieve apache will retry when connected setFolderNotLoadedYet false "+ cedossier);
                  model.setFolderNotLoadedYet(idossier, false);
                } else if (listmedia.size() == 0) {
                  llog.d(TAG, "reloadFolderOrRemoveFromGallery empty apache folder remove do not retry nettoiedeldossier " + cedossier);
                  removeOrdnerFromDatabase(cedossier);
                  return false;
                } else {
                  llog.d(TAG, "reloadFolderOrRemoveFromGallery getApache setFilesOrSetNotLoadedYet " + cedossier);
                  model.setFilesOrSetNotLoadedYet(idossier, listmedia);
                }
              }
            } else if (disonline == Media.online_json) {
              boolean notemptyfailed = retrieveonlinefilesonlyjson(idossier, -1);
              if (!notemptyfailed) {
                removeOrdnerFromDatabase(cedossier);
                return false;
              }
            }
            try {
              model.commandethreadbrowser.put(new String[]{"-1", "update"});
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          }
        } else if (foldernotloadedyet || forcerescan) {
          List<Fichier> listfichier = model.db.dbFichier().getAll(cedossier);
          int listfichierl = listfichier.size();

          if (listfichierl == 0 || forcerescan) {
            if (forcerescan) {
              llog.d(TAG, "reloadFolderOrRemoveFromGallery forcerescan subfolders " + cedossier);
              // forcerescan always rescan subfolders as well
              addSubFoldersToDatabaseAndGallery(cedossier);
              //model.db.dbFichier().deleteFolderOrZip(cedossier); // on garde les références
              //listfichier.clear();
            } else {
              llog.d(TAG, "reloadFolderOrRemoveFromGallery aucun fichier trouvé dans la database pour le dossier on le scanne " + cedossier);
            }

            // il faut supprimer tout ce qu'on a déjà en mémoire
            // sinon ça alterne anciens / nouveaux
            // car scannetouslesfichiersdudossier va les exclure de notre nouvelle sélection
            //model.setFilesOrSetNotLoadedYet(idossier, null); // on garde les miniatures
            int finsererl = scanFilesOnDiskFromFolder(cedossier, listfichier);
            if (finsererl == 0) {
              llog.d(TAG, "reloadFolderOrRemoveFromGallery on supprime le dossier vide " + idossier);
              removeOrdnerFromDatabase(cedossier);
              //if (idossier < model.folderCount - 1) {
              //  llog.d(TAG, "rechargetoutledossier on scanne le dossier suivant 1+" + idossier);
                //return rechargetoutledossier(idossier + 1, forcerescan);
              //} // on ne peut pas faire dans les deux directions sinon +1 -> -1 -> +1 -> -1 -> ...
              //llog.d(TAG, "rechargetoutledossier on ne scanne pas le suivant on renvoie false");
              return false;
            } else {
              listfichier = model.db.dbFichier().getAll(cedossier);
            }
          }


          listfichierl = listfichier.size();
          List<Media> listmedia = new ArrayList<>();
          for (int i = 0; i < listfichierl; i++) {
            Fichier dbfichier = listfichier.get(i);
            Media media = new Media();
            media.addressToGetLibextractorsThumbnail = dbfichier.uid;
            media.address = dbfichier.fichier;
            media.ordnerAddress = dbfichier.dossier;
            media.type = Media.getType(dbfichier.fichier);
            media.isInsideAnArchive = Media.getArchived(dbfichier.dossier);
            media.index = i;
            media.ordnerIndex = idossier;
            listmedia.add(media);
            //llog.d(TAG, " + f " + media.address + " : " + media.addressToGetLibextractorsThumbnail);
          }
          //llog.d(TAG, listfichierl + " fichiers trouvés dans " + cedossier);
          model.setFilesOrSetNotLoadedYet(idossier, listmedia);
          listfichier.clear();
          try {
            model.commandethreadbrowser.put(new String[]{"-1", "update"});
          } catch (InterruptedException e) {
            e.printStackTrace();
          }

        } else if (tailledudossier == 0) {
          llog.d(TAG, "reloadFolderOrRemoveFromGallery on supprime le dossier vide " + cedossier);
          removeOrdnerFromDatabase(cedossier);
          if (idossier < model.folderCount - 1) {
            return reloadFolderOrRemoveFromGallery(idossier + 1, forcerescan);
          }
        }
      }
    }
    return true;
  }

  private void addSubFoldersToDatabaseAndGallery(String rootfolder) {
    dinserers.clear();
    if (!rootfolder.endsWith("/"))
      rootfolder += "/";
    File rootfile = new File(rootfolder);
    dinserers.add(rootfolder);
    scannetouslesdossiers(rootfile);
    int dinsererl = dinserers.size();
    if (dinsererl > 0) {
      List<String> onadejadossier = model.findFoldersStartsWith(rootfolder);
      int onadejadossierl = onadejadossier.size();
      for (int d = 0; d < onadejadossierl; d++) {
        String nomdossier = onadejadossier.get(d);
        boolean onadejacedossier = false;
        dinsererl = dinserers.size();
        for (int nd = 0; nd < dinsererl; nd++) {
          if (dinserers.get(nd).equals(nomdossier)) {
            onadejacedossier = true;
            dinserers.remove(nd);
            break;
          }
        }
        if (!onadejacedossier) {
          llog.d(TAG, "  -d addSubFoldersToDatabaseAndGallery on n'a plus ce dossier on le supprime de la database avec ses miniatures : " + nomdossier);
          removeOrdnerFromDatabase(nomdossier);
        }
      }
      onadejadossier.clear();
      dinsererl = dinserers.size();
      if (dinsererl > 0) {
        for (int i = 0 ; i < dinsererl ; i++) {
          Dossier dd = new Dossier();
          dd.dossier = dinserers.get(i);
          //llog.d(TAG, "  +d addSubFoldersToDatabaseAndGallery " + i + "/" + dinsererl + " : " + dd.dossier);
          model.db.dbDossier().insert(dd);
          model.addFolder(-1, dd.dossier,  false, 0, null, Media.online_no);
        }
      }
      llog.d(TAG, "addSubFoldersToDatabaseAndGallery added " + dinsererl + " folders to the database.");
      dinserers.clear();
    }
  }
  
  private void scannetouslesdossiers(File root) {
    File[] filelist = root.listFiles();
    if (filelist == null)
      return;
    for (File f : filelist) {
      if (f.isDirectory()) {
        String fnom = f.getAbsolutePath();
        if (!fnom.endsWith("/"))
          fnom += "/";
        dinserers.add(fnom);
        scannetouslesdossiers(f);
      }
    }
  }
  
  private int scanFilesOnDiskFromFolder(String rootfolder, List<Fichier> listfichier) {
    dinserers.clear();
    finserer.clear();
    int nbdefichiernepassupprimer = 0;
    File basefile = new File(rootfolder);
    if (!basefile.isDirectory()) {
      Matcher matcher = Gallery.archivepattern.matcher(rootfolder);
      if (matcher.find()) {
        ziplist(basefile);
      } else {
        llog.d(TAG, "scanFilesOnDiskFromFolder neither a dir, nor an archive : " + rootfolder);
        return nbdefichiernepassupprimer;
      }
    } else {
      if (!rootfolder.endsWith("/"))
        rootfolder += "/";
      File[] directory = basefile.listFiles();
      if (directory == null) {
        llog.d(TAG, "scanFilesOnDiskFromFolder no single file found : " + rootfolder);
        return nbdefichiernepassupprimer;
      }
      for (File f : directory) {
        String fnom = f.getAbsolutePath();
        if (f.isDirectory()) { // if wanted needs to be done beforehand addSubFoldersToDatabaseAndGallery()
          //if (!fnom.endsWith("/"))
          //  fnom += "/";
          //dinserers.add(fnom);
        } else {
          Matcher matcher = Gallery.archivepattern.matcher(fnom);
          if (matcher.find()) { // ici pas de slash
            //llog.d(TAG, "  + archive        : "+fnom);
            dinserers.add(fnom);
          } else {
            int type = Media.getType(fnom);
            if (type != Media.media_refused) {
              //llog.d(TAG, "  + vidéo          : "+fnom);
              Fichier fichier = new Fichier();
              fichier.fichier = fnom;
              fichier.dossier = rootfolder;
              finserer.add(fichier);
            }
          }
        }
      }
    }
    
    int dinsererl = dinserers.size();
    if (dinsererl > 0) {
      List<String> onadejadossier = model.findFoldersStartsWith(rootfolder);
      int onadejadossierl = onadejadossier.size();
      for (int d = 0; d < onadejadossierl; d++) {
        String nomdossier = onadejadossier.get(d);
        if (nomdossier.endsWith("/")) // only care about archives, subfolders are handled when browsed over
          continue;
        boolean onadejacedossier = false;
        dinsererl = dinserers.size();
        for (int nd = 0; nd < dinsererl; nd++) {
          if (dinserers.get(nd).equals(nomdossier)) {
            onadejacedossier = true;
            dinserers.remove(nd);
            break;
          }
        }
        if (!onadejacedossier) {
          llog.d(TAG, "  -d scanFilesOnDiskFromFolder on n'a plus ce dossier on le supprime de la database avec ses miniatures : " + nomdossier);
          removeOrdnerFromDatabase(nomdossier);
        }
      }
      onadejadossier.clear();
      dinsererl = dinserers.size();
      if (dinsererl > 0) {
        for (int i = 0 ; i < dinsererl ; i++) {
          Dossier dd = new Dossier();
          dd.dossier = dinserers.get(i);
          llog.d(TAG, "  +d scanFilesOnDiskFromFolder " + i + "/" + dinsererl + " : " + dd.dossier);
          model.db.dbDossier().insert(dd);
          model.addFolder(-1, dd.dossier,  false, 0, null, Media.online_no);
        }
      }
      dinserers.clear();
    }

    int finsererl = finserer.size();
    nbdefichiernepassupprimer = finsererl;
    if (finsererl > 0) {
      int listfichierl = listfichier.size();
      if (listfichierl > 0) {
        // on n'ajoute pas les fichiers de ce dossier déjà dans la database et chargés
        for (int f = 0; f < listfichierl; f++) {
          String nomfichier = listfichier.get(f).fichier;
          boolean onadejacefichier = false;
          finsererl = finserer.size();
          for (int nd = 0; nd < finsererl; nd++) {
            if (finserer.get(nd).fichier.equals(nomfichier)) {
              onadejacefichier = true;
              finserer.remove(nd);
              break;
            }
          }
          if (!onadejacefichier) {
            llog.d(TAG, "  -ff on n'a plus ce fichier on le supprime de la database avec sa miniature : " + nomfichier);
            removeMediaFromDatabase(rootfolder, nomfichier);
          }
        }
      }
      finsererl = finserer.size();
      if (finsererl > 0) {
        model.db.dbFichier().insertAll(finserer);
        /*for (int i = 0 ; i < finsererl ; i++) {
          Fichier f = finserer.get(i);
          //llog.d(TAG, "  +d scanFilesOnDiskFromFolder " + i + "/" + finsererl + " : " + f.fichier);
          model.db.dbFichier().insert(f);
          int uid = model.db.dbFichier().fichierGetUID(f.fichier);
          File miniature = new File(model.dossierminiature + uid + "_0");
          if (miniature.exists()) { // force regenration
            boolean deleted = miniature.delete();
            llog.d(TAG, "deleted old miniature " + model.dossierminiature + uid + "_0 : " + deleted);
          }
        }*/
      }
    }

    dinserers.clear();
    finserer.clear();
    return nbdefichiernepassupprimer;
  }
  
  public int ziplist(File zipFile) {
    int nfichier = 0;
    String cedossier = zipFile.getAbsolutePath();
    //llog.d(TAG, "ziplist                  : " +cedossier);
    ZipInputStream zis = null;
    try {
      zis = new ZipInputStream(new BufferedInputStream(new FileInputStream(zipFile)));
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
    if (zis == null) {
      return nfichier;
    }
    ZipEntry ze = null;
    while (true) {
      try {
        ze = zis.getNextEntry();
      } catch (IOException e) {
        e.printStackTrace();
      } catch (IllegalArgumentException e) {
        e.printStackTrace();
      }
      if (ze == null) {
        break;
      }
      if (ze.isDirectory()) {
        continue;
      }
      String fichiernom = ze.getName();
      Matcher matcher = Gallery.imagepattern.matcher(fichiernom);
      if (matcher.find()) {
        //llog.d(TAG, "zip                  : " +cedossier+"           /"+fichiernom);
        Fichier fichier = new Fichier();
        fichier.fichier = cedossier+"/"+fichiernom;
        fichier.dossier = cedossier;
        finserer.add(fichier);
        nfichier += 1;
      }
    }
    try {
      zis.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return nfichier;
  }
  
  public void readfile() {
    HashMap<String, String> h1 = new HashMap<>();
    try {
      if (new File(model.dossierconfig + "/onlinesites.txt").exists()) {
        InputStream is = new FileInputStream(model.dossierconfig + "/onlinesites.txt");
        BufferedReader in = new BufferedReader(new InputStreamReader(is));
        String line;
        while ((line = in.readLine()) != null) {
          int egal = line.indexOf('=');
          if (egal != -1) {
            String cle = line.substring(0, egal);
            String valeur = line.substring(egal+1);
            if (h1.size() > 5 && cle.equals("address")) {
              registersite(h1);
            }
            h1.put(cle, valeur);
            //llog.d(TAG, cle+"   :     "+valeur);
          }
        }
        in.close();
        if (h1.size() > 5) {
          registersite(h1);
        }
      } else {
        llog.d(TAG, "Please edit "+model.dossierconfig + "/onlinesites.txt to add online sites.");
      }
    } catch (IOException e) {
      llog.d(TAG, "readfile error! " + e.getMessage());
    }
  }
  
  private void registersite(HashMap<String, String> h1){
    HashMap<String, String> h2 = new HashMap<>();
    for (String cle : h1.keySet()){
      h2.put(cle, h1.get(cle));
    }
    onlinedata.add(h2);
    String adresse = h1.get("address");
    //llog.d(TAG, " -> "+h1.get("address"));
    boolean onadejacesitev = false;
    int nombreonline = model.preferences.getInt("nombreonline", 0);
    //llog.d(TAG, "nombreonline = "+nombreonline);
    for (int i = 0 ; i < nombreonline ; i++) {
      String onadejacesite = model.preferences.getString("mediaonline"+i, null);
      //llog.d(TAG, "mediaonline"+i+" -> "+onadejacesite);
      if (onadejacesite.equals(adresse)){
        onadejacesitev = true;
        break;
      }
    }
    if (!onadejacesitev) {
      SharedPreferences.Editor editor = model.preferences.edit();
      editor.putString("mediaonline"+nombreonline, adresse);
      editor.putBoolean("autoload"+nombreonline, false);
      editor.putInt("nombreonline",nombreonline+1);
      editor.apply();
    }
    h1.clear();
  }
  
  public boolean browseonlinesite(int currid, String onaffichecesiteinternet) {
    /**
     *    on a cliqué dans le menu online sites
     *    on n'a que le nom
     */
    int onlinedatal = onlinedata.size();
    llog.d(TAG, "browseonlinesite " + onlinedatal + " total : " + onaffichecesiteinternet);
    for (int i = 0 ; i < onlinedatal ; i++) {
      HashMap<String, String> data = onlinedata.get(i);
      llog.d(TAG, i + " : " + data.get("address"));
      if (onaffichecesiteinternet.equals(data.get("address"))) {
        return browseonlineforum(i, -1, -1);
      }
    }
    if (onaffichecesiteinternet.endsWith(".txt"))
      return retrieveonlineindextxt(onaffichecesiteinternet);
    else if (onaffichecesiteinternet.endsWith(".sel"))
      return retrieveonlineindexcollection(onaffichecesiteinternet, onaffichecesiteinternet, -1, -1);
    else if (onaffichecesiteinternet.contains("/api?") || onaffichecesiteinternet.endsWith("/api"))
      return retrieveonlineindexjson(onaffichecesiteinternet);
    else // directly browse apache html server
      return getApacheFolderInsertAfterParentReplaceOtherChildren(onaffichecesiteinternet, -1, -1);
  }
  
  public boolean browseonlinefolder(int currid, String moncedossier, String moncefichier) {
    /**
     *   là on possède toujours un fichier qui nous envoie qqpart
     */
    int cedossier = Integer.parseInt(moncedossier);
    int cefichier = Integer.parseInt(moncefichier);
    int isonline = model.getMediaIsOnline(cedossier, cefichier);
    llog.d(TAG, "browseonlinefolder " + moncedossier + " " + moncefichier + " isonline " + isonline);
    if (isonline == Media.online_forum) {
      int isonlineforumlevel = model.getFileIsOnlineForumLevel(cedossier, cefichier);
      return browseonlineforum(isonlineforumlevel, cedossier, cefichier);
    } else if (isonline == Media.online_json) {
      return retrieveonlinefilesonlyjson(cedossier, cefichier);
    } else { // directly browse apache html server
      String onaffichecesiteinternet = model.getMediaAddress(cedossier, cefichier);
      boolean success;
      if (onaffichecesiteinternet.endsWith(".sel")) {
        success = retrieveonlineindexcollection(onaffichecesiteinternet, onaffichecesiteinternet, cedossier, cefichier);
      } else {
        success = getApacheFolderInsertAfterParentReplaceOtherChildren(onaffichecesiteinternet, cedossier, cefichier);
      }
      return success;
    }
  }

  public boolean browseonlineforum(int idata, int cedossier, int cefichier){
    HashMap<String, String> data = onlinedata.get(idata);
    String onaffichecesiteinternet;
    String cettepage = "";
    int cenextpagei = -1;
    int cenextpagetot = -1;
    int level = 0;
    if (cedossier != -1) {
      onaffichecesiteinternet =  model.getMediaAddress(cedossier, cefichier);
      // TODO : à revoir
      level = model.getMediaIsOnlineLevel(cedossier, cefichier);
      cenextpagei = model.getFileIsOnlineLinkPage(cedossier, cefichier);
      cenextpagetot = model.getFileIsOnlineLinkPageL(cedossier, cefichier);
      if (level == 1) {
        int paginationdelta = Integer.parseInt(data.get("1paginationdelta"));
        int paginationstart = Integer.parseInt(data.get("1paginationstart"));
        int thispage = cenextpagei * paginationdelta;
        if (paginationstart == 0) {
          thispage -= paginationdelta;
        }
        cettepage = data.get("1pagination").replace("0", String.valueOf(thispage));
      } else if (level == 2) {
        int paginationdelta = Integer.parseInt(data.get("2paginationdelta"));
        int paginationstart = Integer.parseInt(data.get("2paginationstart"));
        int thispage = cenextpagei * paginationdelta;
        if (paginationstart == 0) {
          thispage -= paginationdelta;
        }
        cettepage = data.get("2pagination").replace("0", String.valueOf(thispage));
      }
    } else {
      onaffichecesiteinternet = data.get("address");
    }
    //model.message("get : " + onaffichecesiteinternet + cettepage);
    llog.d(TAG, "browse online : " + onaffichecesiteinternet + cettepage);
    String html = InternetSession.gethtml(model, onaffichecesiteinternet + cettepage, onaffichecesiteinternet);
    if (html == null && cedossier >= 0 && cefichier >= 0) {
      model.message("null : " + onaffichecesiteinternet + cettepage);
      llog.d(TAG, "browsing online : getHtml == null " + onaffichecesiteinternet + cettepage);
      if (level == 1) {
        int paginationstart = Integer.parseInt(data.get("1paginationstart"));
        if (model.getFileIsOnlineLinkPage(cedossier, cefichier) > paginationstart) {
          model.setFileIsOnlineLinkPage(cedossier, cefichier, -Integer.parseInt(data.get("1paginationdelta")));
          return browseonlineforum(idata, cedossier, cefichier);
        }
      } else if (level == 2) {
        int paginationstart = Integer.parseInt(data.get("2paginationstart"));
        if (model.getFileIsOnlineLinkPage(cedossier, cefichier) > paginationstart) {
          model.setFileIsOnlineLinkPage(cedossier, cefichier, -Integer.parseInt(data.get("2paginationdelta")));
          return browseonlineforum(idata, cedossier, cefichier);
        }
      }
    }
    if (html == null) {
      model.message("null : " + onaffichecesiteinternet + cettepage);
      llog.d(TAG, "browse online : " + onaffichecesiteinternet + cettepage+" failed");
      return false;
    }
    //model.message("200 : " + onaffichecesiteinternet + cettepage);
    llog.d(TAG, "200 : "+ onaffichecesiteinternet + cettepage+"      "+level);
    
    Pattern findpattern;
    Matcher matcher;
    String racine = onaffichecesiteinternet;
    int racinef = onaffichecesiteinternet.indexOf("//");
    if (racinef >= 0) {
      racinef = onaffichecesiteinternet.indexOf("/", racinef+2);
      if (racinef >= 0) {
        racine = onaffichecesiteinternet.substring(0, racinef);
      }
    }
    if (level == 0) {
      /**
       *   tous les forums de cette page d'acceuil
       */
      llog.d(TAG, "0forums : findpattern "+data.get("0forums"));
      findpattern = Pattern.compile(data.get("0forums"));
      matcher = findpattern.matcher(html);
      List<String> listdossier = new ArrayList<>();
      String lastlienbase = "";
      while (matcher.find()) {
        String lienbase = racine + "/" + matcher.group(1);
        if (!lienbase.equals(lastlienbase)) {
          lastlienbase = lienbase;
          llog.d(TAG, "0forums : "+lienbase);
          listdossier.add(lienbase);
        }
      }
      /**
       *    la première page de chaque forum de cette page d'acceuil
       */
      int paginationstart = Integer.parseInt(data.get("1paginationstart"));
      int listdossierl = listdossier.size();
      for (int i = 0 ; i < listdossierl ; i++) {
        List<Media> listmedia = new ArrayList<>();
        Media media = new Media();
        media.addressToGetLibextractorsThumbnail = 10000;
        media.address = listdossier.get(i);
        media.ordnerAddress = listdossier.get(i);
        //media.addressToGetThumbnail = listdossier.get(i);
        media.isOnline = Media.online_forum;
        media.getDirectPictureAddressBypassAds = true;
        media.isonlinelevel = 1;
        media.isonlineforumlevel = idata;
        media.isALinkThatCreatesAnOrdner = true;
        media.isonlinelinktonextpagei = 1;
        media.isonlinelinktonextpagetot = 1;
        media.isonlineparentlink = null;
        media.type = Media.media_picture;
        media.isInsideAnArchive = false;
        media.isSelected = false;
        listmedia.add(media);
        model.addFolder(-1, listdossier.get(i),  false, 0, listmedia, Media.online_forum);
      }
      
      
      
      
      
      
      
    } else if (level == 1) {
      //InternetSession.savehtml(html);
      /**
       *     toutes les pages de ce forum
        */
      int paginationstart = Integer.parseInt(data.get("1paginationstart"));
      int paginationdelta = Integer.parseInt(data.get("1paginationdelta"));
      int paginationstart2 = Integer.parseInt(data.get("2paginationstart"));
      int paginationdelta2 = Integer.parseInt(data.get("2paginationdelta"));
      int listepage = 1;
      findpattern = Pattern.compile(data.get("1forumspages"));
      matcher = findpattern.matcher(html);
      while (matcher.find()) {
        int ilyadejanbpages = model.getFolderFileCount(cedossier);
        listepage = Integer.parseInt(matcher.group(1)) / paginationdelta;
        if (paginationstart == 0) {
          listepage += 1;
        }
        if (listepage > ilyadejanbpages) {
          for (int j = ilyadejanbpages + 1; j <= listepage ; j++) {
            String lienbase = onaffichecesiteinternet;
            //llog.d(TAG, "1forumspages "+lienbase);
            Media media = new Media();
            media.addressToGetLibextractorsThumbnail = 10000;
            media.address = lienbase;
            media.ordnerAddress = lienbase;
            //media.addressToGetThumbnail = lienbase;
            media.isOnline = Media.online_forum;
            media.getDirectPictureAddressBypassAds = true;
            media.isonlinelevel = 2;
            media.isonlineforumlevel = idata;
            media.isALinkThatCreatesAnOrdner = true;
            media.isonlinelinktonextpagei = j;
            media.isonlinelinktonextpagetot = listepage;
            media.isonlineparentlink = null;
            media.type = Media.media_picture;
            media.isInsideAnArchive = false;
            media.isSelected = false;
            model.addFileToFolder(cedossier, media);
          }
        }
      }
      /**
       *      tous les threads de ce forum
        */
      findpattern = Pattern.compile(data.get("1threads+page"));
      matcher = findpattern.matcher(html);
      List<String> listdossier = new ArrayList<>();
      while (matcher.find()) {
        String lienbase = racine + "/" + matcher.group(1);
        //llog.d(TAG, matcher.group(1)+" --------- "+matcher.group(3));
        int nbdepage = 1;
        if (matcher.group(2) != null) {
          if (matcher.group(3) != null) {
            nbdepage = Integer.parseInt(matcher.group(3));
          }
        }
        boolean onadeja = false;
        for (int j = 0; j < listdossier.size() ; j++) {
          if (listdossier.get(j).equals(lienbase)) {
            onadeja = true;
            /*
            if (listdossier.get(j).isonlinepage < nbdepage) {
              listdossier.get(j).isonlinepage = nbdepage;
              //llog.d(TAG, lienbase+" > "+nbdepage);
            }
            */
            break;
          }
        }
        if (!onadeja) {
          llog.d(TAG, "1threads+page "+lienbase);
          //dossier.isonline = true;
          /*
          dossier.isonlinelevel = 2;
          dossier.isonlinepage = nbdepage / paginationdelta2;
          if (paginationstart2 == 0) {
            dossier.isonlinepage += 1;
          }
           */
          listdossier.add(lienbase);
        }
      }
      int positiondossiersuivant = cedossier + 1;
      /**
       *     d'abord la page source isonlineparentlink onaffichecesiteinternet
       *     la première page de chaque thread de ce forum
        */
      int listdossierl = listdossier.size();
      for (int i = 0 ; i < listdossierl ; i++) {


        List<Media> listmedia = new ArrayList<>();
        Media media = new Media();

        /*
        media.uidfichier = 10000;
        media.uiddossier = 10000;
        media.fichier = listdossier.get(i).dossier;
        media.dossier = listdossier.get(i).dossier;
        media.minipic = listdossier.get(i).dossier;
        media.isonline = true;
        media.isonlinelevel = 2;
        media.isonlineforum = idata;
        media.isonlinelinktonextpage = true;
        media.isonlinelinktonextpagei = cenextpagei ;
        media.isonlinelinktonextpagetot = cenextpagetot;
        media.isonlineparentlink = onaffichecesiteinternet;
        media.isvideo = false;
        media.isarchived = false;
        media.bitmapselected = false;
        media.wallpaperselected = false;
        media.lockscreenselected = false;
        listmedia.add(media);
        */

        media.addressToGetLibextractorsThumbnail = 10000;
        media.address = listdossier.get(i);
        media.ordnerAddress = listdossier.get(i);
        //media.addressToGetThumbnail = listdossier.get(i);
        media.isOnline = Media.online_forum;
        media.getDirectPictureAddressBypassAds = true;
        media.isonlinelevel = 2;
        media.isonlineforumlevel = idata;
        media.isALinkThatCreatesAnOrdner = true;
        //media.isonlinelinktonextpagei = listdossier.get(i).isonlinepage ;
        //media.isonlinelinktonextpagetot = listdossier.get(i).isonlinepage;
        media.isonlineparentlink = null;
        media.type = Media.media_picture;
        media.isInsideAnArchive = false;
        media.isSelected = false;
        listmedia.add(media);


        model.addFolder(positiondossiersuivant, listdossier.get(i),  false, 0, listmedia, Media.online_forum);
        positiondossiersuivant += 1;
      }
      
      
      
      
      
      
    } else if (level == 2) {
      /**
       *      les images
        */
      findpattern = Pattern.compile(data.get("2images+mini"));
      //llog.d(TAG, "data get : "+data.get("2images+mini"));
      matcher = findpattern.matcher(html);
      List<Media> listmedia = new ArrayList<>();
      while (matcher.find()) {
        String lienimage = matcher.group(1);
        //llog.d(TAG, "lien image : "+lienimage);
        String cherminiature = matcher.group(0);
        Pattern infindpattern = Pattern.compile(data.get("2miniinimages"));
        Matcher inmatcher = infindpattern.matcher(cherminiature);
        if (inmatcher.find()) {
          String lienminiature = inmatcher.group(1);
          //llog.d(TAG, lienimage + " i:th " + lienminiature);
          Media media = new Media();
          media.addressToGetLibextractorsThumbnail = 10000;
          media.address = lienimage;
          media.ordnerAddress = onaffichecesiteinternet;
          media.addressToGetThumbnail = lienminiature;
          media.getDirectPictureAddressBypassAds = true;
          media.isOnline = Media.online_forum;
          media.type = Media.media_picture;
          media.isInsideAnArchive = false;
          media.isSelected = false;
          listmedia.add(media);
        }
      }
      
      if (listmedia.size() == 0) {
        /**
         *      les images grandes, sans miniature
         */
        findpattern = Pattern.compile("<img src=.(https?://[^\'\"]+)");
        matcher = findpattern.matcher(html);
        while (matcher.find()) {
          String lienimage = matcher.group(1);
          if (lienimage.indexOf(data.get("host")) == -1) {
            String lienminiature = lienimage;
            //llog.d(TAG, lienimage + " : " + lienminiature);
            Media media = new Media();
            media.addressToGetLibextractorsThumbnail = 10000;
            media.address = lienimage;
            media.ordnerAddress = onaffichecesiteinternet;
            media.addressToGetThumbnail = lienminiature;
            media.getDirectPictureAddressBypassAds = true;
            media.isOnline = Media.online_forum;
            media.type = Media.media_picture;
            media.isInsideAnArchive = false;
            media.isSelected = false;
            listmedia.add(media);
          }
        }
      }
      
      if (listmedia.size() == 0 && model.getFileIsOnlineLinkPage(cedossier, cefichier) > 1) {
        model.setFileIsOnlineLinkPage(cedossier, cefichier, -1);
        llog.d(TAG, "no picture found in this thread, loading next page");
        return browseonlineforum(idata, cedossier, cefichier);
      }
  
      /**
       *      seulement la page suivante si elle existe
        */
      int isonlinelinktonextpagei = model.getFileIsOnlineLinkPage(cedossier, cefichier);
      int isonlinelinktonextpagetot = model.getFileIsOnlineLinkPageL(cedossier, cefichier);
      if (isonlinelinktonextpagetot > 1 && isonlinelinktonextpagei > 1) {
        llog.d(TAG, "   on ajoute la page suivante à la fin des thumbnails : " + onaffichecesiteinternet);
        Media media = new Media();
        media.addressToGetLibextractorsThumbnail = 10000;
        media.address = onaffichecesiteinternet;
        media.ordnerAddress = onaffichecesiteinternet;
        //media.addressToGetThumbnail = onaffichecesiteinternet;
        media.isOnline = Media.online_forum;
        media.getDirectPictureAddressBypassAds = true;
        media.isonlinelevel = 2;
        media.isonlineforumlevel = idata;
        media.isALinkThatCreatesAnOrdner = true;
        media.isonlinelinktonextpagei = isonlinelinktonextpagei - 1;
        media.isonlinelinktonextpagetot = isonlinelinktonextpagetot;
        media.isonlineparentlink = null;
        media.type = Media.media_picture;
        media.isInsideAnArchive = false;
        media.isSelected = false;
        listmedia.add(media);
      } else {
        llog.d(TAG, "   pas de page précédente on ajoute un dummy fin");
        //llog.d(TAG, "   on ajoute la page suivante à la fin des thumbnails : " + lienbase);
        Media media = new Media();
        media.addressToGetLibextractorsThumbnail = 10000;
        media.address = "end";
        media.ordnerAddress = onaffichecesiteinternet;
        //media.addressToGetThumbnail = null;
        media.isOnline = Media.online_forum;
        media.getDirectPictureAddressBypassAds = true;
        media.isonlinelevel = 2;
        media.isonlineforumlevel = idata;
        media.isALinkThatCreatesAnOrdner = true;
        media.isonlinelinktonextpagei = isonlinelinktonextpagei - 1;
        media.isonlinelinktonextpagetot = isonlinelinktonextpagetot;
        media.isonlineparentlink = null;
        media.type = Media.media_picture;
        media.isInsideAnArchive = false;
        media.isSelected = false;
        listmedia.add(media);
      }

      model.addFilesToFolder(cedossier, listmedia);
    }

    if (level < 2)
      model.redrawsplitviewbitmapm = true;
    
    //model.message();
    return true;
  }
  
  public boolean retrieveonlineindextxt(String onaffichecesiteinternet){
    llog.d(TAG, "retrieveonlineindextxt : " + onaffichecesiteinternet);

    InputStreamReader ins = null;
    BufferedReader in = null;
    try {
      URL urlurl = new URL(onaffichecesiteinternet);
      HttpURLConnection c = (HttpURLConnection) urlurl.openConnection();
      c.setRequestProperty("Connection", "Keep-Alive");
      c.setRequestMethod("GET");
      c.setConnectTimeout(InternetSession.connecttimeout);
      int responsecode = c.getResponseCode();
      if (responsecode >= 200 && responsecode <= 310) {
        c.connect();
        InputStream is = c.getInputStream();
        ins = new InputStreamReader(is);
        in = new BufferedReader(ins);

        int dernierslash = onaffichecesiteinternet.lastIndexOf("/");
        if (dernierslash == -1)
          return false;
        onaffichecesiteinternet = onaffichecesiteinternet.substring(0, dernierslash) + "/";
        llog.d(TAG, "base = " + onaffichecesiteinternet);
        int ndoss = 0;
        String fnom = null;
        String dnom;
        String dernierdossier = "";

        while ((fnom = in.readLine()) != null) {

            int lastslash = fnom.lastIndexOf("/");
            if (lastslash < 0 || fnom.equals("./") || fnom.equals("/"))         //  . .. pic.jpg ./ /
              dnom = onaffichecesiteinternet;
            else {
              lastslash += 1;
              if (fnom.startsWith("./"))                                        //  ./pic.jpg ./a/pic.jpg
                dnom = onaffichecesiteinternet + fnom.substring(2, lastslash);
              else if (fnom.startsWith("/"))                                    //  /pic.jpg  /a/pic.jpg
                dnom = onaffichecesiteinternet + fnom.substring(1, lastslash);
              else                                                              //  a/pic.jpg
                dnom = onaffichecesiteinternet + fnom.substring(0, lastslash);
            }

            if (!dernierdossier.equals(dnom)) {
              dernierdossier = dnom;

              String dnomcorr = Gallery.urlEncode(dnom);

              model.addFolder(-1, dnomcorr,  true, 0, null, Media.online_apache);
              ndoss += 1;
            }
        }
        in.close();
        ins.close();
        is.close();

        if (ndoss == 0)
          model.message("no folder found.");
        else {
          model.redrawsplitviewbitmapm = true;
          return true;
        }

      }
      Gallery.isConnectedToInternet = true;
    } catch (IOException e) {
      Gallery.isConnectedToInternet = false;
      llog.d(TAG, "downloadlist error! " + e.getMessage());
      //model.message("retrieveonlineindextxt " + e.getMessage());
      model.message(e.getMessage() + "\n" + onaffichecesiteinternet);
    }

    return false;
  }

  public boolean getApacheFolderInsertAfterParentReplaceOtherChildren(String onaffichecesiteinternet, int idossier, int ifichier){
    llog.d(TAG, "getApacheFolderInsertAfterParentReplaceOtherChildren : " + onaffichecesiteinternet);
    List<Media> listmediaaddd = InternetSession.getApache(onaffichecesiteinternet, false);
    if (listmediaaddd == null) {
      llog.d(TAG, "no connection or error apache " + onaffichecesiteinternet);
      model.message("http get error\n" + onaffichecesiteinternet);
    } else if (listmediaaddd.size() > 0) {
      // dynamic navigation
      //dossier.isonline = true;
      //dossier.info = model.GETAPACHE;
      /**
       *   ici on insère juste en-desous
       *   et on supprime d'abord tous les dossiers enfants
       */
      int inserealaposition;
      if (idossier >= 0) {
        // -1 si lancé depuis les options
        if (idossier < model.folderCount) {
          String dossierparent = model.getOrnderAddress(idossier);
          if (dossierparent != null) {
            llog.d(TAG, dossierparent + " = dossier parent on supprime après 1+" + idossier);
            List<String> startswithparent = model.findFoldersStartsWith(dossierparent, idossier + 1, model.folderCount);
            for (int zz = 0; zz < startswithparent.size(); zz++) {
              llog.d(TAG, dossierparent + " = dossier parent on supprime zz " + zz + " : " + startswithparent.get(zz));
              removeOrdnerFromDatabase(startswithparent.get(zz));
            }
          }
        }
        inserealaposition = idossier + 1;
      } else {
        inserealaposition = model.folderCount;
      }
      llog.d(TAG, "------------------ inserealaposition " + inserealaposition + " / ");
      model.addFolder(inserealaposition, onaffichecesiteinternet, true, 0, listmediaaddd, Media.online_apache);
    } else {
      llog.d(TAG, "empty apache folder should be removed " + onaffichecesiteinternet);
    }
    
    return true;
  }
  
  public boolean retrieveonlinefilesonlyjson(int idossier, int ifichier){
    /**
     *    ifichier == -1 : dossier développé mais vide
     *             sinon : fichier lien vers dossier cliqué
     */
    boolean expand = true;
    boolean randomize = false;
    int preferthis = -1;
    String responseData = null;
    String onaffichecesiteinternet;
    if (ifichier == -1) {
      onaffichecesiteinternet = model.getOrnderAddress(idossier);
    } else {
      onaffichecesiteinternet = model.getMediaAddress(idossier, ifichier);
      expand = false;
      // dynamic browsing
    }
    llog.d(TAG, "retrieveonlinefilesonlyjson : " + onaffichecesiteinternet+" : d "+idossier+" f "+ifichier);
    
    try {
      URL urlurl = new URL(onaffichecesiteinternet);
      HttpURLConnection c = (HttpURLConnection) urlurl.openConnection();
      c.setRequestProperty("Connection", "Keep-Alive");
      c.setRequestMethod("GET");
      c.setConnectTimeout(InternetSession.connecttimeout);
      c.connect();

      //llog.d(TAG, "Request getHeaderFields");
      for (Map.Entry<String, List<String>> entries : c.getHeaderFields().entrySet()) {
        String mykey = entries.getKey();
        if (mykey != null) {
          //llog.d(TAG, mykey);
          //for (String value : entries.getValue()) {
          //  headerfields += " " + mykey + "=" + value;
          //}
          if (mykey.equals("Dynamic")) {
            expand = false;
            // inutile si on part d'un fichier on expand
            //llog.d(TAG, "Dynamic found -------------------");
          }
          if (mykey.equals("Randomize")) {
            randomize = true;
            // inutile si on part d'un fichier on expand
            //llog.d(TAG, "Randomize found -------------------");
          }
          if (mykey.equals("Preferthis")) {
            for (String value : entries.getValue()) {
              preferthis = Integer.parseInt(value);
            }
            llog.d(TAG, "------------ Preferthis " + preferthis);
          }
        }
      }

      BufferedReader in = new BufferedReader(new InputStreamReader(c.getInputStream()));
      String line;
      StringBuffer responseStringBuffer = new StringBuffer();
      while ((line = in.readLine()) != null) {
        responseStringBuffer.append(line);
      }
      in.close();
      responseData = responseStringBuffer.toString();
      Gallery.isConnectedToInternet = true;
    } catch (IOException e) {
      Gallery.isConnectedToInternet = false;
      llog.d(TAG, "downloadlist error! " + e.getMessage());
    }
    
    if (responseData == null) {
      llog.d(TAG, "responseData == null");
      return false;
    }
  
    JSONArray reader = null;
    try {
      reader = new JSONArray(responseData);
    } catch (JSONException e) {
      e.printStackTrace();
    }
  
    if (reader == null) {
      llog.d(TAG, "reader == null");
      return false;
    }
    
    int responseDatal = reader.length();
    if (responseDatal <= 0) {
      llog.d(TAG, "empty folder responseDatal = "+responseDatal);
      return false;
    } else if (responseDatal > 0) {
      List<Media> listmediaaddd = new ArrayList<>();
      int uidfichier = 1000000000;
      int uiddossier = 1000000000;
      int nfich = 0;
      int ndoss = 0;
      List<Media> listmediaadd = new ArrayList<>();
      listmediaaddd.clear();
      String dernierdossier = "";
      for (int i = 0 ; i < responseDatal ; i++) {
        JSONObject objet = null;
        try {
          objet = reader.getJSONObject(i);
          if (objet.has("d")) {
            if (objet.has("c") && objet.has("l")) {
              String folder = objet.getString("d");
              String link = objet.getString("l");
              //llog.d(TAG, "folder " + folder);
              Media media = new Media();
              media.printName = folder;
              media.ordnerPrintName = onaffichecesiteinternet;
              media.addressToGetLibextractorsThumbnail = uidfichier;
              media.address = link;
              media.addressToGetPreviewFullSize = link + "&b=y";
              media.addressToGetThumbnail = link + "&t=y";
              media.ordnerAddress = onaffichecesiteinternet;
              media.isOnline = Media.online_json;
              media.isALinkThatCreatesAnOrdner = true;
              if (expand) {
                listmediaadd.add(media);
              } else {
                listmediaaddd.add(media);
              }
              ndoss += 1;
            }
          } else if (objet.has("f")) {
            if (objet.has("lp") && objet.has("lt") && objet.has("lv")) {
              String linkp = objet.getString("lp");
              String linkt = objet.getString("lt");
              String linkv = objet.getString("lv");
              String file = objet.getString("f");
              //llog.d(TAG, "file " + file + " " + linkp + " " + linkt + " " + linkv);
              int estimage = -1;
              if (objet.has("p")) {
                estimage = 1;
              } else {
                estimage = 0;
              }
              if (estimage >= 0) {
                //llog.d(TAG, "link ::::::::::: " + linkv);
                Media media = new Media();
                media.printName = file;
                media.ordnerPrintName = onaffichecesiteinternet;
                media.addressToGetLibextractorsThumbnail = uidfichier;
                media.addressToGetPreviewFullSize = linkp;
                media.addressToGetThumbnail = linkt;
                media.ordnerAddress = onaffichecesiteinternet;
                media.isOnline = Media.online_json;
                if (estimage == 0) {
                  media.type = Media.media_video;
                  media.address = linkv;
                } else {
                  media.type = Media.media_picture;
                  media.address = linkp;
                }
                if (expand) {
                  listmediaadd.add(media);
                } else {
                  listmediaaddd.add(media);
                }
                nfich += 1;
              }
            }
          }
        } catch (JSONException e) {
          e.printStackTrace();
        }
      }
      
      if (expand) {
        // load files into existing folder
        // si 0 fichiers on ne supprime pas mais on ne réessaie pas pour l'instant
        int dossiersize = listmediaadd.size();
        if (randomize) { // 1 base + 4
          int nextint = Gallery.rand.nextInt(dossiersize);
          model.setFolderDefaultFile(idossier, nextint);
          //llog.d(TAG, idossier + " random picked : " + nextint + " / " + dossiersize + " : default " + model.getFolderDefaultFile(idossier));
        }
        model.setFilesOrSetNotLoadedYet(idossier, listmediaadd);
      } else {
        // dynamic navigation
        //dossier.dossier = onaffichecesiteinternet;
        //dossier.isonline = true;
        //dossier.info = myViewModel.GETJSON;
        int fichierpardefaut = 0;
        if (randomize) { // 1 base + 4
          int nextint = Gallery.rand.nextInt(nfich);
          fichierpardefaut = nextint;
          //llog.d(TAG, "random 2 picked : " + nextint + " / " + nfich);
        }
        /**
         *   ici on insère juste en-desous
         *   et on supprime d'abord tous les dossiers enfants
         */
        int inserealaposition;
        if (idossier >= 0) {
          // -1 si lancé depuis les options
          if (idossier < model.folderCount) {
            String dossierparent = model.getOrnderAddress(idossier);
            if (dossierparent != null) {
              llog.d(TAG, dossierparent + " = dossier parent on supprime après 1+" + idossier);
              // ici il faut 0,20 startswith 0,2 donc startswith 0,2,
              List<String> onsupprime = model.findFoldersStartsWith(dossierparent + ",", idossier + 1, model.folderCount);
              for (int zz = 0 ; zz < onsupprime.size() ; zz++)
                removeOrdnerFromDatabase(onsupprime.get(zz));
            }
          }
          inserealaposition = idossier + 1;
        } else {
          inserealaposition = model.folderCount;
        }
        llog.d(TAG, "inserealaposition "+inserealaposition+" /  : " + nfich + " files and " + ndoss + " folders found.");
        if (nfich <= 0 && ndoss <= 0)
          model.message(nfich + " files and " + ndoss + " folders found.");
        model.addFolder(inserealaposition, onaffichecesiteinternet,  false, fichierpardefaut, listmediaaddd, Media.online_json);
      }
      
    }
    
    return true;
  }
  
  public boolean retrieveonlineindexjson(String onaffichecesiteinternet){
    /**
     *     ifichier == -1  :  on récupère dans le dossier idossier
     *     idossier == -1  :  on créé une liste de dossiers
     */
    
    String responseData = null;
    llog.d(TAG, "retrieveonlineindexjson : " + onaffichecesiteinternet);
    boolean expand = true;
    
    try {
      URL urlurl = new URL(onaffichecesiteinternet);
      HttpURLConnection c = (HttpURLConnection) urlurl.openConnection();
      c.setRequestProperty("Connection", "Keep-Alive");
      c.setRequestMethod("GET");
      c.setConnectTimeout(InternetSession.connecttimeout);
      c.connect();
      /*
      for (Map.Entry<String, List<String>> entries : c.getHeaderFields().entrySet()) {
        String mykey = entries.getKey();
        if (mykey != null) {
          //llog.d(TAG, mykey);
          //for (String value : entries.getValue()) {
          //  headerfields += " " + mykey + "=" + value;
          //}
          if (mykey.equals("Dynamic")) {
            expand = false;
            llog.d(TAG, "Dynamic found -------------------");
          }
        }
      }
      */
      BufferedReader in = new BufferedReader(new InputStreamReader(c.getInputStream()));
      String line;
      StringBuffer responseStringBuffer = new StringBuffer();
      while ((line = in.readLine()) != null) {
        responseStringBuffer.append(line);
      }
      in.close();
      responseData = responseStringBuffer.toString();
      Gallery.isConnectedToInternet = true;
    } catch (IOException e) {
      Gallery.isConnectedToInternet = false;
      llog.d(TAG, "downloadlist error! " + e.getMessage());
    }
    
    if (responseData == null) {
      return false;
    }
    
    JSONArray reader = null;
    try {
      reader = new JSONArray(responseData);
    } catch (JSONException e) {
      e.printStackTrace();
    }
    
    if (reader == null) {
      return false;
    }
    
    int responseDatal = reader.length();
    if (responseDatal > 0) {
      int uidfichier = 1000000000;
      int uiddossier = 1000000000;
      int indexe = 0;
      int nfich = 0;
      int ndoss = 0;
      String dernierdossier = "";
      for (int i = 0 ; i < responseDatal ; i++) {
        JSONObject objet = null;
        try {
          objet = reader.getJSONObject(i);
          if (objet.has("d")) {
            if (objet.has("c") && objet.has("l")) {
              String folder = objet.getString("d");
              String link = objet.getString("l");
              //llog.d(TAG, "folder "+folder);
              //dossier.isonline = true;
              //dossier.info = model.GETJSON;
              //dossier.isonlinelevel = model.ISONLINELEVEL_GETJSON;
              // ici escape = folder
              model.addFolder(-1, link, false, 0, null, Media.online_json);
              ndoss += 1;
              /*
              on récupère ça quand on arrive dessus seulement
              JSONArray channel = objet.getJSONArray("c");
              int channell = channel.length();
              for (int j = 0; j < channell ; j++) {
              }
              */
            }
          } else if (objet.has("f")) {
            if (objet.has("l")) {
              String file = objet.getString("f");
              String link = objet.getString("l");
              llog.d(TAG, "file ::::::à revoir:::::::::: "+file);
              /*
              dossier.dossier = link;
              dossier.pprint = file;*/
              //dossier.isonline = true;
              //dossier.info = model.GETJSON;
              //dossier.isonlinelevel = model.ISONLINELEVEL_GETJSON;
              List<Media> listmedia = new ArrayList<>();
              int estimage = -1;
              if (objet.has("p")) {
                estimage = 1;
              } else {
                estimage = 0;
              }
              if (estimage >= 0) {
                //llog.d(TAG, "handle estimage : " + nomclean);
                Media media = new Media();
                media.printName = file;
                media.ordnerPrintName = onaffichecesiteinternet;
                media.addressToGetLibextractorsThumbnail = uidfichier;
                media.addressToGetThumbnail = link + "&t=y";
                media.ordnerAddress = onaffichecesiteinternet;
                media.isOnline = Media.online_json;
                if (estimage == 0) {
                  media.type = Media.media_video;
                  media.address = link + "&p=y";
                } else {
                  media.type = Media.media_picture;
                  media.address = link + "&b=y";
                }
                listmedia.add(media);
                nfich += 1;
              }
              // ici escape = file
              model.addFolder(-1, link, false, 0, listmedia, Media.online_json);
              ndoss += 1;
            }
          } else if (objet.has("channel_title")) {
            if (objet.has("channel_url")) {
              String channel_title = objet.getString("channel_title");
              String channel_url = objet.getString("channel_url");
              String source = "";
              if (objet.has("source")) {
                source = objet.getString("source");
              }
              String channel_user_agent = "";
              if (objet.has("channel_user_agent")) {
                channel_user_agent = objet.getString("channel_user_agent");
              }
              String date = "";
              if (objet.has("date")) {
                date = objet.getString("date");
              }
              String resume = "";
              if (objet.has("resume")) {
                resume = objet.getString("resume");
              }
              String image = "";
              if (objet.has("image")) {
                image = objet.getString("image");
              }
              String error_code = "";
              if (objet.has("error_code")) {
                error_code = objet.getString("error_code");
              }
              llog.d(TAG, channel_title+" : "+channel_url);
            }
          }
          
        } catch (JSONException e) {
          e.printStackTrace();
        }
        /*
        int estimage = -1;
        if (model.imagepattern.matcher(fnom).find()) {
          estimage = 1;
        } else if (model.videopattern.matcher(fnom).find()) {
          estimage = 0;
        }
        if (estimage >= 0) {
          //llog.d(TAG, "handle : " + fnom);
          int dossierf = fnom.lastIndexOf('/');
          String dnom = onaffichecesiteinternet + fnom.substring(1, dossierf); // ./ghfgh
          fnom = dnom + "/" + fnom.substring(dossierf + 1);
          media media = new media();
          media.fichierprettyprint = fnom;
          media.dossierprettyprint = dnom;
          //llog.d(TAG, "add new online file : " + fnom);
          dnom = encodeURIcomponent(dnom);
          fnom = encodeURIcomponent(fnom);
          //llog.d(TAG, "" + fnom);
          if (!dernierdossier.equals(dnom)) {
            dernierdossier = dnom;
            // au début on ajoute le dossier à la liste de dossiers
            //llog.d(TAG, "add new online folder : " + dnom);
            dossier.dossier = dnom;
            dossier.nfichier = -1;
            dossier.isonline = true;
            dossier.archive = false;
            listdossier.add(dossier);
            listmedia.add(new ArrayList<>());
            indexe += 1;
            ndoss += 1;
          }
          media.uidfichier = uidfichier;
          media.uiddossier = uiddossier;
          media.fichier = fnom;
          media.dossier = dnom;
          media.isonline = true;
          if (estimage == 0) {
            media.ispicture = false;
            media.isvideo = true;
            media.miniaturenom = fnom + ".mm";
          } else {
            media.ispicture = true;
            media.isvideo = false;
            media.miniaturenom = fnom + ".mm";
          }
          media.isarchived = false;
          media.bitmapselected = false;
          media.wallpaperselected = false;
          media.lockscreenselected = false;
          listmedia.get(indexe - 1).add(media);
          nfich += 1;
        }
         */
      }
      if (nfich <= 0 && ndoss <= 0) {
        model.message(nfich + " files and " + ndoss + " folders found");
      }

      model.redrawsplitviewbitmapm = true;
    }
    
    return true;
  }



  public boolean retrieveonlineindexcollection(String subCollectionAddress, String main, int cedossier, int cefichier){
    //llog.d(TAG, "retrieveonlineindexcollection " + subCollectionAddress + " cedossier " + cedossier +  " cefichier " + cefichier);
    if (cedossier != -1 && cefichier != -1) {
      boolean isonlinelink = model.getMediaIsALinkThatCreatesAnOrdner(cedossier, cefichier);
      if (isonlinelink) {
        String ordneraddress = model.getOrnderAddress(cedossier);
        llog.d(TAG, "retrieveonlineindexcollection isonlinelink ordneraddress " + ordneraddress + " fileaddress " + subCollectionAddress);
        for (Collection collection : model.collections) {
          if (collection.address != null) {
            if (collection.loadedByFileInFolderAddress != null) {
              if (collection.loadedByFileInFolderAddress.equals(ordneraddress)) {
                if (collection.elementList.size() > 0) {
                  collection.selectedCount = 0;
                  collection.elementList.clear();
                  collection.displayedOrdnerList.clear();
                }
              }
            }
          }
        }
      }
    }
    Collection collection = null;
    try {
      URL urlurl = new URL(subCollectionAddress);
      HttpURLConnection c = (HttpURLConnection) urlurl.openConnection();
      c.setRequestProperty("Connection", "Keep-Alive");
      c.setRequestMethod("GET");
      c.setConnectTimeout(InternetSession.connecttimeout);
      int rcode = c.getResponseCode();
      if (rcode >= 200 && rcode <= 310) {
        collection = model.getCollection(subCollectionAddress);
        if (collection == null) {
          //llog.d(TAG, "retrieveonlineindexcollection create new collection " + subCollectionAddress);
          String printName = Gallery.colladdresstoname(subCollectionAddress);
          collection = new Collection(subCollectionAddress, printName, model);
        }// else {
        //  llog.d(TAG, "retrieveonlineindexcollection reload existing collection " + subCollectionAddress + " master " + collection.masterCollection);
        //}
        c.connect();
        BufferedReader in = new BufferedReader(new InputStreamReader(c.getInputStream()));
        collection.loadFromDiscSource(in, model);
        in.close();
        collection.isOnline = Media.online_apache;
        model.collections.add(collection);
      } else {
        llog.d(TAG, "retrieveonlineindexcollection responsecode wrong " + subCollectionAddress);
        model.message("HTTP " + rcode + "\n" + subCollectionAddress);
      }
    } catch (IOException e) {
      //llog.d(TAG, "retrieveonlineindexcollection downloadlist error! " + e.getMessage());
      model.message(e.getMessage() + "\n" + subCollectionAddress);
      e.printStackTrace();
    }

    if (collection != null) {
      if (collection.masterCollection == null) {
        llog.d(TAG, "retrieveonlineindexcollection error mastercollection = null " + subCollectionAddress);
      } else {
        int currid = 0;
        int posadd = model.folderCount;
        if (!subCollectionAddress.equals(collection.masterCollection) && cedossier != -1)
          posadd = cedossier;

        posadd = model.collectionsToDisplay(collection.masterCollection, posadd);

        if (subCollectionAddress.equals(collection.masterCollection)) {
          llog.d(TAG, subCollectionAddress + " add collection jump to folder " + posadd);
          model.changeBigPicture(currid, posadd, 0, -1, 0, true, false);
        }
        try {
          model.commandethreadbrowser.put(new String[]{"-1", "update"});
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }

    return true;
  }

  /*private boolean isNetworkConnected(Context context) {
    ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
  }*/

  /*private LinkedBlockingQueue<String[]> commandeupload = new LinkedBlockingQueue<>();
  private void uploadethread(String dstfolder, boolean move){
  }*/

  private void reinitialize(){

    int appl = model.allappinfo.size();
    ArrayList<AppInfo> unsorted = new ArrayList<>();

    // scan active packages to update data
    model.packageManager = model.activitycontext.getPackageManager();
    List<PackageInfo> scannedpackages = model.packageManager.getInstalledPackages(0);
        /*
        PackageManager.GET_PERMISSIONS
        & PackageManager.GET_SERVICES
        & PackageManager.GET_ACTIVITIES
        & PackageManager.GET_PROVIDERS
        & PackageManager.GET_DISABLED_COMPONENTS
        & PackageManager.GET_RECEIVERS
         */
    int scannedpackagesl = scannedpackages.size();
    for (int i = 0; i < scannedpackagesl ; i++) {

      PackageInfo packageInfo = scannedpackages.get(i);
      String packagename = packageInfo.packageName;

      //ComponentName componentName = new ComponentName(packagename, packageInfo.);
      //int componentEnabledSetting =  model.packageManager.getComponentEnabledSetting(componentName);

      //model.packageManager.getComponentEnabledSetting(ComponentInfo);

      int presentdansdatabase = -1;
      int databaseappsl = model.allappinfo.size();
      for (int j = 0 ; j < databaseappsl ; j++) {
        if (model.allappinfo.get(j).packagename.equals(packagename)) {
          presentdansdatabase = j;
          break;
        }
      }

      AppInfo myappinfo;
      if (presentdansdatabase != -1) {
        myappinfo = model.allappinfo.get(presentdansdatabase);
      } else {
        myappinfo = new AppInfo();
      }

      //myappinfo.appicon = Bitmap.createBitmap(model.thumbsize, model.thumbsize, Bitmap.Config.ARGB_8888);
      //Canvas canvas = new Canvas(myappinfo.appicon);
      //Drawable drawable = model.packageManager.getApplicationIcon(packageInfo.applicationInfo);
      //drawable.setBounds(0, 0, model.thumbsize, model.thumbsize);
      //drawable.draw(canvas);
      myappinfo.appname = model.packageManager.getApplicationLabel(packageInfo.applicationInfo).toString();
      myappinfo.packagename = packagename;
      myappinfo.versionname = packageInfo.versionName;
      myappinfo.targetsdkversion = Integer.toString(packageInfo.applicationInfo.targetSdkVersion);
      myappinfo.sourcedir = packageInfo.applicationInfo.sourceDir;
      myappinfo.firstinstall = packageInfo.firstInstallTime;
      myappinfo.lastupdate = packageInfo.lastUpdateTime;
      //if (myappinfo.statuscurrent == isDISABLED) {
      //  Log.d(TAG, " --------------------------- " + packagename);
      //}
      //myappinfo.features = getFeatures(packageInfo.reqFeatures);
      //myappinfo.permission = getPermissions(packageInfo.requestedPermissions);
      if (myappinfo.permissionl <= 0) {
        PermissionInfo[] permissions = null;
        try {
          permissions = model.packageManager.getPackageInfo(packagename, PackageManager.GET_PERMISSIONS).permissions;
        } catch (PackageManager.NameNotFoundException e) {
          e.printStackTrace();
        }
        if (permissions != null) {
          myappinfo.permissionl = permissions.length;
          //Log.d(TAG, " -> "+packagename+" "+permissions.length);
          myappinfo.permissionname = new String[myappinfo.permissionl];
          myappinfo.permissionstatuscurrent = new int[myappinfo.permissionl];
          myappinfo.permissionstatuswanted = new int[myappinfo.permissionl];
          for (int ii = 0; ii < myappinfo.permissionl; ii++) {
            //Log.d(TAG, " -> "+permissions[ii].name);
            myappinfo.permissionname[ii] = permissions[ii].name;
          }
        } else {
          myappinfo.permissionl = 0;
          myappinfo.permissionname = new String[0];
          myappinfo.permissionstatuscurrent = new int[0];
          myappinfo.permissionstatuswanted = new int[0];
        }
      }
      //Log.d(TAG, "on écrit les services pour "+packagename);
      ServiceInfo[] services = null;
      try {
        services = model.packageManager.getPackageInfo(packagename, PackageManager.GET_SERVICES).services;
      } catch (PackageManager.NameNotFoundException e) {
        e.printStackTrace();
      }
      if (myappinfo.servicel > 0) {
        if (services != null) {
          for (int jj = 0; jj < myappinfo.servicel; jj++)
            myappinfo.servicestatuscurrent[jj] = ShellExecuter.isUNKNOWN;
          for (int ii = 0; ii < services.length ; ii++) {
            for (int jj = 0; jj < myappinfo.servicel; jj++) {
              if (myappinfo.servicename[jj].equals(services[ii].name)) {
                if (!services[ii].isEnabled()) {
                  myappinfo.servicestatuscurrent[jj] = ShellExecuter.isDISABLED;
                }
                break;
              }
            }
          }
        }
      } else if (services != null) {
        myappinfo.servicel = services.length;
        myappinfo.servicename = new String[myappinfo.servicel];
        myappinfo.servicestatuscurrent = new int[myappinfo.servicel];
        myappinfo.servicestatuswanted = new int[myappinfo.servicel];
        for (int ii = 0; ii < myappinfo.servicel; ii++) {
          myappinfo.servicename[ii] = services[ii].name;
          //Log.d(TAG, packagename + " - " + myappinfo.servicename[ii]);
          if (!services[ii].isEnabled()) {
            myappinfo.servicestatuscurrent[ii] = ShellExecuter.isDISABLED;
            //llog.d(TAG, packagename + " : " + myappinfo.servicename[ii] + " service isDISABLED");
          }
        }
      } else {
        myappinfo.servicel = 0;
        myappinfo.servicename = new String[0];
        myappinfo.servicestatuscurrent = new int[0];
        myappinfo.servicestatuswanted = new int[0];
      }
      //Log.d(TAG, "on écrit les activités pour "+packagename);
      if (myappinfo.activityl <= 0) {
        ActivityInfo[] activities = null;
        try {
          activities = model.packageManager.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES).activities;
        } catch (PackageManager.NameNotFoundException e) {
          e.printStackTrace();
        }
        if (activities != null) {
          myappinfo.activityl = activities.length;
          myappinfo.activityname = new String[myappinfo.activityl];
          myappinfo.activitystatuscurrent = new int[myappinfo.activityl];
          myappinfo.activitystatuswanted = new int[myappinfo.activityl];
          for (int ii = 0; ii < myappinfo.activityl; ii++) {
            myappinfo.activityname[ii] = activities[ii].name;
          /*
          if (activities[ii].isEnabled()) {
            myappinfo.activitystatuscurrent[ii] = isENABLED;
          } else {
            myappinfo.activitystatuscurrent[ii] = isDISABLED;
          }
          */
          }
        } else {
          myappinfo.activityl = 0;
          myappinfo.activityname = new String[0];
          myappinfo.activitystatuscurrent = new int[0];
          myappinfo.activitystatuswanted = new int[0];
        }
      }

      if (presentdansdatabase == -1) {
        if (appl <= 0)
          model.allappinfo.add(myappinfo);
        else
          unsorted.add(myappinfo);
      }
    }

    if (appl <= 0) {
      int appinfol = unsorted.size();
      String[] sorted = new String[appinfol];
      for (int i = 0; i < appinfol; i++) {
        sorted[i] = unsorted.get(i).packagename;
      }
      Arrays.sort(sorted);
      for (int i = 0; i < appinfol; i++) {
        for (int j = 0; j < appinfol; j++) {
          AppInfo appinfo = unsorted.get(j);
          if (appinfo.packagename.equals(sorted[i])) {
            model.allappinfo.add(appinfo);
            //llog.d(TAG, appinfo.packagename + " " + appinfo.appname + " " + appinfo.versionname);
            break;
          }
        }
      }
    }


  }


}















































