package la.daube.photochiotte;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.os.Build;
import android.text.format.DateFormat;
import android.view.Surface;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class ThreadDrawing implements Runnable {
    private static final String TAG = "YYYtdrw";

    private long[] tempsinit = new long[30];
    private long[] tempsdiff = new long[30];
    private int[] tempscompte = new int[30];

    private Surf mysurf;
    private int currid = -1;

    private int backgroundcolor = Color.BLACK;

    private boolean clickquicknav = false;
    private boolean clickquicknavmenu = false;
    private boolean clicktohandle = false;
    private float clicktohandlex = 0.0f;
    private float clicktohandley = 0.0f;

    private Gallery model;

    private float bpx = 0;
    private float bpy = 0;
    private float bscale = 0;
    private int rotation = 0;

    private Bitmap bg = null;
    private Bitmap pickcolor = null;
    private Bitmap menusize = null;
    private Bitmap bezier = null;
    private Bitmap select = null;
    private Bitmap recycle = null;
    private Bitmap save = null;
    private Bitmap addcanvas = null;
    private Bitmap addpic = null;
    private Bitmap change = null;
    private Bitmap contour = null;
    private Bitmap graph = null;
    private Bitmap contrast = null;
    private Bitmap draw = null;
    private Bitmap duplicate = null;
    private Bitmap erase = null;
    private Bitmap fill = null;
    private Bitmap hand = null;
    private Bitmap layers = null;
    private Bitmap quit = null;
    private Bitmap stylus = null;
    private Bitmap undo = null;
    private Bitmap zoom = null;
    private Bitmap drawbgspot = null;
    private Bitmap drawchangethickness = null;
    private Bitmap drawduplicate = null;
    private Bitmap drawline = null;
    private Bitmap drawmove = null;
    private Bitmap drawmovepoint = null;
    private Bitmap drawmoveto = null;
    private Bitmap drawradialfill = null;
    private Bitmap drawselectmultiple = null;
    private Bitmap drawselectone = null;
    private Bitmap drawtraceremove = null;
    private Bitmap drawchangecolor = null;
    private Bitmap drawchangedown = null;
    private Bitmap drawchangeup = null;
    private Bitmap drawchangeresize = null;
    private Bitmap drawchangetype = null;

    private boolean selectiona = false;
    private boolean selectionz = false;
    private int selectionadoss = -1;
    private int selectionafich = -1;
    private int selectionzdoss = -1;
    private int selectionzfich = -1;
    private boolean selectionplus = false;
    private boolean selectionmoins = false;
    private boolean alloxdeletefile = false;

    private volatile long POLL_TIMEOUT = Gallery.polltimeoutdefault / 5;
    private String[] neworder = null;

    protected ThreadDrawing(Gallery mmodel) {
        model = mmodel;
    }

    boolean checkready() {
        for (int i = 0; i < 1000; i++) {
            if (model.dossierminiature != null
                    && model.surf.size() > 0)
                return true;
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (i % 50 == 0) {
                llog.d(TAG, "error checkready() " + i);
            }
        }
        return false;
    }

    @Override
    public void run() {
        model.threaddrawingrunning = true;
        llog.d(TAG, "++++++++++++++++++++ ThreadDrawing");
        if (!checkready()) {
            llog.d(TAG, "error checkready bail out");
            model.threaddrawingrunning = false;
            return;
        }

        //boolean nextpictureposition0 = false;
        boolean updateonefinaltime = false;

        while (model.surf.size() <= 0) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        currid = 0;
        int surfl = model.surf.size();
        for (int i = 0 ; i < surfl ; i++) {
            if (model.surf.get(i).fragmenttype == Surf.FRAGMENT_TYPE_DRAWING) {
                currid = i;
                break;
            }
        }
        mysurf = model.surf.get(currid);

        mysurf.drawtracecurrcolorh = model.preferences.getFloat("drawtracecurrcolorh", mysurf.drawtracecurrcolorh);
        mysurf.drawtracecurrcolors = model.preferences.getFloat("drawtracecurrcolors", mysurf.drawtracecurrcolors);
        mysurf.drawtracecurrcolorv = model.preferences.getFloat("drawtracecurrcolorv", mysurf.drawtracecurrcolorv);
        mysurf.drawtracecurrcolora = model.preferences.getFloat("drawtracecurrcolora", mysurf.drawtracecurrcolora);
        mysurf.drawtracecurrsizemin = model.preferences.getFloat("drawtracecurrsizemin", mysurf.drawtracecurrsizemin);
        mysurf.drawtracecurrsizemax = model.preferences.getFloat("drawtracecurrsizemax", mysurf.drawtracecurrsizemax);

        drawmenuimageload();



        while (true) {
            boolean encore = true;
            while (encore) {

                // on vide la liste
                if (model.commandethreaddrawing.isEmpty() && updateonefinaltime) {
                    /**
                     *   on rafraîchit l'écran une dernière fois
                     *   tout à la fin si jamais on est disponible
                     *   car les options sont modifiées en même temps que l'on dessine
                     *   et on risque donc de ne pas avoir la version finale de l'écran
                     */
                    updateonefinaltime = false;
                    neworder = new String[]{"-1", "update"};
                    // on ne modifie pas myid on reste sur le précédent
                } else {
                    try {
                        neworder = model.commandethreaddrawing.poll(POLL_TIMEOUT, TimeUnit.MILLISECONDS);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    updateonefinaltime = false;
                }

                if (neworder == null) {
                    neworder = new String[]{"-1", "poll"};
                    surfl = model.surf.size();
                    for (int i = 0; i < surfl; i++) {
                        currid = (currid + 1) % surfl;
                        mysurf = model.surf.get(currid);
                        if (mysurf.fragmenttype == Surf.FRAGMENT_TYPE_DRAWING)
                            break;
                    }
                }
                int targetid = Integer.parseInt(neworder[0]);
                if (targetid != -1 && targetid < model.surf.size()) {
                    if (targetid != currid)
                        encore = false;
                    currid = targetid;
                    mysurf = model.surf.get(currid);
                }

                if (neworder[1].equals("quit")) {
                    /*if (neworder.length > 2)
                        if (neworder[2].equals("threaddrawingoff"))
                            model.threaddrawingon = false;*/
                    if (model.threaddrawingon) {
                        llog.d(TAG, "////////////////////// thread drawing keep running");
                    } else {
                        llog.d(TAG, "---------------------- thread drawing");
                        drawmenuimageloadrecycle();
                        model.threaddrawingrunning = false;
                        return;
                    }

                } else if (!model.threaddrawingrunning) {
                    llog.d(TAG, "++++++++++++++++++++++!model.threaddrawingrunning");
                    // si on pause on efface toutes les commandes moins importantes suivantes dans la queue

                } else if (neworder[1].equals("closedrawing")) { // called by closing fragment
                    llog.d(TAG, "++++++++++++++++++++++closedrawing");
                    if (mysurf.drawingcurrfichier != null) {
                        File file = new File(mysurf.drawingcurrfichier + ".ldpc");
                        if (file.exists()) {
                            file.delete();
                            llog.d(TAG, "--------------------closedrawing deleted " + mysurf.drawingcurrfichier + ".ldpc");
                        }
                        file = new File(mysurf.drawingcurrfichier + ".png");
                        if (file.exists()) {
                            file.delete();
                            llog.d(TAG, "--------------------closedrawing deleted " + mysurf.drawingcurrfichier + ".png");
                        }
                    }
                    if (neworder[2].equals("save")) {
                        llog.d(TAG, "--------------------closedrawing writelastdrawing because " + mysurf.drawingcurrfichier + " != " + mysurf.mediaIndexAddress + " " + neworder[2] + " " + alloxdeletefile);
                        writelastdrawing();
                    } else {
                        llog.d(TAG, "--------------------closedrawing donotwritelast because " + mysurf.drawingcurrfichier + " != " + mysurf.mediaIndexAddress + " " + neworder[2] + " " + alloxdeletefile);
                        model.message("removed " + mysurf.drawingcurrfichier + ".png");
                    }

                    int listecouchel = mysurf.listecouche.size();
                    for (int i = 0; i < listecouchel; i++) {
                        if (mysurf.listecouche.get(i).currentbitmap != null) {
                            if (!mysurf.listecouche.get(i).currentbitmap.isRecycled()) {
                                mysurf.listecouche.get(i).currentbitmap.recycle();
                            }
                        }
                        if (mysurf.listecouche.get(i).originalbitmap != null) {
                            if (!mysurf.listecouche.get(i).originalbitmap.isRecycled()) {
                                mysurf.listecouche.get(i).originalbitmap.recycle();
                            }
                        }
                    }
                    mysurf.listecouche.clear();
                    mysurf.listedessin.clear();
                    llog.d(TAG, "--------------------closedrawing clear couches");

                    if (mysurf.drawpilebitmap != null) {
                        if (!mysurf.drawpilebitmap.isRecycled()) {
                            mysurf.drawpilebitmap.recycle();
                        }
                        mysurf.drawpilebitmap = null;
                    }
                    if (mysurf.drawtoptempbitmap != null) {
                        if (!mysurf.drawtoptempbitmap.isRecycled()) {
                            mysurf.drawtoptempbitmap.recycle();
                        }
                        mysurf.drawtoptempbitmap = null;
                    }
                    llog.d(TAG, "--------------------closedrawing recycle bitmaps");

                } else if (neworder[1].equals("surfacechanged")) {
                    llog.d(TAG, "++++++++++++++++++++++surfacechanged");
                    if (mysurf.drawpilebitmap != null) {
                        if (!mysurf.drawpilebitmap.isRecycled()) {
                            mysurf.drawpilebitmap.recycle();
                        }
                        mysurf.drawpilebitmap = null;
                    }
                    if (mysurf.drawtoptempbitmap != null) {
                        if (!mysurf.drawtoptempbitmap.isRecycled()) {
                            mysurf.drawtoptempbitmap.recycle();
                        }
                        mysurf.drawtoptempbitmap = null;
                    }
                    try {
                        model.commandethreaddrawing.put(new String[]{String.valueOf(currid), "initialize", mysurf.mediaIndexAddress});
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    updateonefinaltime = true;
                    encore = false;

                } else if (neworder[1].equals("initialize")) {
                    llog.d(TAG, "++++++++++++++++++++++initialize");

                    if (mysurf.drawtoptempbitmap == null) {
                        mysurf.drawtoptempbitmap = Bitmap.createBitmap(mysurf.ScreenWidth, mysurf.ScreenHeight, Bitmap.Config.ARGB_8888);
                        mysurf.drawtoptempcanvas = new Canvas(mysurf.drawtoptempbitmap);
                    }
                    if (mysurf.drawpilebitmap == null) {
                        mysurf.drawpilebitmap = Bitmap.createBitmap(mysurf.ScreenWidth, mysurf.ScreenHeight, Bitmap.Config.ARGB_8888);
                        mysurf.drawpilecanvas = new Canvas(mysurf.drawpilebitmap);
                        llog.d(TAG, "++++++++++++++++++++++initialize create bitmaps");
                    }

                    int couchel = mysurf.listecouche.size();
                    if (couchel == 0) {
                        if (!neworder[2].equals("startblank")) {
                            llog.d(TAG, "++++++++++++++++++++++initialize read " + mysurf.mediaIndexAddress);
                            readlastdrawing(mysurf.mediaIndexAddress);
                        }
                    }

                    couchel = mysurf.listecouche.size();
                    if (couchel == 0) {
                        llog.d(TAG, "++++++++++++++++++++++initialize start blank");
                        startblankdrawing();
                    }

                    couchel = mysurf.listecouche.size();
                    int dessinl = mysurf.listedessin.size();
                    if (couchel > 1 || dessinl > 0) {
                        boolean atleastonein = false;
                        float minx = 0;
                        float miny = 0;
                        for (int c = 0; c < couchel && !atleastonein; c++) {
                            Couche cettecouche = mysurf.listecouche.get(c);
                            float monzoom = cettecouche.scale;
                            float originx = cettecouche.x / monzoom;
                            float originy = cettecouche.y / monzoom;
                            if (cettecouche.isaphoto) {
                                if (originx < minx || (c == 0))
                                    minx = originx;
                                if (originy < miny || (c == 0))
                                    miny = originy;
                            } else {
                                int nbdetrace = mysurf.listedessin.size();
                                for (int i = 0; i < nbdetrace && !atleastonein; i++) {
                                    if (c == mysurf.listedessin.get(i).couche) {
                                        int nbdepoint = mysurf.listedessin.get(i).x.length;
                                        for (int j = 0; j < nbdepoint; j++) {
                                            float x = mysurf.listedessin.get(i).x[j] / monzoom;
                                            float y = mysurf.listedessin.get(i).y[j] / monzoom;
                                            if (x >= 0 && x < mysurf.mywidth && y >= 0 && y < mysurf.myheight)
                                                atleastonein = true;
                                            if (x < minx || (i == 0 && c == 0))
                                                minx = mysurf.listedessin.get(i).x[j] / monzoom;
                                            if (y < miny || (i == 0 && c == 0))
                                                miny = mysurf.listedessin.get(i).y[j] / monzoom;
                                        }
                                    }
                                }
                            }
                        }
                        if (!atleastonein) {
                            llog.d(TAG, "++++++++++++++++++++++initialize !atleastonein");
                            recreecanvaspoursauvegarde(0, null);
                        }
                    }

                    alloxdeletefile = false;
                    if (mysurf.drawingcurrfichier != null && mysurf.mediaIndexAddress != null) {
                        if (mysurf.mediaIndexAddress.equals(mysurf.drawingcurrfichier + ".png")) {
                            alloxdeletefile = true;
                        }
                    }

                    for (int i = 0; i < couchel; i++) {
                        if (mysurf.listecouche.get(i).isaphoto) {
                            recreephoto(i);
                        } else {
                            recreecanvasenzero(i, mysurf.ScreenWidth, mysurf.ScreenHeight);
                        }
                    }
                    encore = false;

                } else if (neworder[1].equals("poll")) {

                    // ne pas dépendre de currid ici ça peut venir sans currid
                    if (mysurf.fragmenttype != Surf.FRAGMENT_TYPE_DRAWING)
                        continue;

                    if (mysurf.selectedtrace == Gallery.selectedtracemoveto || selectmultiple) {
                        encore = false;
                    } else {
                        continue;
                    }

                } else if (neworder[1].equals("trace")) {

                    /**
                     *  on sauvegarde toutes les traces
                     *  mais il faut corriger avec
                     *  - décalage
                     *  / zoom
                     *  float[] pss = model.drawpossizescale.get(0);
                     */
                    int cettelayer;
                    if (mysurf.drawcurrlayer == -1) {
                        cettelayer = 0;
                    } else {
                        cettelayer = mysurf.drawcurrlayer;
                    }

                    float onscreentracepx = Float.parseFloat(neworder[3]);
                    float onscreentracepy = Float.parseFloat(neworder[4]);
                    float tracepx = (onscreentracepx - mysurf.listecouche.get(cettelayer).x) / mysurf.listecouche.get(cettelayer).scale;
                    float tracepy = (onscreentracepy - mysurf.listecouche.get(cettelayer).y) / mysurf.listecouche.get(cettelayer).scale;
                    // llog.d(TAG, "nouveau point en xy "+tracepx+" "+tracepy+" ; Oxy "+pss[0]+" "+pss[1]+" zoom "+pss[4]+" ; "+neworder[3]+" "+neworder[4]);
                    float tracepp = Float.parseFloat(neworder[5]);
                    float tracept = Float.parseFloat(neworder[6]);
                    float tracepd = Float.parseFloat(neworder[7]);
                    mysurf.drawtracecurr.add(new float[]{tracepx, tracepy, tracepp, tracept, tracepd});
                    int tracecurrlast = mysurf.drawtracecurr.size() - 1;
                    if (neworder[2].equals("i")) {
                        // on ne fait rien on attend de voir la suite
                        model.TracePaint.setColor(Color.HSVToColor((int) mysurf.drawtracecurrcolora, new float[]{mysurf.drawtracecurrcolorh, mysurf.drawtracecurrcolors, mysurf.drawtracecurrcolorv}));
                    } else if (neworder[2].equals("f") && tracecurrlast == 1) {
                        // point on ignore le premier qui devrait être identique de toute façon car on n'a pas bougé
                        float epaisseur = mysurf.drawtracecurrsizemin;
                        if (mysurf.drawtracecurrsizemin != mysurf.drawtracecurrsizemax && model.drawtracepressuremax > 0.3f + model.drawtracepressuremin) {
                            float cettepression = mysurf.drawtracecurr.get(tracecurrlast)[2];
                            if (cettepression > model.drawtracepressuremax) {
                                cettepression = model.drawtracepressuremax;
                            }
                            if (cettepression < model.drawtracepressuremin) {
                                cettepression = model.drawtracepressuremin;
                            }
                            float sizepente = (mysurf.drawtracecurrsizemax - mysurf.drawtracecurrsizemin) / (model.drawtracepressuremax - model.drawtracepressuremin);
                            float sizeordonnee = mysurf.drawtracecurrsizemin - sizepente * model.drawtracepressuremin;
                            epaisseur = sizeordonnee + sizepente * cettepression;
                        }
                        model.TracePaint.setStrokeWidth(epaisseur);
                        // model.drawcanvas.get(0).drawPoint(tracepx, tracepy, model.TracePaint);
                        // on va de toute façon redessiner puisque c'est un f

                    } else {
                        // ligne
                        float[] tracepreced = mysurf.drawtracecurr.get(tracecurrlast - 1);
                        float epaisseur = mysurf.drawtracecurrsizemin;
                        if (mysurf.drawtracecurrsizemin != mysurf.drawtracecurrsizemax && model.drawtracepressuremax > 0.3f + model.drawtracepressuremin) {
                            float cettepression = (tracepreced[2] + tracepp) / 2.0f;
                            if (cettepression > model.drawtracepressuremax) {
                                cettepression = model.drawtracepressuremax;
                            }
                            if (cettepression < model.drawtracepressuremin) {
                                cettepression = model.drawtracepressuremin;
                            }
                            float sizepente = (mysurf.drawtracecurrsizemax - mysurf.drawtracecurrsizemin) / (model.drawtracepressuremax - model.drawtracepressuremin);
                            float sizeordonnee = mysurf.drawtracecurrsizemin - sizepente * model.drawtracepressuremin;
                            epaisseur = sizeordonnee + sizepente * cettepression;
                        }
                        model.TracePaint.setStrokeWidth(epaisseur);
                        // model.drawcanvas.get(0).drawLine(tracepreced[0], tracepreced[1], tracepx, tracepy, model.TracePaint);


                        if (!neworder[2].equals("f")) {
                            // si ce n'est pas la fin de la trace
                            float xi = tracepreced[0] * mysurf.listecouche.get(cettelayer).scale + mysurf.listecouche.get(cettelayer).x;
                            float yi = tracepreced[1] * mysurf.listecouche.get(cettelayer).scale + mysurf.listecouche.get(cettelayer).y;
                            float xf = onscreentracepx;
                            float yf = onscreentracepy;
                            mysurf.drawtoptempcanvas.drawLine(xi, yi, xf, yf, model.TracePaint);

                            if (model.commandethreaddrawing.isEmpty()) {
                                // si il n'y a pas encore d'autre points qui attendent d'être dessinés
                                drawall(true);
                            }
                        }


                    }


                    if (!neworder[2].equals("f")) {
                        /**
                         *   si ce n'est pas la trace finale
                         *   on acquiert vite les prochains points
                         */
                        updateonefinaltime = false;
                        continue;
                    } else {
                        int drawtracecurrl = mysurf.drawtracecurr.size();
                        if (drawtracecurrl > 0) {
                            // si le dernier point est identique à l'avant dernier
                            // ce qui devrait toujours être le cas
                            // on ignore le dernier point
                            // pour éviter point transparent * 2x ce qu'on voulait
                            if (drawtracecurrl > 1) {
                                drawtracecurrl -= 1;
                            }
                            int drawtracexl = mysurf.listedessin.size();
                            Dessin cedessin = new Dessin();
                            cedessin.x = new float[drawtracecurrl];
                            cedessin.y = new float[drawtracecurrl];
                            cedessin.p = new float[drawtracecurrl];
                            cedessin.t = new float[drawtracecurrl];
                            cedessin.d = new float[drawtracecurrl];
                            for (int i = 0; i < drawtracecurrl; i++) {
                                cedessin.x[i] = mysurf.drawtracecurr.get(i)[0];
                                cedessin.y[i] = mysurf.drawtracecurr.get(i)[1];
                                cedessin.p[i] = mysurf.drawtracecurr.get(i)[2];
                                cedessin.t[i] = mysurf.drawtracecurr.get(i)[3];
                                cedessin.d[i] = mysurf.drawtracecurr.get(i)[4];
                            }
                            mysurf.drawtracecurr.clear();
                            cedessin.couleura = mysurf.drawtracecurrcolora;
                            cedessin.couleurh = mysurf.drawtracecurrcolorh;
                            cedessin.couleurs = mysurf.drawtracecurrcolors;
                            cedessin.couleurv = mysurf.drawtracecurrcolorv;
                            cedessin.generecouleur();
                            cedessin.sizemax = mysurf.drawtracecurrsizemax;
                            cedessin.sizemin = mysurf.drawtracecurrsizemin;
                            cedessin.pressuremax = model.drawtracepressuremax;
                            cedessin.pressuremin = model.drawtracepressuremin;
              /*
              float sizepente = 0.0f;
              float sizeordonnee = mysurf.drawtracecurrsizemin;
              if(mysurf.drawtracecurrsizemin != mysurf.drawtracecurrsizemax && model.drawtracepressuremax > 0.3f + model.drawtracepressuremin){
                sizepente = (mysurf.drawtracecurrsizemax - mysurf.drawtracecurrsizemin) / (model.drawtracepressuremax - model.drawtracepressuremin);
                sizeordonnee = mysurf.drawtracecurrsizemin - sizepente * model.drawtracepressuremin;
              }
              */
                            cedessin.drawerasefillcontour = mysurf.drawtracecurrdefcb;
                            cedessin.bezierlength = mysurf.drawtracecurrbezier;
                            cedessin.couche = cettelayer;
                            mysurf.listedessin.add(cedessin);
                            // on dessine la nouvelle dernière trace sur sa layer
                            Canvas surfacecanvas = mysurf.listecouche.get(cettelayer).canvas;
                            dessinelatrace(drawtracexl, surfacecanvas);
                            mysurf.drawtoptempcanvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
                        }

                        // c'est la trace finale on doit faire encore un update
                        // sinon il faut attendre de toucher de nouveau l'écran pour voir la fin de la trace précédente
                        encore = false;
                    }

                } else if (neworder[1].equals("tracecompleteredraw")) {

                    int cettelayer;
                    if (mysurf.drawcurrlayer == -1) {
                        cettelayer = 0;
                    } else {
                        cettelayer = mysurf.drawcurrlayer;
                    }

                    int drawtracecurrl = mysurf.drawtracecurr.size();
                    if (drawtracecurrl > 0) {
                        // si le dernier point est identique à l'avant dernier
                        // ce qui devrait toujours être le cas
                        // on ignore le dernier point
                        // pour éviter point transparent * 2x ce qu'on voulait
                        int drawtracexl = mysurf.listedessin.size();
                        Dessin cedessin = new Dessin();
                        cedessin.x = new float[drawtracecurrl];
                        cedessin.y = new float[drawtracecurrl];
                        cedessin.p = new float[drawtracecurrl];
                        cedessin.t = new float[drawtracecurrl];
                        cedessin.d = new float[drawtracecurrl];
                        for (int i = 0; i < drawtracecurrl; i++) {
                            cedessin.x[i] = mysurf.drawtracecurr.get(i)[0];
                            cedessin.y[i] = mysurf.drawtracecurr.get(i)[1];
                            cedessin.p[i] = mysurf.drawtracecurr.get(i)[2];
                            cedessin.t[i] = mysurf.drawtracecurr.get(i)[3];
                            cedessin.d[i] = mysurf.drawtracecurr.get(i)[4];
                        }
                        mysurf.drawtracecurr.clear();
                        cedessin.couleura = mysurf.drawtracecurrcolora;
                        cedessin.couleurh = mysurf.drawtracecurrcolorh;
                        cedessin.couleurs = mysurf.drawtracecurrcolors;
                        cedessin.couleurv = mysurf.drawtracecurrcolorv;
                        cedessin.generecouleur();
                        cedessin.sizemax = mysurf.drawtracecurrsizemax;
                        cedessin.sizemin = mysurf.drawtracecurrsizemin;
                        cedessin.pressuremax = model.drawtracepressuremax;
                        cedessin.pressuremin = model.drawtracepressuremin;
              /*
              float sizepente = 0.0f;
              float sizeordonnee = mysurf.drawtracecurrsizemin;
              if(mysurf.drawtracecurrsizemin != mysurf.drawtracecurrsizemax && model.drawtracepressuremax > 0.3f + model.drawtracepressuremin){
                sizepente = (mysurf.drawtracecurrsizemax - mysurf.drawtracecurrsizemin) / (model.drawtracepressuremax - model.drawtracepressuremin);
                sizeordonnee = mysurf.drawtracecurrsizemin - sizepente * model.drawtracepressuremin;
              }
              */
                        cedessin.drawerasefillcontour = mysurf.drawtracecurrdefcb;
                        cedessin.bezierlength = mysurf.drawtracecurrbezier;
                        cedessin.couche = cettelayer;
                        mysurf.listedessin.add(cedessin);
                        // on dessine la nouvelle dernière trace sur sa layer
                        Canvas surfacecanvas = mysurf.listecouche.get(cettelayer).canvas;
                        dessinelatrace(drawtracexl, surfacecanvas);
                        mysurf.drawtoptempcanvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
                    }

                    // c'est la trace finale on doit faire encore un update
                    // sinon il faut attendre de toucher de nouveau l'écran pour voir la fin de la trace précédente
                    encore = false;


                } else if (neworder[1].equals("menudessiner")) {
                    /**
                     *    on attrape tous les menudessiner init on n'en rate jamais un seul
                     *    move on en rate pour ne pas traîner
                     */
                    updateonefinaltime = true;
                    encore = false;
                } else if (neworder[1].equals("option")) {
                    /**
                     *    on attrape tous les option on n'en rate jamais un seul
                     */
                    updateonefinaltime = true;
                    encore = false;
                } else if (neworder[1].equals("next")) {
                    /**
                     *    on attrape tous les next on n'en rate jamais un seul
                     */
                    encore = false;
                } else if (neworder[1].equals("dontmissupdate")) {
                    /**
                     *    on attrape tous les initupdate on n'en rate jamais un seul
                     */
                    encore = false;
                } else if (model.commandethreaddrawing.isEmpty() == true) {
                    /**
                     toutes les autres commandes :
                     on attend que la queue soit vide
                     et on ne prend que la dernière
                     */
                    encore = false;
                }
            }

            if (mysurf.fragmenttype != Surf.FRAGMENT_TYPE_DRAWING) {
                llog.d(TAG, currid + " fragmenttype " + mysurf.fragmenttype);
                continue;
            }

            if (mysurf.ScreenWidth < 10 || mysurf.ScreenHeight < 10 || mysurf.drawingSurfaceHolder == null) {
                llog.d(TAG, currid + " Screen too small " + mysurf.ScreenWidth + " " + mysurf.ScreenHeight + " or mysurf.drawingSurfaceHolder == null ");
                continue;
            }

            if (mysurf.drawtoptempbitmap == null || mysurf.drawpilebitmap == null) {
                llog.d(TAG, currid + " drawtoptempbitmap drawpilebitmap null");
                continue;
            }

            clicktohandle = false;
            clickquicknav = false;
            clickquicknavmenu = false;
            // pour ceux-là on attend que la liste soit vide et on ne prend que le dernier
            // on se permet donc d'en rater plein
            if (neworder[1].equals("update")) {

            } else if (neworder[1].equals("updateredrawicons")) {
                drawmenuimageloadrecycle();
                drawmenuimageload();

            } else if (neworder[1].equals("move")) {

            } else if (neworder[1].equals("rescale")) {

            } else if (neworder[1].equals("rotate")) {

            } else if (neworder[1].equals("next")) {
                float posfx = Float.parseFloat(neworder[2]);
                float posfy = Float.parseFloat(neworder[3]);

                clicktohandle = true;
                clicktohandlex = posfx;
                clicktohandley = posfy;
            }

            long tempsinitial = System.currentTimeMillis();

            int drawbitmapl = mysurf.listecouche.size();
            int drawbitmaplast = drawbitmapl - 1;
            mysurf.drawpilecanvas.drawColor(backgroundcolor);
            for (int k = drawbitmaplast; k >= 0; k--) {
                if (mysurf.listecouche.get(k).currentbitmap != null) {
                    boolean scaled = scalebitmaponcanvas(mysurf.listecouche.get(k).x, mysurf.listecouche.get(k).y, mysurf.listecouche.get(k).originalw, mysurf.listecouche.get(k).originalh, mysurf.listecouche.get(k).scale, mysurf.ScreenWidth, mysurf.ScreenHeight);
                    if (scaled) {
                        mysurf.drawpilecanvas.drawBitmap(mysurf.listecouche.get(k).currentbitmap, mysurf.srcrect, mysurf.dstrect, null);
                    }
                }
            }
            boolean success = dessinehandleoptiondraw(mysurf.drawpilecanvas);

            if (success)
                drawall(false);

            //long tempsdaffichage = System.currentTimeMillis() - tempsinitial;
      /*if (tempsdaffichage < 30) { // on a 16ms en moyenne on peut ralentir le bordel
        try {
          Thread.sleep(30-tempsdaffichage);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }*/

        }
    }



    private void drawall(boolean toptemp) {

        mysurf.surfaceIsCurrentlyDrawing = true;
        if (mysurf.mysurfacestopdrawing) {
            mysurf.surfaceIsCurrentlyDrawing = false;
            return;
        }
        if (mysurf.drawingSurfaceHolder == null) {
            mysurf.surfaceIsCurrentlyDrawing = false;
            return;
        }
        Surface surface = mysurf.drawingSurfaceHolder.getSurface();
        if (surface == null) {
            mysurf.surfaceIsCurrentlyDrawing = false;
            return;
        }
        if (!surface.isValid()) {
            mysurf.surfaceIsCurrentlyDrawing = false;
            return;
        }
        Canvas canv = null;
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                canv = mysurf.drawingSurfaceHolder.lockHardwareCanvas();
            } else {
                canv = mysurf.drawingSurfaceHolder.lockCanvas();
            }
        } catch (Exception e) {
            llog.d(TAG, "IllegalStateException " + e.toString());
            e.printStackTrace();
            canv = null;
            return;
        }
        if (canv == null) {
            mysurf.surfaceIsCurrentlyDrawing = false;
            return;
        }

        canv.drawBitmap(mysurf.drawpilebitmap, 0, 0, null);
        if (toptemp)
            canv.drawBitmap(mysurf.drawtoptempbitmap, 0, 0, null);

        try {
            mysurf.drawingSurfaceHolder.unlockCanvasAndPost(canv);
        } catch (Exception e) {
            llog.d(TAG, " drawall unlockCanvasAndPost " + e.toString());
        }
        mysurf.surfaceIsCurrentlyDrawing = false;
    }


    private boolean scalebitmaponcanvas(float ix0, float iy0, float iw, float ih, float scale, int screenwidth, int screenheight) {
        float ix1 = ix0 + iw * scale;
        float iy1 = iy0 + ih * scale;
        float fwidth = screenwidth;
        float fheight = screenheight;
        if (ix0 < fwidth && iy0 < fheight && ix1 >= 0 && iy1 >= 0) {
            float srcx0, srcy0, srcx1, srcy1;
            float dstx0, dsty0, dstx1, dsty1;
            if (ix0 >= 0.0f) {
                srcx0 = 0.0f;                           // dès le début
                if (ix1 <= fwidth) {
                    srcx1 = iw;                                // jusqu'à la fin
                    dstx1 = ix1;
                } else {
                    srcx1 = (fwidth - ix0) / scale;            // pas jusqu'à la fin
                    dstx1 = fwidth;
                }
                dstx0 = ix0;                                      // on placera en ix0
            } else {
                srcx0 = -ix0 / scale;                  // pas depuis le début
                if (ix1 <= fwidth) {
                    srcx1 = iw;                                // jusqu'à la fin
                    dstx1 = ix1;
                } else {
                    srcx1 = srcx0 + fwidth / scale;            // pas juqu'à la fin
                    dstx1 = fwidth;
                }
                dstx0 = 0.0f;                                      // on placera en 0
            }
            if (iy0 >= 0.0f) {
                srcy0 = 0.0f;                           // dès le début
                if (iy1 <= fheight) {
                    srcy1 = ih;                                // jusqu'à la fin
                    dsty1 = iy1;
                } else {
                    srcy1 = (fheight - iy0) / scale;            // pas jusqu'à la fin
                    dsty1 = fheight;
                }
                dsty0 = iy0;                                      // on placera en iy0
            } else {
                srcy0 = -iy0 / scale;                  // pas depuis le début
                if (iy1 <= fheight) {
                    srcy1 = ih;                                // jusqu'à la fin
                    dsty1 = iy1;
                } else {
                    srcy1 = srcy0 + fheight / scale;            // pas juqu'à la fin
                    dsty1 = fheight;
                }
                dsty0 = 0.0f;                                      // on placera en 0
            }

            mysurf.srcrect = new Rect((int) Math.floor(srcx0),
                    (int) Math.floor(srcy0),
                    (int) Math.floor(srcx1),
                    (int) Math.floor(srcy1));
            mysurf.dstrect = new RectF(dstx0, dsty0, dstx1, dsty1);
            return true;
        }
        return false;
    }

    private void dessinelatrace(int i, Canvas surfacecanvas) {
        float x, y, ox, oy;
        int nbdepoint = mysurf.listedessin.get(i).x.length;
        if (nbdepoint > 0) {
            int cemodedesseindefc = mysurf.listedessin.get(i).drawerasefillcontour;
            model.TracePaint.setColor(mysurf.listedessin.get(i).couleur);
            float epaisseur;
            if (nbdepoint == 1) {
                model.TracePaint.setStrokeWidth(mysurf.listedessin.get(i).sizemin);
                x = mysurf.listedessin.get(i).x[0];
                y = mysurf.listedessin.get(i).y[0];
                surfacecanvas.drawPoint(x, y, model.TracePaint);
            } else if (nbdepoint == 2) {
                ox = mysurf.listedessin.get(i).x[0];
                oy = mysurf.listedessin.get(i).y[0];
                for (int j = 1; j < nbdepoint; j++) {
                    model.TracePaint.setStrokeWidth(mysurf.listedessin.get(i).sizemin);
                    x = mysurf.listedessin.get(i).x[j];
                    y = mysurf.listedessin.get(i).y[j];
                    surfacecanvas.drawLine(ox, oy, x, y, model.TracePaint);
                    ox = x;
                    oy = y;
                }
            } else {
                if (cemodedesseindefc == 0) {
                    // draw juste ce qu'on a sauvé
                    ox = mysurf.listedessin.get(i).x[0];
                    oy = mysurf.listedessin.get(i).y[0];
                    for (int j = 1; j < nbdepoint; j++) {
                        model.TracePaint.setStrokeWidth(mysurf.listedessin.get(i).sizemin);
                        x = mysurf.listedessin.get(i).x[j];
                        y = mysurf.listedessin.get(i).y[j];
                        surfacecanvas.drawLine(ox, oy, x, y, model.TracePaint);
                        ox = x;
                        oy = y;
                    }
                } else if (cemodedesseindefc == 1) {
                    // erase ce qu'on a dessiné avec un contour fermé juste ce qu'on a sauvé
                    model.TracePaint.setStyle(Paint.Style.FILL);
                    model.TracePaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
                    ox = mysurf.listedessin.get(i).x[0];
                    oy = mysurf.listedessin.get(i).y[0];
                    Path path = new Path();
                    path.moveTo(ox, oy);
                    for (int j = 1; j < nbdepoint; j++) {
                        model.TracePaint.setStrokeWidth(mysurf.listedessin.get(i).sizemin);
                        x = mysurf.listedessin.get(i).x[j];
                        y = mysurf.listedessin.get(i).y[j];
                        path.lineTo(x, y);
                        ox = x;
                        oy = y;
                    }
                    path.close();
                    surfacecanvas.drawPath(path, model.TracePaint);
                    model.TracePaint.setStyle(Paint.Style.STROKE);
                    model.TracePaint.setXfermode(null);
                } else if (cemodedesseindefc == 2) {
                    // fill juste ce qu'on a sauvé
                    model.TracePaint.setStyle(Paint.Style.FILL);
                    ox = mysurf.listedessin.get(i).x[0];
                    oy = mysurf.listedessin.get(i).y[0];
                    Path path = new Path();
                    path.moveTo(ox, oy);
                    for (int j = 1; j < nbdepoint; j++) {
                        model.TracePaint.setStrokeWidth(mysurf.listedessin.get(i).sizemin);
                        x = mysurf.listedessin.get(i).x[j];
                        y = mysurf.listedessin.get(i).y[j];
                        path.lineTo(x, y);
                        ox = x;
                        oy = y;
                    }
                    path.close();
                    path.close();
                    surfacecanvas.drawPath(path, model.TracePaint);
                    model.TracePaint.setStyle(Paint.Style.STROKE);
                } else if (cemodedesseindefc == 4) {
                    // fill radial gradient juste ce qu'on a sauvé
                    model.TracePaint.setStyle(Paint.Style.FILL);
                    float cx = 0.0f;
                    float cy = 0.0f;
                    float dx = 0.0f;
                    float sum = 0.0f;
                    ox = mysurf.listedessin.get(i).x[0];
                    oy = mysurf.listedessin.get(i).y[0];
                    cx += ox;
                    cy += oy;
                    sum += 1.0f;
                    Path path = new Path();
                    path.moveTo(ox, oy);
                    for (int j = 1; j < nbdepoint; j++) {
                        model.TracePaint.setStrokeWidth(mysurf.listedessin.get(i).sizemin);
                        x = mysurf.listedessin.get(i).x[j];
                        y = mysurf.listedessin.get(i).y[j];
                        cx += x;
                        cy += y;
                        sum += 1.0f;
                        path.lineTo(x, y);
                        ox = x;
                        oy = y;
                    }
                    path.close();
                    path.close();
                    cx /= sum;
                    cy /= sum;
                    for (int j = 0; j < nbdepoint; j++) {
                        x = mysurf.listedessin.get(i).x[j];
                        y = mysurf.listedessin.get(i).y[j];
                        if (dx < Math.abs(x - cx)) {
                            dx = Math.abs(x - cx);
                        }
                        if (dx < Math.abs(y - cy)) {
                            dx = Math.abs(y - cy);
                        }
                    }
                    int couleurcentre = mysurf.listedessin.get(i).couleur;
                    int couleurbord = Color.argb(0x00, Color.red(couleurcentre), Color.green(couleurcentre), Color.blue(couleurcentre));
                    model.TracePaint.setShader(new RadialGradient(cx, cy, dx, couleurcentre, couleurbord, Shader.TileMode.CLAMP));
                    surfacecanvas.drawPath(path, model.TracePaint);
                    model.TracePaint.setShader(null);
                    model.TracePaint.setStyle(Paint.Style.STROKE);
                } else if (cemodedesseindefc == 3) {
                    // contour fermé juste ce qu'on a sauvé
                    ox = mysurf.listedessin.get(i).x[0];
                    oy = mysurf.listedessin.get(i).y[0];
                    Path path = new Path();
                    path.moveTo(ox, oy);
                    for (int j = 1; j < nbdepoint; j++) {
                        model.TracePaint.setStrokeWidth(mysurf.listedessin.get(i).sizemin);
                        x = mysurf.listedessin.get(i).x[j];
                        y = mysurf.listedessin.get(i).y[j];
                        path.lineTo(x, y);
                        ox = x;
                        oy = y;
                    }
                    path.close();
                    surfacecanvas.drawPath(path, model.TracePaint);
                }
        /*
        mysurf.listedessin.get(i).x[0] += originx / mynewfinalscale;
        mysurf.listedessin.get(i).y[0] += originy / mynewfinalscale;
        Path path = new Path();
        path.moveTo(mysurf.listedessin.get(i).x[0], mysurf.listedessin.get(i).y[0]);
        nbdepoint -= 1;
        for (int j = 1; j < nbdepoint; j++) {
          epaisseur = model.drawtracesizeordonnee.get(i) + model.drawtracesizepente.get(i) * (model.drawtracep.get(i)[j - 1] + model.drawtracep.get(i)[j]) / 2.0f;
          model.TracePaint.setStrokeWidth(epaisseur);
          mysurf.listedessin.get(i).x[j] += originx / mynewfinalscale;
          mysurf.listedessin.get(i).y[j] += originy / mynewfinalscale;
          path.quadTo(mysurf.listedessin.get(i).x[j], mysurf.listedessin.get(i).y[j], mysurf.listedessin.get(i).x[j + 1], mysurf.listedessin.get(i).y[j + 1]);
          surfacecanvas.drawPath(path, model.TracePaint);
          path.reset();
          path.moveTo(mysurf.listedessin.get(i).x[j], mysurf.listedessin.get(i).y[j]);
        }
        path.reset();
        */
            }
        }
    }

    private void recreephoto(int cettelayer) {
        Couche cettecouche = mysurf.listecouche.get(cettelayer);
        // on sauve originalbitmapname au début ou lorsqu'on appelle getthebitmap dans la fonction
        /**
         *   si on avait déjà chargé le même fichier du disque on ne le recharge pas
         */
    /*
    ColorMatrix colormatrix = new ColorMatrix();
    if(model.drawcolormatrix.get(cettelayer)[2] != null) {
      colormatrix.setSaturation(model.drawcolormatrix.get(cettelayer)[2][0]);
    }
    if(model.drawcolormatrix.get(cettelayer)[0] != null) {
      colormatrix.postConcat(new ColorMatrix(model.drawcolormatrix.get(cettelayer)[0]));
    }
    if(model.drawcolormatrix.get(cettelayer)[1] != null) {
      colormatrix.postConcat(new ColorMatrix(model.drawcolormatrix.get(cettelayer)[1]));
    }
    Paint colorpaint = new Paint();
    colorpaint.setColorFilter(new ColorMatrixColorFilter(colormatrix));
    */

        if (cettecouche.originalbitmap == null) {
            llog.d(TAG, "error recreephoto null");
            return;
        }
        if (cettecouche.originalbitmap.isRecycled()) {
            llog.d(TAG, "error recreephoto isRecycled");
            return;
        }
        cettecouche.canvas.drawBitmap(cettecouche.originalbitmap, 0, 0, null);

        // on redessine les traces uniquement pour cette layer
        int nbdetrace = mysurf.listedessin.size();
        for (int i = 0; i < nbdetrace; i++) {
            if (cettelayer == mysurf.listedessin.get(i).couche) {
                Canvas surfacecanvas = mysurf.listecouche.get(cettelayer).canvas;
                dessinelatrace(i, surfacecanvas);
            }
        }

    }

    private void recreecanvasenzero(int cettelayer, int screenwidth, int screenheight) {
        /**
         *   on créé un nouveau bitmap en 0, 0 <-> model.ScreenWidth, model.ScreenHeight
         *   il faut corriger toutes les traces enregistrées
         *   + ( décalage / zoom )
         *   une fois redessiné avec le décalage / zoom
         *   on réinitialise drawpossizescale car c'est comme si c'était une nouvelle image
         */
        // on recréé une image à la bonne taille
        Couche cettecouche = mysurf.listecouche.get(cettelayer);
        float monzoom = cettecouche.scale;
        float originx = cettecouche.x / monzoom;
        float originy = cettecouche.y / monzoom;
        cettecouche.originalw = ((float) screenwidth) / monzoom;
        cettecouche.originalh = ((float) screenheight) / monzoom;
        //llog.d(TAG, "recreecanvasenzero " + monzoom + "x " + originx + " " + originy);

        if (cettecouche.currentbitmap != null) {
            if (!cettecouche.currentbitmap.isRecycled()) {
                cettecouche.currentbitmap.recycle();
            }
        }
        cettecouche.currentbitmap = Bitmap.createBitmap((int) cettecouche.originalw, (int) cettecouche.originalh, Bitmap.Config.ARGB_8888);
        cettecouche.canvas = new Canvas(cettecouche.currentbitmap);

        /**
         *   pour les traces de l'image de base où l'on dessine
         *   on décale tous les points
         *   donc l'image (0, 0) <-> s * (w, h)
         */
        Canvas surfacecanvas = cettecouche.canvas;
        int nbdetrace = mysurf.listedessin.size();
        for (int i = 0; i < nbdetrace; i++) {
            if (cettelayer == mysurf.listedessin.get(i).couche) {
                int nbdepoint = mysurf.listedessin.get(i).x.length;
                for (int j = 0; j < nbdepoint; j++) {
                    mysurf.listedessin.get(i).x[j] += originx;
                    mysurf.listedessin.get(i).y[j] += originy;
                }
                dessinelatrace(i, surfacecanvas);
            }
        }
        cettecouche.x = 0;
        cettecouche.y = 0;
    }

    private void recreecanvaspoursauvegarde(int cettelayer, String cefichier) {
        /**
         *   on créé un nouveau bitmap en 0, 0 <-> model.ScreenWidth, model.ScreenHeight
         *   il faut corriger toutes les traces enregistrées
         *   + ( décalage / zoom )
         *   une fois redessiné avec le décalage / zoom
         *   on réinitialise drawpossizescale car c'est comme si c'était une nouvelle image
         */
        Couche cettecouche = mysurf.listecouche.get(cettelayer);
        int couchel = mysurf.listecouche.size();
        int nbdetrace = mysurf.listedessin.size();
        float refzoom = cettecouche.scale;
        float xmin = 99999.f;
        float ymin = 99999.f;
        float xmax = -99999.f;
        float ymax = -99999.f;
        for (int i = 0; i < nbdetrace; i++) {
            cettecouche = mysurf.listecouche.get(mysurf.listedessin.get(i).couche);
            if (!cettecouche.isaphoto) {
                // si c'est une photo on ne dépasse jamais du cadre original
                // si plus petit on vire par la suite avec les pixels transparents
                int nbdepoint = mysurf.listedessin.get(i).x.length;
                for (int j = 0; j < nbdepoint; j++) {
                    float dessinx = mysurf.listedessin.get(i).x[j];
                    float dessiny = mysurf.listedessin.get(i).y[j];
                    if (dessinx < xmin) {
                        xmin = dessinx;
                    } else if (dessinx > xmax) {
                        xmax = dessinx;
                    }
                    if (dessiny < ymin) {
                        ymin = dessiny;
                    } else if (dessiny > ymax) {
                        ymax = dessiny;
                    }
                }
            }
        }
        //llog.d(TAG, "           contour traces : depuis "+xmin+","+ymin+" jusqu'à "+xmax+","+ymax);
        for (int i = 0; i < couchel; i++) {
            cettecouche = mysurf.listecouche.get(i);
            if (cettecouche.isaphoto) {
                float minix = cettecouche.x / refzoom;
                float maxix = (cettecouche.x + cettecouche.originalw * cettecouche.scale) / refzoom;
                float miniy = cettecouche.y / refzoom;
                float maxiy = (cettecouche.y + cettecouche.originalh * cettecouche.scale) / refzoom;
                //llog.d(TAG, "         "+i + " : " +cettecouche.x + "," + cettecouche.y + " " + cettecouche.originalw + "x" + cettecouche.originalh + " x" +cettecouche.scale+ " : " +minix + ", " + miniy + " -> " + maxix + ", " +maxiy);
                if (minix < xmin) {
                    xmin = minix;
                } else if (minix > xmax) {
                    xmax = minix;
                }
                if (maxix < xmin) {
                    xmin = maxix;
                } else if (maxix > xmax) {
                    xmax = maxix;
                }
                if (miniy < ymin) {
                    ymin = miniy;
                } else if (miniy > ymax) {
                    ymax = miniy;
                }
                if (maxiy < ymin) {
                    ymin = maxiy;
                } else if (maxiy > ymax) {
                    ymax = maxiy;
                }
            }
        }
        //llog.d(TAG, "           contour        : depuis "+xmin+","+ymin+" jusqu'à "+xmax+","+ymax);
        // xmin, ymin déjà corrigés sur le rapport 1.0f
        float margeenpixel = 0.0f;
        int originalw = (int) Math.ceil(xmax - xmin + 2.0f * margeenpixel);
        int originalh = (int) Math.ceil(ymax - ymin + 2.0f * margeenpixel);
        xmin -= margeenpixel;
        ymin -= margeenpixel;
        // les traces sont déjà rapportées à 1.0f sur originalbitmap size
        if (originalw > 0 && originalh > 0) {
            for (int i = 0; i < couchel; i++) {
                mysurf.listecouche.get(i).x = mysurf.listecouche.get(i).x / refzoom - xmin;
                mysurf.listecouche.get(i).y = mysurf.listecouche.get(i).y / refzoom - ymin;
                mysurf.listecouche.get(i).scale = mysurf.listecouche.get(i).scale / refzoom;
                //llog.d(TAG, "            "+i+" : "+mysurf.listecouche.get(i).x+", "+mysurf.listecouche.get(i).y+" "+mysurf.listecouche.get(i).originalw+"x"+mysurf.listecouche.get(i).originalh+" x"+mysurf.listecouche.get(i).scale);
                if (mysurf.listecouche.get(i).isaphoto) {
                    recreephoto(i);
                } else {
                    recreecanvasenzero(i, originalw, originalh);
                }
            }
            if (cefichier != null) {
                Bitmap sauvegardececi = Bitmap.createBitmap(originalw, originalh, Bitmap.Config.ARGB_8888);
                Canvas sauvegardecanvas = new Canvas(sauvegardececi);
                int drawbitmaplast = couchel - 1;
                //sauvegardecanvas.drawColor(backgroundcolor);
                for (int k = drawbitmaplast; k >= 0; k--) {
                    if (mysurf.listecouche.get(k).currentbitmap != null) {
                        boolean scaled = scalebitmaponcanvas(mysurf.listecouche.get(k).x, mysurf.listecouche.get(k).y, mysurf.listecouche.get(k).originalw, mysurf.listecouche.get(k).originalh, mysurf.listecouche.get(k).scale, originalw, originalh);
                        //llog.d(TAG, "             "+k+" : "+mysurf.listecouche.get(k).x+", "+mysurf.listecouche.get(k).y+" : "+mysurf.listecouche.get(k).originalw+"x"+mysurf.listecouche.get(k).originalh+" : x"+mysurf.listecouche.get(k).scale);
                        if (scaled) {
                            sauvegardecanvas.drawBitmap(mysurf.listecouche.get(k).currentbitmap, mysurf.srcrect, mysurf.dstrect, null);
                        }
                    }
                }
                savebitmaptofile(sauvegardececi, cefichier);
                sauvegardececi.recycle();
            }
        }
    }

    float posxi, posyi, taillex, ecritx, ecrity, posxf, posyf, posxmax, posymax;
    String ecritoption;
    RectF recti;
    Paint rectangle = new Paint();
    boolean onacliqueinit = false;
    boolean onacliquemove = false;
    float cliquex = 0.0f;
    float cliquey = 0.0f;
    Paint rectangle2 = new Paint();
    float posyibase = 0.0f;
    float quartdelargeurrestante = 5.0f;

    private boolean dessinehandleoptiondraw(Canvas surfacecanvas) {
        rectangle.setStrokeWidth(0.0f);
        rectangle.setAntiAlias(false);
        rectangle.setStyle(Paint.Style.FILL);
        rectangle2.setColor(Color.BLACK);
        rectangle2.setAntiAlias(true);
        rectangle2.setStrokeWidth(0.0f);
        rectangle2.setStyle(Paint.Style.FILL);
        posxmax = 0.0f;
        posymax = 0.0f;
        onacliqueinit = false;
        onacliquemove = false;
        if (neworder[1].equals("menudessiner")) {
            cliquex = Float.parseFloat(neworder[3]);
            cliquey = Float.parseFloat(neworder[4]);
            if (cliquex < mysurf.drawmenutotalw && cliquey < mysurf.drawmenutotalh) {
                if (neworder[2].equals("init")) {
                    onacliqueinit = true;
                } else if (neworder[2].equals("move")) {
                    onacliquemove = true;
                }
            } else if (mysurf.pickacolor > 0) {
                if (neworder[2].equals("init")) {
                    onacliqueinit = true;
                } else if (neworder[2].equals("move")) {
                    onacliquemove = true;
                }
            } else if (mysurf.selectedtrace > 0) {
                if (neworder[2].equals("init")) {
                    onacliqueinit = true;
                } else if (neworder[2].equals("move")) {
                    onacliquemove = true;
                }
            }
        }

        posyf = mysurf.drawmovemenusizeyi;

        posxf = 0.0f;
        posyi = posyf + model.DessinInterSpace;
        menusize(surfacecanvas);
        if (!menuquit(surfacecanvas))
            return false;
        menusave(surfacecanvas);
        menuclear(surfacecanvas);
        if (alloxdeletefile)
            menurecycle(surfacecanvas);
        menustylus(surfacecanvas);
        posxf += model.DessinInterSpace * 4.0f;
        menuaddpic(surfacecanvas);
        menulayer(surfacecanvas);
        menucolor(surfacecanvas);
        //menucontrast(surfacecanvas);
        menudrawsize(surfacecanvas);
        posxf += model.DessinInterSpace * 4.0f;
        menudrawtype(surfacecanvas);
        float drawtypexr = posxf;
        menudrawlines(surfacecanvas);
        menuselection(surfacecanvas);
        float drawselxr = posxf;
        menuzoom(surfacecanvas);
        menuundo(surfacecanvas);
        //menuselectlast(surfacecanvas);
        //menuduplicate(surfacecanvas);
        //menuchange(surfacecanvas);
        float drawxrmax = posxf;

        if (mysurf.drawmenucolor || mysurf.drawdevelopmenutype || mysurf.drawdevelopmenuselection)
            posyi = posyf + model.DessinInterSpace;
        posxf = 0.0f;

        if (mysurf.drawmenucolor) {
            menubgcolor(surfacecanvas);
            menupickcolor(surfacecanvas);
            menupickcolorclosest(surfacecanvas);
            menupickthicknessclosest(surfacecanvas);
            menucolorexpand(surfacecanvas);
        }

        if (mysurf.drawdevelopmenutype) {
            posxf = (model.DessinCaseH + model.DessinInterSpace * 2.0f) * 5.0f;
            menudrawtypedevelop(surfacecanvas, 0);
            menudrawtypedevelop(surfacecanvas, 1);
            menudrawtypedevelop(surfacecanvas, 2);
            menudrawtypedevelop(surfacecanvas, 3);
            menudrawtypedevelop(surfacecanvas, 4);
        }

        if (mysurf.drawdevelopmenuselection) {
            posxf = (model.DessinCaseH + model.DessinInterSpace * 2.0f) * (5.0f + 5.0f);
            menuselectiondevelop(surfacecanvas, Gallery.selectedtraceselectmultiple);
            if (selectmultiple) {
                menuselectiondevelop(surfacecanvas, Gallery.selectedtraceselectmultipleremove);
                menuselectiondevelop(surfacecanvas, Gallery.selectedtraceselectmultipleadd);
            }
            if (!selectmultiple) {
                menuselectiondevelop(surfacecanvas, Gallery.selectedtraceerase);
            }
            menuselectiondevelop(surfacecanvas, Gallery.selectedtracecolor);
            menuselectiondevelop(surfacecanvas, Gallery.selectedtracethickness);
            menuselectiondevelop(surfacecanvas, Gallery.selectedtraceduplicate);
            if (!selectmultiple) {
                menuselectiondevelop(surfacecanvas, Gallery.selectedtracemovetrace);
                menuselectiondevelop(surfacecanvas, Gallery.selectedtracemovepoint);
            }
            menuselectiondevelop(surfacecanvas, Gallery.selectedtracemoveto);
            menuselectiondevelop(surfacecanvas, Gallery.selectedtracechangetype);
            menuselectiondevelop(surfacecanvas, Gallery.selectedtraceunder);
            menuselectiondevelop(surfacecanvas, Gallery.selectedtraceover);
            menuselectiondevelop(surfacecanvas, Gallery.selectedtraceresize);
        }

        if (mysurf.drawmenucolor) {
            posxf = 0.0f;
            posyi = posyf + model.DessinInterSpace;
            if (!colormenuexpand)
                quartdelargeurrestante = (mysurf.ScreenWidth - posxf - 8.0f * model.DessinInterSpace) / 4.0f;
            if (colormenuexpand) {
                posxf = 0.0f;
                quartdelargeurrestante = mysurf.ScreenWidth - 2.0f * model.DessinInterSpace;
            }
            menucolorh(surfacecanvas);
            if (colormenuexpand) {
                posxf = 0.0f;
                posyi = posyf + model.DessinInterSpace;
                quartdelargeurrestante = mysurf.ScreenWidth - 2.0f * model.DessinInterSpace;
            }
            menucolors(surfacecanvas);
            if (colormenuexpand) {
                posxf = 0.0f;
                posyi = posyf + model.DessinInterSpace;
                quartdelargeurrestante = mysurf.ScreenWidth - 2.0f * model.DessinInterSpace;
            }
            menucolorv(surfacecanvas);
            if (colormenuexpand) {
                posxf = 0.0f;
                posyi = posyf + model.DessinInterSpace;
                quartdelargeurrestante = mysurf.ScreenWidth - 2.0f * model.DessinInterSpace;
            }
            menucolora(surfacecanvas);
        }

    /*if (mysurf.drawmenusize){
      posxf = 0.0f;
      posyi = posyf + model.DessinInterSpace;
      menupressurecst(surfacecanvas);
      menupointsize(surfacecanvas);
      if (mysurf.pressuresize) {
        posxf = 0.0f;
        posyi = posyf + model.DessinInterSpace;
        menupressure(surfacecanvas);
        menupressure2(surfacecanvas);
      }
    }*/

        if (mysurf.drawmenupath) {
            posxf = 0.0f;
            posyi = posyf + model.DessinInterSpace;
            menudrawpath(surfacecanvas);
        }

        posxmax = mysurf.ScreenWidth;
        if (posyf > posymax)
            posymax = posyf;

        int cettelayer = -1;

        if (onacliqueinit && mysurf.selectedtrace > 0) {
            int cl = selclosestpath.size();
            if (mysurf.selectedtrace == Gallery.selectedtraceselectmultipleadd) {
                cl = findclosestpath(cliquex, cliquey);
            } else if (mysurf.selectedtrace == Gallery.selectedtraceselectmultipleremove) {
            } else if (mysurf.selectedtrace == Gallery.selectedtraceerase) {
                if (!selectmultiple)
                    cl = findclosestpath(cliquex, cliquey);
                if (cl > 0) {
                    cettelayer = mysurf.listedessin.get(selclosestpath.get(0)).couche;
                    mysurf.listedessin.remove((int) selclosestpath.get(0));
                }
            } else if (mysurf.selectedtrace == Gallery.selectedtracecolor) {
                if (!selectmultiple)
                    cl = findclosestpath(cliquex, cliquey);
                if (cl > 0) {
                    cettelayer = mysurf.listedessin.get(selclosestpath.get(0)).couche;
                    for (int i = 0; i < cl; i++) {
                        Dessin cedessin = mysurf.listedessin.get(selclosestpath.get(i));
                        cedessin.couleura = mysurf.drawtracecurrcolora;
                        cedessin.couleurh = mysurf.drawtracecurrcolorh;
                        cedessin.couleurs = mysurf.drawtracecurrcolors;
                        cedessin.couleurv = mysurf.drawtracecurrcolorv;
                        cedessin.generecouleur();
                    }
                }
            } else if (mysurf.selectedtrace == Gallery.selectedtracethickness) {
                if (!selectmultiple)
                    cl = findclosestpath(cliquex, cliquey);
                if (cl > 0) {
                    cettelayer = mysurf.listedessin.get(selclosestpath.get(0)).couche;
                    for (int i = 0; i < cl; i++) {
                        Dessin cedessin = mysurf.listedessin.get(selclosestpath.get(i));
                        cedessin.pressuremax = model.drawtracepressuremax;
                        cedessin.pressuremin = model.drawtracepressuremin;
                        cedessin.sizemin = mysurf.drawtracecurrsizemin;
                        cedessin.sizemax = mysurf.drawtracecurrsizemax;
                    }
                }
            } else if (mysurf.selectedtrace == Gallery.selectedtracechangetype) {
                if (!selectmultiple)
                    cl = findclosestpath(cliquex, cliquey);
                if (cl > 0) {
                    cettelayer = mysurf.listedessin.get(selclosestpath.get(0)).couche;
                    for (int i = 0; i < cl; i++) {
                        Dessin cedessin = mysurf.listedessin.get(selclosestpath.get(i));
                        cedessin.drawerasefillcontour = mysurf.drawtracecurrdefcb;
                    }
                }
            } else if (mysurf.selectedtrace == Gallery.selectedtraceunder) {
                if (!selectmultiple)
                    cl = findclosestpath(cliquex, cliquey);
                if (cl > 0) {
                    cettelayer = mysurf.listedessin.get(selclosestpath.get(0)).couche;
                    for (int i = 0; i < cl; i++) {
                        int oldpos = selclosestpath.get(i);
                        if (oldpos > 0) {
                            Dessin cedessin = mysurf.listedessin.get(oldpos);
                            mysurf.listedessin.add(oldpos - 1, cedessin);
                            mysurf.listedessin.remove(oldpos + 1);
                        }
                    }
                }
            } else if (mysurf.selectedtrace == Gallery.selectedtraceover) {
                if (!selectmultiple)
                    cl = findclosestpath(cliquex, cliquey);
                if (cl > 0) {
                    cettelayer = mysurf.listedessin.get(selclosestpath.get(0)).couche;
                    int dl = mysurf.listedessin.size() - 1;
                    for (int i = 0; i < cl; i++) {
                        int oldpos = selclosestpath.get(i);
                        if (oldpos < dl) {
                            Dessin cedessin = mysurf.listedessin.get(oldpos);
                            mysurf.listedessin.add(oldpos + 2, cedessin);
                            mysurf.listedessin.remove(oldpos);
                        }
                    }
                }
            } else if (mysurf.selectedtrace == Gallery.selectedtraceresize) {
                if (!selectmultiple)
                    cl = findclosestpath(cliquex, cliquey);
                if (cl > 0) {
                    cettelayer = mysurf.listedessin.get(selclosestpath.get(0)).couche;
                    float ratio = mysurf.selectedtraceresizeratio;
                    boolean init = true;
                    float xi = 0;
                    float yi = 0;
                    float xf = 0;
                    float yf = 0;
                    for (int i = 0; i < cl; i++) {
                        int oldpos = selclosestpath.get(i);
                        Dessin cedessin = mysurf.listedessin.get(oldpos);
                        int nbpointrace = cedessin.x.length;
                        for (int k = 0; k < nbpointrace; k++) {
                            if (init || cedessin.x[k] < xi)
                                xi = cedessin.x[k];
                            if (init || cedessin.y[k] < yi)
                                yi = cedessin.y[k];
                            if (init || cedessin.x[k] > xf)
                                xf = cedessin.x[k];
                            if (init || cedessin.y[k] > yf)
                                yf = cedessin.y[k];
                            if (init)
                                init = false;
                        }
                    }
                    float cx = (xi + xf) * 0.5f;
                    float cy = (yi + yf) * 0.5f;
                    for (int i = 0; i < cl; i++) {
                        int oldpos = selclosestpath.get(i);
                        Dessin cedessin = mysurf.listedessin.get(oldpos);
                        int nbpointrace = cedessin.x.length;
                        for (int k = 0; k < nbpointrace; k++) {
                            cedessin.x[k] = (cedessin.x[k] - cx) * ratio + cx;
                            cedessin.y[k] = (cedessin.y[k] - cy) * ratio + cy;
                        }
                    }
                }
            } else if (mysurf.selectedtrace == Gallery.selectedtracemovetrace) {
                if (!selectmultiple)
                    cl = findclosestpath(cliquex, cliquey);
                if (cl > 0 && !selectmultiple)
                    mysurf.selectedtrace = Gallery.selectedtracemoveto;
            } else if (mysurf.selectedtrace == Gallery.selectedtracemovepoint) {
                if (!selectmultiple)
                    cl = findclosestpath(cliquex, cliquey);
                if (cl > 0 && !selectmultiple)
                    mysurf.selectedtrace = Gallery.selectedtracemoveto;
            } else if (mysurf.selectedtrace == Gallery.selectedtraceduplicate) {
                if (!selectmultiple)
                    cl = findclosestpath(cliquex, cliquey);
                if (cl > 0) {
                    cettelayer = mysurf.listedessin.get(selclosestpath.get(0)).couche;
                    for (int i = 0; i < cl; i++) {
                        Dessin olddessin = mysurf.listedessin.get(selclosestpath.get(i));
                        Dessin cedessin = new Dessin();
                        int nbpointrace = olddessin.x.length;
                        cedessin.x = new float[nbpointrace];
                        cedessin.y = new float[nbpointrace];
                        cedessin.p = new float[nbpointrace];
                        cedessin.d = new float[nbpointrace];
                        cedessin.t = new float[nbpointrace];
                        for (int k = 0; k < nbpointrace; k++) {
                            cedessin.x[k] = olddessin.x[k];
                            cedessin.y[k] = olddessin.y[k];
                            cedessin.p[k] = olddessin.p[k];
                            cedessin.d[k] = olddessin.d[k];
                            cedessin.t[k] = olddessin.t[k];
                        }
                        cedessin.couleura = olddessin.couleura;
                        cedessin.couleurh = olddessin.couleurh;
                        cedessin.couleurs = olddessin.couleurs;
                        cedessin.couleurv = olddessin.couleurv;
                        cedessin.generecouleur();
                        cedessin.pressuremax = olddessin.pressuremax;
                        cedessin.pressuremin = olddessin.pressuremin;
                        cedessin.sizemin = olddessin.sizemin;
                        cedessin.sizemax = olddessin.sizemax;
                        cedessin.drawerasefillcontour = olddessin.drawerasefillcontour;
                        cedessin.bezierlength = olddessin.bezierlength;
                        int cettelayer2 = mysurf.drawcurrlayer;
                        if (cettelayer2 < 0)
                            cettelayer2 = 0;
                        cedessin.couche = cettelayer2;
                        mysurf.listedessin.add(cedessin);
                        //Canvas thissurfacecanvas = mysurf.listecouche.get(cettelayer2).canvas;
                        //dessinelatrace(closestpath + 1, thissurfacecanvas);
                    }
                }
            } else if (mysurf.selectedtrace == Gallery.selectedtracemoveto) {
                if (cl > 0) {
                    cettelayer = mysurf.listedessin.get(selclosestpath.get(0)).couche;
                    float dx = 0;
                    float dy = 0;
                    for (int i = 0; i < cl; i++) {
                        Dessin cedessin = mysurf.listedessin.get(selclosestpath.get(i));
                        if (cedessin.x != null) {
                            if (selectedtracemovepointortrace == Gallery.selectedtracemovetrace || selectmultiple) {
                                if (selclosestpathpoint.get(i) < cedessin.x.length && selclosestpathpoint.get(i) < cedessin.y.length) {
                                    float scale = mysurf.listecouche.get(cettelayer).scale;
                                    if (scale == 0)
                                        scale = 1;
                                    if (i == 0) {
                                        dx = cliquex / scale - cedessin.x[selclosestpathpoint.get(i)];
                                        dy = cliquey / scale - cedessin.y[selclosestpathpoint.get(i)];
                                    }
                                    int xl = cedessin.x.length;
                                    for (int ii = 0; ii < xl; ii++) {
                                        cedessin.x[ii] += dx;
                                        cedessin.y[ii] += dy;
                                    }
                                    if (selectedtraceclicked != Gallery.selectedtracemoveto && !selectmultiple)
                                        mysurf.selectedtrace = Gallery.selectedtracemovetrace;
                                }
                            } else if (selectedtracemovepointortrace == Gallery.selectedtracemovepoint) {
                                if (selclosestpathpoint.get(i) < cedessin.x.length && selclosestpathpoint.get(i) < cedessin.y.length) {
                                    float scale = mysurf.listecouche.get(cettelayer).scale;
                                    if (scale == 0)
                                        scale = 1;
                                    cedessin.x[selclosestpathpoint.get(i)] = cliquex / scale;
                                    cedessin.y[selclosestpathpoint.get(i)] = cliquey / scale;
                                    if (selectedtraceclicked != Gallery.selectedtracemoveto)
                                        mysurf.selectedtrace = Gallery.selectedtracemovepoint;
                                }
                            }
                        }
                    }
                }
            }
        }

        if ((mysurf.selectedtrace == Gallery.selectedtraceselectmultipleadd || mysurf.selectedtrace == Gallery.selectedtraceselectmultipleremove) && selectmultiple) {
            animateselection = 1;
        } else if (animateselection == 1) {
            animateselection = 2;
        }

        if (mysurf.selectedtrace != Gallery.selectedtracecolor
                && (mysurf.selectedtrace == Gallery.selectedtracemoveto || selectmultiple)) {
            int cl = selclosestpath.size();
            float scale = 1;

            if (cettelayer == -1) {
                cettelayer = mysurf.drawcurrlayer;
                if (mysurf.drawcurrlayer == -1)
                    cettelayer = 0;
                if (cettelayer < mysurf.listecouche.size())
                    scale = mysurf.listecouche.get(cettelayer).scale;
                if (scale == 0)
                    scale = 1;
            }

            float radius = 20;
            alphad = (alphad + 50.0f) % 255.0f;
            for (int i = 0; i < cl; i++) {
                if (animateselection == 1) {
                    int currsel = selclosestpath.get(i);
                    int listedessinl = mysurf.listedessin.size();
                    if (currsel < listedessinl) {
                        Dessin dessin = mysurf.listedessin.get(currsel);
                        dessin.couleura = alphad;
                        dessin.generecouleur();
                    }
                }
                if (mysurf.selectedtrace == Gallery.selectedtracemoveto
                        || mysurf.selectedtrace == Gallery.selectedtracemovepoint) {
                    if (i == 0) {
                        surfacecanvas.drawCircle(selclosestpathpointx.get(i) * scale, selclosestpathpointy.get(i) * scale, radius, model.DessinSelectedPaint);
                    }
                }
            }
        }

        if (animateselection == 2) {
            resetselclosestpathalpha();
            if (cettelayer == -1) {
                cettelayer = mysurf.drawcurrlayer;
                if (mysurf.drawcurrlayer == -1)
                    cettelayer = 0;
            }
            animateselection = 0;
        }

        if (cettelayer >= 0) {
            if (cettelayer < mysurf.listecouche.size()) {
                if (!mysurf.listecouche.get(cettelayer).isaphoto) {
                    recreecanvasenzero(cettelayer, mysurf.ScreenWidth, mysurf.ScreenHeight);
                } else {
                    recreephoto(cettelayer);
                }
            }
        }

        if (posxmax != mysurf.drawmenutotalw)
            mysurf.drawmenutotalw = posxmax;
        if (posymax != mysurf.drawmenutotalh)
            mysurf.drawmenutotalh = posymax;

        return true;
    }

    private int animateselection = 0;

    private void resetselclosestpathalpha() {
        int cl = selclosestpath.size();
        for (int i = 0; i < cl; i++) {
            int currsel = selclosestpath.get(i);
            int listedessinl = mysurf.listedessin.size();
            if (currsel < listedessinl) {
                Dessin dessin = mysurf.listedessin.get(currsel);
                dessin.couleura = selclosestpathalpha.get(i);
                dessin.generecouleur();
            }
        }
    }

    private float alphad = 0.0f;
    private ArrayList<Integer> selclosestpath = new ArrayList<>();
    private ArrayList<Integer> selclosestpathpoint = new ArrayList<>();
    private ArrayList<Float> selclosestpathpointx = new ArrayList<>();
    private ArrayList<Float> selclosestpathpointy = new ArrayList<>();
    private ArrayList<Float> selclosestpathalpha = new ArrayList<>();
    private int selectedtracemovepointortrace = 0;

    public int findclosestpath(float x, float y) {
        if (!selectmultiple) {
            if (animateselection == 1) {
                resetselclosestpathalpha();
                animateselection = 2;
            }
            selclosestpath.clear();
            selclosestpathpoint.clear();
            selclosestpathpointx.clear();
            selclosestpathpointy.clear();
            selclosestpathalpha.clear();
        }
        int selclosestpathu = -1;
        int selclosestpathpointu = -1;
        float selclosestpathpointxu = -1;
        float selclosestpathpointyu = -1;
        float selclosestpathalphau = -1;
        float scale = 1;
        int cettelayer = mysurf.drawcurrlayer;
        if (cettelayer == -1)
            cettelayer = 0;
        if (cettelayer < mysurf.listecouche.size()) {
            Couche cettecouche = mysurf.listecouche.get(cettelayer);
            scale = cettecouche.scale;
            if (scale == 0)
                scale = 1;
            x = (x - cettecouche.x) / scale;
            y = (y - cettecouche.y) / scale;
        }
        int listedessinl = mysurf.listedessin.size();
        float meilleurv = -1.0f;
        Dessin cedessin;
        int selclosestpathl = selclosestpath.size();
        boolean adeja;
        for (int i = 0; i < listedessinl; i++) {
            adeja = false;
            for (int k = 0; k < selclosestpathl; k++) {
                if (selclosestpath.get(k) == i) {
                    adeja = true;
                    break;
                }
            }
            if (adeja)
                continue;
            cedessin = mysurf.listedessin.get(i);
            if (cedessin.couche == cettelayer) {
                if (cedessin.x != null) {
                    if (cedessin.x.length > 0) {
                        for (int j = 0; j < cedessin.x.length; j++) {
                            float d2 = (cedessin.x[j] - x) * (cedessin.x[j] - x)
                                    + (cedessin.y[j] - y) * (cedessin.y[j] - y);
                            if (d2 < meilleurv || meilleurv < 0) {
                                meilleurv = d2;
                                selclosestpathu = i;
                                selclosestpathpointu = j;
                                selclosestpathpointxu = cedessin.x[j];
                                selclosestpathpointyu = cedessin.y[j];
                                selclosestpathalphau = cedessin.couleura;
                            }
                        }
                    }
                }
            }
        }
        if (selclosestpathu >= 0) {
            selclosestpath.add(selclosestpathu);
            selclosestpathpoint.add(selclosestpathpointu);
            selclosestpathpointx.add(selclosestpathpointxu);
            selclosestpathpointy.add(selclosestpathpointyu);
            selclosestpathalpha.add(selclosestpathalphau);
        }
        return selclosestpath.size();
    }

    public boolean menusize(Canvas surfacecanvas) {
        posxi = posxf + model.DessinInterSpace;
        posxf = posxi + model.DessinCaseH + model.DessinInterSpace;
        posyf = posyi + model.DessinCaseH;
        if (onacliqueinit) {
            if (posxi < cliquex && cliquex < posxf && posyi < cliquey && cliquey < posyf) {
                onacliqueinit = false;
                onacliquemove = false;
                mysurf.drawmovemenusize = !mysurf.drawmovemenusize;
            }
        }
        recti = new RectF(posxi, posyi, posxf, posyf);
        surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinBgPaint);
        if (mysurf.drawmovemenusize)
            surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinSelectedPaint);
        surfacecanvas.drawBitmap(menusize, posxi, posyi, null);
        if (posxf > posxmax)
            posxmax = posxf;
        return true;
    }

    public boolean menuquit(Canvas surfacecanvas) {
        posxi = posxf + model.DessinInterSpace;
        posxf = posxi + model.DessinCaseH + model.DessinInterSpace;
        posyf = posyi + model.DessinCaseH;
        if (onacliqueinit) {
            if (posxi < cliquex && cliquex < posxf && posyi < cliquey && cliquey < posyf) {
                onacliqueinit = false;
                onacliquemove = false;
                mysurf.drawmenucolor = false;
                mysurf.drawmenusize = false;
                Intent intent = new Intent();
                intent.setAction(Gallery.broadcastname);
                intent.putExtra("goal", "startbrowser");
                intent.putExtra("id", currid);
                LocalBroadcastManager.getInstance(model.activitycontext).sendBroadcast(intent);
                return false;
            }
        }
        recti = new RectF(posxi, posyi, posxf, posyf);
        surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinBgPaint);
        surfacecanvas.drawBitmap(quit, posxi, posyi, null);
        if (posxf > posxmax)
            posxmax = posxf;
        return true;
    }

    public boolean menusave(Canvas surfacecanvas) {
        posxi = posxf + model.DessinInterSpace;
        posxf = posxi + model.DessinCaseH + model.DessinInterSpace;
        posyf = posyi + model.DessinCaseH;
        if (onacliqueinit) {
            if (posxi < cliquex && cliquex < posxf && posyi < cliquey && cliquey < posyf) {
                onacliqueinit = false;
                onacliquemove = false;
                writelastdrawing();
            }
        }
        recti = new RectF(posxi, posyi, posxf, posyf);
        surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinBgPaint);
        surfacecanvas.drawBitmap(save, posxi, posyi, null);
        if (posxf > posxmax)
            posxmax = posxf;
        return true;
    }

    public boolean menuclear(Canvas surfacecanvas) {
        posxi = posxf + model.DessinInterSpace;
        posxf = posxi + model.DessinCaseH + model.DessinInterSpace;
        posyf = posyi + model.DessinCaseH;
        if (onacliqueinit) {
            if (posxi < cliquex && cliquex < posxf && posyi < cliquey && cliquey < posyf) {
                onacliqueinit = false;
                onacliquemove = false;
                try {
                    model.commandethreaddrawing.put(new String[]{String.valueOf(currid), "closedrawing", "save"});
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                try {
                    model.commandethreaddrawing.put(new String[]{String.valueOf(currid), "initialize", "startblank"});
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        recti = new RectF(posxi, posyi, posxf, posyf);
        surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinBgPaint);
        surfacecanvas.drawBitmap(erase, posxi, posyi, null);
        if (posxf > posxmax)
            posxmax = posxf;
        return true;
    }

    public boolean menurecycle(Canvas surfacecanvas) {
        posxi = posxf + model.DessinInterSpace;
        posxf = posxi + model.DessinCaseH + model.DessinInterSpace;
        posyf = posyi + model.DessinCaseH;
        if (onacliqueinit) {
            if (posxi < cliquex && cliquex < posxf && posyi < cliquey && cliquey < posyf) {
                onacliqueinit = false;
                onacliquemove = false;
                try {
                    model.commandethreaddrawing.put(new String[]{String.valueOf(currid), "closedrawing", "donotsave"});
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                try {
                    model.commandethreaddrawing.put(new String[]{String.valueOf(currid), "initialize", "startblank"});
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        recti = new RectF(posxi, posyi, posxf, posyf);
        surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinBgPaint);
        surfacecanvas.drawBitmap(recycle, posxi, posyi, null);
        if (posxf > posxmax)
            posxmax = posxf;
        return true;
    }


    public boolean menustylus(Canvas surfacecanvas) {
        posxi = posxf + model.DessinInterSpace;
        posxf = posxi + model.DessinCaseH + model.DessinInterSpace;
        posyf = posyi + model.DessinCaseH;
        if (onacliqueinit) {
            if (posxi < cliquex && cliquex < posxf && posyi < cliquey && cliquey < posyf) {
                onacliqueinit = false;
                onacliquemove = false;
                mysurf.styluspalmrejection = !mysurf.styluspalmrejection;
            }
        }
        recti = new RectF(posxi, posyi, posxf, posyf);
        surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinBgPaint);
        if (mysurf.styluspalmrejection) {
            surfacecanvas.drawBitmap(stylus, posxi, posyi, null);
        } else {
            surfacecanvas.drawBitmap(hand, posxi, posyi, null);
        }
        if (posxf > posxmax)
            posxmax = posxf;
        return true;
    }

    public boolean menuaddpic(Canvas surfacecanvas) {
        posxi = posxf + model.DessinInterSpace;
        posxf = posxi + model.DessinCaseH + model.DessinInterSpace;
        posyf = posyi + model.DessinCaseH;
        if (onacliqueinit) {
            if (posxi < cliquex && cliquex < posxf && posyi < cliquey && cliquey < posyf) {
                onacliqueinit = false;
                onacliquemove = false;
                llog.d(TAG, "on charge le fichier " + mysurf.mediaIndexAddress);
                if (mysurf.isincache(mysurf.mediaIndexAddress)) {
                    llog.d(TAG, "on charge le fichier en cache");
                    float[] cachedbitmapnfo = mysurf.getcachedbitmapnfo(mysurf.mediaIndexAddress);
                    int bitmapwidth = (int) cachedbitmapnfo[0];
                    int bitmapheight = (int) cachedbitmapnfo[1];
                    Couche macouche = new Couche();
                    macouche.originalbitmap = Bitmap.createBitmap(bitmapwidth, bitmapheight, Bitmap.Config.ARGB_8888);
                    macouche.canvas = new Canvas(macouche.originalbitmap);
                    macouche.canvas.drawBitmap(mysurf.getcachedbitmap(mysurf.mediaIndexAddress), 0, 0, null);
                    macouche.currentbitmap = Bitmap.createBitmap(bitmapwidth, bitmapheight, Bitmap.Config.ARGB_8888);
                    macouche.canvas = new Canvas(macouche.currentbitmap);
                    macouche.canvas.drawBitmap(mysurf.getcachedbitmap(mysurf.mediaIndexAddress), 0, 0, null);
                    macouche.isaphoto = true;
                    macouche.x = 0.0f;
                    macouche.y = 0.0f;
                    macouche.originalw = bitmapwidth;
                    macouche.originalh = bitmapheight;
                    macouche.scale = 1.0f;
                    mysurf.listecouche.add(macouche);
                }
            }
        }
        recti = new RectF(posxi, posyi, posxf, posyf);
        surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinBgPaint);
        surfacecanvas.drawBitmap(addpic, posxi, posyi, null);
        if (posxf > posxmax)
            posxmax = posxf;
        return true;
    }


    public boolean menulayer(Canvas surfacecanvas) {
        if (mysurf.drawcurrlayer == -1) {
            ecritoption = "=";
        } else if (mysurf.drawcurrlayer == 0) {
            ecritoption = "" + mysurf.drawcurrlayer;
        } else {
            ecritoption = "" + mysurf.drawcurrlayer;
        }
        posxi = posxf + model.DessinInterSpace;
        ecritx = posxi + model.DessinInterSpace;
        ecrity = posyi + model.DessinCaseH * 0.5f + 0.33f * model.DessinCaseTextH;
        posxf = posxi + model.DessinCaseH + model.DessinInterSpace;
        posyf = posyi + model.DessinCaseH;
        if (onacliqueinit) {
            if (posxi < cliquex && cliquex < posxf && posyi < cliquey && cliquey < posyf) {
                onacliqueinit = false;
                onacliquemove = false;
                if (mysurf.drawmovezoom) {
                    // si on est en zoom
                    // il faut sauver le décalage dû à notre zoom sinon ça va merder
                    if (mysurf.drawcurrlayer <= 0) {
                        // on redessine la trace sur un nouvel écran
                        // mais inutile de recreephoto
                        /**
                         *  il faut décaler tous les points x et y
                         *  inutile de supprimer l'image et de la redessiner ? pas sûr.
                         */
                        recreecanvasenzero(0, mysurf.ScreenWidth, mysurf.ScreenHeight);
                    }
                }
                mysurf.drawcurrlayer += 1;
                if (mysurf.drawcurrlayer >= mysurf.listecouche.size()) {
                    mysurf.drawcurrlayer = -1;
                }
                if (mysurf.drawmovezoom) {
                    // si on est en zoom on change de layer à zoomer
                    if (mysurf.drawcurrlayer == -1) {
                        int drawbitmapl = mysurf.listecouche.size();
                        for (int k = 0; k < drawbitmapl; k++) {
                            mysurf.listecouche.get(k).active = true;
                        }
                    } else {
                        int drawbitmapl = mysurf.listecouche.size();
                        for (int k = 0; k < drawbitmapl; k++) {
                            mysurf.listecouche.get(k).active = false;
                        }
                        mysurf.listecouche.get(mysurf.drawcurrlayer).active = true;
                    }
                }
            }
        }
        recti = new RectF(posxi, posyi, posxf, posyf);
        surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinBgPaint);
        surfacecanvas.drawBitmap(layers, posxi, posyi, null);
        surfacecanvas.drawText(ecritoption, posxi, posyf, model.DessinTextPaint);
        if (posxf > posxmax)
            posxmax = posxf;
        return true;
    }

    public boolean menucolor(Canvas surfacecanvas) {
        posxi = posxf + model.DessinInterSpace * 5.0f;
        posxf = posxi + model.DessinCaseH + model.DessinInterSpace;
        posyf = posyi + model.DessinCaseH;
        if (onacliqueinit) {
            if (posxi < cliquex && cliquex < posxf && posyi < cliquey && cliquey < posyf) {
                onacliqueinit = false;
                onacliquemove = false;
                mysurf.drawmenucolor = !mysurf.drawmenucolor;
            }
        }
        rectangle.setColor(Color.HSVToColor((int) mysurf.drawtracecurrcolora, new float[]{mysurf.drawtracecurrcolorh, mysurf.drawtracecurrcolors, mysurf.drawtracecurrcolorv}));
        rectangle.setStrokeWidth(0.0f);
        recti = new RectF(posxi, posyi, posxf, posyf);
        surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinBgPaint);
        surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, rectangle);
        if (posxf > posxmax)
            posxmax = posxf;
        return true;
    }


    public boolean menucontrast(Canvas surfacecanvas) {
        posxi = posxf + model.DessinInterSpace;
        posxf = posxi + model.DessinCaseH + model.DessinInterSpace;
        posyf = posyi + model.DessinCaseH;
        if (onacliqueinit) {
            if (posxi < cliquex && cliquex < posxf && posyi < cliquey && cliquey < posyf) {
                onacliqueinit = false;
                onacliquemove = false;
                mysurf.drawmenumod = !mysurf.drawmenumod;
            }
        }
        recti = new RectF(posxi, posyi, posxf, posyf);
        surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinBgPaint);
        surfacecanvas.drawBitmap(contrast, posxi, posyi, null);
        if (posxf > posxmax)
            posxmax = posxf;
        return true;
    }

    public boolean menudrawsize(Canvas surfacecanvas) {
        posxi = posxf + model.DessinInterSpace;
        posxf = posxi + model.DessinCaseH + model.DessinInterSpace;
        posyf = posyi + model.DessinCaseH;
        if (onacliqueinit) {
            if (posxi < cliquex && cliquex < posxf && posyi < cliquey && cliquey < posyf) {
                onacliqueinit = false;
                onacliquemove = false;
                mysurf.drawmenusize = !mysurf.drawmenusize;
            }
        }
        float milieuy = (posyf + posyi) / 2.0f;
        recti = new RectF(posxi, posyi, posxf, posyf);
        surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.Menu1BgPainto);
        if (mysurf.drawmenusize) {
            surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinSelectedPaint);
        }
        rectangle.setColor(Color.BLACK);
        rectangle.setStrokeWidth(mysurf.drawtracecurrsizemin);
        surfacecanvas.drawLine(posxi, milieuy, posxf, milieuy, rectangle);
        surfacecanvas.drawText(String.format("%.02f", mysurf.drawtracecurrsizemax), posxi, posyf, model.DessinTextPaint);
        if (posxf > posxmax)
            posxmax = posxf;
        return true;
    }

    public boolean menudrawtype(Canvas surfacecanvas) {
        //ecritoption = "Draw";
        posxi = posxf + model.DessinInterSpace;
        posxf = posxi + model.DessinCaseH + model.DessinInterSpace;
        posyf = posyi + model.DessinCaseH;
        if (onacliqueinit) {
            if (posxi < cliquex && cliquex < posxf && posyi < cliquey && cliquey < posyf) {
                onacliqueinit = false;
                onacliquemove = false;
                //if (cliquey < (posyi + posyf) / 2.0f) {
                mysurf.drawdevelopmenutype = !mysurf.drawdevelopmenutype;
        /*} else {
          mysurf.drawtracecurrdefcb = (mysurf.drawtracecurrdefcb + 1) % 5;
          if (mysurf.drawtracecurrdefcb == 0) {
            model.message("draw");
          } else if (mysurf.drawtracecurrdefcb == 1) {
            model.message("erase zones");
          } else if (mysurf.drawtracecurrdefcb == 2) {
            model.message("draw and then fill");
          } else if (mysurf.drawtracecurrdefcb == 3) {
            model.message("draw and then close contour");
          } else if (mysurf.drawtracecurrdefcb == 4) {
            model.message("draw and then fill radial");
          }
        }*/
            }
        }
        recti = new RectF(posxi, posyi, posxf, posyf);
        surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinBgPaint);
        if (mysurf.drawtracecurrdefcb == 0) {
            //surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinSelectedPaint);
            surfacecanvas.drawBitmap(draw, posxi, posyi, null);
        } else if (mysurf.drawtracecurrdefcb == 1) {
            //surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinSelectedPaint);
            surfacecanvas.drawBitmap(erase, posxi, posyi, null);
        } else if (mysurf.drawtracecurrdefcb == 2) {
            //surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinSelectedPaint);
            surfacecanvas.drawBitmap(fill, posxi, posyi, null);
        } else if (mysurf.drawtracecurrdefcb == 3) {
            //surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinSelectedPaint);
            surfacecanvas.drawBitmap(contour, posxi, posyi, null);
        } else if (mysurf.drawtracecurrdefcb == 4) {
            //surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinSelectedPaint);
            surfacecanvas.drawBitmap(fill, posxi, posyi, null);
        }
        if (posxf > posxmax)
            posxmax = posxf;
        return true;
    }

    public boolean menudrawtypedevelop(Canvas surfacecanvas, int type) {
        //ecritoption = "Draw";
        posxi = posxf + model.DessinInterSpace;
        posxf = posxi + model.DessinCaseH + model.DessinInterSpace;
        posyf = posyi + model.DessinCaseH;
        if (onacliqueinit) {
            if (posxi < cliquex && cliquex < posxf && posyi < cliquey && cliquey < posyf) {
                onacliqueinit = false;
                onacliquemove = false;
                mysurf.drawtracecurrdefcb = type;
                if (mysurf.drawtracecurrdefcb == 0) {
                    model.message("draw");
                } else if (mysurf.drawtracecurrdefcb == 1) {
                    model.message("erase zones");
                } else if (mysurf.drawtracecurrdefcb == 2) {
                    model.message("draw and then fill");
                } else if (mysurf.drawtracecurrdefcb == 3) {
                    model.message("draw and then close contour");
                } else if (mysurf.drawtracecurrdefcb == 4) {
                    model.message("draw and then fill radial");
                }
            }
        }
        recti = new RectF(posxi, posyi, posxf, posyf);
        surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinBgPaint);
        if (type == 0) {
            if (mysurf.drawtracecurrdefcb == 0)
                surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinSelectedPaint);
            surfacecanvas.drawBitmap(draw, posxi, posyi, null);
        } else if (type == 1) {
            if (mysurf.drawtracecurrdefcb == 1)
                surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinSelectedPaint);
            surfacecanvas.drawBitmap(drawbgspot, posxi, posyi, null);
        } else if (type == 2) {
            if (mysurf.drawtracecurrdefcb == 2)
                surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinSelectedPaint);
            surfacecanvas.drawBitmap(fill, posxi, posyi, null);
        } else if (type == 3) {
            if (mysurf.drawtracecurrdefcb == 3)
                surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinSelectedPaint);
            surfacecanvas.drawBitmap(contour, posxi, posyi, null);
        } else if (type == 4) {
            if (mysurf.drawtracecurrdefcb == 4)
                surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinSelectedPaint);
            surfacecanvas.drawBitmap(drawradialfill, posxi, posyi, null);
        }
        if (posxf > posxmax)
            posxmax = posxf;
        return true;
    }

    public boolean menudrawlines(Canvas surfacecanvas) {
        // draw lines
        posxi = posxf + model.DessinInterSpace;
        posxf = posxi + model.DessinCaseH + model.DessinInterSpace;
        posyf = posyi + model.DessinCaseH;
        if (onacliqueinit) {
            if (posxi < cliquex && cliquex < posxf && posyi < cliquey && cliquey < posyf) {
                onacliqueinit = false;
                onacliquemove = false;
                mysurf.onlymouseup = !mysurf.onlymouseup;
                if (mysurf.onlymouseup) {
                    mysurf.onlymouseupstatus = 0;
                } else {
                    mysurf.onlymouseupstatus = -1;
                    try {
                        model.commandethreaddrawing.put(new String[]{String.valueOf(currid), "tracecompleteredraw"});
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        recti = new RectF(posxi, posyi, posxf, posyf);
        surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinBgPaint);
        if (mysurf.onlymouseup) {
            surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinSelectedPaint);
        }
        surfacecanvas.drawBitmap(drawline, posxi, posyi, null);
        if (posxf > posxmax)
            posxmax = posxf;
        return true;
    }

    public boolean menuselection(Canvas surfacecanvas) {
        posxi = posxf + model.DessinInterSpace;
        posxf = posxi + model.DessinCaseH + model.DessinInterSpace;
        posyf = posyi + model.DessinCaseH;
        if (onacliqueinit) {
            if (posxi < cliquex && cliquex < posxf && posyi < cliquey && cliquey < posyf) {
                onacliqueinit = false;
                onacliquemove = false;
                mysurf.drawdevelopmenuselection = !mysurf.drawdevelopmenuselection;
                if (!mysurf.drawdevelopmenuselection) {
                    mysurf.selectedtrace = Gallery.selectedtracecancel;
                    selectmultiple = false;
                    if (animateselection == 1) {
                        resetselclosestpathalpha();
                        animateselection = 2;
                    }
                    selclosestpath.clear();
                    selclosestpathpoint.clear();
                    selclosestpathpointx.clear();
                    selclosestpathpointy.clear();
                    selclosestpathalpha.clear();
                }
            }
        }
        recti = new RectF(posxi, posyi, posxf, posyf);
        if (mysurf.selectedtrace != Gallery.selectedtracecancel)
            surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinBgPaint);
        surfacecanvas.drawBitmap(change, posxi, posyi, null);
        if (posxf > posxmax)
            posxmax = posxf;
        return true;
    }

    private int selectedtraceclicked = 0;
    private boolean selectmultiple = false;

    public boolean menuselectiondevelop(Canvas surfacecanvas, int sel) {
        posxi = posxf + model.DessinInterSpace;
        posxf = posxi + model.DessinCaseH + model.DessinInterSpace;
        posyf = posyi + model.DessinCaseH;
        if (onacliqueinit) {
            if (posxi < cliquex && cliquex < posxf && posyi < cliquey && cliquey < posyf) {
                onacliqueinit = false;
                onacliquemove = false;
                if (sel == selectedtraceclicked && sel != Gallery.selectedtraceselectmultiple) {
                    mysurf.selectedtrace = Gallery.selectedtracecancel;
                    selectedtraceclicked = Gallery.selectedtracecancel;
                } else {
                    mysurf.selectedtrace = sel;
                    if (sel == Gallery.selectedtraceerase) {
                        model.message("remove closest");
                        selectedtraceclicked = Gallery.selectedtraceerase;
                    } else if (sel == Gallery.selectedtraceselectmultiple) {
                        selectmultiple = !selectmultiple;
                        if (!selectmultiple) {
                            model.message("modify closest trace");
                            selectedtraceclicked = Gallery.selectedtracecancel;
                            mysurf.selectedtrace = Gallery.selectedtracecancel;
                        } else {
                            model.message("select multiple traces");
                            selectedtraceclicked = Gallery.selectedtraceselectmultipleadd;
                            mysurf.selectedtrace = Gallery.selectedtraceselectmultipleadd;
                        }
                        if (animateselection == 1) {
                            resetselclosestpathalpha();
                            animateselection = 2;
                        }
                        selclosestpath.clear();
                        selclosestpathpoint.clear();
                        selclosestpathpointx.clear();
                        selclosestpathpointy.clear();
                        selclosestpathalpha.clear();
                        try {
                            model.commandethreaddrawing.put(new String[]{String.valueOf(currid), "update"});
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    } else if (sel == Gallery.selectedtraceselectmultipleremove) {
                        selectedtraceclicked = Gallery.selectedtraceselectmultipleadd;
                        mysurf.selectedtrace = Gallery.selectedtraceselectmultipleadd;
                        int cl = selclosestpath.size();
                        if (cl > 0) {
                            cl -= 1;
                            int currsel = selclosestpath.get(cl);
                            int listedessinl = mysurf.listedessin.size();
                            if (currsel < listedessinl) {
                                Dessin dessin = mysurf.listedessin.get(currsel);
                                dessin.couleura = selclosestpathalpha.get(cl);
                                dessin.generecouleur();
                            }
                            selclosestpath.remove(cl);
                            selclosestpathpoint.remove(cl);
                            selclosestpathpointx.remove(cl);
                            selclosestpathpointy.remove(cl);
                            selclosestpathalpha.remove(cl);
                        }
                    } else if (sel == Gallery.selectedtraceselectmultipleadd) {
                        selectedtraceclicked = Gallery.selectedtraceselectmultipleadd;
                    } else if (sel == Gallery.selectedtracecolor) {
                        model.message("change color");
                        selectedtraceclicked = Gallery.selectedtracecolor;
                    } else if (sel == Gallery.selectedtracethickness) {
                        model.message("change thickness");
                        selectedtraceclicked = Gallery.selectedtracethickness;
                    } else if (sel == Gallery.selectedtracemovetrace) {
                        model.message("move trace");
                        selectedtraceclicked = Gallery.selectedtracemovetrace;
                        selectedtracemovepointortrace = Gallery.selectedtracemovetrace;
                    } else if (sel == Gallery.selectedtracemovepoint) {
                        model.message("move one point");
                        selectedtraceclicked = Gallery.selectedtracemovepoint;
                        selectedtracemovepointortrace = Gallery.selectedtracemovepoint;
                    } else if (sel == Gallery.selectedtracemoveto) {
                        model.message("move here");
                        selectedtraceclicked = Gallery.selectedtracemoveto;
                    } else if (sel == Gallery.selectedtraceduplicate) {
                        model.message("duplicate");
                        selectedtraceclicked = Gallery.selectedtraceduplicate;
                    } else if (sel == Gallery.selectedtracechangetype) {
                        model.message("change type");
                        selectedtraceclicked = Gallery.selectedtracechangetype;
                    } else if (sel == Gallery.selectedtraceunder) {
                        selectedtraceclicked = Gallery.selectedtraceunder;
                    } else if (sel == Gallery.selectedtraceover) {
                        selectedtraceclicked = Gallery.selectedtraceover;
                    } else if (sel == Gallery.selectedtraceresize) {
                        model.message("resize");
                        selectedtraceclicked = Gallery.selectedtraceresize;
                    } else if (sel == Gallery.selectedtracecancel) {
                        mysurf.drawdevelopmenuselection = false;
                        model.message();
                        selectedtraceclicked = Gallery.selectedtracecancel;
                    } else {
                        model.message();
                        mysurf.selectedtrace = Gallery.selectedtracecancel;
                        selectedtraceclicked = Gallery.selectedtracecancel;
                    }
                }
            }
        }
        recti = new RectF(posxi, posyi, posxf, posyf);
        surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinBgPaint);
        if (sel == Gallery.selectedtraceerase) {
            surfacecanvas.drawBitmap(drawtraceremove, posxi, posyi, null);
            if (mysurf.selectedtrace == Gallery.selectedtraceerase)
                surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinSelectedPaint);
            surfacecanvas.drawText("rm", posxi, posyf, model.DessinTextPaint);
        } else if (sel == Gallery.selectedtraceselectmultiple) {
            if (selectmultiple) {
                surfacecanvas.drawBitmap(drawselectmultiple, posxi, posyi, null);
                int cl = selclosestpath.size();
                if (cl == 0)
                    surfacecanvas.drawText("mul", posxi, posyf, model.DessinTextPaint);
                else
                    surfacecanvas.drawText("" + cl, posxi, posyf, model.DessinTextPaint);
            } else {
                surfacecanvas.drawBitmap(drawselectone, posxi, posyi, null);
                surfacecanvas.drawText("one", posxi, posyf, model.DessinTextPaint);
            }
        } else if (sel == Gallery.selectedtraceselectmultipleremove) {
            surfacecanvas.drawBitmap(drawselectone, posxi, posyi, null);
            if (mysurf.selectedtrace == Gallery.selectedtraceselectmultipleremove)
                surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinSelectedPaint);
            surfacecanvas.drawText("-", posxi, posyf, model.DessinTextPaint);
        } else if (sel == Gallery.selectedtraceselectmultipleadd) {
            surfacecanvas.drawBitmap(drawselectone, posxi, posyi, null);
            if (mysurf.selectedtrace == Gallery.selectedtraceselectmultipleadd)
                surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinSelectedPaint);
            surfacecanvas.drawText("+", posxi, posyf, model.DessinTextPaint);
        } else if (sel == Gallery.selectedtracecolor) {
            surfacecanvas.drawBitmap(drawchangecolor, posxi, posyi, null);
            if (mysurf.selectedtrace == Gallery.selectedtracecolor)
                surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinSelectedPaint);
            surfacecanvas.drawText("col", posxi, posyf, model.DessinTextPaint);
        } else if (sel == Gallery.selectedtracethickness) {
            surfacecanvas.drawBitmap(drawchangethickness, posxi, posyi, null);
            if (mysurf.selectedtrace == Gallery.selectedtracethickness)
                surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinSelectedPaint);
            surfacecanvas.drawText("thck", posxi, posyf, model.DessinTextPaint);
        } else if (sel == Gallery.selectedtraceduplicate) {
            surfacecanvas.drawBitmap(drawduplicate, posxi, posyi, null);
            if (mysurf.selectedtrace == Gallery.selectedtraceduplicate)
                surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinSelectedPaint);
            surfacecanvas.drawText("dupl", posxi, posyf, model.DessinTextPaint);
        } else if (sel == Gallery.selectedtracemovetrace) {
            surfacecanvas.drawBitmap(drawmove, posxi, posyi, null);
            if (mysurf.selectedtrace == Gallery.selectedtracemovetrace)
                surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinSelectedPaint);
            surfacecanvas.drawText("mv", posxi, posyf, model.DessinTextPaint);
        } else if (sel == Gallery.selectedtracemovepoint) {
            surfacecanvas.drawBitmap(drawmovepoint, posxi, posyi, null);
            if (mysurf.selectedtrace == Gallery.selectedtracemovepoint)
                surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinSelectedPaint);
            surfacecanvas.drawText("mv pt", posxi, posyf, model.DessinTextPaint);
        } else if (sel == Gallery.selectedtracemoveto) {
            surfacecanvas.drawBitmap(drawmoveto, posxi, posyi, null);
            if (mysurf.selectedtrace == Gallery.selectedtracemoveto)
                surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinSelectedPaint);
            surfacecanvas.drawText("mv to", posxi, posyf, model.DessinTextPaint);
        } else if (sel == Gallery.selectedtracechangetype) {
            surfacecanvas.drawBitmap(drawchangetype, posxi, posyi, null);
            if (mysurf.selectedtrace == Gallery.selectedtracechangetype)
                surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinSelectedPaint);
            surfacecanvas.drawText("type", posxi, posyf, model.DessinTextPaint);
        } else if (sel == Gallery.selectedtraceunder) {
            surfacecanvas.drawBitmap(drawchangedown, posxi, posyi, null);
            if (mysurf.selectedtrace == Gallery.selectedtraceunder)
                surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinSelectedPaint);
            surfacecanvas.drawText("dwn", posxi, posyf, model.DessinTextPaint);
        } else if (sel == Gallery.selectedtraceover) {
            surfacecanvas.drawBitmap(drawchangeup, posxi, posyi, null);
            if (mysurf.selectedtrace == Gallery.selectedtraceover)
                surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinSelectedPaint);
            surfacecanvas.drawText("up", posxi, posyf, model.DessinTextPaint);
        } else if (sel == Gallery.selectedtraceresize) {
            surfacecanvas.drawBitmap(drawchangeresize, posxi, posyi, null);
            if (mysurf.selectedtrace == Gallery.selectedtraceresize)
                surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinSelectedPaint);
            surfacecanvas.drawText(String.format("%.2f", mysurf.selectedtraceresizeratio), posxi, posyf, model.DessinTextPaint);
        } else {
            surfacecanvas.drawBitmap(change, posxi, posyi, null);
            surfacecanvas.drawText("-", posxi, posyf, model.DessinTextPaint);
        }
        if (posxf > posxmax)
            posxmax = posxf;
        return true;
    }

    public boolean menuzoom(Canvas surfacecanvas) {
        if (!mysurf.drawmovezoom) {
            ecritoption = "";
        } else {
            int cettelayer = mysurf.drawcurrlayer;
            if (mysurf.drawcurrlayer == -1)
                cettelayer = 0;
            if (cettelayer < mysurf.listecouche.size()) {
                float zzoom = mysurf.listecouche.get(cettelayer).scale;
                if (zzoom == 1.0f)
                    ecritoption = "1";
                else
                    ecritoption = String.format("%.2f", zzoom);
            }
        }
        posxi = posxf + model.DessinInterSpace * 5.0f;
        ecritx = posxi + model.DessinInterSpace;
        ecrity = posyi + model.DessinCaseH * 0.5f + 0.33f * model.DessinCaseTextH;
        posxf = posxi + model.DessinCaseH + model.DessinInterSpace;
        posyf = posyi + model.DessinCaseH;
        if (onacliqueinit) {
            if (posxi < cliquex && cliquex < posxf && posyi < cliquey && cliquey < posyf) {
                onacliqueinit = false;
                onacliquemove = false;
        /*if (cliquey < (posyi + posyf) / 3.0f) {
          int dl = mysurf.listecouche.size();
          for (int i = 0 ; i < dl ; i++) {
            if (mysurf.listecouche.get(i).active) {
              mysurf.listecouche.get(i).scale = 1.0f;
            }
          }
          mysurf.drawmovezoom = false;
        } else {*/
                if (mysurf.drawmovezoom) {
                    // on a fini notre zoom
                    if (mysurf.drawcurrlayer <= 0) {
                        // on redessine la trace sur un nouvel écran
                        // mais inutile de recreephoto
                        recreecanvasenzero(0, mysurf.ScreenWidth, mysurf.ScreenHeight);
                    }
                } else {
                    // on va commencer le zoom
                    // on ne peut plus dessiner
                    if (mysurf.drawcurrlayer == -1) {
                        int drawbitmapl = mysurf.listecouche.size();
                        for (int k = 0; k < drawbitmapl; k++) {
                            mysurf.listecouche.get(k).active = true;
                        }
                    } else {
                        int drawbitmapl = mysurf.listecouche.size();
                        for (int k = 0; k < drawbitmapl; k++) {
                            if (k == mysurf.drawcurrlayer) {
                                mysurf.listecouche.get(k).active = true;
                            } else {
                                mysurf.listecouche.get(k).active = false;
                            }
                        }
                    }
                }
                mysurf.drawmovezoom = !mysurf.drawmovezoom;
                //}
            }
        }
        recti = new RectF(posxi, posyi, posxf, posyf);
        surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinBgPaint);
        if (mysurf.drawmovezoom) {
            surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinSelectedPaint);
        }
        surfacecanvas.drawBitmap(zoom, posxi, posyi, null);
        surfacecanvas.drawText(ecritoption, posxi, posyf, model.DessinTextPaint);
        if (posxf > posxmax)
            posxmax = posxf;
        return true;
    }

    public boolean menuundo(Canvas surfacecanvas) {
        posxi = posxf + model.DessinInterSpace;
        posxf = posxi + model.DessinCaseH + model.DessinInterSpace;
        posyf = posyi + model.DessinCaseH;
        if (onacliqueinit) {
            if (posxi < cliquex && cliquex < posxf && posyi < cliquey && cliquey < posyf) {
                onacliqueinit = false;
                onacliquemove = false;
                int dernierelement = mysurf.listedessin.size() - 1;
                if (dernierelement >= 0) {
                    int cettelayer = mysurf.listedessin.get(dernierelement).couche;
                    mysurf.listedessin.remove(dernierelement);
                    if (!mysurf.listecouche.get(cettelayer).isaphoto) {
                        recreecanvasenzero(cettelayer, mysurf.ScreenWidth, mysurf.ScreenHeight);
                    } else {
                        recreephoto(cettelayer);
                    }
                }
            }
        }
        recti = new RectF(posxi, posyi, posxf, posyf);
        surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinBgPaint);
        surfacecanvas.drawBitmap(undo, posxi, posyi, null);
        if (posxf > posxmax)
            posxmax = posxf;
        return true;
    }

    public boolean menuselectlast(Canvas surfacecanvas) {
        posxi = posxf + model.DessinInterSpace;
        posxf = posxi + model.DessinCaseH + model.DessinInterSpace;
        posyf = posyi + model.DessinCaseH;
        if (onacliqueinit) {
            if (posxi < cliquex && cliquex < posxf && posyi < cliquey && cliquey < posyf) {
                onacliqueinit = false;
                onacliquemove = false;
                int dernierelement = mysurf.listedessin.size() - 1;
                if (dernierelement >= 0) {
                    int cettelayer = mysurf.listedessin.get(dernierelement).couche;
                    mysurf.listedessin.remove(dernierelement);
                    if (!mysurf.listecouche.get(cettelayer).isaphoto) {
                        recreecanvasenzero(cettelayer, mysurf.ScreenWidth, mysurf.ScreenHeight);
                    } else {
                        recreephoto(cettelayer);
                    }
                }
            }
        }
        recti = new RectF(posxi, posyi, posxf, posyf);
        surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinBgPaint);
        surfacecanvas.drawBitmap(select, posxi, posyi, null);
        if (posxf > posxmax)
            posxmax = posxf;
        return true;
    }

    public boolean menuduplicate(Canvas surfacecanvas) {
        posxi = posxf + model.DessinInterSpace * 5.0f;
        posxf = posxi + model.DessinCaseH + model.DessinInterSpace;
        posyf = posyi + model.DessinCaseH;
        if (onacliqueinit) {
            if (posxi < cliquex && cliquex < posxf && posyi < cliquey && cliquey < posyf) {
                onacliqueinit = false;
                onacliquemove = false;
                int dernierelement = mysurf.listedessin.size() - 1;
                if (dernierelement >= 0) {
                    Dessin olddessin = mysurf.listedessin.get(dernierelement);
                    Dessin cedessin = new Dessin();
                    int nbpointrace = olddessin.x.length;
                    cedessin.x = new float[nbpointrace];
                    cedessin.y = new float[nbpointrace];
                    cedessin.p = new float[nbpointrace];
                    cedessin.d = new float[nbpointrace];
                    cedessin.t = new float[nbpointrace];
                    for (int k = 0; k < nbpointrace; k++) {
                        cedessin.x[k] = olddessin.x[k];
                        cedessin.y[k] = olddessin.y[k];
                        cedessin.p[k] = olddessin.p[k];
                        cedessin.d[k] = olddessin.d[k];
                        cedessin.t[k] = olddessin.t[k];

                    }
                    cedessin.couleura = mysurf.drawtracecurrcolora;
                    cedessin.couleurh = mysurf.drawtracecurrcolorh;
                    cedessin.couleurs = mysurf.drawtracecurrcolors;
                    cedessin.couleurv = mysurf.drawtracecurrcolorv;
                    cedessin.generecouleur();
                    cedessin.pressuremax = model.drawtracepressuremax;
                    cedessin.pressuremin = model.drawtracepressuremin;
                    cedessin.sizemin = mysurf.drawtracecurrsizemin;
                    cedessin.sizemax = mysurf.drawtracecurrsizemax;
                    cedessin.drawerasefillcontour = mysurf.drawtracecurrdefcb;
                    cedessin.bezierlength = mysurf.drawtracecurrbezier;
                    int cettelayer = mysurf.drawcurrlayer;
                    if (cettelayer < 0) {
                        cettelayer = 0;
                    }
                    cedessin.couche = cettelayer;
                    mysurf.listedessin.add(cedessin);
                    Canvas thissurfacecanvas = mysurf.listecouche.get(cettelayer).canvas;
                    dessinelatrace(dernierelement + 1, thissurfacecanvas);
                }
            }
        }
        recti = new RectF(posxi, posyi, posxf, posyf);
        surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinBgPaint);
        surfacecanvas.drawBitmap(duplicate, posxi, posyi, null);
        if (posxf > posxmax)
            posxmax = posxf;
        return true;
    }

    public boolean menuchange(Canvas surfacecanvas) {
        posxi = posxf + model.DessinInterSpace;
        posxf = posxi + model.DessinCaseH + model.DessinInterSpace;
        posyf = posyi + model.DessinCaseH;
        if (onacliqueinit) {
            if (posxi < cliquex && cliquex < posxf && posyi < cliquey && cliquey < posyf) {
                onacliqueinit = false;
                onacliquemove = false;
                int dernierelement = mysurf.listedessin.size() - 1;
                if (dernierelement >= 0) {
                    Dessin olddessin = mysurf.listedessin.get(dernierelement);
                    Dessin cedessin = new Dessin();
                    int nbpointrace = olddessin.x.length;
                    cedessin.x = new float[nbpointrace];
                    cedessin.y = new float[nbpointrace];
                    cedessin.p = new float[nbpointrace];
                    cedessin.d = new float[nbpointrace];
                    cedessin.t = new float[nbpointrace];
                    for (int k = 0; k < nbpointrace; k++) {
                        cedessin.x[k] = olddessin.x[k];
                        cedessin.y[k] = olddessin.y[k];
                        cedessin.p[k] = olddessin.p[k];
                        cedessin.d[k] = olddessin.d[k];
                        cedessin.t[k] = olddessin.t[k];

                    }
                    cedessin.couleura = mysurf.drawtracecurrcolora;
                    cedessin.couleurh = mysurf.drawtracecurrcolorh;
                    cedessin.couleurs = mysurf.drawtracecurrcolors;
                    cedessin.couleurv = mysurf.drawtracecurrcolorv;
                    cedessin.generecouleur();
                    cedessin.pressuremax = model.drawtracepressuremax;
                    cedessin.pressuremin = model.drawtracepressuremin;
                    cedessin.sizemin = mysurf.drawtracecurrsizemin;
                    cedessin.sizemax = mysurf.drawtracecurrsizemax;
                    cedessin.drawerasefillcontour = mysurf.drawtracecurrdefcb;
                    cedessin.bezierlength = mysurf.drawtracecurrbezier;
                    int cettelayer = mysurf.drawcurrlayer;
                    if (cettelayer < 0) {
                        cettelayer = 0;
                    }
                    cedessin.couche = cettelayer;
                    mysurf.listedessin.set(dernierelement, cedessin);
                    if (!mysurf.listecouche.get(cettelayer).isaphoto) {
                        recreecanvasenzero(cettelayer, mysurf.ScreenWidth, mysurf.ScreenHeight);
                    } else {
                        recreephoto(cettelayer);
                    }
                }
            }
        }
        recti = new RectF(posxi, posyi, posxf, posyf);
        surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinBgPaint);
        surfacecanvas.drawBitmap(change, posxi, posyi, null);
        if (posxf > posxmax)
            posxmax = posxf;
        return true;
    }

    public boolean menubgcolor(Canvas surfacecanvas) {
        posxi = posxf + model.DessinInterSpace;
        posxf = posxi + model.DessinCaseH + model.DessinInterSpace;
        posyf = posyi + model.DessinCaseH;
        if (onacliqueinit) {
            if (posxi < cliquex && cliquex < posxf && posyi < cliquey && cliquey < posyf) {
                onacliqueinit = false;
                onacliquemove = false;
                llog.d(TAG, posxi + " " + cliquex + " " + posxf + " ; " + posyi + " " + cliquey + " " + posyf);
                backgroundcolor = Color.HSVToColor(new float[]{mysurf.drawtracecurrcolorh, mysurf.drawtracecurrcolors, mysurf.drawtracecurrcolorv});
            }
        }
        recti = new RectF(posxi, posyi, posxf, posyf);
        surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinBgPaint);
        surfacecanvas.drawBitmap(bg, posxi, posyi, null);
        if (posxf > posxmax)
            posxmax = posxf;
        return true;
    }

    public boolean menupickcolor(Canvas surfacecanvas) {
        posxi = posxf + model.DessinInterSpace;
        posxf = posxi + model.DessinCaseH + model.DessinInterSpace;
        posyf = posyi + model.DessinCaseH;
        if (onacliqueinit || onacliquemove) {
            if (onacliqueinit && posxi < cliquex && cliquex < posxf && posyi < cliquey && cliquey < posyf) {
                // attention il faut un onacliqueinit uniquement sinon chain click ! switches !
                onacliqueinit = false;
                onacliquemove = false;
                if (mysurf.pickacolor == mysurf.pickacoloralllayers)
                    mysurf.pickacolor = 0;
                else
                    mysurf.pickacolor = mysurf.pickacoloralllayers;
            } else if (mysurf.pickacolor == mysurf.pickacoloralllayers && !(cliquex < mysurf.drawmenutotalw && cliquey < mysurf.drawmenutotalh)) {
                // attention on ne prend pas dans le menudessiner
                onacliqueinit = false;
                onacliquemove = false;
                int celayer = -1;
                if (mysurf.drawcurrlayer >= 0) {
                    celayer = mysurf.drawcurrlayer;
                    Couche cettecouche = mysurf.listecouche.get(celayer);
                    float coulenx = (cliquex - cettecouche.x) / cettecouche.scale;
                    float couleny = (cliquey - cettecouche.y) / cettecouche.scale;
                    if (0 <= coulenx && coulenx < cettecouche.originalw && 0 <= couleny && couleny < cettecouche.originalh) {
                        int couleur = cettecouche.currentbitmap.getPixel((int) coulenx, (int) couleny);
                        if ((couleur & 0xffffff) != (backgroundcolor & 0xffffff)) {
                            // on regarde s'il n'y a rien sur les autres couches
                            celayer = -1;
                        } else {
                            float[] couleurhsv = new float[3];
                            Color.colorToHSV(couleur, couleurhsv);
                            mysurf.drawtracecurrcolorh = couleurhsv[0];
                            mysurf.drawtracecurrcolors = couleurhsv[1];
                            mysurf.drawtracecurrcolorv = couleurhsv[2];
                        }
                    } else {
                        celayer = -1;
                    }
                }
                if (celayer == -1) {
                    int drawbitmapl = mysurf.listecouche.size();
                    for (int i = 0; i < drawbitmapl; i++) {
                        celayer = i;
                        Couche cettecouche = mysurf.listecouche.get(celayer);
                        float coulenx = (cliquex - cettecouche.x) / cettecouche.scale;
                        float couleny = (cliquey - cettecouche.y) / cettecouche.scale;
                        if (0 <= coulenx && coulenx < cettecouche.originalw && 0 <= couleny && couleny < cettecouche.originalh) {
                            int couleur = cettecouche.currentbitmap.getPixel((int) coulenx, (int) couleny);
                            float[] couleurhsv = new float[3];
                            Color.colorToHSV(couleur, couleurhsv);
                            mysurf.drawtracecurrcolorh = couleurhsv[0];
                            mysurf.drawtracecurrcolors = couleurhsv[1];
                            mysurf.drawtracecurrcolorv = couleurhsv[2];
                            if ((couleur & 0xffffff) != (backgroundcolor & 0xffffff)) {
                                // sinon on attend de voir
                                // s'il y a plus intéressant derrière
                                // on écrira par-dessus et on quittera
                                break;
                            }
                        }
                    }
                }
            }
        }
        recti = new RectF(posxi, posyi, posxf, posyf);
        surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinBgPaint);
        if (mysurf.pickacolor == mysurf.pickacoloralllayers) {
            surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinSelectedPaint);
        }
        surfacecanvas.drawBitmap(pickcolor, posxi, posyi, null);
        if (posxf > posxmax)
            posxmax = posxf;
        return true;
    }

    public boolean menupickcolorclosest(Canvas surfacecanvas) {
        posxi = posxf + model.DessinInterSpace;
        posxf = posxi + model.DessinCaseH + model.DessinInterSpace;
        posyf = posyi + model.DessinCaseH;
        if (onacliqueinit || onacliquemove) {
            if (onacliqueinit && posxi < cliquex && cliquex < posxf && posyi < cliquey && cliquey < posyf) {
                // attention il faut un onacliqueinit uniquement sinon chain click ! switches !
                onacliqueinit = false;
                onacliquemove = false;
                if (mysurf.pickacolor == mysurf.pickacolorclosest)
                    mysurf.pickacolor = 0;
                else
                    mysurf.pickacolor = mysurf.pickacolorclosest;
            } else if (mysurf.pickacolor == mysurf.pickacolorclosest && !(cliquex < mysurf.drawmenutotalw && cliquey < mysurf.drawmenutotalh)) {
                // attention on ne prend pas dans le menudessiner
                onacliqueinit = false;
                onacliquemove = false;
                int celayer = mysurf.drawcurrlayer;
                if (celayer == -1)
                    celayer = 0;
                int selclosestpathu = -1;
                Couche cettecouche = mysurf.listecouche.get(celayer);
                float x = (cliquex - cettecouche.x) / cettecouche.scale;
                float y = (cliquey - cettecouche.y) / cettecouche.scale;
                int listedessinl = mysurf.listedessin.size();
                float meilleurv = -1.0f;
                Dessin cedessin;
                for (int i = 0; i < listedessinl; i++) {
                    cedessin = mysurf.listedessin.get(i);
                    if (cedessin.couche == celayer) {
                        if (cedessin.x != null) {
                            if (cedessin.x.length > 0) {
                                for (int j = 0; j < cedessin.x.length; j++) {
                                    float d2 = (cedessin.x[j] - x) * (cedessin.x[j] - x)
                                            + (cedessin.y[j] - y) * (cedessin.y[j] - y);
                                    if (d2 < meilleurv || meilleurv < 0) {
                                        meilleurv = d2;
                                        selclosestpathu = i;
                                    }
                                }
                            }
                        }
                    }
                }
                if (selclosestpathu >= 0) {
                    cedessin = mysurf.listedessin.get(selclosestpathu);
                    mysurf.drawtracecurrcolora = cedessin.couleura;
                    mysurf.drawtracecurrcolorh = cedessin.couleurh;
                    mysurf.drawtracecurrcolors = cedessin.couleurs;
                    mysurf.drawtracecurrcolorv = cedessin.couleurv;
                }
            }
        }
        recti = new RectF(posxi, posyi, posxf, posyf);
        surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinBgPaint);
        if (mysurf.pickacolor == mysurf.pickacolorclosest) {
            surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinSelectedPaint);
        }
        surfacecanvas.drawBitmap(drawselectone, posxi, posyi, null);
        surfacecanvas.drawText("col", posxi, posyf, model.DessinTextPaint);
        if (posxf > posxmax)
            posxmax = posxf;
        return true;
    }

    public boolean menupickthicknessclosest(Canvas surfacecanvas) {
        posxi = posxf + model.DessinInterSpace;
        posxf = posxi + model.DessinCaseH + model.DessinInterSpace;
        posyf = posyi + model.DessinCaseH;
        if (onacliqueinit || onacliquemove) {
            if (onacliqueinit && posxi < cliquex && cliquex < posxf && posyi < cliquey && cliquey < posyf) {
                // attention il faut un onacliqueinit uniquement sinon chain click ! switches !
                onacliqueinit = false;
                onacliquemove = false;
                if (mysurf.pickacolor == mysurf.pickacolorthickness)
                    mysurf.pickacolor = 0;
                else
                    mysurf.pickacolor = mysurf.pickacolorthickness;
            } else if (mysurf.pickacolor == mysurf.pickacolorthickness && !(cliquex < mysurf.drawmenutotalw && cliquey < mysurf.drawmenutotalh)) {
                // attention on ne prend pas dans le menudessiner
                onacliqueinit = false;
                onacliquemove = false;
                int celayer = mysurf.drawcurrlayer;
                if (celayer == -1)
                    celayer = 0;
                int selclosestpathu = -1;
                Couche cettecouche = mysurf.listecouche.get(celayer);
                float x = (cliquex - cettecouche.x) / cettecouche.scale;
                float y = (cliquey - cettecouche.y) / cettecouche.scale;
                int listedessinl = mysurf.listedessin.size();
                float meilleurv = -1.0f;
                Dessin cedessin;
                for (int i = 0; i < listedessinl; i++) {
                    cedessin = mysurf.listedessin.get(i);
                    if (cedessin.couche == celayer) {
                        if (cedessin.x != null) {
                            if (cedessin.x.length > 0) {
                                for (int j = 0; j < cedessin.x.length; j++) {
                                    float d2 = (cedessin.x[j] - x) * (cedessin.x[j] - x)
                                            + (cedessin.y[j] - y) * (cedessin.y[j] - y);
                                    if (d2 < meilleurv || meilleurv < 0) {
                                        meilleurv = d2;
                                        selclosestpathu = i;
                                    }
                                }
                            }
                        }
                    }
                }
                if (selclosestpathu >= 0) {
                    cedessin = mysurf.listedessin.get(selclosestpathu);
                    mysurf.drawtracecurrsizemin = cedessin.sizemin;
                    mysurf.drawtracecurrsizemax = cedessin.sizemax;
                }
            }
        }
        recti = new RectF(posxi, posyi, posxf, posyf);
        surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinBgPaint);
        if (mysurf.pickacolor == mysurf.pickacolorthickness) {
            surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinSelectedPaint);
        }
        surfacecanvas.drawBitmap(drawselectone, posxi, posyi, null);
        surfacecanvas.drawText("thck", posxi, posyf, model.DessinTextPaint);
        if (posxf > posxmax)
            posxmax = posxf;
        return true;
    }

    private boolean colormenuexpand = false;

    public boolean menucolorexpand(Canvas surfacecanvas) {
        posxi = posxf + model.DessinInterSpace;
        posxf = posxi + model.DessinCaseH + model.DessinInterSpace;
        posyf = posyi + model.DessinCaseH;
        if (onacliqueinit) {
            if (posxi < cliquex && cliquex < posxf && posyi < cliquey && cliquey < posyf) {
                colormenuexpand = !colormenuexpand;
                onacliqueinit = false;
                onacliquemove = false;
            }
        }
        recti = new RectF(posxi, posyi, posxf, posyf);
        surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.DessinBgPaint);
        surfacecanvas.drawBitmap(menusize, posxi, posyi, null);
        if (posxf > posxmax)
            posxmax = posxf;
        return true;
    }

    public boolean menucolorh(Canvas surfacecanvas) {
        posxi = posxf + model.DessinInterSpace;
        posxf = posxi + quartdelargeurrestante;
        posyf = posyi + model.DessinCaseH;
        if (onacliqueinit || onacliquemove) {
            if (posxi - model.DessinInterSpace < cliquex && cliquex < posxf + model.DessinInterSpace && posyi < cliquey && cliquey < posyf) {
                onacliqueinit = false;
                onacliquemove = false;
                float coulpente = 360.0f / (posxf - posxi);
                float coulord = -coulpente * posxi;
                if (cliquex < posxi)
                    cliquex = posxi;
                if (cliquex > posxf)
                    cliquex = posxf;
                float coul = coulpente * cliquex + coulord;
                mysurf.drawtracecurrcolorh = coul;
                model.preferences.edit().putFloat("drawtracecurrcolorh", mysurf.drawtracecurrcolorh).commit();
            }
        }
        float coulpenteinv = (posxf - posxi) / 360.0f;
        float coulordinv = posxi;
        float maposxi = posxi;
        int increment = 6;
        for (int i = 0; i < 360; i += increment) {
            int macouleur = Color.HSVToColor(new float[]{((float) i + increment / 2.0f), mysurf.drawtracecurrcolors, mysurf.drawtracecurrcolorv});
            rectangle.setColor(macouleur);
            float maposxf = coulpenteinv * ((float) i + increment) + coulordinv;
            recti = new RectF(maposxi, posyi, maposxf, posyf);
            surfacecanvas.drawRect(recti, rectangle);
            maposxi = maposxf;
        }
        float curseurpos = coulpenteinv * mysurf.drawtracecurrcolorh + coulordinv;
        rectangle2.setColor(Color.BLACK);
        surfacecanvas.drawRect(curseurpos - model.unmillimetre, posyf - model.unmillimetre, curseurpos + model.unmillimetre, posyf, rectangle2);
        rectangle2.setColor(Color.WHITE);
        surfacecanvas.drawRect(curseurpos - model.unmillimetre * 0.5f, posyf - model.unmillimetre * 0.5f, curseurpos + model.unmillimetre * 0.5f, posyf, rectangle2);
        int rgb = Color.HSVToColor(new float[]{mysurf.drawtracecurrcolorh, mysurf.drawtracecurrcolors, mysurf.drawtracecurrcolorv});
        if (!colormenuexpand)
            surfacecanvas.drawText(String.format("h %.1f            r %d g %d b %d", mysurf.drawtracecurrcolorh, Color.red(rgb), Color.green(rgb), Color.blue(rgb)), posxi, posyf + model.DessinCaseTextH, model.DessinTextPaint);
        if (posxf > posxmax)
            posxmax = posxf;
        return true;
    }

    public boolean menucolors(Canvas surfacecanvas) {
        posxi = posxf + model.DessinInterSpace;
        posxf = posxi + quartdelargeurrestante;
        posyf = posyi + model.DessinCaseH;
        if (onacliqueinit || onacliquemove) {
            if (posxi - model.DessinInterSpace < cliquex && cliquex < posxf + model.DessinInterSpace && posyi < cliquey && cliquey < posyf) {
                onacliqueinit = false;
                onacliquemove = false;
                float coulpente = 1.0f / (posxf - posxi);
                float coulord = -coulpente * posxi;
                if (cliquex < posxi)
                    cliquex = posxi;
                if (cliquex > posxf)
                    cliquex = posxf;
                float coul = coulpente * cliquex + coulord;
                mysurf.drawtracecurrcolors = coul;
                model.preferences.edit().putFloat("drawtracecurrcolors", mysurf.drawtracecurrcolors).commit();
            }
        }
        float coulpenteinv = (posxf - posxi) / 1.0f;
        float coulordinv = posxi;
        float maposxi = posxi;
        float incrementf = 0.20f;
        for (float i = 0.0f; i < 1.0f; i += incrementf) {
            int macouleur = Color.HSVToColor(new float[]{mysurf.drawtracecurrcolorh, ((float) i + incrementf / 2.0f), mysurf.drawtracecurrcolorv});
            rectangle.setColor(macouleur);
            float maposxf = coulpenteinv * ((float) i + incrementf) + coulordinv;
            recti = new RectF(maposxi, posyi, maposxf, posyf);
            surfacecanvas.drawRect(recti, rectangle);
            maposxi = maposxf;
        }
        float curseurpos = coulpenteinv * mysurf.drawtracecurrcolors + coulordinv;
        rectangle2.setColor(Color.BLACK);
        surfacecanvas.drawRect(curseurpos - model.unmillimetre, posyf - model.unmillimetre, curseurpos + model.unmillimetre, posyf, rectangle2);
        rectangle2.setColor(Color.WHITE);
        surfacecanvas.drawRect(curseurpos - model.unmillimetre * 0.5f, posyf - model.unmillimetre * 0.5f, curseurpos + model.unmillimetre * 0.5f, posyf, rectangle2);
        if (!colormenuexpand)
            surfacecanvas.drawText(String.format("s %.3f", mysurf.drawtracecurrcolors), posxi, posyf + model.DessinCaseTextH, model.DessinTextPaint);
        if (posxf > posxmax)
            posxmax = posxf;
        return true;
    }

    public boolean menucolorv(Canvas surfacecanvas) {
        posxi = posxf + model.DessinInterSpace;
        posxf = posxi + quartdelargeurrestante;
        posyf = posyi + model.DessinCaseH;
        if (onacliqueinit || onacliquemove) {
            if (posxi - model.DessinInterSpace < cliquex && cliquex < posxf + model.DessinInterSpace && posyi < cliquey && cliquey < posyf) {
                onacliqueinit = false;
                onacliquemove = false;
                float coulpente = 1.0f / (posxf - posxi);
                float coulord = -coulpente * posxi;
                if (cliquex < posxi)
                    cliquex = posxi;
                if (cliquex > posxf)
                    cliquex = posxf;
                float coul = coulpente * cliquex + coulord;
                mysurf.drawtracecurrcolorv = coul;
                model.preferences.edit().putFloat("drawtracecurrcolorv", mysurf.drawtracecurrcolorv).commit();
            }
        }
        float coulpenteinv = (posxf - posxi) / 1.0f;
        float coulordinv = posxi;
        float maposxi = posxi;
        float incrementf = 0.10f;
        for (float i = 0.0f; i < 1.0f; i += incrementf) {
            int macouleur = Color.HSVToColor(new float[]{mysurf.drawtracecurrcolorh, mysurf.drawtracecurrcolors, ((float) i + incrementf / 2.0f)});
            rectangle.setColor(macouleur);
            float maposxf = coulpenteinv * ((float) i + incrementf) + coulordinv;
            recti = new RectF(maposxi, posyi, maposxf, posyf);
            surfacecanvas.drawRect(recti, rectangle);
            maposxi = maposxf;
        }
        float curseurpos = coulpenteinv * mysurf.drawtracecurrcolorv + coulordinv;
        rectangle2.setColor(Color.BLACK);
        surfacecanvas.drawRect(curseurpos - model.unmillimetre, posyf - model.unmillimetre, curseurpos + model.unmillimetre, posyf, rectangle2);
        rectangle2.setColor(Color.WHITE);
        surfacecanvas.drawRect(curseurpos - model.unmillimetre * 0.5f, posyf - model.unmillimetre * 0.5f, curseurpos + model.unmillimetre * 0.5f, posyf, rectangle2);
        if (!colormenuexpand)
            surfacecanvas.drawText(String.format("v %.3f", mysurf.drawtracecurrcolorv), posxi, posyf + model.DessinCaseTextH, model.DessinTextPaint);
        if (posxf > posxmax)
            posxmax = posxf;
        return true;
    }

    public boolean menucolora(Canvas surfacecanvas) {
        posxi = posxf + model.DessinInterSpace;
        posxf = posxi + quartdelargeurrestante;
        posyf = posyi + model.DessinCaseH;
        if (onacliqueinit || onacliquemove) {
            if (posxi - model.DessinInterSpace < cliquex && cliquex < posxf + model.DessinInterSpace && posyi < cliquey && cliquey < posyf) {
                onacliqueinit = false;
                onacliquemove = false;
                float coulpente = 255.0f / (posxf - posxi);
                float coulord = -coulpente * posxi;
                if (cliquex < posxi)
                    cliquex = posxi;
                if (cliquex > posxf)
                    cliquex = posxf;
                float coul = coulpente * cliquex + coulord;
                mysurf.drawtracecurrcolora = coul;
                model.preferences.edit().putFloat("drawtracecurrcolora", mysurf.drawtracecurrcolora).commit();
            }
        }
        float coulpenteinv = (posxf - posxi) / 255.0f;
        float coulordinv = posxi;
        float maposxi = posxi;
        float increment = 15;
        for (int i = 0; i < 255; i += increment) {
            int macouleur = Color.HSVToColor((int) ((float) i + increment / 2.0f), new float[]{mysurf.drawtracecurrcolorh, mysurf.drawtracecurrcolors, mysurf.drawtracecurrcolorv});
            rectangle.setColor(macouleur);
            float maposxf = coulpenteinv * ((float) i + increment) + coulordinv;
            recti = new RectF(maposxi, posyi, maposxf, posyf);
            surfacecanvas.drawRect(recti, rectangle);
            maposxi = maposxf;
        }
        float curseurpos = coulpenteinv * mysurf.drawtracecurrcolora + coulordinv;
        rectangle2.setColor(Color.BLACK);
        surfacecanvas.drawRect(curseurpos - model.unmillimetre, posyf - model.unmillimetre, curseurpos + model.unmillimetre, posyf, rectangle2);
        rectangle2.setColor(Color.WHITE);
        surfacecanvas.drawRect(curseurpos - model.unmillimetre * 0.5f, posyf - model.unmillimetre * 0.5f, curseurpos + model.unmillimetre * 0.5f, posyf, rectangle2);
        if (!colormenuexpand)
            surfacecanvas.drawText(String.format("a %.1f", mysurf.drawtracecurrcolora), posxi, posyf + model.DessinCaseTextH, model.DessinTextPaint);
        else {
            int rgb = Color.HSVToColor(new float[]{mysurf.drawtracecurrcolorh, mysurf.drawtracecurrcolors, mysurf.drawtracecurrcolorv});
            surfacecanvas.drawText(String.format("h %.1f s %.3f v %.3f a %.0f           r %d g %d b %d", mysurf.drawtracecurrcolorh, mysurf.drawtracecurrcolors, mysurf.drawtracecurrcolorv, mysurf.drawtracecurrcolora, Color.red(rgb), Color.green(rgb), Color.blue(rgb)), posxi, posyf + model.DessinCaseTextH, model.DessinTextPaint);
        }
        if (posxf > posxmax)
            posxmax = posxf;
        return true;
    }

    public boolean menupressurecst(Canvas surfacecanvas) {
        if (mysurf.pressuresize) {
            ecritoption = "Pressure sensitive size";
        } else {
            ecritoption = "Keep constant size";
        }
        posxi = 0.0f + model.DessinInterSpace;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        ecritx = posxi + model.DessinInterSpace;
        ecrity = posyi + model.DessinCaseH * 0.5f + 0.33f * model.DessinCaseTextH;
        posxf = posxi + taillex + model.DessinInterSpace * 2.0f;
        posyf = posyi + model.DessinCaseH;
        if (onacliqueinit) {
            if (posxi < cliquex && cliquex < posxf && posyi < cliquey && cliquey < posyf) {
                onacliqueinit = false;
                onacliquemove = false;
                mysurf.pressuresize = !mysurf.pressuresize;
                if (!mysurf.pressuresize) {
                    mysurf.drawtracecurrsizemax = mysurf.drawtracecurrsizemin;
                }
            }
        }
        recti = new RectF(posxi, posyi, posxf, posyf);
        surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.Menu1BgPainto);
        surfacecanvas.drawText(ecritoption, posxf, ecrity, model.DessinTextPaint);
        if (posxf > posxmax)
            posxmax = posxf;
        return true;
    }

    public boolean menupointsize(Canvas surfacecanvas) {
        posxi = posxf + model.DessinInterSpace;
        posxf = mysurf.ScreenWidth - model.DessinInterSpace;
        posyf = posyi + model.DessinCaseH;
        if (onacliqueinit) {
            if (posxi < cliquex && cliquex < posxf && posyi < cliquey && cliquey < posyf) {
                onacliqueinit = false;
                onacliquemove = false;
                float rapport = (cliquex - posxi) / (posxf - posxi);
                if (rapport > 0.5f) {
                    mysurf.drawtracecurrsizemin += mysurf.drawtracemaximumadd * 2.0f * (rapport - 0.5f);
                } else if (rapport < 0.5f) {
                    mysurf.drawtracecurrsizemin -= mysurf.drawtracemaximumadd * 2.0f * (0.5f - rapport);
                }
                if (mysurf.drawtracecurrsizemin < model.unmillimetre / 100.0f) {
                    mysurf.drawtracecurrsizemin = model.unmillimetre / 100.0f;
                }
                if (mysurf.drawtracecurrsizemin > model.unmillimetre * 50.0f) {
                    mysurf.drawtracecurrsizemin = model.unmillimetre * 50.0f;
                }
                if (mysurf.drawtracecurrsizemin > mysurf.drawtracecurrsizemax) {
                    mysurf.drawtracecurrsizemax = mysurf.drawtracecurrsizemin;
                }
                model.preferences.edit().putFloat("drawtracecurrsizemin", mysurf.drawtracecurrsizemin).commit();
                model.preferences.edit().putFloat("drawtracecurrsizemax", mysurf.drawtracecurrsizemax).commit();
            }
        }
        recti = new RectF(posxi, posyi, posxf, posyf);
        surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.Menu1BgPainto);
        rectangle.setStrokeWidth(mysurf.drawtracecurrsizemin);
        rectangle.setStyle(Paint.Style.STROKE);
        float midx = (posxi + posxf) / 2.0f;
        float midy = (posyi + posyf) / 2.0f;
        surfacecanvas.drawPoint(midx, midy, rectangle);
        rectangle.setStyle(Paint.Style.FILL);
        if (posxf > posxmax)
            posxmax = posxf;
        return true;
    }

    public boolean menupressure(Canvas surfacecanvas) {
        if (model.drawtracepressuremax >= 0) {
            ecritoption = String.format("Pressure [%.1f:%.1f]", model.drawtracepressuremin, model.drawtracepressuremax);
        } else {
            ecritoption = String.format("Calibrate pressure here");
        }
        posxi = posxf + model.DessinInterSpace;
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        ecritx = posxi + model.DessinInterSpace;
        ecrity = posyi + model.DessinCaseH * 0.5f + 0.33f * model.DessinCaseTextH;
        posxf = posxi + taillex + model.DessinInterSpace * 2.0f;
        posyf = posyi + model.DessinCaseH;
        if (onacliqueinit) {
            if (posxi < cliquex && cliquex < posxf && posyi < cliquey && cliquey < posyf) {
                onacliqueinit = false;
                onacliquemove = false;
                if (neworder.length > 5) {
                    float macurrentclicpressure = Float.parseFloat(neworder[5]);
                    if (macurrentclicpressure < model.drawtracepressuremin || model.drawtracepressuremin == -99.99f) {
                        model.drawtracepressuremin = macurrentclicpressure;
                    }
                    if (macurrentclicpressure > model.drawtracepressuremax || model.drawtracepressuremax == -99.99f) {
                        model.drawtracepressuremax = macurrentclicpressure;
                    }
                    llog.d(TAG, model.drawtracepressuremin + " < pressure < " + model.drawtracepressuremax);
                }
            }
        }
        recti = new RectF(posxi, posyi, posxf, posyf);
        surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.Menu1BgPainto);
        surfacecanvas.drawText(ecritoption, posxf, ecrity, model.DessinTextPaint);
        if (posxf > posxmax)
            posxmax = posxf;
        return true;
    }

    public boolean menupressure2(Canvas surfacecanvas) {
        posxi = posxf + model.DessinInterSpace;
        posxf = mysurf.ScreenWidth - model.DessinInterSpace;
        posyf = posyi + model.DessinCaseH;
        if (onacliqueinit) {
            if (posxi < cliquex && cliquex < posxf && posyi < cliquey && cliquey < posyf) {
                onacliqueinit = false;
                onacliquemove = false;
                float rapport = (cliquex - posxi) / (posxf - posxi);
                if (rapport > 0.5f) {
                    mysurf.drawtracecurrsizemax += mysurf.drawtracemaximumadd * 2.0f * (rapport - 0.5f);
                } else if (rapport < 0.5f) {
                    mysurf.drawtracecurrsizemax -= mysurf.drawtracemaximumadd * 2.0f * (0.5f - rapport);
                }
                if (mysurf.drawtracecurrsizemax < model.unmillimetre / 100.0f) {
                    mysurf.drawtracecurrsizemax = model.unmillimetre / 100.0f;
                }
                if (mysurf.drawtracecurrsizemax > model.unmillimetre * 50.0f) {
                    mysurf.drawtracecurrsizemax = model.unmillimetre * 50.0f;
                }
                if (mysurf.drawtracecurrsizemax < mysurf.drawtracecurrsizemin) {
                    mysurf.drawtracecurrsizemin = mysurf.drawtracecurrsizemax;
                }
            }
        }
        recti = new RectF(posxi, posyi, posxf, posyf);
        surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.Menu1BgPainto);
        rectangle.setStrokeWidth(mysurf.drawtracecurrsizemax);
        rectangle.setStyle(Paint.Style.STROKE);
        float midx = (posxi + posxf) / 2.0f;
        float midy = (posyi + posyf) / 2.0f;
        surfacecanvas.drawPoint(midx, midy, rectangle);
        rectangle.setStyle(Paint.Style.FILL);
        if (posxf > posxmax)
            posxmax = posxf;
        return true;
    }

    public boolean menudrawpath(Canvas surfacecanvas) {
        rectangle.setStyle(Paint.Style.STROKE);
        rectangle.setColor(Color.BLACK);
        posxi = posxf + model.DessinInterSpace;
        posxf = mysurf.ScreenWidth - model.DessinInterSpace;
        posyf = posyi + model.DessinCaseH;
        ecritoption = "no Bézier rounding";
        taillex = model.Menu1TextePaint.measureText(ecritoption);
        ecritx = posxi + model.DessinInterSpace;
        ecrity = posyi + model.DessinCaseH * 0.5f + 0.33f * model.DessinCaseTextH;
        if (onacliqueinit) {
            if (posxi < cliquex && cliquex < posxf && posyi < cliquey && cliquey < posyf) {
                onacliqueinit = false;
                onacliquemove = false;
                float rapport = (cliquex - posxi) / (posxf - posxi);
                if (rapport > 0.5f) {
                    mysurf.drawtracecurrbezier += model.unmillimetre * 2.0f * (rapport - 0.5f);
                } else if (rapport < 0.5f) {
                    mysurf.drawtracecurrbezier -= model.unmillimetre * 2.0f * (0.5f - rapport);
                }
                if (mysurf.drawtracecurrbezier < 0.0f) {
                    mysurf.drawtracecurrbezier = 0.0f;
                }
                if (mysurf.drawtracecurrbezier > model.unmillimetre * 50.0f) {
                    mysurf.drawtracecurrbezier = model.unmillimetre * 50.0f;
                }
            }
        }
        recti = new RectF(posxi, posyi, posxf, posyf);
        surfacecanvas.drawRoundRect(recti, model.DessinCaseH * 0.2f, model.DessinCaseH * 0.2f, model.Menu1BgPainto);
        if (mysurf.drawtracecurrbezier > 0.0f) {
            rectangle.setStrokeWidth(model.unmillimetre * 0.4f);
            float midy = (posyi + posyf) / 2.0f;
            surfacecanvas.drawLine(posxi, midy, mysurf.drawtracecurrbezier, midy, rectangle);
        } else {
            surfacecanvas.drawText(ecritoption, ecritx, ecrity, model.DessinTextPaint);
        }
        rectangle.setStyle(Paint.Style.FILL);
        if (posxf > posxmax)
            posxmax = posxf;
        return true;
    }
/*
  public boolean menu(Canvas surfacecanvas) {
    if (posxf > posxmax)
      posxmax = posxf;
    return true;
  }
*/

    private void drawmenuimageloadrecycle() {
        if (pickcolor != null)
            if (!pickcolor.isRecycled())
                pickcolor.recycle();
        pickcolor = null;
        if (menusize != null)
            if (!menusize.isRecycled())
                menusize.recycle();
        menusize = null;
        if (bezier != null)
            if (!bezier.isRecycled())
                bezier.recycle();
        bezier = null;
        if (select != null)
            if (!select.isRecycled())
                select.recycle();
        select = null;
        if (recycle != null)
            if (!recycle.isRecycled())
                recycle.recycle();
        recycle = null;
        if (save != null)
            if (!save.isRecycled())
                save.recycle();
        save = null;
        if (addcanvas != null)
            if (!addcanvas.isRecycled())
                addcanvas.recycle();
        addcanvas = null;
        if (addpic != null)
            if (!addpic.isRecycled())
                addpic.recycle();
        addpic = null;
        if (change != null)
            if (!change.isRecycled())
                change.recycle();
        change = null;
        if (contour != null)
            if (!contour.isRecycled())
                contour.recycle();
        contour = null;
        if (graph != null)
            if (!graph.isRecycled())
                graph.recycle();
        graph = null;
        if (contrast != null)
            if (!contrast.isRecycled())
                contrast.recycle();
        contrast = null;
        if (draw != null)
            if (!draw.isRecycled())
                draw.recycle();
        draw = null;
        if (duplicate != null)
            if (!duplicate.isRecycled())
                duplicate.recycle();
        duplicate = null;
        if (erase != null)
            if (!erase.isRecycled())
                erase.recycle();
        erase = null;
        if (fill != null)
            if (!fill.isRecycled())
                fill.recycle();
        fill = null;
        if (hand != null)
            if (!hand.isRecycled())
                hand.recycle();
        hand = null;
        if (layers != null)
            if (!layers.isRecycled())
                layers.recycle();
        layers = null;
        if (quit != null)
            if (!quit.isRecycled())
                quit.recycle();
        quit = null;
        if (stylus != null)
            if (!stylus.isRecycled())
                stylus.recycle();
        stylus = null;
        if (undo != null)
            if (!undo.isRecycled())
                undo.recycle();
        undo = null;
        if (zoom != null)
            if (!zoom.isRecycled())
                zoom.recycle();
        zoom = null;
        if (drawbgspot != null)
            if (!drawbgspot.isRecycled())
                drawbgspot.recycle();
        drawbgspot = null;
        if (drawchangethickness != null)
            if (!drawchangethickness.isRecycled())
                drawchangethickness.recycle();
        drawchangethickness = null;
        if (drawduplicate != null)
            if (!drawduplicate.isRecycled())
                drawduplicate.recycle();
        drawduplicate = null;
        if (drawline != null)
            if (!drawline.isRecycled())
                drawline.recycle();
        drawline = null;
        if (drawmove != null)
            if (!drawmove.isRecycled())
                drawmove.recycle();
        drawmove = null;
        if (drawmovepoint != null)
            if (!drawmovepoint.isRecycled())
                drawmovepoint.recycle();
        drawmovepoint = null;
        if (drawmoveto != null)
            if (!drawmoveto.isRecycled())
                drawmoveto.recycle();
        drawmoveto = null;
        if (drawradialfill != null)
            if (!drawradialfill.isRecycled())
                drawradialfill.recycle();
        drawradialfill = null;
        if (drawselectmultiple != null)
            if (!drawselectmultiple.isRecycled())
                drawselectmultiple.recycle();
        drawselectmultiple = null;
        if (drawselectone != null)
            if (!drawselectone.isRecycled())
                drawselectone.recycle();
        drawselectone = null;
        if (drawtraceremove != null)
            if (!drawtraceremove.isRecycled())
                drawtraceremove.recycle();
        drawtraceremove = null;
        if (drawchangecolor != null)
            if (!drawchangecolor.isRecycled())
                drawchangecolor.recycle();
        drawchangecolor = null;
        if (drawchangedown != null)
            if (!drawchangedown.isRecycled())
                drawchangedown.recycle();
        drawchangedown = null;
        if (drawchangeup != null)
            if (!drawchangeup.isRecycled())
                drawchangeup.recycle();
        drawchangeup = null;
        if (drawchangeresize != null)
            if (!drawchangeresize.isRecycled())
                drawchangeresize.recycle();
        drawchangeresize = null;
        if (drawchangetype != null)
            if (!drawchangetype.isRecycled())
                drawchangetype.recycle();
        drawchangetype = null;
    }

    private void drawmenuimageload() {
        addcanvas = BitmapFactory.decodeResource(model.activitycontext.getResources(), R.drawable.addcanvas);
        addcanvas = Bitmap.createScaledBitmap(addcanvas, (int) model.DessinCaseH, (int) model.DessinCaseH, false);
        addpic = BitmapFactory.decodeResource(model.activitycontext.getResources(), R.drawable.addpic);
        addpic = Bitmap.createScaledBitmap(addpic, (int) model.DessinCaseH, (int) model.DessinCaseH, false);
        bezier = BitmapFactory.decodeResource(model.activitycontext.getResources(), R.drawable.bezier);
        bezier = Bitmap.createScaledBitmap(bezier, (int) model.DessinCaseH, (int) model.DessinCaseH, false);
        bg = BitmapFactory.decodeResource(model.activitycontext.getResources(), R.drawable.bg);
        bg = Bitmap.createScaledBitmap(bg, (int) model.DessinCaseH, (int) model.DessinCaseH, false);
        change = BitmapFactory.decodeResource(model.activitycontext.getResources(), R.drawable.change);
        change = Bitmap.createScaledBitmap(change, (int) model.DessinCaseH, (int) model.DessinCaseH, false);
        contour = BitmapFactory.decodeResource(model.activitycontext.getResources(), R.drawable.contour);
        contour = Bitmap.createScaledBitmap(contour, (int) model.DessinCaseH, (int) model.DessinCaseH, false);
        contrast = BitmapFactory.decodeResource(model.activitycontext.getResources(), R.drawable.contrast);
        contrast = Bitmap.createScaledBitmap(contrast, (int) model.DessinCaseH, (int) model.DessinCaseH, false);
        draw = BitmapFactory.decodeResource(model.activitycontext.getResources(), R.drawable.draw);
        draw = Bitmap.createScaledBitmap(draw, (int) model.DessinCaseH, (int) model.DessinCaseH, false);
        duplicate = BitmapFactory.decodeResource(model.activitycontext.getResources(), R.drawable.duplicate);
        duplicate = Bitmap.createScaledBitmap(duplicate, (int) model.DessinCaseH, (int) model.DessinCaseH, false);
        erase = BitmapFactory.decodeResource(model.activitycontext.getResources(), R.drawable.erase);
        erase = Bitmap.createScaledBitmap(erase, (int) model.DessinCaseH, (int) model.DessinCaseH, false);
        fill = BitmapFactory.decodeResource(model.activitycontext.getResources(), R.drawable.fill);
        fill = Bitmap.createScaledBitmap(fill, (int) model.DessinCaseH, (int) model.DessinCaseH, false);
        graph = BitmapFactory.decodeResource(model.activitycontext.getResources(), R.drawable.graph);
        graph = Bitmap.createScaledBitmap(graph, (int) model.DessinCaseH, (int) model.DessinCaseH, false);
        hand = BitmapFactory.decodeResource(model.activitycontext.getResources(), R.drawable.hand);
        hand = Bitmap.createScaledBitmap(hand, (int) model.DessinCaseH, (int) model.DessinCaseH, false);
        layers = BitmapFactory.decodeResource(model.activitycontext.getResources(), R.drawable.layers);
        layers = Bitmap.createScaledBitmap(layers, (int) model.DessinCaseH, (int) model.DessinCaseH, false);
        menusize = BitmapFactory.decodeResource(model.activitycontext.getResources(), R.drawable.menusize);
        menusize = Bitmap.createScaledBitmap(menusize, (int) model.DessinCaseH, (int) model.DessinCaseH, false);
        pickcolor = BitmapFactory.decodeResource(model.activitycontext.getResources(), R.drawable.pickcolor);
        pickcolor = Bitmap.createScaledBitmap(pickcolor, (int) model.DessinCaseH, (int) model.DessinCaseH, false);
        quit = BitmapFactory.decodeResource(model.activitycontext.getResources(), R.drawable.quit);
        quit = Bitmap.createScaledBitmap(quit, (int) model.DessinCaseH, (int) model.DessinCaseH, false);
        recycle = BitmapFactory.decodeResource(model.activitycontext.getResources(), R.drawable.recycle);
        recycle = Bitmap.createScaledBitmap(recycle, (int) model.DessinCaseH, (int) model.DessinCaseH, false);
        save = BitmapFactory.decodeResource(model.activitycontext.getResources(), R.drawable.save);
        save = Bitmap.createScaledBitmap(save, (int) model.DessinCaseH, (int) model.DessinCaseH, false);
        select = BitmapFactory.decodeResource(model.activitycontext.getResources(), R.drawable.select);
        select = Bitmap.createScaledBitmap(select, (int) model.DessinCaseH, (int) model.DessinCaseH, false);
        stylus = BitmapFactory.decodeResource(model.activitycontext.getResources(), R.drawable.stylus);
        stylus = Bitmap.createScaledBitmap(stylus, (int) model.DessinCaseH, (int) model.DessinCaseH, false);
        undo = BitmapFactory.decodeResource(model.activitycontext.getResources(), R.drawable.undo);
        undo = Bitmap.createScaledBitmap(undo, (int) model.DessinCaseH, (int) model.DessinCaseH, false);
        zoom = BitmapFactory.decodeResource(model.activitycontext.getResources(), R.drawable.zoom);
        zoom = Bitmap.createScaledBitmap(zoom, (int) model.DessinCaseH, (int) model.DessinCaseH, false);
        drawbgspot = BitmapFactory.decodeResource(model.activitycontext.getResources(), R.drawable.drawbgspot);
        drawbgspot = Bitmap.createScaledBitmap(drawbgspot, (int) model.DessinCaseH, (int) model.DessinCaseH, false);
        drawchangethickness = BitmapFactory.decodeResource(model.activitycontext.getResources(), R.drawable.drawchangethickness);
        drawchangethickness = Bitmap.createScaledBitmap(drawchangethickness, (int) model.DessinCaseH, (int) model.DessinCaseH, false);
        drawduplicate = BitmapFactory.decodeResource(model.activitycontext.getResources(), R.drawable.drawduplicate);
        drawduplicate = Bitmap.createScaledBitmap(drawduplicate, (int) model.DessinCaseH, (int) model.DessinCaseH, false);
        drawline = BitmapFactory.decodeResource(model.activitycontext.getResources(), R.drawable.drawline);
        drawline = Bitmap.createScaledBitmap(drawline, (int) model.DessinCaseH, (int) model.DessinCaseH, false);
        drawmove = BitmapFactory.decodeResource(model.activitycontext.getResources(), R.drawable.drawmove);
        drawmove = Bitmap.createScaledBitmap(drawmove, (int) model.DessinCaseH, (int) model.DessinCaseH, false);
        drawmovepoint = BitmapFactory.decodeResource(model.activitycontext.getResources(), R.drawable.drawmovepoint);
        drawmovepoint = Bitmap.createScaledBitmap(drawmovepoint, (int) model.DessinCaseH, (int) model.DessinCaseH, false);
        drawmoveto = BitmapFactory.decodeResource(model.activitycontext.getResources(), R.drawable.drawmoveto);
        drawmoveto = Bitmap.createScaledBitmap(drawmoveto, (int) model.DessinCaseH, (int) model.DessinCaseH, false);
        drawradialfill = BitmapFactory.decodeResource(model.activitycontext.getResources(), R.drawable.drawradialfill);
        drawradialfill = Bitmap.createScaledBitmap(drawradialfill, (int) model.DessinCaseH, (int) model.DessinCaseH, false);
        drawselectmultiple = BitmapFactory.decodeResource(model.activitycontext.getResources(), R.drawable.drawselectmultiple);
        drawselectmultiple = Bitmap.createScaledBitmap(drawselectmultiple, (int) model.DessinCaseH, (int) model.DessinCaseH, false);
        drawselectone = BitmapFactory.decodeResource(model.activitycontext.getResources(), R.drawable.drawselectone);
        drawselectone = Bitmap.createScaledBitmap(drawselectone, (int) model.DessinCaseH, (int) model.DessinCaseH, false);
        drawtraceremove = BitmapFactory.decodeResource(model.activitycontext.getResources(), R.drawable.drawtraceremove);
        drawtraceremove = Bitmap.createScaledBitmap(drawtraceremove, (int) model.DessinCaseH, (int) model.DessinCaseH, false);
        drawchangecolor = BitmapFactory.decodeResource(model.activitycontext.getResources(), R.drawable.drawchangecolor);
        drawchangecolor = Bitmap.createScaledBitmap(drawchangecolor, (int) model.DessinCaseH, (int) model.DessinCaseH, false);
        drawchangeresize = BitmapFactory.decodeResource(model.activitycontext.getResources(), R.drawable.drawchangeresize);
        drawchangeresize = Bitmap.createScaledBitmap(drawchangeresize, (int) model.DessinCaseH, (int) model.DessinCaseH, false);
        drawchangeup = BitmapFactory.decodeResource(model.activitycontext.getResources(), R.drawable.drawchangeup);
        drawchangeup = Bitmap.createScaledBitmap(drawchangeup, (int) model.DessinCaseH, (int) model.DessinCaseH, false);
        drawchangedown = BitmapFactory.decodeResource(model.activitycontext.getResources(), R.drawable.drawchangedown);
        drawchangedown = Bitmap.createScaledBitmap(drawchangedown, (int) model.DessinCaseH, (int) model.DessinCaseH, false);
        drawchangetype = BitmapFactory.decodeResource(model.activitycontext.getResources(), R.drawable.drawchangetype);
        drawchangetype = Bitmap.createScaledBitmap(drawchangetype, (int) model.DessinCaseH, (int) model.DessinCaseH, false);
    }

    private void savebitmaptofile(Bitmap bitmap, String name) {
        llog.d(TAG, "++++++++++++++++++++++++savebitmaptofile() " + name);
        OutputStream outStream = null;
        File file = new File(name);
        try {
            outStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream);
            outStream.flush();
            outStream.close();
            llog.d(TAG, "bitmap saved to " + name);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

  /*
  public static byte[] generatebytes(){
    ByteBuffer buffer = ByteBuffer.allocate(4 * values.length);
    for (float value : values){
      buffer.putFloat(value);
    }
    return buffer.array();
  }*/

    public void startblankdrawing() {
        String onchargefichier = model.dossierdessin + DateFormat.format("yyyyMMddHHmmss", new java.util.Date()).toString();
        if (mysurf.drawingcurrfichier != null)
            if (onchargefichier.equals(mysurf.drawingcurrfichier))
                onchargefichier += "n";
        mysurf.drawingcurrfichier = onchargefichier;
        model.preferences.edit().putString("lastdrawing", onchargefichier).commit();
        llog.d(TAG, "+++++++++++++++++++++++++++startblankdrawing() create " + onchargefichier);
        Couche macouche = new Couche();
        macouche.currentbitmap = Bitmap.createBitmap(mysurf.ScreenWidth, mysurf.ScreenHeight, Bitmap.Config.ARGB_8888);
        macouche.isaphoto = false;
        macouche.canvas = new Canvas(macouche.currentbitmap);
        macouche.x = 0.0f;
        macouche.y = 0.0f;
        macouche.originalw = mysurf.ScreenWidth;
        macouche.originalh = mysurf.ScreenHeight;
        macouche.scale = 1.0f;
        mysurf.listecouche.add(macouche);
    }

    public String writelastdrawing() {
        int couchel = mysurf.listecouche.size();
        int dessinl = mysurf.listedessin.size();
        if (couchel > 1 || dessinl > 0) {
            String onchargefichier = model.dossierdessin + DateFormat.format("yyyyMMddHHmmss", new java.util.Date()).toString();
            mysurf.drawingcurrfichier = onchargefichier;
            model.preferences.edit().putString("lastdrawing", onchargefichier).commit();
            llog.d(TAG, "+++++++++++++++++++++++++++writelastdrawing() create " + onchargefichier);
            model.message("saving " + onchargefichier + ".png");
            FileOutputStream fos;
            ObjectOutputStream oos;
            try {
                fos = new FileOutputStream(onchargefichier + ".ldpc");
                oos = new ObjectOutputStream(fos);
                oos.writeInt(couchel);
                for (int i = 0; i < couchel; i++) {
                    Couche cettecouche = mysurf.listecouche.get(i);
                    oos.writeBoolean(cettecouche.isaphoto);
                    if (cettecouche.isaphoto) {

                        int width = cettecouche.originalbitmap.getWidth();
                        int height = cettecouche.originalbitmap.getHeight();
                        int size = width * height;
                        oos.writeInt(width);
                        oos.writeInt(height);

                        int[] mespixels = new int[size];
                        cettecouche.originalbitmap.getPixels(mespixels, 0, width, 0, 0, width, height);
                        oos.writeObject(mespixels);

                    } else {
                        oos.writeInt(cettecouche.currentbitmap.getWidth());
                        oos.writeInt(cettecouche.currentbitmap.getHeight());
                    }
                    oos.writeFloat(cettecouche.x);
                    oos.writeFloat(cettecouche.y);
                    oos.writeFloat(cettecouche.originalw);
                    oos.writeFloat(cettecouche.originalh);
                    oos.writeFloat(cettecouche.scale);
                }
                oos.writeInt(dessinl);
                for (int i = 0; i < dessinl; i++) {
                    Dessin cedessin = mysurf.listedessin.get(i);
                    oos.writeInt(cedessin.couche);
                    oos.writeInt(cedessin.drawerasefillcontour);
                    oos.writeInt(cedessin.nbdepoints);
                    oos.writeObject(cedessin.x);
                    oos.writeObject(cedessin.y);
                    oos.writeObject(cedessin.p);
                    oos.writeObject(cedessin.d);
                    oos.writeObject(cedessin.t);
                    oos.writeInt(cedessin.couleur);
                    oos.writeFloat(cedessin.couleura);
                    oos.writeFloat(cedessin.couleurh);
                    oos.writeFloat(cedessin.couleurs);
                    oos.writeFloat(cedessin.couleurv);
                    oos.writeFloat(cedessin.pressuremin);
                    oos.writeFloat(cedessin.pressuremax);
                    oos.writeFloat(cedessin.sizemin);
                    oos.writeFloat(cedessin.sizemax);
                    oos.writeFloat(cedessin.bezierlength);
                    oos.writeObject(cedessin.colormatrix);
                }
                oos.flush();
                oos.close();
                fos.close();
                llog.d(TAG, "Drawing data stored to " + onchargefichier + ".ldpc");
                String pngoutfullpath = onchargefichier + ".png";
                recreecanvaspoursauvegarde(0, pngoutfullpath);
                llog.d(TAG, "recreecanvaspoursauvegarde() " + pngoutfullpath);
                try {
                    model.commandethreaddatabase.put(new String[]{"chercheFichierDansDatabaseSinonRescanSonDossier", model.dossierdessin, pngoutfullpath, "-1"});
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
                llog.d(TAG, "Error saving to " + onchargefichier + ".ldpc");
                model.message("error did not save " + onchargefichier + ".png");
                return onchargefichier + ".png";
            }
            model.message("saved " + onchargefichier + ".png");
            return onchargefichier + ".png";
        } else {
            llog.d(TAG, "empty picture cannot save " + mysurf.drawingcurrfichier);
        }
        return "";
    }

    public void readlastdrawing(String currentfileinfocus) {
        /**
         *   si on avait chargé un fichier avant on le recharge
         */
        String onchargefichier = model.preferences.getString("lastdrawing", null);
        llog.d(TAG, "si on avait chargé un fichier avant on le recharge " + onchargefichier);
        /**
         *   si on cible un fichier ldpc on charge plutôt celui-là
         */
        if (currentfileinfocus != null) {
            if (currentfileinfocus.endsWith(".png")) {
                int extension = currentfileinfocus.lastIndexOf(".png");
                if (extension != -1) {
                    String sansextension = currentfileinfocus.substring(0, extension);
                    File fichier = new File(sansextension + ".ldpc");
                    if (fichier.exists()) {
                        onchargefichier = sansextension;
                    }
                }
            }
        }
        llog.d(TAG, "si on cible un fichier ldpc on charge plutôt celui-là " + model.showthisfile + " onchargefichier " + onchargefichier);
        /**
         *   sinon on commence un nouveau fichier
         */
        if (onchargefichier == null) {
            llog.d(TAG, "----------------------readlastdrawing() null");
            return;
        } else {
            mysurf.drawingcurrfichier = onchargefichier;
            model.preferences.edit().putString("lastdrawing", onchargefichier).commit();
        }
        llog.d(TAG, "++++++++++++++++++++++readlastdrawing() " + onchargefichier);
        model.message("reading " + onchargefichier + ".png");
        FileInputStream fis;
        ObjectInputStream oos;
        try {
            fis = new FileInputStream(onchargefichier + ".ldpc");
            oos = new ObjectInputStream(fis);
            //mysurf.listecouche = (ArrayList<Couche>) ois.readObject();
            int couchel = oos.readInt();
            for (int i = 0; i < couchel; i++) {
                Couche cettecouche = new Couche();
                cettecouche.active = false;
                cettecouche.isaphoto = oos.readBoolean();
                if (cettecouche.isaphoto) {

                    int width = oos.readInt();
                    int height = oos.readInt();
                    int size = width * height;

                    int[] mespixels = (int[]) oos.readObject();

                    cettecouche.originalbitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                    cettecouche.originalbitmap.setPixels(mespixels, 0, width, 0, 0, width, height);

                    cettecouche.currentbitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                    cettecouche.canvas = new Canvas(cettecouche.currentbitmap);

                } else {
                    int width = oos.readInt();
                    int height = oos.readInt();
                    cettecouche.currentbitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                    cettecouche.canvas = new Canvas(cettecouche.currentbitmap);
                }
                cettecouche.x = oos.readFloat();
                cettecouche.y = oos.readFloat();
                cettecouche.originalw = oos.readFloat();
                cettecouche.originalh = oos.readFloat();
                cettecouche.scale = oos.readFloat();
                mysurf.listecouche.add(cettecouche);
            }
            int dessinl = oos.readInt();
            for (int i = 0; i < dessinl; i++) {
                Dessin cedessin = new Dessin();
                cedessin.couche = oos.readInt();
                cedessin.drawerasefillcontour = oos.readInt();
                cedessin.nbdepoints = oos.readInt();
                cedessin.x = (float[]) oos.readObject();
                cedessin.y = (float[]) oos.readObject();
                cedessin.p = (float[]) oos.readObject();
                cedessin.d = (float[]) oos.readObject();
                cedessin.t = (float[]) oos.readObject();
                cedessin.couleur = oos.readInt();
                cedessin.couleura = oos.readFloat();
                cedessin.couleurh = oos.readFloat();
                cedessin.couleurs = oos.readFloat();
                cedessin.couleurv = oos.readFloat();
                cedessin.pressuremin = oos.readFloat();
                cedessin.pressuremax = oos.readFloat();
                cedessin.sizemin = oos.readFloat();
                cedessin.sizemax = oos.readFloat();
                cedessin.bezierlength = oos.readFloat();
                cedessin.colormatrix = (ArrayList<float[]>) oos.readObject();
                mysurf.listedessin.add(cedessin);
            }
            int avail = oos.available();
            llog.d(TAG, "available : " + avail);
            oos.close();
            fis.close();

            if (avail == 0) {
                for (int i = 0; i < couchel; i++) {
                    if (mysurf.listecouche.get(i).isaphoto) {
                        recreephoto(i);
                    } else {
                        recreecanvasenzero(i, mysurf.ScreenWidth, mysurf.ScreenHeight);
                    }
                }
                llog.d(TAG, "Last drawing loaded " + onchargefichier + ".ldpc");
                model.message();
                return;
            } else {
                mysurf.listecouche.clear();
                mysurf.listedessin.clear();
                llog.d(TAG, "Error reading " + onchargefichier + ".ldpc");
                model.message("Error reading " + onchargefichier + ".png");
            }
        } catch (IOException e) {
            mysurf.listecouche.clear();
            mysurf.listedessin.clear();
            e.printStackTrace();
            llog.d(TAG, "Error reading " + onchargefichier + ".ldpc");
            model.message("Error reading " + onchargefichier + ".png");
        } catch (ClassNotFoundException e) {
            mysurf.listecouche.clear();
            mysurf.listedessin.clear();
            e.printStackTrace();
            llog.d(TAG, "Error reading " + onchargefichier + ".ldpc");
            model.message("Error reading " + onchargefichier + ".png");
        } catch (Exception e) {
            mysurf.listecouche.clear();
            mysurf.listedessin.clear();
            e.printStackTrace();
            llog.d(TAG, "Error reading " + onchargefichier + ".ldpc");
            model.message("Error reading " + onchargefichier + ".png");
        }
        model.message();
    }

    void deleteRecursive(File fileOrDirectory) {
    /*if (fileOrDirectory.isDirectory()) {
      for (File child : fileOrDirectory.listFiles()) {
        deleteRecursive(child);
      }
    }
    fileOrDirectory.delete();*/
    }
}






























