package la.daube.photochiotte;

import android.graphics.Color;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Random;
import java.util.Vector;

public class ballz {

	public static void writedebug(String file, String header, ArrayList<String> array){
		try {
			File myFile = new File("/sdcard/"+file);
			myFile.createNewFile();
			FileOutputStream fOut = new FileOutputStream(myFile);
			OutputStreamWriter myOutWriter =
									new OutputStreamWriter(fOut);
			for(String arra : array)
				myOutWriter.append(arra);
			myOutWriter.close();
			fOut.close();
		} catch (Exception e) {
		}
	}

//il manque l'acceleration !
	/**
	 *
	 * @param {distance,
	 * @param oneisfixed, x1, y1, vx1, vy1,
	 * @param twoisfixed, x2, y2, vx2, vy2}
	 * @return {x1, y1, x2, y2, vx1, vy1, vx2, vy2}
	 */
	public static double[] replassage(
			double distance,
			boolean oneisfixed, double x1, double y1, double vx1, double vy1,
			boolean twoisfixed, double x2, double y2, double vx2, double vy2) {


		double[] ecartees=ecartement(distance,oneisfixed,x1,y1,twoisfixed,x2,y2);
		x1=ecartees[0];
		y1=ecartees[1];
		x2=ecartees[2];
		y2=ecartees[3];

		double[] vect=vitesses(oneisfixed, x1, y1, vx1, vy1, twoisfixed, x2, y2, vx2, vy2);
		/*
		double vect[]=new double[8];
		vect[0] = x1;
		vect[1] = y1;
		vect[2] = x2;
		vect[3] = y2;
		vect[4] = vx1;
		vect[5] = vy1;
		vect[6] = vx2;
		vect[7] = vy2;*/

		return vect;
	}

	public static double[] replassagepousse(
			double distance,
			boolean oneisfixed, double x1, double y1, double vx1, double vy1,
			boolean twoisfixed, double x2, double y2, double vx2, double vy2) {


		double[] ecartees=ecartement(distance,oneisfixed,x1,y1,twoisfixed,x2,y2);
		x1=ecartees[0];
		y1=ecartees[1];
		x2=ecartees[2];
		y2=ecartees[3];

		double[] vect=vitessespousse(oneisfixed, x1, y1, vx1, vy1, twoisfixed, x2, y2, vx2, vy2);
		/*
		double vect[]=new double[8];
		vect[0] = x1;
		vect[1] = y1;
		vect[2] = x2;
		vect[3] = y2;
		vect[4] = vx1;
		vect[5] = vy1;
		vect[6] = vx2;
		vect[7] = vy2;*/

		return vect;
	}

	public static double[] ecartement(double distance, boolean oneisfixed, double x1, double y1,boolean twoisfixed, double x2, double y2) {
		double[] vect = new double[4];
		double dx = x1 - x2;
		double dy = y1 - y2;
		double dx2 = dx * dx;
		double dy2 = dy * dy;
		double racine = Math.sqrt(dx2 + dy2);
		double deltaa = distance - racine;
		double deltax, deltay;
		if (dx==0.0) {
			deltax = 0.0;
			deltay = deltaa;
		} else {
			double penta = dy / dx;
			double deltax2 = (deltaa * deltaa) / (1.0 + penta * penta);
			deltax = Math.sqrt(deltax2);
			deltay = Math.abs(penta * deltax);
		}
		/*
		double majoration=1.00001;
		deltax*=majoration;
		deltay*=majoration;*/
		if(!oneisfixed && !twoisfixed){
			deltax /= 2.0;
			deltay /= 2.0;
			if (x1 > x2) {
				x1 += deltax;
				x2 -= deltax;
			} else {
				x1 -= deltax;
				x2 += deltax;
			}
			if (y1 > y2) {
				y1 += deltay;
				y2 -= deltay;
			} else {
				y1 -= deltay;
				y2 += deltay;
			}
		}else if(twoisfixed){
			if (x1 > x2) {
				x1 += deltax;
			} else {
				x1 -= deltax;
			}
			if (y1 > y2) {
				y1 += deltay;
			} else {
				y1 -= deltay;
			}
		}else if(oneisfixed){
			if (x1 > x2) {
				x2 -= deltax;
			} else {
				x2 += deltax;
			}
			if (y1 > y2) {
				y2 -= deltay;
			} else {
				y2 += deltay;
			}
		}
		vect[0] = x1;
		vect[1] = y1;
		vect[2] = x2;
		vect[3] = y2;
		return vect;
	}

	public static double []vitesses(
			boolean oneisfixed, double x1, double y1, double vx1, double vy1,
			boolean twoisfixed, double x2, double y2, double vx2, double vy2) {

		double[] vect = new double[8];

		double dx = x2 - x1;
		double dy = y2 - y1;
		double dvx = vx1 - vx2;
		double dvy = vy1 - vy2;
		/*
		if(dvx==0.0f)
			dvx+=Math.random()*0.000000003-0.000000002;
		if(dvy==0.0f)
			dvy+=Math.random()*0.000000003-0.000000002;
		*/
			double a,dv,dx1,dx2,dy1,dy2;
			if(dx!=0.0){
				a = dy / dx;
				double dvt = 1.0+a*a;
				dv = (dvx + a*dvy);
				dv = dv/dvt;
				dx1 = -dv;
				dy1 = -a*dv;
				dx2 = dv;
				dy2 = a*dv;
			}else{
				dx1 = 0.0 ;
				dy1 = -vy1;
				dx2 = 0.0 ;
				dy2 = vy1;
			}

			if(twoisfixed){
				dx1 = dx1 - dx2;
				dy1 = dy1 - dy2;
				dx2 = 0.0;
				dy2 = 0.0;
			}

			if(oneisfixed){
				dx1 = 0.0;
				dy1 = 0.0;
				dx2 = dx2 - dx1;
				dy2 = dy2 - dy1;
			}

			vect[0] = x1;
			vect[1] = y1;
			vect[2] = x2;
			vect[3] = y2;
			vect[4] = vx1+dx1;
			vect[5] = vy1+dy1;
			vect[6] = vx2+dx2;
			vect[7] = vy2+dy2;

		return vect;
	}

	public static double []vitessespousse(
			boolean oneisfixed, double x1, double y1, double vx1, double vy1,
			boolean twoisfixed, double x2, double y2, double vx2, double vy2) {

		double[] vect = new double[8];

		double dx = x2 - x1;
		double dy = y2 - y1;
		double dvx = vx1 - vx2;
		double dvy = vy1 - vy2;
		/*
		if(dvx==0.0f)
			dvx+=Math.random()*0.000000003-0.000000002;
		if(dvy==0.0f)
			dvy+=Math.random()*0.000000003-0.000000002;
		*/
			double a,dv,dx1,dx2,dy1,dy2;
			if(dx!=0.0){
				a = dy / dx;
				double dvt = 1.0+a*a;
				dv = (dvx + a*dvy);
				dv = dv/dvt;
				dx1 = -dv;
				dy1 = -a*dv;
				dx2 = dv;
				dy2 = a*dv;
			}else{
				dx1 = 0.0 ;
				dy1 = -vy1;
				dx2 = 0.0 ;
				dy2 = vy1;
			}

			if(twoisfixed){
				dx1 = dx1 - dx2;
				dy1 = dy1 - dy2;
				//dx2 = 0.0;
				//dy2 = 0.0;
			}

			if(oneisfixed){
				dx1 = 0.0;
				dy1 = 0.0;
				dx2 = dx2 - dx1;
				dy2 = dy2 - dy1;
			}

			vect[0] = x1;
			vect[1] = y1;
			vect[2] = x2;
			vect[3] = y2;
			vect[4] = vx1+dx1;
			vect[5] = vy1+dy1;
			vect[6] = vx2+dx2;
			vect[7] = vy2+dy2;

		return vect;
	}
	/**
	 *
	 * @param dist
	 * @param mx1
	 * @param my1
	 * @param mx2
	 * @param my2
	 * @param alpha
	 * @param beta
	 * @param f
	 * @param x
	 * @param y
	 * @param vx
	 * @param vy
	 * @param ax
	 * @param ay
	 * @return
	 */
	public static double[] hache(double dist, double mx1, double my1, double mx2, double my2,
			double alpha, double f, double x, double y, double vx, double vy, double ax, double ay
			){

		double[] h=murhorivert(dist, mx1, my1, mx2, my2,alpha, f, x, y, vx, vy, ax, ay);

		if(h!=null){
			h=hinrange(h[0],h[1],mx1,my1,mx2,my2);

			double dx=x-h[0];
			double dy=y-h[1];
			double d=dx*dx+dy*dy;
			double dist2=dist*dist;
			if(d<=dist2)
				return vecteur(h);
		}

		return null;
	}

	public static double[] hinrange(double hx, double hy, double ax, double ay, double bx, double by){
		double[] h={hx,hy};
		if(((ax<=hx && hx<=bx)||(bx<=hx && hx<=ax))&&((ay<=hy && hy<=by)||(by<=hy && hy<=ay)))
			return h;
		else{
			double ha2=(ax-hx)*(ax-hx)+(ay-hy)*(ay-hy);
			double hb2=(bx-hx)*(bx-hx)+(by-hy)*(by-hy);
			if(ha2<hb2){
				h[0]=ax;
				h[1]=ay;
				return h;
			}else{
				h[0]=bx;
				h[1]=by;
				return h;
			}
		}
	}

	public static double []murhorivert(double dist, double mx1, double my1, double mx2, double my2,
			double alpha, double f, double x, double y, double vx, double vy, double ax, double ay
			){
		double nv[]=new double[2];
		if(mx1==mx2){
				double dt=getdtmur(dist, mx1, mx2,alpha, f, x, vx, ax);
				nv[0]=mx1;
				nv[1]=setdt( dt, alpha, f, y, vy, ay);
				return nv;
		}
		else if(my1==my2){
				double dt=getdtmur(dist, my1, my2,alpha, f, y, vy, ay);
				nv[0]=setdt( dt, alpha, f, x, vx, ax);
				nv[1]=my1;
				return nv;
		}
		return null;
	}

	public static double setdt(double dt,
			double alpha, double f, double x, double vx, double ax){
		double xa=0.5*ax-vx*f;
		double xb= Math.cos(alpha)*vx;
		double xc=x;
		double result=xc+dt*xb+dt*dt*xa;
		return result;
	}

	public static double getdtmur(double dist, double mx1, double mx2,
			double alpha, double f, double x, double vx, double ax){
		double xa=0.5*ax-vx*f;
		double xb= Math.cos(alpha)*vx;
		double xc;
		if(x<mx1)
			xc=x-(mx1-dist);
		else
			xc=x-(mx1+dist);
		double delta=xb*xb-4.0*xa*xc;
		if(delta>=0){
			delta= Math.sqrt(delta);
			double dt1=(-xb-delta)/(2.0*xa);
			double dt2=(-xb-delta)/(2.0*xa);
			double dt= Math.min(dt1,dt2);
			if(dt<=0.0){
				return dt;
			}
		}
		return 0.0;
	}

	/**
	 *
	 * @param dist
	 * @param x
	 * @param y
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @return
	 */
	public static boolean isinrange(double dist, double x, double y, double x1, double y1, double x2, double y2){
		double d1=(x1-x)*(x1-x)+(y1-y)*(y1-y);
		double d2=(x2-x)*(x2-x)+(y2-y)*(y2-y);
		double d12=(x1-x2)*(x1-x2)+(y1-y2)*(y1-y2);
		double d=dist*dist;
		double reel=d1+d2;
		double mini=d12+2.0f*d;
		if(reel<mini)
			return true;
		else
			return false;
	}

	/**
	 *
	 * @param dist
	 * @param x
	 * @param y
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @return
	 */
	public static double []perpengle(double dist, double x, double y, double x1, double y1, double x2, double y2){
		double []perp=new double[2];
		double alpha= Math.atan2(y2-y1, x2-x1);
			alpha+= Math.PI/2.0;
			double a= (double) Math.tan(alpha);
			if(a==0.0){
				perp[0]=x;
				perp[1]=y1;
				return perp;
			}else{
				double b= y-a*x;
				double a21=(y2-y1)/(x2-x1);
				double b21= y1-a21*x1;
				perp[0]=(a21*x+b21-b)/a;
				perp[1]=perp[0]*a+b;
				return perp;
			}
	}
	/**
	 *
	 * @param dist
	 * @param x
	 * @param y
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @return
	 */
	public static double []perpen(double dist, double x, double y, double x1, double y1, double x2, double y2){
		double []perp=new double[2];
		double dx12=x2-x1;
		if(dx12==0.0f){
			perp[0]=x1;
			perp[1]=y;
			return perp;
		}
		else{
			double a=(y2-y1)/dx12;
			double b=y1-a*x1;
			double alpha=2.0f*x*(x2-x1)+2.0f*y*(y2-y1);
			double div=2.0f*(x1-x2+a*(y1-y2));
			if(div!=0.0f){
				double x0=2.0f*b*(y2-y1);
				perp[0]=(x0-alpha)/div;
				perp[1]=a*perp[0]+b;
				return perp;
			}
		}
		llog.d("node","error div/0 ballz.perpen()");
		return null;
	}

	/**
	 * Collisions
	 * donner les valeurs anciennes mLast...
	 * @param distance
	 * @param oneisfixed
	 * @param f1
	 * @param x1
	 * @param y1
	 * @param vx1
	 * @param vy1
	 * @param ax1
	 * @param ay1
	 * @param twoisfixed
	 * @param f2
	 * @param x2
	 * @param y2
	 * @param vx2
	 * @param vy2
	 * @param ax2
	 * @param ay2
	 * @return
	 */
	public static double[] collision(
			double distance,
			boolean oneisfixed, double alpha1, double f1, double x1, double y1, double vx1, double vy1, double ax1, double ay1,
			boolean twoisfixed, double alpha2, double f2, double x2, double y2, double vx2, double vy2, double ax2, double ay2) {

		alpha1= Math.cos(alpha1);
		alpha2= Math.cos(alpha2);
		double dx = x1 - x2;
		double dy = y1 - y2;
		double dvx = alpha1 * (vx1 - vx2);
		double dvy = alpha2 * (vy1 - vy2);
		if(dvx!=0.0 && dvy!=0.0){
			double dvfx = vx1*f1 - vx2*f2;
			double dvfy = vy1*f1 - vy2*f2;
			double dax = ax1 - ax2;
			double day = ay1 - ay2;
			dax=alpha1 * (dax/2.0-dvfx);
			day=alpha2 * (day/2.0-dvfy);

			double a=dax*dax+day*day;
			double b=2.0*dvx*dax+2.0*dvy*day;
			double c=dvx*dvx+dvy*dvy+2.0*dx*dax+2.0*dy*day;
			double d=2.0*dx*dvx+2.0*dy*dvy;
			double e=dx*dx+dy*dy-distance*distance;

			double dt=0.0;
			int nomorethan=10;
			int than=0;
			double rand=-0.005;
			double onaime=-0.2;
			dt=newraphz(rand,a,b,c,d,e);
			double troplent=-0.2;
			while(dt>=0.0 && than<nomorethan){
				dt=newraphz(rand,a,b,c,d,e);
				rand= Math.random();
				rand*=onaime;
				than++;
			}
			if(dt>troplent && dt<0.0){
				//on revient à la position de collision

				vx1=vx1*(1.0-f1*dt)+ax1*dt/2.0;
				vy1=vy1*(1.0-f1*dt)+ay1*dt/2.0;
				vx2=vx2*(1.0-f2*dt)+ax2*dt/2.0;
				vy2=vy2*(1.0-f2*dt)+ay2*dt/2.0;
				x1=x1+alpha1*vx1*dt;
				y1=y1+alpha1*vy1*dt;
				x2=x2+alpha2*vx2*dt;
				y2=y2+alpha2*vy2*dt;

				//on y calcule les nouveaux vecteurs vitesse
				double[] vect=vitesses(oneisfixed, x1, y1, vx1, vy1, twoisfixed, x2, y2, vx2, vy2);


				//il faut rattraper le temps

				dt=-dt;
				x1=vect[0];
				y1=vect[1];
				x2=vect[2];
				y2=vect[3];
				vx1=vect[4];
				vy1=vect[5];
				vx2=vect[6];
				vy2=vect[7];
				vx1=vx1*(1.0-f1*dt)+ax1*dt/2.0;
				vy1=vy1*(1.0-f1*dt)+ay1*dt/2.0;
				vx2=vx2*(1.0-f2*dt)+ax2*dt/2.0;
				vy2=vy2*(1.0-f2*dt)+ay2*dt/2.0;
				x1=x1+alpha1*vx1*dt;
				y1=y1+alpha1*vy1*dt;
				x2=x2+alpha2*vx2*dt;
				y2=y2+alpha2*vy2*dt;

				vect[0] = x1;
				vect[1] = y1;
				vect[2] = x2;
				vect[3] = y2;
				vect[4] = vx1;
				vect[5] = vy1;
				vect[6] = vx2;
				vect[7] = vy2;
				dt=-dt;
				x1=vect[0];
				y1=vect[1];
				x2=vect[2];
				y2=vect[3];
				x1=x1+vx1*dt;
				y1=y1+vy1*dt;
				x2=x2+vx2*dt;
				y2=y2+vy2*dt;
				vect[0] = x1;
				vect[1] = y1;
				vect[2] = x2;
				vect[3] = y2;

				return vecteur(vect);
			}
		}
		double []vect=replassage(distance, oneisfixed, x1, y1, vx1, vy1, twoisfixed, x2, y2, vx2, vy2);
		return vecteur(vect);
	}

	public static double[] pointperpendiculaire(double xa,double ya, double xb,double yb,double xc, double yc){
		double pis2= Math.PI/2.0;
		double h[]=new double[2];
		double xab=xb-xa;
		double yab=yb-ya;
		double oab= Math.atan2(yab, xab);
		double ohc=oab+pis2;
		double ahc= Math.tan(ohc);
		double bhc=yc-ahc*xc;
		if(xab!=0.0){
			double aab=yab/xab;
			double bab=yb-aab*xb;
			h[0]=(bhc-bab)/(aab-ahc);
			h[1]=aab*h[0]+bab;
		}else{
			h[0]=xa;
			h[1]=yc;
		}
		return h;
	}

	public static double[] hifisinrange(double xa,double ya, double xb,double yb,double xc, double yc,double demil){
		double demil2=demil*demil;
		double xab=xb-xa;
		double yab=yb-ya;
		double ab2=xab*xab+yab*yab;
		double xac=xc-xa;
		double yac=yc-ya;
		double ac2=xac*xac+yac*yac;
		double xcb=xb-xc;
		double ycb=yb-yc;
		double cb2=xcb*xcb+ycb*ycb;
		double ad2=ab2+demil2;
		double i1= Math.sqrt(ac2)+ Math.sqrt(cb2);
		double p1= Math.sqrt(ad2)+demil;
		if(i1<=p1){
			double h[]=pointperpendiculaire(xa,ya,xb,yb,xc,yc);
			if(h!=null){
				double xhc=xc-h[0];
				double yhc=yc-h[1];
				double hc2=xhc*xhc+yhc*yhc;
				if(hc2<=demil2){
					if( ((xa<=h[0]&&h[0]<=xb)||(xb<=h[0]&&h[0]<=xa))&&(ya<=h[1]&&h[1]<=yb)||(yb<=h[1]&&h[1]<=ya)){
						return vecteur(h);
					}
				}
			}
		}
		return null;
	}


	public static double[] vecteur(double[] vect){
		int taille=vect.length;
		double []vecteurz=new double[taille];
		for(int u=0;u<taille;u++){
			vecteurz[u]=(double)(vect[u]);
		}
		return vecteurz;
	}


	public static double quartic(double a, double b, double c, double d, double e){
		if(a!=0.0f){
			double alpha=b*b-3.0f*a*c+12.0f*d;
			double beta=2.0f*b*b*b-9.0f*a*b*c+27.0f*c*c+27.0f*a*a*d-72.0f*b*d;
			double gamma=-a*a*a+4.0f*a*b-8.0f*c;
			double condition1=-4.0f*alpha*alpha*alpha+beta*beta;
			if(condition1>=0.0f){
				double condition2=beta+ (double) Math.sqrt(condition1);
				if(condition2>=0.0f){
					double aa=cubicroot(condition2);
					double bb=cubicroot(2.0f);
					bb=(bb*alpha)/(3.0f*aa);
					double cc=cubicroot(54.0f);
					cc=aa/cc;
					double condition3=a*a/4.0f-2.0f*b/3.0f+bb+cc;
					if(condition3>=0.0f){
						double dd= (double) Math.sqrt(condition3);
						double condition4a=a*a/2.0f-4.0f*b/3.0f-bb-cc;
						double condition4b=gamma/(4.0f*dd);
						double condition4=condition4a-condition4b;
						double []n={5000.0f,5000.0f,5000.0f,5000.0f};
						if(condition4>=0.0f){
							double ee=0.5f* (double) Math.sqrt(condition4);
							n[0]=-a/4.0f-0.5f*dd-ee;
							n[1]=-a/4.0f-0.5f*dd+ee;
						}
						condition4=condition4a+condition4b;
						if(condition4>=0.0f){
							double ee=0.5f* (double) Math.sqrt(condition4);
							n[2]=-a/4.0f+0.5f*dd-ee;
							n[3]=-a/4.0f+0.5f*dd+ee;
						}
						double t=-50000.0f;
						for(int i=0;i<4;i++){
							if(n[i]>t && n[i]<=0.0f){
								t=n[i];
							}
						}
						if(t<-1.0f);// llog.d("node","quartic : t didn't match any");
						else return t;
					}
					//else llog.d("node","quartic : condition 3 <0");
				}
				//else llog.d("node","quartic : condition 2 <0");
			}
			//else llog.d("node","quartic : condition 1 <0");
		}
		//else llog.d("node","div /0 quartic func");
		return 1.0f;
	}

	public static double cubicroot(double x){
		Random rand= Gallery.rand;
		double xmin=0.0f;
		double xmax=x;
		double r=rand.nextFloat();
		double x1=x*r;
		double x3=x1*x1*x1;
		double d=xmax-xmin;
		double dp=d+1.0f;
		while(d!=dp){
			dp=d;
			if(x3<x){//[x1,xmax]
				xmin=x1;
			}else{//[xmin,x1]
				xmax=x1;
			}
			d=xmax-xmin;
			r=rand.nextFloat();
			x1=xmin+r*d;
			x3=x1*x1*x1;
		}
		return x1;
	}

	public static double newraphz(double x, double a, double b, double c, double d, double e){
		double fx, dfx;
		double xstore=x+10000.0;
		int nomorethan=1000;
		int than=0;
		while(Math.abs(xstore-x)>0.000001 && than<nomorethan){
			xstore=x;
			fx=fonction(x, a, b, c, d, e);
			dfx=derivee(x, a, b, c, d, e);
			if(dfx!=0.0){
				x=x-fx/dfx;
			}else{
				llog.d("node","problem N-R div/0");
			}
			than++;
		}
		return x;
	}

	public static double fonction(double x, double a, double b, double c, double d, double e){
		double y=a*x*x*x*x+b*x*x*x+c*x*x+d*x+e;
		return y;
	}

	public static double derivee(double x, double a, double b, double c, double d, double e){
		double dy=4.0f*a*x*x*x+3.0f*b*x*x+2.0f*c*x+d;
		return dy;
	}




	/**
	 * Dérivée Gaussienne 3d
	 * @param sens
	 * @param position [-1:er:ermax]
	 * @return angle de la derivée de la gaussienne en x
	 */
	public static double atanderivgauss3dx(double x, double y){
		double sens=1.0;
			double deviastd=0.26;
			double bums=0.02;
			double deviastd2=deviastd*deviastd;
			double r2p= Math.sqrt(2.0* Math.PI);
			double facteur=-x*sens*bums/(deviastd2*deviastd*r2p);
			double puiss=-(x*x+y*y)/(2.0*deviastd2);
			double dgauss=facteur* Math.exp(puiss);
			double atandg= Math.atan(dgauss);
			return atandg;
	}
	/**
	 * Dérivée Gaussienne 3d
	 * @param sens
	 * @param position [-1:er:ermax]
	 * @return angle de la derivée de la gaussienne en y
	 */
	public static double atanderivgauss3dy(double x, double y){
		double sens=1.0;
			double deviastd=0.26;
			double bums=0.02;
			double deviastd2=deviastd*deviastd;
			double r2p= Math.sqrt(2.0* Math.PI);
			double facteur=-y*sens*bums/(deviastd2*deviastd*r2p);
			double puiss=-(x*x+y*y)/(2.0*deviastd2);
			double dgauss=facteur* Math.exp(puiss);
			double atandg= Math.atan(dgauss);
			return atandg;
	}


	/*public static double atanderivgauss(double sens, double er, double ermax){
		double x=er/ermax;
			double deviastd=0.26;
			double bums=0.02;
			double deviastd2=deviastd*deviastd;
			double r2p= Math.sqrt(2.0* Math.PI);
			double facteur=-x*sens*bums/(deviastd2*deviastd*r2p);
			double puiss=-(x*x)/(2.0*deviastd2);
			double dgauss=facteur* Math.exp(puiss);
			double atandg= Math.atan(dgauss);
			return atandg;
	}*/
	/**
	 * Dérivée Gaussienne
	 * @param sens
	 * @param position [-1:er:ermax]
	 * @return angle de la derivée de la gaussienne en x
	 */
	public static double atanderivgauss(double sens, double er, double ermax){
		double x=(double)(er/ermax);
			double deviastd=0.26;
			double bums=0.02;
			double deviastd2=deviastd*deviastd;
			double r2p= Math.sqrt(2.0* Math.PI);
			double facteur=-x*sens*bums/(deviastd2*deviastd*r2p);
			double puiss=-(x*x)/(2.0*deviastd2);
			double dgauss=facteur* Math.exp(puiss);
			double atandg=(double)(Math.atan(dgauss));
			return atandg;
	}
	public static double []ygauss(int n, double sens){
			double deviastd=0.26;
			double bums=0.02;
			double deviastd2=deviastd*deviastd;
			double r2p= Math.sqrt(2.0* Math.PI);

			double[] gauss=new double[n];
			double y=0.0;
			double maximale=sens*bums/(deviastd*r2p);
			double inter=maximale/((double)n);
			for(int u=0;u<n;u++){

				double facteur=((double)y)*(deviastd*r2p)/(sens*bums);
				double puiss=-2.0*deviastd2;
				double dgauss=puiss* Math.log(facteur);
				dgauss= Math.sqrt(dgauss);

				gauss[u]=(double)dgauss;
				y+=inter;
			}
			return gauss;
	}

	/**
	 * Gaussienne
	 * @param sens
	 * @param position [-1:er:ermax]
	 * @return angle de la derivée de la gaussienne en x
	 */
	public static double gauss(double sens, double er, double ermax){
		double x=(double)(er/ermax);
			double deviastd=0.26;
			double bums=0.02;
			double deviastd2=deviastd*deviastd;
			double r2p= Math.sqrt(2.0* Math.PI);
			double facteur=sens*bums/(deviastd*r2p);
			double puiss=-(x*x)/(2.0*deviastd2);
			double dgauss=facteur* Math.exp(puiss);
			return ((double)dgauss);
	}

	public static double changecoord(int x, double cx, double rx){
		double coord;
		coord=((double)x)-cx;
		coord/=rx;
		return coord;
	}
	/*
	public static int[]pixelin(int[] pixels,int y, double cx, double cy, double rx, double ry){
		int n=pixels.length;
		int[] spixels=new int[n];
		double bums=10.0f;
		double somme=0.0f;
		double yy=changecoord(y,cy,ry);
		//somme de toute la courbe
		double min=3.14f;
		double max=0.0f;
		for(int j=0;j<n;j++){
			double k=changecoord(j,cx,rx);
			double alphak=atanderivgauss3d(k,yy);
			if(alphak<min)min=alphak;
			if(alphak>max)max=alphak;
			alphak*=bums;
			somme+=1.0f/((double) Math.cos(alphak));
		}
		double u=somme/((double)n);
		double i=0.0f;
		int ci,c;
		c=pixels[0];
		double pi,p;
		int r=0;
		int mu=0;
		for(int m=0;m<n;m++){
			double k=changecoord(r,cx,rx);
			double alpha=atanderivgauss3d(k,yy);
			alpha*=bums;
			double j=1.0f/((double) Math.cos(alpha));
			if(mu<n)
				c=pixels[mu];
			if(i+j<=u){
				spixels[m]=c;
			}else{
				i=i-u;
				pi=-i/j;
				ci=c;
				while(i+j>u && mu<n){
					mu++;
					if(mu<n)
						c=pixels[mu];
					p=u/j;
					spixels[m]=colorsom(ci,pi,c,p);
					ci=spixels[m];
					pi=p+pi;
					i=i-u;
				}
				mu++;
				if(mu<n)
					c=pixels[mu];
				p=1.0f-pi;
				spixels[m]=colorsom(ci,pi,c,p);
			}
			r=r+1;
			i=i+j;
		}
		return spixels;
	}*/
	
	public static int colorsom(int couleur, double poids1, int couleur2, double poids2){
		//double a=(double)Color.alpha(couleur);
		double b=(double) Color.red(couleur);
		double c=(double) Color.green(couleur);
		double d=(double) Color.blue(couleur);
		//double e=(double)Color.alpha(couleur2);
		double f=(double) Color.red(couleur2);
		double g=(double) Color.green(couleur2);
		double h=(double) Color.blue(couleur2);
		//a=(a*poids1+poids2*e)/(poids1+poids2);
		b=(b*poids1+poids2*f)/(poids1+poids2);
		c=(c*poids1+poids2*g)/(poids1+poids2);
		d=(d*poids1+poids2*h)/(poids1+poids2);
		return Color.rgb((int)b,(int)c,(int)d);
	}
	
	public static boolean isin(double rayon, int xi,  int yi){
		double x=(double)xi+0.5f;
		double y=(double)yi+0.5f;
		double dx=x-rayon;
		double dy=y-rayon;
		double d2=dx*dx+dy*dy;
		double r2=rayon*rayon;
		if(d2<=r2)
			return true;
		else
			return false;
	}
	
	public static double surzerodeuxpi(double angle) {
		double pi= Math.PI;
		while (angle < 0.0) {
			angle += 2.0 * pi;
		}
		while (angle >= 2.0 * pi) {
			angle -= 2.0 * pi;
		}
		return angle;
	}
	
	public static double angledevecteur(double vx, double vy){
		double angle= Math.atan2(vy, vx);
		angle=surzerodeuxpi(angle);
		return angle;
	}
	
	public static double anglereflechi(double angleincident, double angledeplan){
		double angle=2.0*angledeplan-angleincident;
		return angle;
	}
	
	public static double []reflexion(double vx, double vy, double pax, double pay,double pbx, double pby){
		double []vreflex=new double[2];
		double px=pbx-pax;
		double py=pby-pay;
		double pangle=angledevecteur(px,py);
		double angle=angledevecteur(vx,vy);
		double reflechi=anglereflechi(angle,pangle);
		double hyp= Math.sqrt(vx*vx+vy*vy);
		vreflex[0]= Math.cos(reflechi)*hyp;
		vreflex[1]= Math.sin(reflechi)*hyp;
		return vreflex;
	}
	
	public static int[] transformation(int[] colors, int width, int height, double [][]electrique, int nbums){
		int size=width*height;
		int k=0;
		/*
		double cosmin=500.0;
		double cosmax=-500.0;*/
		double multiple=500.0;
		double cosinus[][][]=new double[width][height][2];
		for(int j=0;j<height;j++){
			for(int i=0;i<width;i++){
				double x=(double)i;
				double y=(double)j;
				double anglex=0.0;
				double angley=0.0;
				for(int bv=0;bv<nbums;bv++){
					final double erx=((double)electrique[bv][0]-x)/(double)electrique[bv][2];
					final double ery=((double)electrique[bv][1]-y)/(double)electrique[bv][2];
					//peut faire mieux atan3dx atan3dy
					double angle=atanderivgauss3dx(erx,ery);
					anglex+=angle*multiple;
					angle=atanderivgauss3dy(erx,ery);
					angley+=angle*multiple;
					/*
					double erx2=erx*erx;
					double ery2=ery*ery;
					double er2=erx2+ery2;
					double er=Math.sqrt(er2);
					double angle=atanderivgauss(1.0,er,(double)electrique[bv][2]);
					double sommeangle=Math.abs(erx)+Math.abs(ery);
					angle=Math.abs(angle);
					angle*=multiple;
					if(angle<cosmin)
						cosmin=angle;
					if(angle>cosmax)
						cosmax=angle;
					if(sommeangle!=0.0){
						anglex+=Math.abs(erx)*angle/sommeangle;
						angley+=Math.abs(ery)*angle/sommeangle;
					}
					else{
						anglex+=angle/2.0;
						angley+=angle/2.0;
					}*/
				}
				cosinus[i][j][0]= Math.cos(anglex);
				cosinus[i][j][1]= Math.cos(angley);
			}
		}
		//llog.d("node",cosmin+" "+cosmax);
		
		int color[][][]=new int[width][height][3];
		k=0;
		for(int j=0;j<height;j++){
			for(int i=0;i<width;i++){
				k=width*j+i;
				color[i][j][0]= Color.red(colors[k]);
				color[i][j][1]= Color.green(colors[k]);
				color[i][j][2]= Color.blue(colors[k]);
			}
		}

		int newcolorx[][]=new int[width][height];
		double largeur=(double)width;
		k=0;
		for(int j=0;j<height;j++){
			double somme=0.0;
			for(int i=0;i<width;i++)
				somme+=cosinus[i][j][0];
			double foisrapport=largeur/somme;
			Vector<Double> caror=new Vector<Double>();
			Vector<Double> carog=new Vector<Double>();
			Vector<Double> carob=new Vector<Double>();
			Vector<Double> caros=new Vector<Double>();
			double caro=0.0;
			for(int i=0;i<width;i++){
				caro=cosinus[i][j][0]*foisrapport;
				
				int carol=caros.size();
				if(carol>0){
					double carot=0.0;
					for (int m=0;m<carol;m++)
						carot+=caros.get(m);
					double reste=1.0-carot;
					if(caro>=reste){
						double colorr=reste*((double)color[i][j][0]);
						double colorg=reste*((double)color[i][j][1]);
						double colorb=reste*((double)color[i][j][2]);
						for(int m=0;m<carol;m++){
							colorr+=caror.get(m)*caros.get(m);
							colorg+=carog.get(m)*caros.get(m);
							colorb+=carob.get(m)*caros.get(m);
						}
						if(colorr>255.0||colorg>255.0||colorb>255.0)
							llog.d("node",carol+" parties : "+colorr+" "+colorg+" "+colorb);
						newcolorx[i-1][j]= Color.rgb((int)colorr,(int)colorg,(int)colorb);
						caro-=reste;
						caror.clear();
						carog.clear();
						carob.clear();
						caros.clear();
					}
				}
				
				k=i;
				while (caro>=1.0&&k<width){
					newcolorx[k][j]= Color.rgb(color[i][j][0],color[i][j][1],color[i][j][2]);
					k++;
					caro-=1.0;
				}
				if(caro>0.0){
					caros.add(caro);
					caror.add((double)color[i][j][0]);
					carog.add((double)color[i][j][1]);
					carob.add((double)color[i][j][2]);
				}
				i=k;
			}
		}

		int newcolors[]=new int[size];
		k=0;
		for(int j=0;j<height;j++){
			for(int i=0;i<width;i++){
				k=width*j+i;
				newcolors[k]=newcolorx[i][j];
			}
		}
		
		return newcolors;
	}

	final static String[][][]defautstr={
		//GENERAL
	{
		{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	}},
	//SWAP
	{
		{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	}},
	//PUZZLE
	{
		{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	}},
	//TOUCH
	{
		{
		"GPU idle delays",
		"Displaying delay",
		"between updates of screen (ms)"
	},{
		"CPU idle delays",
		"Calculations delay",
		"between updates of particles positions (ms)"
	},{
		"Behavior limits",
		"Acceleration limit",
		"before ball release from its spot"
	},{
		"",
		"",
		""
	},{
		"Action field sizes",
		"Attraction range",
		"in ball size"
	},{
		"Action field sizes",
		"Repulsion range",
		"in ball size, applies to (X)"
	},{
		"Trap (X) duration",
		"Trap length",
		"(X) duration in seconds"
	},{
		"Trap (X) duration",
		"Trap strength",
		"(X) deviation importance"
	},{
		"Behavior limits",
		"Ball size",
		"in meters"
	},{
		"Behavior limits",
		"Difficulty level",
		"0.01=hardest ; 0.99=easiest"
	},{
		"",
		"",
		""
	},{
		"Bump",
		"Bump Weakness",
		"when touching screen maximum=1"
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	}},
	//BOUNCE
	{
		{
		"Bums number",
		"Number of Bums",
		"maximum of bums in integers"
	},{
		"CPU idle delays",
		"Calculations delay",
		"between updates of particles positions (ms)"
	},{
		"Bums Height",
		"Bums Height",
		"Intensity of bums"
	},{
		"Behavior limits",
		"Difficulty level",
		"0.01=hardest ; 0.99=easiest"
	},{
		"Action field sizes",
		"Attraction range",
		"in ball size"
	},{
		"Action field sizes",
		"Repulsion range",
		"in ball size, applies to (X)"
	},{
		"Trap (X) duration",
		"Trap length",
		"(X) duration in seconds"
	},{
		"Trap (X) duration",
		"Trap strength",
		"(X) deviation importance"
	},{
		"Shoot Helper",
		"Ball size ratio",
		"Increase ball hit size for the shoot"
	},{
		"Not Straight",
		"Incertitude",
		"Incertitude in the position of the maze corners"
	},{
		"Move Borders",
		"Maze Moval",
		"Borders of the maze are moved, speed dependent"
	},{
		"Bump",
		"Bump Weakness",
		"when touching screen maximum=1"
	},{
		"Friction coef.",
		"Friction",
		"[0:] percentage of speed lost in 1 second"
	},{
		"Mazeness",
		"Maze completeness",
		"[0:1] 1=full maze ; 0=no maze nor wall"
	},{
		"Ball size",
		"Ball size",
		"in meters"
	},{
		"Hole friction",
		"Hole friction coefficient",
		"[0:] percentage of speed lost in 1 second"
	},{
		"Bum Size",
		"Bum field size",
		"in meters"
	},{
		"Hole Gravity",
		"Hole Gravity",
		"in hundreds"
	}},
	//BIBILLES
	{
		{
		"GPU idle delays",
		"Displaying delay",
		"between updates of screen (ms)"
	},{
		"CPU idle delays",
		"Calculations delay",
		"between updates of particles positions (ms)"
	},{
		"Hole friction",
		"Hole friction coefficient",
		"[0:] percentage of speed lost in 1 second"
	},{
		"Behavior limits",
		"Difficulty level",
		"0.01=hardest ; 0.99=easiest"
	},{
		"Action field sizes",
		"Attraction range",
		"in ball size"
	},{
		"Action field sizes",
		"Repulsion range",
		"in ball size, applies to (X)"
	},{
		"Trap (X) duration",
		"Trap length",
		"(X) duration in seconds"
	},{
		"Trap (X) difficulty",
		"Trap strength",
		"(X) deviation importance"
	},{
		"Not Straight",
		"Incertitude",
		"Incertitude in the position of the maze corners"
	},{
		"",
		"",
		""
	},{
		"Ball size",
		"Ball size",
		"in meters"
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"",
		"",
		""
	},{
		"Hole Gravity",
		"Hole Gravity",
		"in hundreds"
	}}
};
	
	final static double [][]defaut={
    		//GENERAL
    		{8.8f,
    		8.8f,
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f},
        	//SWAP
    		{8.8f,
    		8.8f,
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f},
        	//PUZZLE
    		{8.8f,
    		8.8f,
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f},
        	//TOUCH
    		{4.0f,  //nbums
    		4.0f,   //tempscalcul
        	0.3f,	//limaccel
        	8.8f,
        	0.25f,	//attir
        	0.50f,	//multiple
        	0.3f,	//glurp
        	10.0f,	//multipli
        	0.004f,	//sBallDiameter
        	0.30f,  //decalache
        	8.8f,
        	2.0f,	//ballibump
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f},
        	//BOUNCE
    		{2.0f,  //nbums
    		4.0f,   //tempscalcul
        	0.3f,  //bums height
        	0.18f,  //decalache
        	0.60f,	//attir
        	0.50f,	//multiple
        	0.3f,	//glurp
        	10.0f,	//multipli
        	1.0f, //ball aide shoot size
        	0.08f,//incertitude
        	0.006f,//move border of the maze
        	1.6f,	//ballibump
        	0.15f,	//sFriction
        	0.8f,	//completeness 
        	0.004f,	//sBallDiameter
        	1.6f, 	//limaccel = holesfriction
        	0.045f,	//field size
        	5.00f},	//hole gravity
        	//BIBILLES
    		{4.0f,  //nbums
    		4.0f,   //tempscalcul
        	1.3f,	//limaccel = holesfriction
        	0.18f,  //decalache
        	0.50f,	//attir
        	0.50f,	//multiple
        	0.3f,	//glurp
        	6.0f,	//multipli
        	0.04f, //incertitude
        	8.8f,
        	0.0025f,//sBallDiameter
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f,
        	8.8f,
        	2.00f},	//hole gravity
    };

	final static double [][]defautp={
    		//GENERAL
    		{0.1f,
    		0.1f,
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f},
        	//SWAP
    		{0.1f,
    		0.1f,
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f},
        	//PUZZLE
    		{0.1f,
    		0.1f,
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f},
        	//TOUCH
    		{1.0f,  //nbums
    		1.0f,   //tempscalcul
        	0.1f,	//limaccel
        	0.1f,
        	0.01f,	//attir
        	0.01f,	//multiple
        	0.1f,	//glurp
        	1.0f,	//multipli
        	0.001f,	//sBallDiameter
        	0.10f,  //decalache
        	0.1f,
        	1.0f,	//ballibump
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f},
        	//BOUNCE
    		{1.0f,  //nbums
    		1.0f,   //tempscalcul
        	0.1f,
        	0.01f,  //decalache
        	0.1f,	//attir
        	0.10f,	//multiple
        	0.1f,	//glurp
        	1.0f,	//multipli
        	1.0f,   //ball aide shoot size
        	0.01f, //incertitude
        	0.001f,//borders of the maze are moved speed dependent
        	1.0f,	//ballibump
        	0.01f,	//sFriction
        	0.1f,	//completeness 
        	0.001f,	//sBallDiameter
        	0.1f, 	//limaccel = holesfriction
        	0.01f,	//field size
        	0.10f},	//hole gravity
        	//BIBILLES
    		{1.0f,  //nbums
    		1.0f,   //tempscalcul
        	0.1f,	//limaccel = holesfriction
        	0.01f,  //decalache
        	0.10f,	//attir
        	0.10f,	//multiple
        	0.1f,	//glurp
        	1.0f,	//multipli
        	0.01f, //incertitude
        	0.1f,
        	0.001f,//sBallDiameter
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f,
        	0.1f,
        	1.00f},	//hole gravity
    };
	

	
	
	
}














